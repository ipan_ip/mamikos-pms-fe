export const state = () => ({
  hideSidebar: false,
  hideHeader: false,
  bodyBackground: '#fff',
});

export const mutations = {
  setHideSidebar(state, isHide) {
    state.hideSidebar = isHide;
  },
  setHideHeader(state, isHide) {
    state.hideHeader = isHide;
  },
  setBodyBackground(state, background) {
    state.bodyBackground = background;
  },
};
