export const state = () => ({
  previousRoute: null,
});

export const mutations = {
  setPreviousRoute(state, route) {
    state.previousRoute = route;
  },
};
