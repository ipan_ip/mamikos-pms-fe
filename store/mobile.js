export const state = () => ({
  showMobileBackButton: false,
  prevPath: null,
  // this is temporary flag. Still waiting UXD decide it
  hideMobileFloatingChat: false,
  hideTopNavbar: false,
  topNavbarTitle: '',
  topNavbarType: 'normal',
  hideBottomNavbar: false,
  bodyBackground: '',
  topNavbarAttributes: {},
  customPrevPath: null,
});

export const mutations = {
  setShowMobileBackButton(state, isShown) {
    state.showMobileBackButton = isShown;
  },
  setPrevPath(state, prevPath) {
    state.prevPath = prevPath;
  },
  setCustomPrevPath(state, customPrevPath) {
    state.customPrevPath = customPrevPath;
  },
  setHideMobileFloatingChat(state, isHide) {
    state.hideMobileFloatingChat = isHide;
  },
  setBodyBackground(state, background) {
    state.bodyBackground = background;
  },
  setHideBottomNavbar(state, isHide) {
    state.hideBottomNavbar = isHide;
  },
  setHideTopNavbar(state, isHide) {
    state.hideTopNavbar = isHide;
  },
  setTopNavbarTitle(state, title) {
    state.topNavbarTitle = title;
  },
  setTopNavbarType(state, type) {
    state.topNavbarType = type;
  },
  setTopNavbarAttributes(state, attributes) {
    state.topNavbarAttributes = attributes;
  },
};
