export const state = () => ({
  webviewInterface: '',
});

export const mutations = {
  setWebviewInterface(state, payload) {
    state.webviewInterface = payload;
  },
};
