export const state = () => ({
  activePage: '',
  selectedKost: {
    songId: '',
    kostName: '',
  },
  filterOption: {
    month: '',
    year: '',
    rentType: 'all',
    status: 'unpaid',
    sortBy: 'scheduled_date',
    sortOrder: 'asc',
    tenantName: '',
  },
  kostList: [],
  kostListPagination: {
    offset: 0,
    hasMore: true,
  },
  billingCardSummary: {
    unpaid: {
      description: '',
      total: null,
    },
    paid: {
      description: '',
      total: null,
    },
    transferred: {
      description: '',
      total: null,
    },
  },
  pagination: {
    isLoading: false,
    totalData: 0,
    pageNumber: 0,
    limit: 10,
    offset: 0,
    data: [],
  },
});

export const mutations = {
  updateSelectedKost(state, { songId, kostName }) {
    state.selectedKost.songId = songId;
    state.selectedKost.kostName = kostName;
  },
  updateFilterDate(state, { month, year }) {
    state.filterOption.month = month;
    state.filterOption.year = year;
  },
  updateFilterRentType(state, payload) {
    state.filterOption.rentType = payload;
  },
  updateFilterTenantName(state, payload) {
    state.filterOption.tenantName = payload;
  },
  updateFilterSortBy(state, payload) {
    state.filterOption.sortBy = payload;
  },
  updateFilterStatus(state, payload) {
    state.filterOption.status = payload;
  },
  updateFilterInitial(state, payload) {
    state.filterOption = payload;
  },
  setKostList(state, payload) {
    state.kostList = payload;
  },
  addKostList(state, payload) {
    state.kostList.push(...payload);
  },
  setKostListPagination(state, payload) {
    state.kostListPagination = payload;
  },
  setBillingSummaryPaid(state, payload) {
    state.billingCardSummary.paid = payload;
  },
  setBillingSummaryUnpaid(state, payload) {
    state.billingCardSummary.unpaid = payload;
  },
  setBillingSummaryTransferred(state, payload) {
    state.billingCardSummary.transferred = payload;
  },
  setPagination(state, payload) {
    state.pagination = payload;
  },
  setPaginationData(state, payload) {
    state.pagination.data = payload;
  },
  setPaginationLoading(state, payload) {
    state.pagination.isLoading = payload;
  },
  setPaginationTotalData(state, payload) {
    state.pagination.totalData = payload;
  },
  setPaginationOffset(state, payload) {
    state.pagination.offset = payload;
  },
  setPaginationPageNumber(state, payload) {
    state.pagination.pageNumber = payload;
  },
};

export const actions = {
  clearBillingSummaryStatus({ commit }) {
    commit('setBillingSummaryUnpaid', {
      total: 0,
      description: [],
    });
    commit('setBillingSummaryPaid', {
      total: 0,
      description: [],
    });
    commit('setBillingSummaryTransferred', {
      total: 0,
      description: [],
    });
  },

  getBillingSummaryPerStatus({ commit, state }, params) {
    const { status, year, month, rentType } = state.filterOption;
    const { songId } = state.selectedKost;

    if (!songId) return;

    const statusBilling = params || status;

    this.$api
      .getBillingSummaryPerStatus({
        status: statusBilling,
        year,
        month,
        songId,
        rentType: rentType === 'all' ? '' : rentType,
      })
      .then((response) => {
        const { status, data } = response.data;
        if (status) {
          let description = [];
          let total = 0;
          let commitName = '';

          total = data.amount;

          if (statusBilling === 'unpaid') {
            description = [`${data.status_count} dari ${data.total_count} penyewa belum bayar.`];
            commitName = 'setBillingSummaryUnpaid';
          } else if (statusBilling === 'paid') {
            description = [
              `${data.status_count} dari ${data.total_count} Tagihan sedang diproses Mamikos.`,
            ];
            commitName = 'setBillingSummaryPaid';
          } else if (statusBilling === 'transferred') {
            description = [
              `${data.from_mamipay} dari ${data.status_count} dibayar dari Mamikos`,
              `${data.outside_mamipay} dari ${data.status_count} dibayar diluar Mamikos.`,
            ];
            commitName = 'setBillingSummaryTransferred';
          }

          commitName &&
            commit(commitName, {
              total,
              description,
            });
        }
      });
  },
  getBillListInvoices({ commit, state }, { invoiceStatus, pageNumber }) {
    const { limit } = state.pagination;
    commit('setPaginationData', []);
    commit('setPaginationLoading', true);
    commit('setPaginationOffset', pageNumber === 1 ? 0 : limit * pageNumber - limit);
    commit('setPaginationPageNumber', pageNumber);

    const { status, year, month, rentType, sortBy, sortOrder, tenantName } = state.filterOption;
    const { songId } = state.selectedKost;
    const { offset } = state.pagination;

    const statusBilling = invoiceStatus || status;

    this.$api
      .getBillListInvoices({
        year,
        month,
        songId,
        limit,
        offset,
        sortBy,
        sortOrder,
        tenantName,
        status: statusBilling,
        rentType: rentType === 'all' ? '' : rentType,
      })
      .then((response) => {
        const { status, data } = response.data;

        if (status) {
          commit('setPaginationData', data.billings);
          commit('setPaginationTotalData', data.total);
        }
        commit('setPaginationLoading', false);
      })
      .catch(() => {
        commit('setPaginationLoading', false);
      });
  },
};
