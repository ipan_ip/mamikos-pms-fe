export const state = () => ({
  isAuthenticated: false,
});

export const mutations = {
  authenticate(state) {
    state.isAuthenticated = true;
  },
};
