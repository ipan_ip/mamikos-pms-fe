export const state = () => ({
  dashboardData: {},
  isLoading: false,
});

export const mutations = {
  setDashboardData(state, data) {
    state.dashboardData = data;
  },
  updateLoading(state, isLoading) {
    state.isLoading = isLoading;
  },
};

export const actions = {
  getBookingDashboardData({ commit }) {
    commit('updateLoading', true);
    return this.$api.getBookingDashboardData().then((response) => {
      response.data.status && commit('setDashboardData', response.data.data);
      commit('updateLoading', false);
    });
  },
};
