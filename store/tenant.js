export const state = () => ({
  list: [],
  loading: false,
});

export const mutations = {
  updateTenants(state, tenants) {
    state.list.push(...tenants);
  },
  resetTenants(state, list) {
    state.list = list;
  },
  setLoading(state, isLoading) {
    state.loading = isLoading;
  },
};

export const actions = {
  getTenants({ commit }, params) {
    params && commit('setLoading', true);
    return this.$api.getTenants(params).then((res) => {
      res.data.tenant_list.length && commit('updateTenants', res.data.tenant_list);
      commit('setLoading', false);
      return res.data.tenant_list;
    });
  },
};
