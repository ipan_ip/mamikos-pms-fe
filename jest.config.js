module.exports = {
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/$1',
    '^~/(.*)$': '<rootDir>/$1',
    '^vue$': 'vue/dist/vue.common.js',
    '^.+\\.(css|scss)$|^widget.SendBird$': 'babel-jest',
  },
  moduleFileExtensions: ['js', 'vue', 'json'],
  transform: {
    '^.+\\.js$': 'babel-jest',
    '.*\\.(vue)$': 'vue-jest',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/tests/fileTransformer.js',
  },
  collectCoverage: true,
  collectCoverageFrom: [
    '!<rootDir>/components/**/index.js',
    '<rootDir>/components/**/*.{vue,js}',
    '<rootDir>/routes/pages/**/*.vue',
  ],
  coverageReporters: ['html'],
  coverageThreshold: {
    global: {
      statements: 63,
      branches: 53,
      functions: 65,
      lines: 61,
    },
  },
};
