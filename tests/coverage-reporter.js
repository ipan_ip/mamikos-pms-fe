const https = require('https');
// get coverage from coverage json built by Jest
const {
  total: { functions },
} = require('../coverage/coverage-summary.json');

const data = JSON.stringify({
  text: `[FE][pms-fe](develop) Code coverage functions: ${functions.pct}% or ${functions.covered}/${functions.total}`,
});

const options = {
  hostname: 'hooks.slack.com',
  port: 443,
  path: '/services/T04ARFSGC/B01JR11L3CG/UwzAOzSH9UNb8z3LsI4bOcou',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
};

const req = https.request(options, (res) => {
  console.log(`statusCode: ${res.statusCode}`);

  res.on('data', (dt) => {
    process.stdout.write(`Sending to slack: ${dt}`);
  });
});

req.on('error', (error) => {
  console.error(`Failed to send data to slack`);
  console.error(error);
});

req.write(data);
req.end();
