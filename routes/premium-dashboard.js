const path = require('path');

export default [
  {
    name: 'premium-dashboard',
    path: '/premium-dashboard',
    meta: {
      title: 'Pemasaran Kos - Premium',
      type: 'premium-dashboard',
    },
    component: path.resolve(__dirname, 'pages/premium-dashboard/index.vue'),
    chunkName: 'pages/premium-dashboard',
  },
  {
    name: 'premium-dashboard-kelola-iklan',
    path: '/premium-dashboard/kelola-iklan/:id',
    component: path.resolve(__dirname, 'pages/premium/manage_ads.vue'),
    chunkName: 'pages/premium-dashboard/_kos_id',
    meta: {
      type: 'premium-dashboard',
    },
  },
  {
    name: 'premium-dashboard-kelola-promo',
    path: '/premium-dashboard/kelola-iklan/:id/promo',
    component: path.resolve(__dirname, 'pages/premium/promo.vue'),
    chunkName: 'pages/premium-dashboard/_id/promo',
    meta: {
      type: 'premium-dashboard',
    },
  },
  {
    name: 'premium-dashboard-kelola-saldo',
    path: '/premium-dashboard/kelola-iklan/:id/balance',
    component: path.resolve(__dirname, 'pages/premium/balance.vue'),
    chunkName: 'pages/premium-dashboard/_id/balance',
    meta: {
      type: 'premium-dashboard',
    },
  },
];
