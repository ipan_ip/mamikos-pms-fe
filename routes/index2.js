/**
 * this file is used for adding extra route that nuxt can't handle it.
 * ex: non-nested page inside nested page
 *
 * TODO: move all pages to this. Then, we don't use nuxt routing anymore.
 */

import kosRoutes from './kos';

const path = require('path');

export default [
  ...kosRoutes,
  {
    name: 'index',
    path: '/',
    component: path.resolve(__dirname, 'pages/index.vue'),
    chunkName: 'pages/dashboard',
  },
  {
    name: 'version',
    path: '/version',
    component: path.resolve(__dirname, 'pages/version.vue'),
    chunkName: 'pages/version',
  },
  {
    name: 'mamipoin',
    path: '/mamipoin',
    component: path.resolve(__dirname, 'pages/mamipoin/index.vue'),
    chunkName: 'pages/mamipoin',
  },
  {
    name: 'mamipoin-rewards',
    path: '/mamipoin/rewards',
    component: path.resolve(__dirname, 'pages/mamipoin/rewards/index.vue'),
    chunkName: 'pages/mamipoin/rewards',
  },
  {
    name: 'mamipoin-scheme',
    path: '/mamipoin/scheme',
    component: path.resolve(__dirname, 'pages/mamipoin/scheme.vue'),
    chunkName: 'pages/mamipoin/scheme',
  },
  {
    name: 'mamipoin-redeem-detail',
    path: '/mamipoin/redeem/:redeem_id',
    component: path.resolve(__dirname, 'pages/mamipoin/redeem/detail.vue'),
    chunkName: 'pages/mamipoin/redeem/redeem_id',
  },
  {
    name: 'mamipoin-rewards-detail',
    path: '/mamipoin/rewards/:reward_id',
    component: path.resolve(__dirname, 'pages/mamipoin/rewards/detail.vue'),
    chunkName: 'pages/mamipoin/rewards/reward_id',
  },
  {
    name: 'mamipoin-rewards-exchange',
    path: '/mamipoin/rewards/:reward_id/exchange',
    component: path.resolve(__dirname, 'pages/mamipoin/rewards/exchange.vue'),
    chunkName: 'pages/mamipoin/rewards/reward_id/exchange',
  },
  {
    name: 'goldplus',
    path: '/goldplus',
    component: path.resolve(__dirname, 'pages/goldplus/index.vue'),
    chunkName: 'pages/goldplus',
  },
  {
    name: 'gp-statistic',
    path: '/goldplus/statistic',
    component: path.resolve(__dirname, 'pages/goldplus/statistic/index.vue'),
    chunkName: 'pages/goldplus',
  },
  {
    name: 'goldplus-statistic-id',
    path: '/goldplus/statistic/:id',
    component: path.resolve(__dirname, 'pages/goldplus/statistic/detail.vue'),
    chunkName: 'pages/goldplus/statistic/_id',
  },
  {
    name: 'kelola-tagihan',
    path: '/kelola-tagihan',
    component: path.resolve(__dirname, 'pages/kelola-tagihan/index.vue'),
    chunkName: 'pages/kelola-tagihan',
  },
  {
    name: 'kelola-tagihan-id-detail-tagihan',
    path: '/kelola-tagihan/:id/detail-tagihan',
    component: path.resolve(__dirname, 'pages/kelola-tagihan/detailBill/index.vue'),
    chunkName: 'pages/kelola-tagihan/detailBill',
  },
];
