export default [
  {
    name: 'statistic-menu',
    path: '/statistic',
    component: '~/routes/pages/statistic/StatisticMenu.vue',
    chunkName: 'pages/statistic/menu',
  },
  {
    name: 'statistic',
    path: '/exit-gp-statistic',
    component: '~/routes/pages/statistic/index.vue',
    chunkName: 'pages/statistic',
  },
  {
    name: 'review',
    path: '/exit-gp-statistic/review',
    component: '~/routes/pages/statistic/KosStatisticInfoPage.vue',
    chunkName: 'pages/statistic/review',
  },

  {
    name: 'benefit-gp',
    path: '/exit-gp-statistic/benefit-gp',
    component: '~/routes/pages/statistic/BenefitFromGpPage.vue',
    chunkName: 'pages/statistic/benefit-gp',
  },

  {
    name: 'statistic-price',
    path: '/exit-gp-statistic/statistic-price',
    component: '~/routes/pages/statistic/StatisticPricePage.vue',
    chunkName: 'pages/statistic/statistic-price',
  },

  {
    name: 'offer-extend-gp',
    path: '/exit-gp-statistic/offer-extend-gp',
    component: '~/routes/pages/statistic/OfferExtendGpPage.vue',
    chunkName: 'pages/statistic/offer-extend-gp',
  },

  {
    name: 'see-other-performance',
    path: '/exit-gp-statistic/see-other-performance',
    component: '~/routes/pages/statistic/SeeOtherPerformancePage.vue',
    chunkName: 'pages/statistic/see-other-performance',
  },
];
