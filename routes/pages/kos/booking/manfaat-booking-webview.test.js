import { shallowMount } from '@vue/test-utils';
import VueMeta from 'vue-meta';
import manfaatBooking from './manfaat-booking-webview.vue';
import localVueWithBuefy from '~/utils/addBuefy';
import mockLazy from '~/utils/mocks/lazy';

localVueWithBuefy.use(VueMeta, { keyName: 'head' });

mockLazy(localVueWithBuefy);

const mount = (adtMountData = {}) => {
  return shallowMount(manfaatBooking, {
    localVue: localVueWithBuefy,
    ...adtMountData,
  });
};

describe('manfaatBooking.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('Should render 5 direct booking benefit information', () => {
    const listItem = wrapper.findAll('.list-items').length;
    expect(listItem).toBe(5);
  });
});
