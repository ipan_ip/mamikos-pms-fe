import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import VueMeta from 'vue-meta';
import index from './index.vue';
import localVueWithBuefy from '~/utils/addBuefy';
import pageView from '~/mixins/page-view';

const mockComponent = jest.fn().mockResolvedValue({
  default: {
    template: '<div />',
  },
});

const $device = {
  isDesktop: true,
};

const $router = {
  replace: jest.fn(),
};

const $route = {
  name: '/',
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.use(VueMeta, { keyName: 'head' });
const mountData = {
  localVue: localVueWithBuefy,
  mixins: [pageView],
  mocks: {
    $device,
    $router,
    $route,
    $options: {
      components: {
        PageView: null,
      },
    },
    component: mockComponent,
  },
};

const mount = (adtMountData = {}) => {
  return shallowMount(index, {
    ...mountData,
    ...adtMountData,
  });
};

describe('index.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render wrapper properly', () => {
    // defaults to desktop view
    expect(wrapper.isVueInstance()).toBeTruthy();

    // mobile view
    wrapper = mount({
      mocks: {
        ...mountData.mocks,
        ...{
          $device: {
            isDesktop: false,
          },
        },
      },
    });
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('should set PageView properly', () => {
    expect(wrapper.vm.view).toBe('PageView');
    expect(wrapper.vm.$options.components.PageView).toBeTruthy();
  });

  it('should render metaInfo header properly', () => {
    // default: desktop view
    expect(wrapper.vm.$metaInfo).toEqual({
      title: 'Manajemen Kos - List penyewa Anda',
      meta: [{ hid: 'viewport', name: 'viewport', content: 'width=1200' }],
    });

    // mobile view
    wrapper.vm.$device.isDesktop = false;
    expect(wrapper.vm.$metaInfo).toEqual({
      title: 'Manajemen Kos - List penyewa Anda',
    });
  });
});
