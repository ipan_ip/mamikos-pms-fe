import { shallowMount } from '@vue/test-utils';
import Vuex, { mapGetters } from 'vuex';
import VueMeta from 'vue-meta';
import exchange from '../rewards/exchange.vue';
import localVueWithBuefy from '~/utils/addBuefy';
import blacklistRouterMixin from '~/mixins/blacklist-router';
import pageView from '~/mixins/page-view';

const mockComponent = jest.fn().mockResolvedValue({
  default: {
    template: '<div />',
  },
});

const $device = {
  isDesktop: true,
};

const $router = {
  replace: jest.fn(),
};

const $route = {
  name: 'mamipoin',
  params: {
    reward_id: '1',
  },
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.use(VueMeta, { keyName: 'head' });

const storeData = {
  modules: {
    mobile: {
      namespaced: true,
      state: {
        prevPath: null,
      },
      mutations: {
        setPrevPath(state, prevPath) {
          state.prevPath = prevPath;
        },
      },
    },
    mamipoin: {
      namespaced: true,
      state: {
        redeemexchangePage: {
          prevPath: null,
        },
      },
    },
    profile: {
      namespaced: true,
      getters: {
        getBlacklistRouterNames: () => {
          return [];
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mixins: [blacklistRouterMixin, pageView],
  store,
  mocks: {
    $device,
    $router,
    $route,
    $options: {
      components: {
        PageView: null,
      },
    },
    mapGetters,
    component: mockComponent,
  },
};

const mount = (adtMountData = {}) => {
  return shallowMount(exchange, {
    ...mountData,
    ...adtMountData,
  });
};

describe('exchange.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render wrapper properly', () => {
    // defaults to desktop view
    expect(wrapper.isVueInstance()).toBeTruthy();

    // mobile view
    wrapper = mount({
      mocks: {
        ...mountData.mocks,
        ...{
          $device: {
            isDesktop: false,
          },
        },
      },
    });
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('should set PageView properly', () => {
    expect(wrapper.vm.view).toBe('PageView');
    expect(wrapper.vm.$options.components.PageView).toBeTruthy();
  });

  it('should return isDesktop value from $device.isDesktop properly', () => {
    expect(wrapper.vm.isDesktop).toBe(wrapper.vm.$device.isDesktop);
  });

  it('should return rewardId properly', () => {
    // no params
    wrapper.vm.$route.params = {};
    expect(wrapper.vm.rewardId).toBe('');

    // has params
    wrapper.vm.$route.params = { reward_id: '1' };
    expect(wrapper.vm.rewardId).toBe('1');
  });

  it('should render metaInfo (header) properly', () => {
    // default: desktop view
    expect(wrapper.vm.$metaInfo).toEqual({
      meta: [{ hid: 'viewport', name: 'viewport', content: 'width=1200' }],
      title: 'Form Penukaran Hadiah Mamipoin',
    });

    // mobile view
    wrapper.vm.$device.isDesktop = false;
    expect(wrapper.vm.$metaInfo).toEqual({
      title: 'Form Penukaran Hadiah Mamipoin',
    });
  });

  it('should set prevPath when has prevPath info and is in mobile view', () => {
    wrapper = mount({
      mocks: {
        ...mountData.mocks,
        ...{
          $device: {
            isDesktop: false,
          },
        },
      },
    });

    expect(wrapper.vm.$store.state.mobile.prevPath).toEqual({
      path: '/mamipoin/rewards/1',
    });
  });
});
