import { shallowMount } from '@vue/test-utils';
import Vuex, { mapGetters } from 'vuex';
import VueMeta from 'vue-meta';
import index from '../mamipoin/index.vue';
import localVueWithBuefy from '~/utils/addBuefy';
import blacklistRouterMixin from '~/mixins/blacklist-router';
import pageView from '~/mixins/page-view';

const mockComponent = jest.fn().mockResolvedValue({
  default: {
    template: '<div />',
  },
});

const $device = {
  isDesktop: true,
};

const $router = {
  replace: jest.fn(),
};

const $route = {
  name: 'mamipoin',
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.use(VueMeta, { keyName: 'head' });

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      getters: {
        getBlacklistRouterNames: () => {
          return [];
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mixins: [blacklistRouterMixin, pageView],
  store,
  mocks: {
    $device,
    $router,
    $route,
    $options: {
      components: {
        PageView: null,
      },
    },
    mapGetters,
    component: mockComponent,
  },
};

const mount = (adtMountData = {}) => {
  return shallowMount(index, {
    ...mountData,
    ...adtMountData,
  });
};

describe('index.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render wrapper properly', () => {
    // defaults to desktop view
    expect(wrapper.isVueInstance()).toBeTruthy();

    // mobile view
    wrapper = mount({
      mocks: {
        ...mountData.mocks,
        ...{
          $device: {
            isDesktop: false,
          },
        },
      },
    });
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('should set PageView properly', () => {
    expect(wrapper.vm.view).toBe('PageView');
    expect(wrapper.vm.$options.components.PageView).toBeTruthy();
  });

  it('should render metaInfo (header) properly', () => {
    // default: desktop view
    expect(wrapper.vm.$metaInfo).toEqual({
      meta: [{ hid: 'viewport', name: 'viewport', content: 'width=1200' }],
      title: 'Mamipoin Ownerpage Mamikos',
    });

    // mobile view
    wrapper.vm.$device.isDesktop = false;
    expect(wrapper.vm.$metaInfo).toEqual({
      title: 'Mamipoin Ownerpage Mamikos',
    });
  });
});
