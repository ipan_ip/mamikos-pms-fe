import { shallowMount } from '@vue/test-utils';
import Vuex, { mapGetters } from 'vuex';
import VueMeta from 'vue-meta';
import detail from '../redeem/detail.vue';
import localVueWithBuefy from '~/utils/addBuefy';
import blacklistRouterMixin from '~/mixins/blacklist-router';
import pageView from '~/mixins/page-view';

const mockComponent = jest.fn().mockResolvedValue({
  default: {
    template: '<div />',
  },
});

const $device = {
  isDesktop: true,
};

const $router = {
  replace: jest.fn(),
};

const $route = {
  name: 'mamipoin',
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.use(VueMeta, { keyName: 'head' });

const storeData = {
  modules: {
    mobile: {
      namespaced: true,
      state: {
        prevPath: null,
      },
      mutations: {
        setPrevPath(state, prevPath) {
          state.prevPath = prevPath;
        },
      },
    },
    mamipoin: {
      namespaced: true,
      state: {
        redeemDetailPage: {
          prevPath: {
            name: 'test',
            path: '/',
          },
        },
      },
    },
    profile: {
      namespaced: true,
      getters: {
        getBlacklistRouterNames: () => {
          return [];
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mixins: [blacklistRouterMixin, pageView],
  store,
  mocks: {
    $device,
    $router,
    $route,
    $options: {
      components: {
        PageView: null,
      },
    },
    mapGetters,
    component: mockComponent,
  },
};

const mount = (adtMountData = {}) => {
  return shallowMount(detail, {
    ...mountData,
    ...adtMountData,
  });
};

describe('detail.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render wrapper properly', () => {
    // defaults to desktop view
    expect(wrapper.isVueInstance()).toBeTruthy();

    // mobile view
    wrapper = mount({
      mocks: {
        ...mountData.mocks,
        ...{
          $device: {
            isDesktop: false,
          },
        },
      },
    });
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('should set PageView properly', () => {
    expect(wrapper.vm.view).toBe('PageView');
    expect(wrapper.vm.$options.components.PageView).toBeTruthy();
  });

  it('should return isDesktop value from $device.isDesktop properly', () => {
    expect(wrapper.vm.isDesktop).toBe(wrapper.vm.$device.isDesktop);
  });

  it('should render metaInfo (header) properly', () => {
    // default: desktop view
    expect(wrapper.vm.$metaInfo).toEqual({
      meta: [{ hid: 'viewport', name: 'viewport', content: 'width=1200' }],
      title: 'Detail Hadiah Mamipoin',
    });

    // mobile view
    wrapper.vm.$device.isDesktop = false;
    expect(wrapper.vm.$metaInfo).toEqual({
      title: 'Detail Hadiah Mamipoin',
    });
  });

  it('should set prevPath when has prevPath info and is in mobile view', () => {
    wrapper = mount({
      mocks: {
        ...mountData.mocks,
        ...{
          $device: {
            isDesktop: false,
          },
        },
      },
    });

    expect(wrapper.vm.$store.state.mobile.prevPath).toEqual({
      name: 'test',
      path: '/',
    });
  });
});
