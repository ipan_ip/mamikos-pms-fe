import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import VueMeta from 'vue-meta';
import index from './index.vue';
import localVueWithBuefy from '~/utils/addBuefy';
import { state, getters, mutations, actions } from '~/store/profile';

const mockComponent = (type) => ({
  template: `<div class="detail-bill-${type}" />`,
  render: jest.fn(),
});

jest.mock('~/components/page-features/kelola-tagihan/pages/detailBill/desktop', () => {
  return mockComponent('desktop');
});

jest.mock('~/components/page-features/kelola-tagihan/pages/detailBill/mobile', () => {
  return mockComponent('mobile');
});

const mocks = {
  $device: {
    isDesktop: true,
  },
  $router: {
    replace: jest.fn(),
  },
  $route: {
    name: 'billing-management',
  },
};

const store = {
  modules: {
    profile: {
      namespaced: true,
      state,
      getters,
      mutations,
      actions,
    },
  },
};

const localVue = localVueWithBuefy;

localVue.use(Vuex);
localVue.use(VueMeta, { keyName: 'head' });

describe('routes/pages/kelola-tagihan/detailBill/index.vue', () => {
  const mount = (isDesktop = false) =>
    shallowMount(index, {
      localVue,
      mocks: {
        ...mocks,
        $device: { isDesktop },
      },
      store: new Vuex.Store(store),
    });

  describe('desktop', () => {
    const wrapper = mount(true);

    it('should render wrapper properly', async () => {
      await wrapper.vm.$nextTick();
      const component = await wrapper.vm.component;

      expect(wrapper.isVueInstance()).toBeTruthy();
      expect(wrapper.find('pageview-stub').exists()).toBe(true);
      expect(component.default).toEqual(
        expect.objectContaining({
          template: `<div class="detail-bill-desktop" />`,
        }),
      );
    });

    it('should render metaInfo header properly', () => {
      expect(wrapper.vm.$metaInfo).toEqual({
        title: 'Kelola Tagihan - Detail Tagihan',
        meta: [{ hid: 'viewport', name: 'viewport', content: 'width=1200' }],
      });
    });

    it('should return layout properly', () => {
      expect(wrapper.vm.$options.layout(wrapper.vm.$device)).toBe('default/desktop/index');
    });
  });

  describe('mobile', () => {
    const wrapper = mount();

    it('should render wrapper properly', async () => {
      await wrapper.vm.$nextTick();
      const component = await wrapper.vm.component;

      expect(wrapper.isVueInstance()).toBeTruthy();
      expect(wrapper.find('pageview-stub').exists()).toBe(true);
      expect(component.default).toEqual(
        expect.objectContaining({
          template: `<div class="detail-bill-mobile" />`,
        }),
      );
    });

    it('should render metaInfo header properly', () => {
      expect(wrapper.vm.$metaInfo).toEqual({
        title: 'Kelola Tagihan - Detail Tagihan',
      });
    });

    it('should return layout properly', () => {
      expect(wrapper.vm.$options.layout(wrapper.vm.$device)).toBe('default/mobile/index');
    });
  });
});
