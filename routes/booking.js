export default [
  {
    name: 'booking-manage-status',
    path: '/booking/manage/status',
    component: '~/routes/pages/booking/manage-status.vue',
    chunkName: 'pages/booking/manage/status',
  },
];
