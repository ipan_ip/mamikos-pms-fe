export default [
  {
    name: 'kos-rooms',
    path: '/kos/rooms',
    component: '~/routes/pages/kos/rooms/index.vue',
    chunkName: 'pages/kos/rooms/_kos_id',
    children: [
      {
        name: 'kos-rooms-index',
        path: '/',
        component: '~/components/page-features/rooms-edit/desktop',
        chunkName: 'pages/kos/rooms/_kos_id/index',
      },
      {
        name: 'kos-rooms-kos_id',
        path: ':kos_id',
        component: '~/components/page-features/rooms-edit/desktop',
        chunkName: 'pages/kos/rooms/_kos_id/index',
      },
    ],
  },
  {
    name: 'kos-rooms-price',
    path: '/kos/rooms-price',
    component: '~/routes/pages/kos/rooms/index.vue',
    chunkName: 'pages/kos/rooms-price/_kos_id',
    children: [
      {
        name: 'kos-rooms-price-index',
        path: '/',
        component: '~/components/page-features/rooms/components/PropertyRoomUpdateCard',
        chunkName: 'pages/kos/rooms-price/_kos_id/index',
      },
      {
        name: 'kos-rooms-price-kos_id',
        path: ':kos_id',
        component: '~/components/page-features/rooms/components/PropertyRoomUpdateCard',
        chunkName: 'pages/kos/rooms-price/_kos_id/index',
      },
    ],
  },
  {
    name: 'kos-rooms-kos_id-edit',
    path: '/kos/rooms/:kos_id/edit',
    component: '~/routes/pages/kos/rooms/edit.vue',
    chunkName: 'pages/kos/rooms/_kos_id/edit',
  },
  {
    name: 'kos-rooms-kos_id-price',
    path: '/kos/rooms/:kos_id/price',
    component: '~/routes/pages/kos/rooms/price.vue',
    chunkName: 'pages/kos/rooms/_kos_id/price',
  },
  {
    name: 'kos-booking-register',
    path: '/kos/booking/register',
    component: '~/routes/pages/kos/booking/register.vue',
    chunkName: 'pages/kos/booking/register',
  },
  {
    name: 'kos-booking-activation',
    path: '/kos/booking/activation',
    component: '~/routes/pages/kos/booking/activation.vue',
    chunkName: 'pages/kos/booking/activation',
  },
  {
    name: 'manfaat-booking-webview',
    path: '/manfaat-booking-webview',
    component: '~/routes/pages/kos/booking/manfaat-booking-webview.vue',
    chunkName: 'pages/webview/manfaat-booking',
  },
  {
    name: 'kos-reviews',
    path: '/kos/reviews',
    component: '~/routes/pages/kos/reviews/index.vue',
    chunkName: 'pages/kos/reviews',
  },
];
