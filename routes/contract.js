export default [
  {
    name: 'add-tenant',
    path: '/add-tenant',
    component: '~/routes/pages/contract/add-contract/index.vue',
    chunkName: 'pages/contract/add-contract',
    children: [
      {
        name: 'add-tenant-onboarding',
        path: '/add-tenant/on-boarding',
        component: '~/components/page-features/contract/add-contract/components/Onboarding',
        chunkName: 'pages/contract/add-contract/index',
      },
      {
        name: 'add-tenant-select-method',
        path: '/add-tenant/select-method',
        component: '~/components/page-features/contract/add-contract/components/ContractType',
        chunkName: 'pages/contract/add-contract/index',
      },
      {
        name: 'add-tenant-select-kost',
        path: '/add-tenant/from-tenant/choose-kos',
        component: '~/components/page-features/contract/add-contract/components/SelectKos',
        chunkName: 'pages/contract/add-contract/index',
      },
      {
        name: 'add-tenant-form-setting',
        path: '/add-tenant/from-tenant/setting',
        component: '~/components/page-features/contract/add-contract/components/FormSetting',
        chunkName: 'pages/contract/add-contract/index',
      },
      {
        name: 'add-tenant-share-link',
        path: '/add-tenant/from-tenant/share-link',
        component: '~/components/page-features/contract/add-contract/components/ShareLink',
        chunkName: 'pages/contract/add-contract/index',
      },
    ],
  },
  {
    name: 'contract-request',
    path: '/contract-request',
    component: '~/routes/pages/contract/contract-request/index.vue',
    chunkName: 'pages/contract/contract-request',
    children: [
      {
        name: 'contract-request-list',
        path: '/',
        component: '~/components/page-features/contract/contract-request/components/ContractList',
        chunkName: 'pages/contract/contract-request/index',
        meta: {
          pageTitle: 'Penyewa Mengajukan Kontrak',
        },
      },
      {
        name: 'contract-request-detail',
        path: '/contract-request/detail/:id',
        component: '~/components/page-features/contract/contract-request/components/ContractDetail',
        chunkName: 'pages/contract/contract-request/index',
        meta: {
          pageTitle: 'Penyewa Mengajukan Kontrak',
        },
      },
    ],
  },
  {
    name: 'contract-request-accept',
    path: '/contract-request/accept/:requestId',
    component: '~/routes/pages/contract/accept-request/index.vue',
    chunkName: 'pages/contract/accept-request',
  },
];
