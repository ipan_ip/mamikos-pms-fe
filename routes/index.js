/**
 * this file is used for adding extra route that nuxt can't handle it.
 * ex: non-nested page inside nested page
 *
 * TODO: move all pages to this. Then, we don't use nuxt routing anymore.
 */

import kosRoutes from './kos';
import bookingRoutes from './booking';
import statisticRoutes from './statistic';
import contractRoutes from './contract';
import premiumDashboardRoutes from './premium-dashboard';

const path = require('path');

export default [
  ...bookingRoutes,
  ...kosRoutes,
  ...statisticRoutes,
  ...contractRoutes,
  ...premiumDashboardRoutes,
  {
    name: 'index',
    path: '/',
    component: path.resolve(__dirname, 'pages/index.vue'),
    chunkName: 'pages/dashboard',
  },
  {
    name: 'manage-kos',
    path: '/manage-kos',
    component: path.resolve(__dirname, 'pages/manage-kos/manage-kos.vue'),
    chunkName: 'pages/manage-kos',
  },
  {
    name: 'version',
    path: '/version',
    component: path.resolve(__dirname, 'pages/version.vue'),
    chunkName: 'pages/version',
  },
  {
    name: 'mamipoin',
    path: '/mamipoin',
    component: path.resolve(__dirname, 'pages/mamipoin/index.vue'),
    chunkName: 'pages/mamipoin',
  },
  {
    name: 'mamipoin-rewards',
    path: '/mamipoin/rewards',
    component: path.resolve(__dirname, 'pages/mamipoin/rewards/index.vue'),
    chunkName: 'pages/mamipoin/rewards',
  },
  {
    name: 'mamipoin-scheme',
    path: '/mamipoin/scheme',
    component: path.resolve(__dirname, 'pages/mamipoin/scheme.vue'),
    chunkName: 'pages/mamipoin/scheme',
  },
  {
    name: 'mamipoin-redeem-detail',
    path: '/mamipoin/redeem/:redeem_id',
    component: path.resolve(__dirname, 'pages/mamipoin/redeem/detail.vue'),
    chunkName: 'pages/mamipoin/redeem/redeem_id',
  },
  {
    name: 'mamipoin-rewards-detail',
    path: '/mamipoin/rewards/:reward_id',
    component: path.resolve(__dirname, 'pages/mamipoin/rewards/detail.vue'),
    chunkName: 'pages/mamipoin/rewards/reward_id',
  },
  {
    name: 'mamipoin-rewards-exchange',
    path: '/mamipoin/rewards/:reward_id/exchange',
    component: path.resolve(__dirname, 'pages/mamipoin/rewards/exchange.vue'),
    chunkName: 'pages/mamipoin/rewards/reward_id/exchange',
  },
  {
    name: 'goldplus',
    path: '/goldplus',
    component: path.resolve(__dirname, 'pages/goldplus/index.vue'),
    chunkName: 'pages/goldplus',
  },
  {
    name: 'goldplus-payment',
    path: '/goldplus/payment',
    component: path.resolve(__dirname, 'pages/goldplus/payment/index.vue'),
    chunkName: 'pages/goldplus/payment',
  },
  {
    name: 'goldplus-payment-id',
    path: '/goldplus/payment/:id',
    component: path.resolve(__dirname, 'pages/goldplus/payment/detail.vue'),
    chunkName: 'pages/goldplus/payment/_id',
  },
  {
    name: 'goldplus-statistic',
    path: '/goldplus/statistic',
    component: path.resolve(__dirname, 'pages/goldplus/statistic/index.vue'),
    chunkName: 'pages/goldplus/statistic',
  },
  {
    name: 'goldplus-statistic-id',
    path: '/goldplus/statistic/:id',
    component: path.resolve(__dirname, 'pages/goldplus/statistic/detail.vue'),
    chunkName: 'pages/goldplus/statistic/_id',
  },
  {
    name: 'goldplus-list',
    path: '/goldplus/list',
    component: path.resolve(__dirname, 'pages/goldplus/list/index.vue'),
    chunkName: 'pages/goldplus/list',
  },
  {
    name: 'goldplus-submission',
    path: '/goldplus/submission',
    component: path.resolve(__dirname, 'pages/goldplus/submission/index.vue'),
    chunkName: 'pages/goldplus/submission/',
  },
  // As for the request from PM, this path will redirect users to the new scheme '/goldplus/submission'
  {
    name: 'goldplus-submission-onboarding-old',
    path: '/goldplus/submission/onboarding',
    redirect: '/goldplus/submission',
    component: path.resolve(__dirname, 'pages/goldplus/submission/index.vue'),
    chunkName: 'pages/goldplus/submission',
  },
  {
    name: 'goldplus-submission-steps',
    path: '/goldplus/submission/steps',
    component: path.resolve(__dirname, 'pages/goldplus/submission/stepper.vue'),
    chunkName: 'pages/goldplus/submission/steps',
  },
  {
    name: 'goldplus-submission-package-code',
    path: '/goldplus/submission/packages/:code',
    component: path.resolve(__dirname, 'pages/goldplus/submission/packages/detail.vue'),
    chunkName: 'pages/goldplus/submission/package_code',
  },
  {
    name: 'goldplus-packages',
    path: '/goldplus/submission/packages',
    component: path.resolve(__dirname, 'pages/goldplus/submission/packages/index.vue'),
    chunkName: 'pages/goldplus/submission/packages',
  },
  {
    name: 'billing-management',
    path: '/billing-management',
    meta: {
      title: 'Manajemen Kos - Kelola tagihan Anda',
    },
    component: path.resolve(__dirname, 'pages/kelola-tagihan/index.vue'),
    chunkName: 'pages/billing-management',
  },
  {
    name: 'billing-management-id-detail-bill',
    path: '/billing-management/:id/detail-bill',
    meta: {
      title: 'Manajemen Kos - Kelola tagihan Anda',
    },
    component: path.resolve(__dirname, 'pages/kelola-tagihan/detailBill/index.vue'),
    chunkName: 'pages/billing-management/detailBill',
  },
  {
    name: 'kelola-tagihan',
    path: '/kelola-tagihan',
    redirect: '/billing-management',
  },
  {
    name: 'kelola-tagihan-id-detail-tagihan',
    path: '/kelola-tagihan/:id/detail-tagihan',
    redirect: '/billing-management/:id/detail-bill',
  },

  // GP survey route
  {
    name: 'survey',
    path: '/exit-gp-survey',
    component: path.resolve(__dirname, 'pages/survey/index.vue'),
    chunkName: 'pages/survey',
  },
  {
    name: 'feedback',
    path: '/exit-gp-survey/feedback',
    component: path.resolve(__dirname, 'pages/survey/questions-gp/questions.vue'),
    chunkName: 'pages/survey/questions-gp',
  },
  {
    name: 'close',
    path: '/exit-gp-survey/close',
    component: path.resolve(__dirname, 'pages/survey/questions-gp/thanksSurveyPage.vue'),
    chunkName: 'pages/survey/close',
  },
  {
    name: 'change-packet',
    path: '/exit-gp-survey/offers',
    component: path.resolve(__dirname, 'pages/survey/questions-gp/offersGpPage.vue'),
    chunkName: 'pages/survey/offers',
  },

  // Tenant List Route
  {
    name: 'tenant-list',
    path: '/tenant-list',
    meta: {
      title: 'Manajemen Kos - List penyewa Anda',
    },
    component: path.resolve(__dirname, 'pages/tenant-list/index.vue'),
    chunkName: 'pages/tenant-list',
  },
  {
    name: 'finance-report',
    path: '/financial-report',
    meta: {
      title: 'Manajemen Kos - Laporan keuangan Anda',
    },
    component: path.resolve(__dirname, 'pages/finance-report/index.vue'),
    chunkName: 'pages/finance-report',
  },
];
