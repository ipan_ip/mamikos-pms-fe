import { createLocalVue, shallowMount } from '@vue/test-utils';
import mobile from './mobile';

const localVue = createLocalVue();

describe('mobile.vue', () => {
  const $router = { go: jest.fn() };
  const $store = { commit: jest.fn() };
  const wrapper = shallowMount(mobile, { localVue, mocks: { $router, $store } });

  it('should back to the previous route', () => {
    wrapper.vm.backRoute();
    expect($router.go).toBeCalledWith(-1);
  });

  it('should update navbar title', () => {
    wrapper.vm.showNavbarTitle();
    expect($store.commit).toBeCalledWith('mobile/setShowMobileBackButton', true);
    expect($store.commit).toBeCalledWith('mobile/setTopNavbarTitle', 'Laporan Keuangan');
  });
});
