import { createLocalVue, shallowMount } from '@vue/test-utils';
import FinancePage from './FinancePage';
import mockWindowProperty from '~/utils/mockWindowProperty';

const localVue = createLocalVue();
window.open = jest.fn();

describe('FinancePage.vue', () => {
  const $device = { isMobile: false };
  const $router = { go: jest.fn() };
  const wrapper = shallowMount(FinancePage, { localVue, mocks: { $device, $router } });

  it('should load the component', () => {
    expect(wrapper.find('.finance-report-content').exists()).toBe(true);
  });

  it('should open mamikos app on appstore', () => {
    mockWindowProperty('navigator.userAgent', 'iPhone');
    wrapper.vm.downloadApp();
    expect(window.open).toBeCalledWith(
      'https://apps.apple.com/id/app/mamikos-cari-kost-mudah/id1055272843',
      '_blank',
    );
  });

  it('should open mamikos app on playstore', () => {
    mockWindowProperty('navigator.userAgent', 'Android');
    wrapper.vm.downloadApp();
    expect(window.open).toBeCalledWith(
      'https://play.google.com/store/apps/details?id=com.git.mami.kos',
      '_blank',
    );
  });

  it('should return to the previous route', () => {
    wrapper.vm.backRoute();
    expect($router.go).toBeCalledWith(-1);
  });
});
