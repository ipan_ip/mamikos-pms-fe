import { createLocalVue, shallowMount } from '@vue/test-utils';
import desktop from './desktop';

const localVue = createLocalVue();

describe('desktop.vue', () => {
  const wrapper = shallowMount(desktop, { localVue });

  it('should load the component', () => {
    expect(wrapper.find('.finance-report').exists()).toBe(true);
  });
});
