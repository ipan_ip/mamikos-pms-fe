import { shallowMount, createLocalVue } from '@vue/test-utils';
import mobile from './mobile.vue';

const localVue = createLocalVue();

describe('mobile.vue', () => {
  const $device = { isDesktop: true };
  const wrapper = shallowMount(mobile, { localVue, mocks: { $device } });

  it('should load the component', () => {
    expect(wrapper.find('.detail-bill-mobile').exists()).toBe(true);
  });
});
