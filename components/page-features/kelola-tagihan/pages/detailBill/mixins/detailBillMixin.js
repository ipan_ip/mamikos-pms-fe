import _reduce from 'lodash/reduce';

export default {
  computed: {
    dateBillText() {
      const { invoice_scheduled_date } = this.detailInvoice;
      const monthYearText = this.$dayjs(invoice_scheduled_date).format('MMMM YYYY');
      return `${monthYearText}`;
    },
    totalPayment() {
      const { base_amount, additional_costs, fine_amount, deposit_amount } = this.detailInvoice;
      const totalAdditionalCosts = _reduce(
        additional_costs,
        (sum, n) => {
          return sum + n.cost_amount;
        },
        0,
      );
      return base_amount + totalAdditionalCosts + fine_amount + deposit_amount;
    },
    totalDiscounts() {
      const { discounts } = this.detailInvoice;
      const totalDiscounts = _reduce(
        discounts,
        (sum, n) => {
          return sum + Math.abs(n.discount_amount);
        },
        0,
      );
      return totalDiscounts;
    },
  },
};
