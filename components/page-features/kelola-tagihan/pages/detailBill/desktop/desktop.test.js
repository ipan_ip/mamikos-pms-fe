import { shallowMount, createLocalVue } from '@vue/test-utils';
import desktop from './desktop.vue';

const localVue = createLocalVue();

describe('desktop.vue', () => {
  const $device = { isDesktop: true };
  const wrapper = shallowMount(desktop, { localVue, mocks: { $device } });

  it('should load the component', () => {
    expect(wrapper.find('.detail-bill-desktop').exists()).toBe(true);
  });

  it('show reward menu section', () => {
    expect(wrapper.find('.reward-menu').exists()).toBe(true);
  });
});
