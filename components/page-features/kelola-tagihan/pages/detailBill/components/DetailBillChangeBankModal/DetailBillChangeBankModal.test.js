import { shallowMount } from '@vue/test-utils';
import DetailBillChangeBankModal from './DetailBillChangeBankModal';
import localVueWithBuefy from '~/utils/addBuefy';

describe('DetailBillChangeBankModal.vue', () => {
  const propsData = { active: true };
  const toggleModal = jest.fn();
  const $route = { query: { page: 1 } };
  const wrapper = shallowMount(DetailBillChangeBankModal, {
    localVue: localVueWithBuefy,
    propsData,
    methods: { toggleModal },
    mocks: { $route },
  });

  it('should load the component', () => {
    expect(wrapper.find('.bm-filter-list-modal').exists()).toBe(true);
  });

  it('should call toggle modal function and set the isShowCard to be false', () => {
    wrapper.vm.hideModal();
    expect(toggleModal).toBeCalled();
    expect(wrapper.vm.isShowCard).toBe(false);
  });
});
