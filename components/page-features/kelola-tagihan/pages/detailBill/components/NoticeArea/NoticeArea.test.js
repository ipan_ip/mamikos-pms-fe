import { createLocalVue, shallowMount } from '@vue/test-utils';
import NoticeArea from '../NoticeArea';

const localVue = createLocalVue();

describe('NoticeArea.vue', () => {
  const wrapper = shallowMount(NoticeArea, { localVue });

  it('should mount component properly', () => {
    expect(wrapper.find('.notice-area').exists()).toBe(true);
  });

  it('should set title properly', async () => {
    await wrapper.setProps({ description: 'description' });
    expect(wrapper.find('.description').text()).toBe('description');
  });
});
