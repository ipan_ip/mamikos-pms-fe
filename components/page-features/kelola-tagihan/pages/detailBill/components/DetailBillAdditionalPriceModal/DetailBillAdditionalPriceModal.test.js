import { shallowMount } from '@vue/test-utils';
import DetailBillAdditionalPriceModal from '../DetailBillAdditionalPriceModal';
import localVueWithBuefy from '~/utils/addBuefy';
import mockWindowProperty from '~/utils/mockWindowProperty';

const additionalPrice = [
  {
    cost_id: 1,
    cost_name: 'Sampah',
    cost_amount: 20000,
    cost_type: 'fixed',
  },
  {
    cost_id: 2,
    cost_name: 'Keamanan',
    cost_amount: 30000,
    cost_type: 'other',
  },
];

const normalizedInitialAdditionalPrice = [
  {
    costAmount: 30000,
    costName: 'Keamanan',
    costType: 'other',
    costId: 2,
  },
  {
    costName: 'Sampah',
    costAmount: 20000,
    costType: 'fixed',
    costId: 1,
  },
];

const propsData = {
  additionalPrice,
  invoiceId: 1,
};

const mockResolve = (val) => jest.fn().mockResolvedValue(val);

const mocks = {
  $device: { isDekstop: true },
  $buefy: { toast: { open: jest.fn() } },
  $api: {
    updateAdditionalPrice: mockResolve({
      data: {
        status: true,
        meta: { message: 'success' },
      },
    }),
  },
};

const stubs = {
  VueNumeric: { template: '<input type="tel" />' },
  MkButton: { template: `<button class="mk-button" @click="$emit('click')"><slot /></button>` },
  BCheckbox: { template: `<input class="b-checkbox" type="checkbox" />`, props: { type: String } },
};

const locationReload = jest.fn();
mockWindowProperty('location', { reload: locationReload });

describe('DetailBillAdditionalPriceModal', () => {
  const generateWrapper = (additionalProps = {}) =>
    shallowMount(DetailBillAdditionalPriceModal, {
      localVue: localVueWithBuefy,
      propsData: { ...propsData, ...additionalProps },
      mocks,
      stubs,
    });

  describe('mounted', () => {
    const wrapper = generateWrapper();

    it('should render detail bill modal', () => {
      expect(wrapper.find('.detail-bill-modal').exists()).toBe(true);
    });

    it('should normalize additional price data on mounted', async () => {
      await wrapper.vm.$nextTick();

      expect(wrapper.vm.otherBill).toEqual(normalizedInitialAdditionalPrice);
    });

    it('should handle all additional price', () => {
      expect(wrapper.findAll('.wrapper-form').length).toBe(2);
    });

    it('should handle empty additional price properly', () => {
      const wrapper = generateWrapper({ additionalPrice: [] });

      const defaultValue = {
        costId: 0,
        costName: '',
        costAmount: 0,
        costType: 'other',
      };

      expect(wrapper.vm.otherBill.length).toBe(1);
      expect(wrapper.vm.otherBill[0]).toEqual(defaultValue);
    });

    it('should handle all is fixed additional price properly', () => {
      const wrapper = generateWrapper({
        additionalPrice: additionalPrice.map((price) => {
          price.cost_type = 'fixed';
          return price;
        }),
      });

      expect(wrapper.vm.isCheckOtherPriceToAllBill).toBe(true);
    });
  });

  describe('modal', () => {
    const wrapper = generateWrapper({ active: false });

    it('should set isShowCard with modal active valud', () => {
      wrapper.setProps({ active: true });

      expect(wrapper.vm.isShowCard).toBe(true);
    });

    it('shoul handle close modal properly', () => {
      wrapper.find('.close-icon').trigger('click.native');

      expect(wrapper.vm.isShowCard).toBe(false);
    });
  });

  describe('actions', () => {
    const wrapper = generateWrapper();

    it('should handle add properly', async () => {
      const addButton = wrapper.find('.detail-bill-modal__body > .columns:first-child .mk-button');

      addButton.trigger('click');
      await wrapper.vm.$nextTick();

      expect(wrapper.findAll('.wrapper-form').length).toBe(3);
      expect(wrapper.vm.otherBill[0]).toEqual({
        costId: 0,
        costName: '',
        costAmount: 0,
        costType: 'other',
      });
    });

    it('should delete additional price item properly', async () => {
      const spyForceUpdate = jest.spyOn(wrapper.vm, '$forceUpdate');
      const deleteButtonFirstItem = () =>
        wrapper
          .findAll('.wrapper-form')
          .at(0)
          .find('.delete-icon__mobile');

      deleteButtonFirstItem().trigger('click');
      await wrapper.vm.$nextTick();

      expect(wrapper.vm.otherBill.length).toBe(2);
      expect(wrapper.findAll('.wrapper-form').length).toBe(2);
      expect(wrapper.vm.otherBill).toEqual(normalizedInitialAdditionalPrice);
      expect(spyForceUpdate).not.toBeCalled();

      deleteButtonFirstItem().trigger('click');
      await wrapper.vm.$nextTick();

      expect(wrapper.vm.otherBill[0].is_deleted).toBe(true);
      expect(wrapper.findAll('.wrapper-form').length).toBe(1);
      expect(spyForceUpdate).toBeCalled();

      deleteButtonFirstItem().trigger('click');
      await wrapper.vm.$nextTick();

      expect(wrapper.vm.otherBill[0].is_deleted).toBe(true);
      expect(wrapper.findAll('.wrapper-form').length).toBe(0);
      expect(spyForceUpdate).toBeCalled();

      spyForceUpdate.mockRestore();
    });

    it('should set all fixed property to true if checkbox apply to all checked', async () => {
      const allOtherTypeBill = normalizedInitialAdditionalPrice.map((price) => {
        price.costType = 'other';
        return price;
      });
      await wrapper.setData({ otherBill: allOtherTypeBill });

      const getApplyToAllCheckbox = () =>
        wrapper.find('.detail-bill-modal__footer').find(stubs.BCheckbox);

      getApplyToAllCheckbox().vm.$emit('input', true);
      await wrapper.vm.$nextTick();

      expect(getApplyToAllCheckbox().attributes('value')).toBeTruthy();
      expect(wrapper.vm.otherBill.every((bill) => bill.costType === 'fixed')).toBe(true);

      getApplyToAllCheckbox().vm.$emit('input', false);
      await wrapper.vm.$nextTick();

      expect(getApplyToAllCheckbox().attributes('value')).toBeFalsy();
      expect(wrapper.vm.otherBill.every((bill) => bill.costType === 'other')).toBe(true);
    });

    it('should update additional price properly', async () => {
      jest.useFakeTimers();
      const updateButton = () => wrapper.find('.detail-bill-modal__footer .mk-button');
      await updateButton().trigger('click.native');
      await new Promise((resolve) => setImmediate(resolve()));
      jest.advanceTimersByTime(3000);

      expect(mocks.$buefy.toast.open).toBeCalledWith({
        duration: 2000,
        message: 'success',
        position: 'is-bottom',
      });
      expect(locationReload).toBeCalled();

      jest.useRealTimers();

      jest.clearAllMocks();
      mocks.$api.updateAdditionalPrice = jest.fn().mockResolvedValue({
        data: {
          status: false,
          data: {},
        },
      });

      await updateButton().trigger('click.native');
      await new Promise((resolve) => setImmediate(resolve()));

      expect(mocks.$buefy.toast.open).not.toBeCalled();
    });
  });
});
