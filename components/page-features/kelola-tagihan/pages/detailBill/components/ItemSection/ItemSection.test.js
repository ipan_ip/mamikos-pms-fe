import { createLocalVue, shallowMount } from '@vue/test-utils';
import ItemSection from '../ItemSection';

const localVue = createLocalVue();

describe('ItemSection.vue', () => {
  const wrapper = shallowMount(ItemSection, { localVue });

  it('should mount component properly', () => {
    expect(wrapper.find('.item-section').exists()).toBe(true);
  });
});
