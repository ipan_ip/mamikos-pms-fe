import { shallowMount } from '@vue/test-utils';
import DetailBillPriceModal from '../DetailBillPriceModal';
import localVueWithBuefy from '~/utils/addBuefy';

const mocks = {
  $api: {
    updatePriceRent: jest.fn().mockResolvedValue({
      data: {
        status: true,
        meta: {
          message: 'success',
        },
      },
    }),
  },
  $buefy: {
    toast: {
      open: jest.fn(),
    },
  },
};

const stubs = {
  VueNumeric: { template: `<input type="tel" />` },
  MkButton: { template: `<button class="mk-button"><slot /></button>` },
};

const locationReload = jest.fn();
global.location.reload = locationReload;

describe('DetailBillPriceModal', () => {
  const wrapper = shallowMount(DetailBillPriceModal, {
    localVue: localVueWithBuefy,
    stubs,
    mocks,
    propsData: {
      rentType: 'Per Bulan',
      rentPrice: 20000,
      invoiceId: 1,
      active: true,
    },
  });

  it('should render component properly', () => {
    expect(wrapper.find('.detail-bill-modal').exists()).toBe(true);
    expect(wrapper.find('.detail-bill-modal__body .description').text()).toContain('Per Bulan');
    expect(wrapper.find('.input-text-custom').attributes('value')).toBe('20000');
  });

  it('should save price properly', async () => {
    jest.useFakeTimers();
    const saveButton = wrapper.find('.has-text-right .mk-button');
    saveButton.trigger('click.native');

    await new Promise((resolve) => setImmediate(resolve));
    jest.advanceTimersByTime(3000);
    jest.useRealTimers();

    expect(mocks.$buefy.toast.open).toBeCalledWith({
      duration: 2000,
      message: 'success',
      position: 'is-bottom',
    });
    expect(locationReload).toBeCalled();
  });

  it('should handle save price properly when response status is false', async () => {
    jest.clearAllMocks();
    jest.useFakeTimers();
    wrapper.vm.$api.updatePriceRent = jest.fn().mockResolvedValue({
      data: {
        status: false,
        meta: { message: 'failed' },
      },
    });
    await wrapper.vm.$nextTick();

    const saveButton = wrapper.find('.has-text-right .mk-button');
    saveButton.trigger('click.native');

    await new Promise((resolve) => setImmediate(resolve));
    jest.advanceTimersByTime(3000);
    jest.useRealTimers();

    expect(mocks.$buefy.toast.open).toBeCalledWith({
      duration: 2000,
      message: 'failed',
      position: 'is-bottom',
    });
    expect(locationReload).not.toBeCalled();
  });

  it('should handle modal properly', async () => {
    await wrapper.setProps({ active: true });
    const closeButton = wrapper.find('.close-icon');
    closeButton.trigger('click.native');

    expect(wrapper.vm.isShowCard).toBe(false);

    await wrapper.setData({ isShowCard: true });
    await wrapper.setProps({ active: false });

    expect(wrapper.vm.isShowCard).toBe(false);
  });

  it('should show message when rent price is 0', async () => {
    await wrapper.setProps({ rentPrice: 0 });

    expect(wrapper.find('.error-message').exists()).toBe(true);
  });
});
