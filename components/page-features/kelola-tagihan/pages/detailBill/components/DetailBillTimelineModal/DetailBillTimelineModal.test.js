import { createLocalVue, shallowMount } from '@vue/test-utils';
import dayjs from 'dayjs';
import DetailBillTimelineModal from '../DetailBillTimelineModal';

const flushPromises = () => {
  return new Promise((resolve) => setImmediate(resolve));
};

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;

const propsData = {
  dueDate: '12-12-2021',
  invoiceStatus: 'unpaid',
  invoiceId: 123,
  active: true,
};

const $api = {
  getBillingTimeline: jest.fn().mockResolvedValue({
    data: {
      status: true,
      data: {
        invoice_created_at: '12/12/2020',
        invoice_status: 'unpaid',
        invoice_paid_at: null,
        invoice_transferred_at: null,
      },
    },
  }),
};

const mocks = { $api };

const mockComponent = () => ({
  template: `<div><slot /></div>`,
});

const stubs = {
  BIcon: mockComponent(),
  MkModal: mockComponent(),
};

describe('DetailBillTimelineModal', () => {
  const wrapper = shallowMount(DetailBillTimelineModal, {
    localVue,
    propsData,
    mocks,
    stubs,
  });

  const getTimelineCards = () => wrapper.findAll('.claim-detail-timeline > .timeline-card');

  const getInvoiceActiveStatus = (i) =>
    getTimelineCards()
      .at(i)
      .find('.bulletpoint')
      .classes('--active');

  it('should render detail bill modal', () => {
    expect(wrapper.find('.detail-bill-modal').exists()).toBe(true);
  });

  it('should render invoice timeline properly when statis is unpaid', () => {
    expect(getTimelineCards().length).toBe(3);
    expect(getInvoiceActiveStatus(0)).toBe(true);
    expect(getInvoiceActiveStatus(1)).toBe(false);
    expect(getInvoiceActiveStatus(2)).toBe(false);
  });

  it('should render render invoice timeline properly when status is transferred', async () => {
    wrapper.vm.$api.getBillingTimeline = jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: {
          invoice_created_at: '12/12/2020',
          invoice_status: 'transferred',
          invoice_paid_at: null,
          invoice_transferred_at: '1/1/2021',
        },
      },
    });

    wrapper.vm.getBillingTimeline(123);

    await flushPromises();
    await wrapper.vm.$nextTick();

    expect(getInvoiceActiveStatus(0)).toBe(true);
    expect(getInvoiceActiveStatus(1)).toBe(true);
    expect(getInvoiceActiveStatus(2)).toBe(true);
  });

  it('should render render invoice timeline properly when status is paid', async () => {
    wrapper.vm.$api.getBillingTimeline = jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: {
          invoice_created_at: '12/12/2020',
          invoice_status: 'paid',
          invoice_paid_at: '1/2/2021',
          invoice_transferred_at: '1/1/2021',
        },
      },
    });

    wrapper.vm.getBillingTimeline(123);

    await flushPromises();
    await wrapper.vm.$nextTick();

    expect(getInvoiceActiveStatus(0)).toBe(true);
    expect(getInvoiceActiveStatus(1)).toBe(true);
    expect(getInvoiceActiveStatus(2)).toBe(false);
  });

  it('should handle modal close properly', async () => {
    await wrapper.setProps({ active: true });
    await wrapper.setData({ isShowCard: true });

    wrapper.find('.close-icon').trigger('click.native');
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.isShowCard).toBe(false);
    expect(wrapper.find('.detail-bill-modal__card').element.style.display).toBe('none');

    wrapper.setData({ isShowCard: true });
    await wrapper.setProps({ active: false });

    expect(wrapper.vm.isShowCard).toBe(false);
  });

  it('should not create timeline when api return status false', async () => {
    jest.clearAllMocks();

    wrapper.vm.$api.getBillingTimeline = jest.fn().mockResolvedValue({
      data: {
        status: false,
        data: {},
      },
    });

    await wrapper.setData({ timelineInvoice: [] });
    wrapper.vm.getBillingTimeline(123);
    await flushPromises();
    await wrapper.vm.$nextTick();

    expect(getTimelineCards().length).toBe(0);
  });
});
