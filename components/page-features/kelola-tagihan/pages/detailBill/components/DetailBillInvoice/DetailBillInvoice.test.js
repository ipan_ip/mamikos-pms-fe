import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailBillInvoice from './DetailBillInvoice.vue';

const localVue = createLocalVue();

describe('DetailBillInvoice.vue', () => {
  const $route = { params: { id: 1 } };
  const $api = {
    getDetailInvoice: jest
      .fn()
      .mockResolvedValue({ data: { status: true, data: { invoice_status: 'paid' } } }),
  };

  const wrapper = shallowMount(DetailBillInvoice, { localVue, mocks: { $route, $api } });

  it('should assign the detail invoice data', () => {
    wrapper.vm.getDetailInvoice();
    expect(wrapper.vm.detailInvoice.invoice_status).toBe('paid');
  });
});
