import { shallowMount } from '@vue/test-utils';
import dayjs from 'dayjs';
import detailBillMixin from '../../mixins/detailBillMixin';
import DetailBillInvoiceUnpaid from './DetailBillInvoiceUnpaid.vue';
import localVueWithBuefy from '~/utils/addBuefy';

localVueWithBuefy.prototype.$dayjs = dayjs;
window.open = jest.fn();

describe('DetailBillInvoiceUnpaid.vue', () => {
  const $route = { query: { changePrice: 'true' } };
  const propsData = {
    detailInvoice: {
      invoice_scheduled_date: '2021-02-22',
      additional_costs: [{ cost_name: 'listrik', cost_amount: 10000, cost_type: '' }],
      discounts: [],
      contract_id: 23,
      tenant_remindered: false,
    },
  };

  const wrapper = shallowMount(DetailBillInvoiceUnpaid, {
    localVue: localVueWithBuefy,
    propsData,
    mixins: [detailBillMixin],
    mocks: { $route },
  });

  it('should load the component', () => {
    expect(wrapper.find('.detail-bill-invoice-unpaid').exists()).toBe(true);
  });

  it('should return the correct reminder label', async () => {
    await wrapper.setProps({ detailInvoice: { tenant_remindered: false } });
    expect(wrapper.vm.tenantRemindered.label).toBe('Ingatkan Lagi');
    await wrapper.setProps({ detailInvoice: { tenant_remindered: true } });
    expect(wrapper.vm.tenantRemindered.label).toBe('Sudah Diingatkan');
  });

  it('should show reminder modal', () => {
    wrapper.vm.showRememberTenant();
    expect(wrapper.vm.isReminderModalVisible).toBe(true);
  });

  it('should show pay outside modal', () => {
    wrapper.vm.showModalPayOutside(true);
    expect(wrapper.vm.isPayModalVisible).toBe(true);
  });

  it('should open profile tenant page', async () => {
    await wrapper.setProps({
      detailInvoice: {
        invoice_scheduled_date: '2021-02-22',
        additional_costs: [{ cost_name: 'listrik', cost_amount: 10000, cost_type: '' }],
        discounts: [],
        contract_id: 23,
        tenant_remindered: false,
      },
    });
    wrapper.vm.openProfileTenant();
    expect(window.open).toBeCalledWith(
      `${process.env.MAMIKOS_URL}/ownerpage/billing-management/profile-tenant/23/payment-tenant`,
      '_blank',
    );
  });

  it('should return correct date format', () => {
    const day = '2021-02-22';
    expect(wrapper.vm.dueDateFormated(day)).toBe('Monday, 22 Feb 2021');
  });

  it('should return correct discount description', () => {
    expect(wrapper.vm.discountDescription('mamikos')).toBe('Ditanggung oleh Mamikos');
    expect(wrapper.vm.discountDescription('owner')).toBe('Diskon yang Anda berikan');
    expect(wrapper.vm.discountDescription('')).toBe('');
  });

  it('should return correct total amount', () => {
    wrapper.setProps({
      detailInvoice: {
        additional_costs: [{ cost_name: 'listrik', cost_amount: 10000, cost_type: '' }],
        base_amount: 1000000,
        deposit_amount: 100000,
        fine_amount: 25000,
      },
    });
    expect(wrapper.vm.totalPayment).toBe(1135000);
  });

  it('should return correct total discount', () => {
    wrapper.setProps({
      detailInvoice: { discounts: [{ discount_amount: 50000 }] },
    });
    expect(wrapper.vm.totalDiscounts).toBe(50000);
  });
});
