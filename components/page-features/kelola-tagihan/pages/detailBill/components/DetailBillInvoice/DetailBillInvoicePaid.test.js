import { shallowMount, createLocalVue } from '@vue/test-utils';
import dayjs from 'dayjs';
import detailBillMixin from '../../mixins/detailBillMixin';
import DetailBillInvoicePaid from './DetailBillInvoicePaid.vue';

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;
window.open = jest.fn();

describe('DetailBillInvoicePaid.vue', () => {
  const propsData = {
    detailInvoice: {
      invoice_paid_label: 'terbayar',
      additional_costs: [],
      discounts: [],
      contract_id: 23,
    },
  };
  const $route = { query: 'query' };

  const wrapper = shallowMount(DetailBillInvoicePaid, {
    localVue,
    propsData,
    mixins: [detailBillMixin],
    mocks: { $route },
  });

  it('should load the component', () => {
    expect(wrapper.find('.detail-bill-invoice-unpaid').exists()).toBe(true);
  });

  it('should return correct label currency', () => {
    expect(wrapper.vm.labelCurrency(-23000)).toBe('-Rp');
    expect(wrapper.vm.labelCurrency(23000)).toBe('Rp');
  });

  it('should open profile tenant page', () => {
    wrapper.vm.openProfileTenant();
    expect(window.open).toBeCalledWith(
      `${process.env.MAMIKOS_URL}/ownerpage/billing-management/profile-tenant/23/payment-tenant`,
      '_blank',
    );
  });

  it('should return correct date format', () => {
    const day = '2021-02-22';
    expect(wrapper.vm.dueDateFormated(day)).toBe('Monday, 22 Feb 2021');
  });

  it('should return correct discount description', () => {
    expect(wrapper.vm.discountDescription('mamikos')).toBe('Ditanggung oleh Mamikos');
    expect(wrapper.vm.discountDescription('owner')).toBe('Diskon yang Anda berikan');
    expect(wrapper.vm.discountDescription('')).toBe('');
  });
});
