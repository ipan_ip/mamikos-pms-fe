import { createLocalVue, shallowMount } from '@vue/test-utils';
import FieldInput from '../FieldInput';

const localVue = createLocalVue();

describe('FieldInput.vue', () => {
  const wrapper = shallowMount(FieldInput, { localVue });

  it('should mount component properly', () => {
    expect(wrapper.find('.field-input').exists()).toBe(true);
  });

  it('should set title properly', async () => {
    await wrapper.setProps({ title: 'field input title' });
    expect(wrapper.find('.title').text()).toBe('field input title');
  });
});
