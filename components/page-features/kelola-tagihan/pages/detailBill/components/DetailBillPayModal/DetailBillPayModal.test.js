import { shallowMount } from '@vue/test-utils';
import DetailBillPayModal from './DetailBillPayModal.vue';
import localVueWithBuefy from '~/utils/addBuefy';

describe('DetailBillPayModal.vue', () => {
  const propsData = { active: true };
  const $route = { query: { page: 123 } };
  const $buefy = { toast: { open: jest.fn() } };
  let $api = {
    setManualPayment: jest.fn().mockResolvedValue({
      data: {
        status: true,
        meta: { message: 'some message jotted here' },
      },
    }),
  };

  const generateWrapper = ({ methodsData = {} } = {}) => {
    const mountProp = {
      localVue: localVueWithBuefy,
      methods: { ...methodsData },
      mocks: { $api, $buefy, $route },
      propsData,
    };

    return shallowMount(DetailBillPayModal, mountProp);
  };

  it('should load the component', () => {
    const wrapper = generateWrapper();
    expect(wrapper.find('.bm-filter-list-modal').exists()).toBe(true);
  });

  it('should show toast message', () => {
    const wrapper = generateWrapper();
    wrapper.vm.toastMessage('');
    expect($buefy.toast.open).toBeCalled();
  });

  it('should set manual payment', () => {
    const wrapper = generateWrapper();
    wrapper.vm.changePayToOutside();
    expect($api.setManualPayment).toBeCalled();
  });

  it('should show toast message with message from server', async () => {
    $api = {
      setManualPayment: jest.fn().mockResolvedValue({
        data: {
          status: false,
          meta: { message: 'there is some errors' },
        },
      }),
    };
    const toastMessage = jest.fn();
    const wrapper = generateWrapper({ methodsData: { toastMessage } });
    await wrapper.vm.changePayToOutside();
    expect(toastMessage).toBeCalledWith('there is some errors');
  });

  it('should hide the card', () => {
    const wrapper = generateWrapper();
    wrapper.vm.hideModal();
    expect(wrapper.vm.isShowCard).toBe(false);
  });

  it('should update card flag value', () => {
    const wrapper = generateWrapper();
    wrapper.vm.active = true;
    expect(wrapper.vm.isShowCard).toBe(true);
  });
});
