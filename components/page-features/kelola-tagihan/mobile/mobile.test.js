import { shallowMount, createLocalVue } from '@vue/test-utils';
import dayjs from 'dayjs';
import initMixin from '../mixins/init';
import mobile from './mobile.vue';

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;

describe('mobile.vue', () => {
  const $route = { query: { page: 1 } };
  const $store = {
    commit: jest.fn(),
    dispatch: jest.fn(),
    state: { billingManagement: { kostList: [] } },
  };
  const $api = {
    getMamipayRooms: jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: [],
      },
    }),
  };

  const wrapper = shallowMount(mobile, {
    localVue,
    mixins: [initMixin],
    mocks: { $route, $store, $api },
  });

  it('should load the component', () => {
    expect(wrapper.find('.billing-management-mobile').exists()).toBe(true);
  });

  it('should open filter kos modal', () => {
    wrapper.vm.openFilterKostModal();
    expect(wrapper.vm.isFilterKostModalVisible).toBe(true);
  });

  it('should open filter rent modal', () => {
    wrapper.vm.openFilterRentModal();
    expect(wrapper.vm.isFilterRentTypeModalVisible).toBe(true);
  });
});
