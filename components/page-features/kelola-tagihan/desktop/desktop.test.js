import { createLocalVue, shallowMount } from '@vue/test-utils';
import dayjs from 'dayjs';
import Vuex from 'vuex';
import desktop from '../desktop';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.prototype.$dayjs = dayjs;

const store = {
  modules: {
    billingManagement: {
      namespaced: true,
      state: {
        kostList: [],
      },
      mutations: {
        updateFilterInitial: jest.fn(),
        updateSelectedKost: jest.fn(),
        setKostList: (state, payload) => {
          state.kostList = payload;
        },
      },
      actions: {
        getBillingSummaryPerStatus: jest.fn(),
      },
    },
  },
};

const mocks = {
  $route: {
    query: { page: 'unpaid' },
  },
  $router: {
    push: jest.fn(),
    replace: jest.fn(),
  },
  $api: {
    getMamipayRooms: jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: [
          {
            id: 12,
            room_title: 'kos bbk',
          },
          {
            id: 13,
            room_title: 'kos bbk 2',
          },
        ],
      },
    }),
  },
};

describe('kelola-tagihan/desktop', () => {
  const mount = () => shallowMount(desktop, { localVue, mocks, store: new Vuex.Store(store) });

  const wrapper = mount();

  it('should mount component properly', () => {
    expect(wrapper.find('.billing-management-desktop').exists()).toBe(true);
    expect(wrapper.find('.billing-management-desktop__title-header').text()).toBe(
      'Kelola Tagihan Penyewa',
    );
  });

  it('should show title properly when query page is transferred', async () => {
    wrapper.vm.$route.query.page = 'transferred';
    await wrapper.vm.$nextTick();

    expect(wrapper.find('.billing-management-desktop__title-header').text()).toBe('Pencairan Dana');
  });

  it('should open filter modal when billing-management-filter emit click:filter-kost-button', () => {
    wrapper.find('billing-management-filter-stub').vm.$emit('click:filter-kost-button');

    expect(wrapper.vm.isFilterKostModalVisible).toBe(true);
  });

  it('should open rent modal when billing-management-filter emit click:filter-rent-button', () => {
    wrapper.find('billing-management-filter-stub').vm.$emit('click:filter-rent-button');

    expect(wrapper.vm.isFilterRentTypeModalVisible).toBe(true);
  });

  it('should handle initial page properly', async () => {
    wrapper.vm.$route.query = {
      page: '',
      month: '01',
      year: '2020',
      rentType: 'monthly',
      selectedKost: 13,
    };

    const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
    wrapper.vm.initialPage();
    await new Promise((resolve) => setImmediate(resolve));

    expect(spyCommit).toBeCalledWith('billingManagement/updateSelectedKost', {
      songId: 13,
      kostName: 'kos bbk 2',
    });
    spyCommit.mockRestore();
  });

  it('should handle response status false properly', async () => {
    const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
    const spyDispatch = jest.spyOn(wrapper.vm.$store, 'dispatch');

    wrapper.vm.$api.getMamipayRooms = jest.fn().mockResolvedValue({
      data: {
        status: false,
      },
    });

    wrapper.vm.getOwnerRoom();
    await new Promise((resolve) => setImmediate(resolve));

    expect(spyCommit).not.toBeCalledWith('billingManagement/updateSelectedKost', expect.anything());
    expect(spyCommit).not.toBeCalledWith('billingManagement/setKostList', expect.anything());
    expect(spyDispatch).not.toBeCalledWith(
      'billingManagement/getBillingSummaryPerStatus',
      expect.anything(),
    );

    spyCommit.mockRestore();
    spyDispatch.mockRestore();
  });

  it('should handle empty kost list properly', async () => {
    const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
    const spyDispatch = jest.spyOn(wrapper.vm.$store, 'dispatch');

    wrapper.vm.$api.getMamipayRooms = jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: [],
      },
    });

    wrapper.vm.getOwnerRoom();
    await new Promise((resolve) => setImmediate(resolve));

    expect(spyCommit).not.toBeCalledWith('billingManagement/updateSelectedKost', expect.anything());
    expect(spyCommit).toBeCalledWith('billingManagement/setKostList', []);
    expect(spyDispatch).not.toBeCalledWith(
      'billingManagement/getBillingSummaryPerStatus',
      expect.anything(),
    );

    spyCommit.mockRestore();
    spyDispatch.mockRestore();
  });
});
