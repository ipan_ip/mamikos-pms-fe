import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import BillingManagementFilterKostModal from './BillingManagementFilterKostModal.vue';
import localVueWithBuefy from '~/utils/addBuefy';

import MkModal, { mixin as modalMixin } from '~/components/global/molecules/MkModal/MkModal';
import MkCard from '~/components/global/molecules/MkCard';
import MkCardHeader from '~/components/global/molecules/MkCard/components/MkCardHeader';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';
import MkCardFooter from '~/components/global/molecules/MkCard/components/MkCardFooter';
import billing from '~/components/page-features/kelola-tagihan/mixins/billing';

const $api = { getMamipayRooms: jest.fn().mockRejectedValue() };

const $bugsnag = { notify: jest.fn() };

const mocks = {
  $device: {
    isDesktop: false,
    isMobile: true,
  },
  $router: {
    replace: jest.fn(),
  },
  $route: {
    query: {
      page: 'unpaid',
    },
  },
  $emit: jest.fn(),
  $api,
  $bugsnag,
};

const stubs = {
  MkModal,
  MkCard,
  MkCardHeader,
  MkCardBody,
  MkCardFooter,
};

localVueWithBuefy.use(Vuex);

const propsData = {
  active: true,
};

const storeData = {
  modules: {
    billingManagement: {
      namespaced: true,
      state: {
        selectedKost: {
          kostName: 'kostName',
          songId: 1,
        },
        kostList: [{ kostName: 'kostName', songId: 1 }],
        kostListPagination: {
          offset: 0,
          hasMore: true,
        },
      },
      mutations: {
        updateSelectedKost: () => jest.fn(),
        setKostListPagination: jest.fn(),
        addKostList: jest.fn(),
      },
      actions: {
        getBillListInvoices: () => jest.fn(),
        getBillingSummaryPerStatus: () => jest.fn(),
        clearBillingSummaryStatus: () => jest.fn(),
      },
    },
  },
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      mocks,
      stubs,
      propsData,
      localVue: localVueWithBuefy,
      mixins: [billing, modalMixin],
      store: new Vuex.Store(storeData),
    },
    ...adtMountData,
  };

  return shallowMount(BillingManagementFilterKostModal, mountData);
};

describe('BillingManagementFilterKostModal.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render component properly', () => {
      expect(wrapper.find('.bm-filter-kost-modal').exists()).toBeTruthy();
    });
  });

  describe('should render computed data', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render selectedKost properly', () => {
      expect(wrapper.vm.selectedKost).toEqual({
        kostName: 'kostName',
        songId: 1,
      });
    });

    it('should render kostList properly', () => {
      expect(wrapper.vm.kostList).toEqual([
        {
          kostName: 'kostName',
          songId: 1,
        },
      ]);
    });

    it('should compute page properly', () => {
      expect(wrapper.vm.page).toBe('unpaid');
    });
  });

  describe('should watch properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should set message to empty string when toast state changed to false', async () => {
      await wrapper.setData({ isShowToast: true, toastMessage: 'message' });

      expect(wrapper.vm.toastMessage).toBe('message');

      await wrapper.setData({ isShowToast: false });

      expect(wrapper.vm.toastMessage).toBe('');
    });
  });

  describe('should execute functions properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should set isShowCard value on active value change', async () => {
      wrapper.setProps({
        active: true,
      });
      await wrapper.vm.$nextTick();
      expect(wrapper.vm.isShowCard).toBeTruthy();

      wrapper.setProps({
        active: false,
      });
      await wrapper.vm.$nextTick();
      expect(wrapper.vm.isShowCard).toBeFalsy();
    });

    it('should return an object with songId and kostName when calling formatSelected method', () => {
      expect(wrapper.vm.formatSelected('songId', 'kostName')).toEqual({
        songId: 'songId',
        kostName: 'kostName',
      });
    });

    it('should set isShowCard value and call toggleModal when clicking on close icon', async () => {
      const closeIcon = wrapper.find('.close-icon');
      if (closeIcon && closeIcon.exists()) {
        await closeIcon.trigger('click');

        expect(wrapper.vm.isShowCard).toBeFalsy();
      }
    });

    it('should call $store.commit and hideModal when calling updateSelectedKost method', async () => {
      const commitSpy = jest.spyOn(wrapper.vm.$store, 'commit');
      const hideModalSpy = jest.spyOn(wrapper.vm, 'hideModal');

      await wrapper.vm.updateSelectedKost({ songId: 'songId', kostName: 'kostName' });
      expect(commitSpy).toBeCalled();
      expect(hideModalSpy).toBeCalled();

      commitSpy.mockRestore();
      hideModalSpy.mockRestore();
    });

    it('should handle scroll properly', async () => {
      const filterBody = () => wrapper.find('.bm-filter-kost-modal__body');
      const spyHandleFilterBodyScrolled = jest.spyOn(wrapper.vm, 'handleFilterBodyScrolled');
      const spyGetMoreKost = jest.spyOn(wrapper.vm, 'getMoreKost');

      filterBody().trigger('scroll.native');

      expect(spyHandleFilterBodyScrolled).toBeCalled();
      spyHandleFilterBodyScrolled.mockRestore();

      await wrapper.setData({ isLoadingMore: false });
      wrapper.vm.handleFilterBodyScrolled({
        target: { scrollTop: 100, scrollHeight: 200, clientHeight: 100 },
      });

      expect(spyGetMoreKost).toBeCalled();
      jest.clearAllMocks();

      await wrapper.setData({ isLoadingMore: false });
      wrapper.vm.handleFilterBodyScrolled({
        target: { scrollTop: 50, scrollHeight: 200, clientHeight: 100 },
      });

      expect(spyGetMoreKost).not.toBeCalled();
    });

    it('should handle load more properly', async () => {
      const moreData = [
        {
          id: 2,
          room_title: 'more room',
        },
      ];

      mocks.$api.getMamipayRooms = jest.fn().mockResolvedValue({
        data: {
          status: true,
          has_more: true,
          data: moreData,
        },
      });

      const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');

      await wrapper.setData({ isLoadingMore: false });
      wrapper.vm.getMoreKost();
      await new Promise((resolve) => setImmediate(resolve));

      expect(spyCommit).toBeCalledWith('billingManagement/addKostList', [
        {
          kostName: 'more room',
          songId: 2,
        },
      ]);

      expect(spyCommit).toBeCalledWith('billingManagement/setKostListPagination', {
        offset: 1,
        hasMore: true,
      });

      mocks.$api.getMamipayRooms = jest.fn().mockResolvedValue({
        data: {
          status: true,
          has_more: true,
          data: [],
        },
      });

      await wrapper.setData({ isLoadingMore: false });
      wrapper.vm.getMoreKost();
      await new Promise((resolve) => setImmediate(resolve));

      expect(spyCommit).toBeCalledWith('billingManagement/addKostList', [
        {
          kostName: 'more room',
          songId: 2,
        },
      ]);

      expect(spyCommit).toBeCalledWith('billingManagement/setKostListPagination', {
        offset: 0,
        hasMore: true,
      });

      spyCommit.mockRestore();
    });

    it('should handle response status:false on fetching kost properly', async () => {
      mocks.$api.getMamipayRooms = jest.fn().mockResolvedValue({
        data: {
          status: false,
          has_more: true,
          data: [],
        },
      });

      const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');

      wrapper.vm.getMoreKost();
      await new Promise((resolve) => setImmediate(resolve));

      expect(spyCommit).not.toBeCalled();
      expect(wrapper.vm.isShowToast).toBe(true);
      spyCommit.mockRestore();
    });
  });
});
