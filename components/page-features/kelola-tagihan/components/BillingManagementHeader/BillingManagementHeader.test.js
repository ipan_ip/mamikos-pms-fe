import { shallowMount } from '@vue/test-utils';
import BillingManagementHeader from '~/components/page-features/kelola-tagihan/components/BillingManagementHeader/BillingManagementHeader.vue';
import localVueWithBuefy from '~/utils/addBuefy';
import { mockComponentWithAttrs } from '~/utils/mockComponent';

// mock event bus
jest.mock('~/components/event-bus/index.js', () => {
  const EventBus = {
    $emit: jest.fn(),
    $on: jest.fn(),
  };

  global.EventBus = EventBus;
  return global.EventBus;
});

const mockGridColumnComponent = {
  template: "<div><slot name='row' /></div>",
};

const stubs = {
  GridColumn: mockGridColumnComponent,
  GridRow: mockComponentWithAttrs({ id: 'GridRow' }),
};

const propsData = {
  title: 'test',
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      stubs,
      propsData,
      localVue: localVueWithBuefy,
    },
    ...adtMountData,
  };

  return shallowMount(BillingManagementHeader, mountData);
};

describe('BillingManagementHeader.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render billing-management__column properly', () => {
      expect(wrapper.find('.billing-management__column').exists()).toBeTruthy();
    });

    it('should render title properly', () => {
      const titleText = wrapper.find('.bm-list-mobile__title');
      titleText.exists() && expect(titleText.text()).toBe('test');
    });
  });
});
