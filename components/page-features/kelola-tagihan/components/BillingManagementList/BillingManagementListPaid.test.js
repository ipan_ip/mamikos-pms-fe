import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import dayjs from 'dayjs';
import BillingManagementListPaid from './BillingManagementListPaid';
import mockComponent from '~/utils/mockComponent';
import billing from '~/components/page-features/kelola-tagihan/mixins/billing';
import localVueWithBuefy from '~/utils/addBuefy';

const stubs = {
  GridColumn: mockComponent,
  GridRow: mockComponent,
  StatusBillLabel: mockComponent,
};

const mocks = {
  $dayjs: dayjs,
  $device: {
    isDesktop: true,
    isMobile: false,
  },
  $router: jest.fn(),
};

const storeData = {
  modules: {
    billingManagement: {
      namespaced: true,
      state: {
        pagination: {
          data: [
            {
              invoice_paid_at: '2020-08-09 10:11:12',
              invoice_type: 'test',
              invoice_id: 1,
              tenant_name: 'tenant_name',
              room_number: 1,
              invoice_paid_amount: 10,
            },
          ],
        },
      },
      actions: {
        getBillListInvoices: () => jest.fn(),
      },
    },
  },
};
localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('currencyIDR', jest.fn());
const mountComponent = (adtMountData = {}) => {
  const mountData = {
    ...{
      stubs,
      mocks,
      mixins: [billing],
      localVue: localVueWithBuefy,
      store: new Vuex.Store(storeData),
    },
    ...adtMountData,
  };
  return shallowMount(BillingManagementListPaid, mountData);
};

describe('BillingManagementListPaid.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should render component properly', () => {
      expect(wrapper.find('.billing-management__data').exists()).toBeTruthy();
    });

    it('should render component in mobile properly', async () => {
      wrapper.vm.$device.isDesktop = false;
      wrapper.vm.$device.isMobile = true;
      await wrapper.vm.$nextTick();
      expect(wrapper.find('.billing-management__data').exists()).toBeFalsy();
    });
  });
  describe('should render computed value properly ', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should compute isDesktop value', () => {
      expect(wrapper.vm.isDesktop).toBe(wrapper.vm.$device.isDesktop);
    });

    it('should compute isMobile value', () => {
      expect(wrapper.vm.isMobile).toBe(wrapper.vm.$device.isMobile);
    });

    it('should compute pagination value', () => {
      expect(wrapper.vm.pagination).toEqual({
        data: [
          {
            invoice_id: 1,
            invoice_paid_amount: 10,
            invoice_paid_at: '2020-08-09 10:11:12',
            invoice_type: 'test',
            room_number: 1,
            tenant_name: 'tenant_name',
          },
        ],
      });
    });
  });

  describe('should run function properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should run function datePayFormated  properly', () => {
      expect(wrapper.vm.datePayFormated('2020-08-09 10:11:12')).toEqual('9 Aug 2020, 10.11');
    });

    it('should run function invoiceType  properly', () => {
      expect(wrapper.vm.invoiceType('payment')).toEqual('Bayar Sewa');
    });

    it('should run function currency  properly', () => {
      expect(wrapper.vm.currency(100000)).toEqual('100,000');
    });
  });
});
