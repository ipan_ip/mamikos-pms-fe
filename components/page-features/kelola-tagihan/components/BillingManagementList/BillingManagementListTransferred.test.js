import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import dayjs from 'dayjs';
import BillingManagementListTransferred from './BillingManagementListTransferred';
import { mockComponentWithAttrs } from '~/utils/mockComponent';
import billing from '~/components/page-features/kelola-tagihan/mixins/billing';
import localVueWithBuefy from '~/utils/addBuefy';

const mockGridColumnComponent = {
  template: "<div><slot name='row' /></div>",
};

const stubs = {
  GridColumn: mockGridColumnComponent,
  GridRow: mockComponentWithAttrs({ id: 'GridRow' }),
  StatusBillLabel: mockComponentWithAttrs({ id: 'StatusBillLabel' }),
};

const mocks = {
  $dayjs: dayjs,
  $device: {
    isDesktop: true,
    isMobile: false,
  },
  $router: jest.fn(),
};

const storeData = {
  modules: {
    billingManagement: {
      namespaced: true,
      state: {
        pagination: {
          data: [
            {
              invoice_id: 1,
              tenant_name: 'tenant_name',
              room_number: 1,
              invoice_transferred_at: '2020-08-09 10:11:12',
              invoice_transferred_amount: 1000,
              room_rent_type: 'test',
              from_mamipay: 'test',
            },
          ],
        },
      },
      actions: {
        getBillListInvoices: () => jest.fn(),
      },
    },
  },
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('currencyIDR', jest.fn());

const mountComponent = (adtMountData = {}) => {
  const mountData = {
    ...{
      stubs,
      mocks,
      mixins: [billing],
      localVue: localVueWithBuefy,
      store: new Vuex.Store(storeData),
    },
    ...adtMountData,
  };
  return shallowMount(BillingManagementListTransferred, mountData);
};

describe('BillingManagementListTransferred.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should render component properly', () => {
      expect(wrapper.find('.billing-management__data').exists()).toBeTruthy();
    });

    it('should render component mobile properly', () => {
      wrapper.vm.$device.isDesktop = false;
      wrapper.vm.$device.isMobile = true;
      expect(wrapper.find('.billing-management__column').exists()).toBeTruthy();
    });
  });

  describe('should render computed value properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should compute isDesktop properly', () => {
      expect(wrapper.vm.isDesktop).toBe(wrapper.vm.$device.isDesktop);
    });

    it('should compute isMobile properly', () => {
      expect(wrapper.vm.isMobile).toBe(wrapper.vm.$device.isMobile);
    });

    it('should compute pagination properly', () => {
      expect(wrapper.vm.pagination).toEqual({
        data: [
          {
            from_mamipay: 'test',
            invoice_id: 1,
            invoice_transferred_amount: 1000,
            invoice_transferred_at: '2020-08-09 10:11:12',
            room_number: 1,
            room_rent_type: 'test',
            tenant_name: 'tenant_name',
          },
        ],
      });
    });
  });

  describe('should run function properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should run function datePayFormated properly', () => {
      expect(wrapper.vm.datePayFormated('2020-08-09 10:11:12')).toEqual('9 Aug 2020, 10.11');
    });

    it('should run function labelBillStatus when true properly', () => {
      expect(wrapper.vm.labelBillStatus(true).inlineText).toEqual('Ditransfer');
    });
    it('should run function labelBillStatus when false properly', () => {
      expect(wrapper.vm.labelBillStatus(false).inlineText).toEqual('Diluar Mamipay');
    });
  });
});
