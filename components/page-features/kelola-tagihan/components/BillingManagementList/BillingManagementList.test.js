import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import BillingManagementList from '../BillingManagementList';
import emptyStateBasedOnPage from '../../data/emptyStateBasedOnPage';
import localVueWithBuefy from '~/utils/addBuefy';
import mockComponent, { mockComponentWithAttrs } from '~/utils/mockComponent';

const stubs = {
  GridColumn: mockComponent,
  GridRow: mockComponent,
  SkeletonLoader: mockComponent,
  BillingManagementListUnpaid: mockComponentWithAttrs({ id: 'BillingManagementListUnpaid' }),
  BillingManagementListPaid: mockComponentWithAttrs({ id: 'BillingManagementListPaid' }),
  BillingManagementListTransferred: mockComponentWithAttrs({
    id: 'BillingManagementListTransferred',
  }),
  EmptyStateList: mockComponentWithAttrs({
    id: 'EmptyStateList',
  }),
};

const mocks = {
  $device: {
    isDesktop: true,
    isMobile: false,
  },
  $route: {
    query: {
      page: 'unpaid', // unpaid, paid, transferred
    },
  },
};

const storeData = {
  modules: {
    billingManagement: {
      namespaced: true,
      state: {
        selectedKost: {
          kostName: 'test',
        },
        pagination: {
          data: [],
          isLoading: true,
        },
      },
    },
  },
};

localVueWithBuefy.use(Vuex);

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      stubs,
      mocks,
      localVue: localVueWithBuefy,
      store: new Vuex.Store(storeData),
    },
    ...adtMountData,
  };

  return shallowMount(BillingManagementList, mountData);
};

describe('BillingManagementList.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render component properly', () => {
      expect(wrapper.find('.bm-list').exists()).toBeTruthy();
    });

    it('should render billing-management__column-header in desktop view', async () => {
      wrapper.vm.$device.isDesktop = true;
      await wrapper.vm.$nextTick();
      expect(wrapper.find('.billing-management__column-header').exists).toBeTruthy();
    });

    it('should render billing-management__loader when isLoading value returns true', () => {
      wrapper.setData({
        isLoading: true,
      });

      expect(wrapper.find('.billing-management__loader').exists()).toBeTruthy();
    });

    it('should render billing loader columns properly', async () => {
      // desktop view
      wrapper.vm.$device.isDesktop = true;
      await wrapper.vm.$nextTick();
      expect(wrapper.findAll('.billing-management__loader .column').length).toBe(4);

      // mobile view
      wrapper.vm.$device.isDesktop = false;
      await wrapper.vm.$nextTick();
      expect(wrapper.findAll('.billing-management__loader .column').length).toBe(2);
    });

    it('should render BillingManagementListUnpaid properly', async () => {
      // either of selectedKost or paginationData doesnt exist
      wrapper.vm.$store.state.billingManagement.selectedKost = false;
      await wrapper.vm.$nextTick();
      expect(wrapper.find('#BillingManagementListUnpaid').exists()).toBeFalsy();

      // both selectedKost & paginationData exist
      wrapper.vm.$store.state.billingManagement.selectedKost = {
        kostName: 'test',
      };
      await wrapper.vm.$nextTick();
      expect(wrapper.find('#BillingManagementListUnpaid').exists()).toBeTruthy();
    });

    it('should render empty state list when the page is not loading and the data is empty', async () => {
      wrapper.vm.$store.state.billingManagement.pagination.isLoading = false;
      wrapper.vm.$store.state.billingManagement.pagination.data = [];

      await wrapper.vm.$nextTick();
      expect(wrapper.find('#EmptyStateList').exists()).toBeTruthy();
    });
  });

  describe('should render computed value properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should compute isDesktop value', () => {
      expect(wrapper.vm.isDesktop).toBe(wrapper.vm.$device.isDesktop);
    });

    it('should compute isMobile value', () => {
      expect(wrapper.vm.isMobile).toBe(wrapper.vm.$device.isMobile);
    });

    it('should compute selectedKost value', () => {
      expect(wrapper.vm.selectedKost).toBe('test');
    });

    it('should compute paginationData value', () => {
      expect(wrapper.vm.paginationData).toEqual([]);
    });

    it('should compute paginationLoading value', () => {
      expect(wrapper.vm.paginationLoading).toBe(
        wrapper.vm.$store.state.billingManagement.pagination.isLoading,
      );
    });

    it('should compute page value', () => {
      expect(wrapper.vm.page).toBe('unpaid');
    });
  });

  describe('should call functions properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should return proper description when calling emptyDescription method', () => {
      const description = emptyStateBasedOnPage['unpaid'];
      expect(description).toEqual({
        title: 'Tidak Ada yang Belum Bayar',
        description:
          'Semua tagihan sewa yang belum dibayar oleh penyewa akan muncul di halaman ini.',
      });
    });
  });
});
