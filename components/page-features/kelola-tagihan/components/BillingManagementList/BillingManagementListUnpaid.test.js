import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import dayjs from 'dayjs';
import localVueWithBuefy from '~/utils/addBuefy';
import { mockComponentWithAttrs } from '~/utils/mockComponent';

import { toIDR } from '~/utils/currencyString';
import BillingManagementListUnpaid from '~/components/page-features/kelola-tagihan/components/BillingManagementList/BillingManagementListUnpaid.vue';
import billing from '~/components/page-features/kelola-tagihan/mixins/billing';

// mock event bus
jest.mock('~/components/event-bus/index.js', () => {
  const EventBus = {
    $emit: jest.fn(),
    $on: jest.fn(),
  };

  global.EventBus = EventBus;
  return global.EventBus;
});

const mockGridColumnComponent = {
  template: "<div><slot name='row' /></div>",
};

const stubs = {
  GridColumn: mockGridColumnComponent,
  GridRow: mockComponentWithAttrs({ id: 'GridRow' }),
  StatusBillLabel: mockComponentWithAttrs({ id: 'StatusBillLabel' }),
};

const mocks = {
  $dayjs: dayjs,
  $device: {
    isDesktop: true,
    isMobile: false,
  },
  $router: {
    push: jest.fn(),
  },
};

const storeData = {
  modules: {
    billingManagement: {
      namespaced: true,
      state: {
        pagination: {
          data: [
            {
              invoice_id: 1,
              tenant_name: 'tenant_name',
              room_number: 1,
              invoice_scheduled_date: '2020-08-09 10:11:12',
              invoice_scheduled_label: 'test',
              room_rent_type: 'test',
              invoice_calculated_amount: 10,
              invoice_is_overdue: true,
              tenant_remindered: false,
            },
          ],
          isLoading: true,
        },
      },
      actions: {
        getBillListInvoices: () => jest.fn(),
      },
    },
  },
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('currencyIDR', toIDR);

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      stubs,
      mocks,
      localVue: localVueWithBuefy,
      mixins: [billing],
      store: new Vuex.Store(storeData),
    },
    ...adtMountData,
  };

  return shallowMount(BillingManagementListUnpaid, mountData);
};

describe('BillingManagementListUnpaid.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render billing-management__column properly', () => {
      // desktop view
      wrapper.vm.$device.isDesktop = true;
      wrapper.vm.$device.isMobile = false;
      wrapper.vm.$nextTick();
      expect(wrapper.find('.billing-management__column').exists()).toBeTruthy();

      // mobile view
      wrapper.vm.$device.isDesktop = false;
      wrapper.vm.$device.isMobile = true;
      wrapper.vm.$nextTick();
      expect(wrapper.find('.billing-management__column').exists()).toBeTruthy();
    });
  });

  describe('should render computed value properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should compute isDesktop value', () => {
      expect(wrapper.vm.isDesktop).toBe(wrapper.vm.$device.isDesktop);
    });

    it('should compute isMobile value', () => {
      expect(wrapper.vm.isMobile).toBe(wrapper.vm.$device.isMobile);
    });

    it('should compute pagination value', () => {
      expect(wrapper.vm.pagination).toEqual({
        data: [
          {
            invoice_id: 1,
            tenant_name: 'tenant_name',
            room_number: 1,
            invoice_scheduled_date: '2020-08-09 10:11:12',
            invoice_scheduled_label: 'test',
            room_rent_type: 'test',
            invoice_calculated_amount: 10,
            invoice_is_overdue: true,
            tenant_remindered: false,
          },
        ],
        isLoading: true,
      });
    });
  });

  describe('should call functions properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should call EventBus.$emit when clicking on remember link', async () => {
      const emitSpy = jest.spyOn(global.EventBus, '$emit');
      const rememberLink = wrapper.find('.billing-management__link-remember');
      if (rememberLink && rememberLink.exists()) {
        await rememberLink.trigger('click');
        expect(emitSpy).toBeCalled();
      }
      emitSpy.mockRestore();
    });

    it('should return formatted due date when calling dueDateFormated method', () => {
      const dueDate = wrapper.vm.dueDateFormated('2020-08-09 10:11:12');

      expect(dueDate).toBe('9 Aug 2020');
    });
  });
});
