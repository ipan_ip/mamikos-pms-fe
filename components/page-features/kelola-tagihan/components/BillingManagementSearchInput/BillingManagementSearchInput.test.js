import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import BillingManagementSearchInput from '../BillingManagementSearchInput';
import localVueWithBuefy from '~/utils/addBuefy';
import billing from '~/components/page-features/kelola-tagihan/mixins/billing';

const mockComponent = {
  template: '<div />',
};

const stubs = {
  MkButton: mockComponent,
};

const mocks = {
  $emit: jest.fn(),
  $route: {
    query: {
      page: 'test',
    },
  },
};

const storeData = {
  modules: {
    billingManagement: {
      namespaced: true,
      mutations: {
        updateFilterTenantName: () => jest.fn(),
      },
      actions: {
        getBillListInvoices: jest.fn(),
      },
    },
  },
};

localVueWithBuefy.use(Vuex);

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      stubs,
      mocks,
      localVue: localVueWithBuefy,
      mixins: [billing],
      store: new Vuex.Store(storeData),
    },
    ...adtMountData,
  };

  return shallowMount(BillingManagementSearchInput, mountData);
};

describe('BillingManagementSearchInput.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render component properly', () => {
      expect(wrapper.find('.bm-search-input').exists()).toBeTruthy();
    });
  });

  describe('should render computed value properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should compute isButtonSearchShow value', () => {
      expect(wrapper.vm.isButtonSearchShow).toBe(wrapper.vm.keywordSearch);
    });

    it('should compute page value', () => {
      expect(wrapper.vm.page).toBe('test');
    });
  });

  describe('should call functions properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should call refreshToFirstPage function when received blur event from search input', async () => {
      const searchInput = wrapper.find('.bm-search-input input');
      const showListBillSpy = jest.spyOn(wrapper.vm, 'showListBill');
      if (searchInput && searchInput.exists()) {
        await wrapper.setData({
          keywordSearch: 'test',
        });
        await searchInput.trigger('blur');
        expect(showListBillSpy).not.toBeCalled();

        await wrapper.setData({
          keywordSearch: '',
        });
        await searchInput.trigger('blur');
        expect(showListBillSpy).toBeCalled();

        showListBillSpy.mockRestore();
      }
    });

    it('should call search function when received keyup.enter event from search input ', async () => {
      const searchInput = wrapper.find('.bm-search-input__input');
      const showListBillSpy = jest.spyOn(wrapper.vm, 'showListBill');
      if (searchInput && searchInput.exists()) {
        await searchInput.trigger('keyup.enter');
        expect(showListBillSpy).toBeCalled();
        showListBillSpy.mockRestore();
      }
    });

    it('should call $store.commit and getBillListInvoices function when calling showListBill method', async () => {
      const getBillListInvoicesSpy = jest.spyOn(wrapper.vm, 'getBillListInvoices');
      const commitSpy = jest.spyOn(wrapper.vm.$store, 'commit');

      await wrapper.vm.showListBill();
      expect(getBillListInvoicesSpy).toBeCalled();
      expect(commitSpy).toBeCalled();

      getBillListInvoicesSpy.mockRestore();
      commitSpy.mockRestore();
    });
  });
});
