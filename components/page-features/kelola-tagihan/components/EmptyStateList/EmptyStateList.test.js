import { shallowMount } from '@vue/test-utils';
import _fill from 'lodash/fill';
import _join from 'lodash/join';
import Vuex from 'vuex';
import GridColumn from '../GridColumn';
import GridRow from '../GridRow';
import EmptyStateList from './EmptyStateList';
import localVueWithBuefy from '~/utils/addBuefy';

global._fill = _fill;
global._join = _join;
global.MutationObserver = jest.fn(function MutationObserver(callback) {
  this.observe = jest.fn();
  this.disconnect = jest.fn();
  this.trigger = (mockedMutationsList) => {
    callback(mockedMutationsList, this);
  };
  callback();
});

const stubs = {
  GridColumn,
  GridRow,
};

const storeData = {
  modules: {
    billingManagement: {
      namespaced: true,
      state: {
        filterOption: {
          tenantName: 'tenant_name',
        },
      },
    },
  },
};

localVueWithBuefy.use(Vuex);

const mountComponent = (adtMountData = {}) => {
  const mountData = {
    ...{
      stubs,
      localVue: localVueWithBuefy,
      store: new Vuex.Store(storeData),
    },
    ...adtMountData,
  };
  return shallowMount(EmptyStateList, mountData);
};

describe('EmptyStateList.vue', () => {
  describe('component rendering', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should render component properly', () => {
      expect(wrapper.find('.empty-state-list').exists()).toBeTruthy();
    });

    it('should not render when filterSearchTenantName false properly', async () => {
      wrapper.vm.$store.state.billingManagement.filterOption.tenantName = false;
      await wrapper.vm.filterSearchTenantName;
      await wrapper.vm.$nextTick();

      await expect(wrapper.find('.billing-management__link-remember__icon').exists()).toBeFalsy();
    });
  });
  describe('computed : filterSearchTenantName ', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should compute value filterSearchTenantName properly ', async () => {
      wrapper.vm.$store.state.billingManagement.filterOption.tenantName = 'test';
      await wrapper.vm.filterSearchTenantName;
      await wrapper.vm.$nextTick();

      expect(wrapper.vm.filterSearchTenantName).toEqual('test');
    });
  });
});
