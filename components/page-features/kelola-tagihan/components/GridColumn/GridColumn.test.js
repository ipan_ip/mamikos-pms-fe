import { shallowMount } from '@vue/test-utils';

import _fill from 'lodash/fill';
import _join from 'lodash/join';

import GridColumn from './GridColumn.vue';
import localVueWithBuefy from '~/utils/addBuefy';

global._fill = _fill;
global._join = _join;
global.MutationObserver = jest.fn(function MutationObserver(callback) {
  this.observe = jest.fn();
  this.disconnect = jest.fn();
  this.trigger = (mockedMutationsList) => {
    callback(mockedMutationsList, this);
  };
  callback();
});

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
    },
    ...adtMountData,
  };

  return shallowMount(GridColumn, mountData);
};

describe('GridColumn.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render component properly', () => {
      expect(wrapper.find('.bm-grid-column').exists()).toBeTruthy();
    });
  });

  describe('should render computed value properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should compute gridTemplateColumn properly', () => {
      wrapper.setData({ gridColumnLength: 1 });
      expect(wrapper.vm.gridTemplateColumn).toEqual({
        'grid-template-columns': '100%',
      });
    });
  });

  describe('should call functions properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should set grid column length based on $refs.gridColumn.children length', () => {
      expect(wrapper.vm.gridColumnLength).toBe(0);
    });

    it('should call observer.observe when calling initMutationObserver method', async () => {
      const observeSpy = jest.spyOn(wrapper.vm.observer, 'observe');
      await wrapper.vm.initMutationObserver();

      expect(observeSpy).toBeCalled();
      observeSpy.mockRestore();
    });

    it('should call observer.disconnect before destroying the component', () => {
      const disconnectSpy = jest.spyOn(wrapper.vm.observer, 'disconnect');
      wrapper.destroy();

      expect(disconnectSpy).toBeCalled();
      disconnectSpy.mockRestore();
    });
  });
});
