import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import BillingManagementTabs from '~/components/page-features/kelola-tagihan/components/BillingManagementTabs/BillingManagementTabs.vue';
import localVueWithBuefy from '~/utils/addBuefy';
import { mockComponentWithAttrs } from '~/utils/mockComponent';

// mock event bus
jest.mock('~/components/event-bus/index.js', () => {
  const EventBus = {
    $emit: jest.fn(),
    $on: jest.fn(),
  };

  global.EventBus = EventBus;
  return global.EventBus;
});

const mockGridColumnComponent = {
  template: "<div><slot name='row' /></div>",
};

const stubs = {
  GridColumn: mockGridColumnComponent,
  GridRow: mockComponentWithAttrs({ id: 'GridRow' }),
};

const mocks = {
  $device: {
    isDesktop: true,
    isMobile: false,
  },
  $route: {
    query: {
      page: 'unpaid',
    },
  },
  $router: {
    push: jest.fn(),
  },
};

const storeData = {
  modules: {
    billingManagement: {
      namespaced: true,
      state: {
        pagination: {
          data: [],
          isLoading: true,
        },
      },
      mutations: {
        updateFilterStatus: () => jest.fn(),
      },
      actions: {
        getBillingSummaryPerStatus: () => jest.fn(),
      },
    },
  },
};

localVueWithBuefy.use(Vuex);

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      stubs,
      mocks,
      localVue: localVueWithBuefy,
      store: new Vuex.Store(storeData),
    },
    ...adtMountData,
  };

  return shallowMount(BillingManagementTabs, mountData);
};

describe('BillingManagementTabs.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render billing-management__column properly', () => {
      expect(wrapper.find('.billing-management__column').exists()).toBeTruthy();
    });

    it('should render notification dot image when tab.notification returns true', async () => {
      wrapper.setData({
        tabsMenu: [
          {
            title: 'Belum Bayar',
            notification: true,
            page: 'unpaid',
            url: '/billing-management?page=unpaid',
          },
          {
            title: 'Di Mamikos',
            notification: false,
            page: 'paid',
            url: '/billing-management?page=paid',
          },
          {
            title: 'Ditransfer',
            notification: false,
            page: 'transferred',
            url: '/billing-management?page=transferred',
          },
        ],
      });
      await wrapper.vm.$nextTick();
      expect(wrapper.find('.billing-management__red-dot').exists()).toBeTruthy();
    });
  });

  describe('should render computed value properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should compute isDesktop value', () => {
      expect(wrapper.vm.isDesktop).toBe(wrapper.vm.$device.isDesktop);
    });

    it('should compute page value', () => {
      expect(wrapper.vm.page).toBe('unpaid');
    });
  });

  describe('should call functions properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should call $store.commit, $store.dispatch, and $router.push when clicking on tab list item', async () => {
      const commitSpy = jest.spyOn(wrapper.vm.$store, 'commit');
      const dispatchSpy = jest.spyOn(wrapper.vm.$store, 'dispatch');
      const tabItem = wrapper.find('.mamipoin-tabs__list > li');

      if (tabItem && tabItem.exists()) {
        await tabItem.trigger('click');

        expect(commitSpy).toBeCalled();
        expect(dispatchSpy).toBeCalled();
        expect(wrapper.vm.$router.push).toBeCalled();
      }

      commitSpy.mockRestore();
      dispatchSpy.mockRestore();
    });
  });
});
