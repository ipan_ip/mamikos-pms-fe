import _map from 'lodash/map';
import _reject from 'lodash/reject';
import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import BillingManagementReminderModal from './BillingManagementReminderModal';
import MkCardHeader from '~/components/global/molecules/MkCard/components/MkCardHeader';
import localVueWithBuefy from '~/utils/addBuefy';
import MkButton from '~/components/global/atoms/MkButton';
import MkModal, { mixin as modalMixin } from '~/components/global/molecules/MkModal/MkModal';
import MkCard from '~/components/global/molecules/MkCard';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';
import MkCardFooter from '~/components/global/molecules/MkCard/components/MkCardFooter';
import billing from '~/components/page-features/kelola-tagihan/mixins/billing';

const stubs = {
  MkCard,
  MkCardBody,
  MkCardFooter,
  MkCardHeader,
  MkButton,
  MkModal,
};

global._map = _map;
global._reject = _reject;
localVueWithBuefy.use(Vuex);

const mocks = {
  $buefy: { toast: { open: jest.fn() } },
  $emit: jest.fn(),
  $api: {
    sendBulkReminder: jest
      .fn()
      .mockResolvedValue({ data: { status: true, meta: { message: 'test' } } }),
  },
  $route: {
    query: {
      page: 'test',
    },
  },
  $store: {
    state: {
      billingManagement: {
        pagination: {
          data: 'test',
        },
      },
    },
    dispatch: jest.fn(),
  },
};

const propsData = {
  typeReminder: 'all',
  invoiceId: 0,
  tenantName: '',
  refreshPageNumber: 0,
};

const mountComponents = (adtMountData = {}) => {
  const mountData = {
    ...{
      stubs,
      mocks,
      propsData,
      localVue: localVueWithBuefy,
      mixins: [modalMixin, billing],
    },
    ...adtMountData,
  };
  return shallowMount(BillingManagementReminderModal, mountData);
};

describe('BillingManagementReminderModal.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponents();
    });

    it('should render component properly', async () => {
      await wrapper.setProps({ active: true });
      expect(wrapper.find('.bm-filter-list-modal').exists()).toBeTruthy();
    });

    it('should close modal when user click close icon', async () => {
      const closeButton = wrapper.find('.close-icon');
      if (closeButton && closeButton.exists()) {
        await wrapper.trigger('click');
        expect(wrapper.find('.bm-filter-list-modal').exists()).toBeFalsy();
      }
    });
  });

  describe('should render computed values', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponents();
    });

    it('should render page properly', () => {
      expect(wrapper.vm.page).toBeTruthy();
    });

    it('should render billingList properly', () => {
      expect(wrapper.vm.billingList).toBe('test');
    });
    it('should render typeReminderText properly', () => {
      expect(wrapper.vm.typeReminderText).toBeTruthy();
    });
  });

  describe('should execute functions properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponents();
    });

    it('should diplay message in toast', async () => {
      await wrapper.vm.openToast();
      expect(wrapper.vm.$buefy.toast.open).toBeCalled();
    });

    it('should execute functions processReminder properly', async () => {
      await wrapper.vm.proccessReminder();
      expect(wrapper.vm.sendBulkReminder).toBeTruthy();

      // when status single
      wrapper.vm.$api.sendReminder = jest
        .fn()
        .mockResolvedValue({ data: { status: true, meta: { message: 'test' } } });
      await wrapper.setProps({ typeReminder: 'single' });
      await wrapper.vm.proccessReminder();
      expect(wrapper.vm.sendReminder).toBeTruthy();
    });

    describe('should execute functions sendBulkReminder properly', () => {
      it('should execute functions sendBulkReminder when page Number 0', async () => {
        const emitSpy = jest.spyOn(wrapper.vm, '$emit');
        wrapper.vm.$api.sendBulkReminder = jest
          .fn()
          .mockResolvedValue({ data: { status: true, meta: { message: 'test' } } });
        await wrapper.setProps({ refreshPageNumber: 0 });
        await wrapper.vm.sendBulkReminder();
        expect(emitSpy).toBeCalled();
        emitSpy.mockRestore();
      });

      it('should execute functions sendBulkReminder when page Number 1', async () => {
        const getBillingInvoiceSpy = jest.spyOn(wrapper.vm, 'getBillListInvoices');
        wrapper.vm.$api.sendBulkReminder = jest
          .fn()
          .mockResolvedValue({ data: { status: true, meta: { message: 'test' } } });
        await wrapper.setProps({ refreshPageNumber: 1 });
        await wrapper.vm.sendBulkReminder();
        expect(getBillingInvoiceSpy).toBeCalled();
        getBillingInvoiceSpy.mockRestore();
      });

      it('should execute functions sendBulkReminder when status false', async () => {
        const toastSpy = jest.spyOn(wrapper.vm, 'openToast');
        wrapper.vm.$api.sendBulkReminder = jest
          .fn()
          .mockResolvedValue({ data: { status: false, meta: { message: 'error' } } });
        await wrapper.vm.sendBulkReminder();
        expect(toastSpy).toBeCalled();
        toastSpy.mockRestore();
      });
    });

    describe('should execute functions sendReminder properly', () => {
      it('should execute functions sendReminder when refreshPage 0', async () => {
        const emitSpy = jest.spyOn(wrapper.vm, '$emit');
        wrapper.vm.$api.sendReminder = jest
          .fn()
          .mockResolvedValue({ data: { status: true, meta: { message: 'test' } } });
        await wrapper.setProps({ refreshPageNumber: 0 });
        await wrapper.vm.sendReminder();
        expect(emitSpy).toBeCalled();
        emitSpy.mockRestore();
      });

      it('should execute functions sendReminder when refreshPage 1', async () => {
        const setBillingInvoicesSpy = jest.spyOn(wrapper.vm, 'getBillListInvoices');
        wrapper.vm.$api.sendReminder = jest
          .fn()
          .mockResolvedValue({ data: { status: true, meta: { message: 'test' } } });
        await wrapper.setProps({ refreshPageNumber: 1 });
        await wrapper.vm.sendReminder();
        expect(setBillingInvoicesSpy).toBeCalled();
        setBillingInvoicesSpy.mockRestore();
      });

      it('should execute functions sendReminder when status false', async () => {
        const toastSpy = jest.spyOn(wrapper.vm, 'openToast');
        wrapper.vm.$api.sendReminder = jest
          .fn()
          .mockResolvedValue({ data: { status: false, meta: { message: 'error' } } });
        await wrapper.vm.sendReminder();
        expect(toastSpy).toBeCalled();
        toastSpy.mockRestore();
      });
    });
  });
});
