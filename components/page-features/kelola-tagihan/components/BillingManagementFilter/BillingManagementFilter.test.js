import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import dayjs from 'dayjs';
import BillingManagementFilter from '~/components/page-features/kelola-tagihan/components/BillingManagementFilter/BillingManagementFilter.vue';
import localVueWithBuefy from '~/utils/addBuefy';
import { mockComponentWithAttrs } from '~/utils/mockComponent';
import billing from '~/components/page-features/kelola-tagihan/mixins/billing';
import { truncate } from '~/utils/string';

const mocks = {
  $dayjs: dayjs,
  $device: {
    isDesktop: false,
    isMobile: true,
  },
  $router: {
    replace: jest.fn(),
  },
  $route: {
    query: {
      page: 'unpaid',
    },
  },
  $emit: jest.fn(),
};

const stubs = {
  MkCard: mockComponentWithAttrs({ id: 'MkCard' }),
  MkCardBody: mockComponentWithAttrs({ id: 'MkCardBody' }),
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('truncate', truncate);

const storeData = {
  modules: {
    billingManagement: {
      namespaced: true,
      state: {
        selectedKost: {
          kostName: 'kostName',
        },
        filterOption: {
          rentType: 'all', // all, day, week, month, 3_month, 6_month, year
        },
      },
      mutations: {
        updateFilterDate: () => jest.fn(),
      },
      actions: {
        getBillListInvoices: () => jest.fn(),
        getBillingSummaryPerStatus: () => jest.fn(),
        clearBillingSummaryStatus: () => jest.fn(),
      },
    },
  },
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      mocks,
      stubs,
      localVue: localVueWithBuefy,
      mixins: [billing],
      store: new Vuex.Store(storeData),
    },
    ...adtMountData,
  };

  return shallowMount(BillingManagementFilter, mountData);
};

describe('BillingManagementFilter.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render component properly', () => {
      expect(wrapper.find('.bm-filter').exists()).toBeTruthy();
    });

    it('should set selectedMonth value on mounted', () => {
      expect(wrapper.vm.selectedMonth).toBeTruthy();
    });

    it('should render selectedKost text properly', () => {
      const mockStoreData = Object.assign({}, storeData);
      mockStoreData.modules.billingManagement.state.selectedKost = {};

      wrapper.destroy();
      wrapper = mount({
        store: new Vuex.Store(mockStoreData),
      });

      const selectedKostLoader = wrapper.find('.c-loader');
      expect(selectedKostLoader.exists()).toBeTruthy();
    });
  });

  describe('should render computed data', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render selectedRentType properly', () => {
      expect(wrapper.vm.selectedRentType).toBe('Hitungan Sewa');
    });

    it('should render page properly', () => {
      expect(wrapper.vm.page).toBe('unpaid');
    });
  });

  describe('should execute functions properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should call $store.commit when calling changeDate method', async () => {
      const commitSpy = jest.spyOn(wrapper.vm.$store, 'commit');
      const getAllBillingSummarySpy = jest.spyOn(wrapper.vm, 'getAllBillingSummary');
      const getBillListInvoicesSpy = jest.spyOn(wrapper.vm, 'getBillListInvoices');

      await wrapper.vm.changeDate('2020-08-09 10:11:12');

      expect(commitSpy).toBeCalled();
      expect(getAllBillingSummarySpy).toBeCalled();
      expect(getBillListInvoicesSpy).toBeCalled();

      commitSpy.mockRestore();
      getAllBillingSummarySpy.mockRestore();
      getBillListInvoicesSpy.mockRestore();
    });

    it('should emit an event when clicking on kost label', async () => {
      const emitSpy = jest.spyOn(wrapper.vm, '$emit');
      const kostLabel = wrapper.find('.bm-filter__kost-label');
      if (kostLabel && kostLabel.exists()) {
        await kostLabel.trigger('click');
        expect(emitSpy).toBeCalled();
      }
      emitSpy.mockRestore();
    });

    it('should emit an event when clicking on rent label', async () => {
      const emitSpy = jest.spyOn(wrapper.vm, '$emit');
      const rentLabel = wrapper.find('.bm-filter__rent');
      if (rentLabel && rentLabel.exists()) {
        await rentLabel.trigger('click');
        expect(emitSpy).toBeCalled();
      }
      emitSpy.mockRestore();
    });

    it('should return formatted date when calling dateFormatter in desktop view', () => {
      wrapper.vm.$device.isDesktop = true;
      wrapper.vm.$device.isMobile = false;
      wrapper.vm.$nextTick();
      const date = wrapper.vm.dateFormatter('2020-08-09 10:11:12');
      expect(date).toBe('August 2020');
    });

    it('should call $options.filters.truncate when calling dateFormatter in mobile view', () => {
      wrapper.vm.$device.isDesktop = false;
      wrapper.vm.$device.isMobile = true;
      wrapper.vm.$nextTick();
      const date = wrapper.vm.dateFormatter('2020-08-09 10:11:12');
      expect(date).toBe('August 2020');
    });

    it('should set datepickerRange data when calling updateDatepickerRange method', async () => {
      await wrapper.vm.updateDatepickerRange('2021');

      expect(wrapper.vm.datepickerRange[1]).toBe(1);
    });
  });
});
