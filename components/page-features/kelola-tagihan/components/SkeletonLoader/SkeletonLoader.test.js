import { shallowMount } from '@vue/test-utils';
import SkeletonLoader from './SkeletonLoader';
import localVueWithBuefy from '~/utils/addBuefy';

const mountComponent = () => {
  const mountData = {
    localVue: localVueWithBuefy,
  };

  return shallowMount(SkeletonLoader, mountData);
};

describe('SkeletonLoader.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mountComponent();
  });

  describe('render component', () => {
    it('should render component properly', () => {
      expect(wrapper.find('.block-loader')).toBeTruthy();
    });
  });

  describe('computed value', () => {
    it('loaderStyle should return the correct object when width, height, & radius props isnt being passed', () => {
      expect(wrapper.vm.loaderStyle).toEqual({
        borderRadius: '3px',
        height: '50px',
        width: '100%',
      });
    });

    it('loaderStyle should return the correct object when width, height, & radius props is passed', () => {
      wrapper.setProps({ width: 1, height: 1, radius: 1 });
      expect(wrapper.vm.loaderStyle).toEqual({ borderRadius: '1px', height: '1px', width: '1px' });
    });

    it('loaderStyle should return the correct object when width, height, & radius props is passed string', () => {
      wrapper.setProps({ width: 'test', height: 'test', radius: 'test' });
      expect(wrapper.vm.loaderStyle).toEqual({
        borderRadius: 'testpx',
        height: 'test',
        width: 'test',
      });
    });
  });
});
