import { shallowMount, createLocalVue } from '@vue/test-utils';
import dayjs from 'dayjs';
import StatusBillLabel from './StatusBillLabel';

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;

describe('StatusBillLabel.vue', () => {
  const wrapper = shallowMount(StatusBillLabel, { localVue });

  it('should return inlineText props as title', () => {
    wrapper.setProps({ inlineText: 'inline text' });
    expect(wrapper.vm.title).toBe('inline text');
  });

  it('should return 1 hari lagi as title', () => {
    const tomorrow = dayjs()
      .add(2, 'day')
      .format('YYYY-MM-DD');
    wrapper.setProps({ dueDate: tomorrow });
    wrapper.setProps({ inlineText: '' });
    expect(wrapper.vm.title).toBe('1 Hari lagi');
  });

  it('should return jatuh tempo as title', () => {
    const today = dayjs().format('YYYY-MM-DD');
    wrapper.setProps({ dueDate: today });
    wrapper.setProps({ inlineText: '' });
    expect(wrapper.vm.title).toBe('Jatuh Tempo');
  });

  it('should return telat 1 hari as title', () => {
    const yesterday = dayjs()
      .subtract(1, 'day')
      .format('YYYY-MM-DD');
    wrapper.setProps({ dueDate: yesterday });
    wrapper.setProps({ inlineText: '' });
    expect(wrapper.vm.title).toBe('Telat 1 Hari');
  });
});
