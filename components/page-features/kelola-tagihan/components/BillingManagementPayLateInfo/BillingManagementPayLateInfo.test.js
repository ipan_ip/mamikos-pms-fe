import { shallowMount, createLocalVue } from '@vue/test-utils';
import BillingManagementPayLateInfo from './BillingManagementPayLateInfo';

const localVue = createLocalVue();

describe('BillingManagementPayLateInfo.vue', () => {
  const wrapper = shallowMount(BillingManagementPayLateInfo, {
    localVue,
    stubs: {
      'router-link': {
        template: `<a />`,
      },
    },
  });

  it('should load the component', () => {
    expect(wrapper.find('.bm-pay-late-info').exists()).toBe(true);
  });
});
