import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import _map from 'lodash/map';
import _reject from 'lodash/reject';
import BillingManagementCard from '../BillingManagementCard';
import localVueWithBuefy from '~/utils/addBuefy';
import { toIDR } from '~/utils/currencyString';

jest.mock('~/components/event-bus/index.js', () => {
  const EventBus = {
    $emit: jest.fn(),
    $on: jest.fn(),
  };

  global.EventBus = EventBus;

  return global.EventBus;
});

const mocks = {
  $device: {
    isDesktop: true,
    isMobile: false,
  },
  $router: {
    replace: jest.fn(),
  },
  $route: {
    query: {
      page: 'unpaid', // unpaid, paid, transferred
    },
  },
};

const storeData = {
  modules: {
    billingManagement: {
      namespaced: true,
      state: {
        billingCardSummary: {
          unpaid: { description: 'test', title: 'test', total: 1 },
          paid: { description: 'test', title: 'test', total: 2 },
          transferred: { description: 'test', title: 'test', total: 3 },
        },
        pagination: {
          data: [
            { invoice_is_overdue: false },
            { invoice_is_overdue: true },
            { invoice_is_overdue: true },
          ],
          isLoading: false,
        },
      },
      mutations: {
        updateFilterStatus: () => jest.fn(),
      },
    },
  },
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('currencyIDR', toIDR);

global._map = _map;
global._reject = _reject;

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      mocks,
      localVue: localVueWithBuefy,
      store: new Vuex.Store(storeData),
    },
    ...adtMountData,
  };

  return shallowMount(BillingManagementCard, mountData);
};

describe('BillingManagementCard.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render component properly', () => {
      expect(wrapper.find('.bm-card').exists()).toBeTruthy();
    });
  });

  describe('should render computed value properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should compute isDesktop value', () => {
      expect(wrapper.vm.isDesktop).toBe(wrapper.vm.$device.isDesktop);
    });

    it('should compute isMobile value', () => {
      expect(wrapper.vm.isMobile).toBe(wrapper.vm.$device.isMobile);
    });

    it('should compute page value', () => {
      expect(wrapper.vm.page).toBe('unpaid');
    });

    it('should compute billingCardSummary', () => {
      expect(wrapper.vm.billingCardSummary).toEqual({
        unpaid: { description: 'test', title: 'test', total: 1 },
        paid: { description: 'test', title: 'test', total: 2 },
        transferred: { description: 'test', title: 'test', total: 3 },
      });
    });

    it('should compute isThereBillOverDue value', () => {
      expect(wrapper.vm.isThereBillOverDue).toBeTruthy();
    });
  });

  describe('should call functions properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should call EventBus.$emit when clicking on overdue link', async () => {
      const emitSpy = jest.spyOn(global.EventBus, '$emit');
      const overdueLink = wrapper.find('.bm-card__list__link a');
      if (overdueLink && overdueLink.exists()) {
        await overdueLink.trigger('click');
        await wrapper.vm.$nextTick();

        expect(emitSpy).toBeCalled();
        emitSpy.mockRestore();
      }
    });

    it('should call $store.commit and $router.replace methods when clicking on card list item', async () => {
      const commitSpy = jest.spyOn(wrapper.vm.$store, 'commit');

      const cardItems = wrapper.findAll('.bm-card__list');
      if (cardItems && cardItems.length) {
        const firstCardItem = cardItems.at(0);
        await firstCardItem.trigger('click');

        expect(commitSpy).toBeCalled();
        expect(wrapper.vm.$router.replace).toBeCalled();

        commitSpy.mockRestore();
      }
    });
  });
});
