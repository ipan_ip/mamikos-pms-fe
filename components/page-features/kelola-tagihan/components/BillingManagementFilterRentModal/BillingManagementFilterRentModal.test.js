import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import BillingManagementFilterRentModal from './BillingManagementFilterRentModal.vue';
import localVueWithBuefy from '~/utils/addBuefy';

import MkModal, { mixin as modalMixin } from '~/components/global/molecules/MkModal/MkModal';
import MkCard from '~/components/global/molecules/MkCard';
import MkCardHeader from '~/components/global/molecules/MkCard/components/MkCardHeader';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';
import MkCardFooter from '~/components/global/molecules/MkCard/components/MkCardFooter';
import billing from '~/components/page-features/kelola-tagihan/mixins/billing';

const mocks = {
  $router: {
    replace: jest.fn(),
  },
  $route: {
    query: {
      page: 'unpaid',
    },
  },
};

const stubs = {
  MkModal,
  MkCard,
  MkCardHeader,
  MkCardBody,
  MkCardFooter,
};

localVueWithBuefy.use(Vuex);

const propsData = {
  active: true,
};

const storeData = {
  modules: {
    billingManagement: {
      namespaced: true,
      mutations: {
        updateFilterRentType: () => jest.fn(),
      },
      actions: {
        getBillListInvoices: () => jest.fn(),
        getBillingSummaryPerStatus: () => jest.fn(),
        clearBillingSummaryStatus: () => jest.fn(),
      },
    },
  },
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      mocks,
      stubs,
      propsData,
      localVue: localVueWithBuefy,
      mixins: [billing, modalMixin],
      store: new Vuex.Store(storeData),
    },
    ...adtMountData,
  };

  return shallowMount(BillingManagementFilterRentModal, mountData);
};

describe('BillingManagementFilterRentModal.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render component properly', () => {
      expect(wrapper.find('.bm-filter-rent-modal').exists()).toBeTruthy();
    });
  });

  describe('should render computed data', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should compute page properly', () => {
      expect(wrapper.vm.page).toBe('unpaid');
    });
  });

  describe('should execute functions properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should set isShowCard value on active value change', async () => {
      wrapper.setProps({
        active: true,
      });
      await wrapper.vm.$nextTick();
      expect(wrapper.vm.isShowCard).toBeTruthy();

      wrapper.setProps({
        active: false,
      });
      await wrapper.vm.$nextTick();
      expect(wrapper.vm.isShowCard).toBeFalsy();
    });

    it('should set isShowCard value and call toggleModal when clicking on close icon', async () => {
      const closeIcon = wrapper.find('.close-icon');
      if (closeIcon && closeIcon.exists()) {
        await closeIcon.trigger('click');

        expect(wrapper.vm.isShowCard).toBeFalsy();
      }
    });

    it('should call $store.commit and hideModal when calling selectRentType method', async () => {
      const commitSpy = jest.spyOn(wrapper.vm.$store, 'commit');
      const hideModalSpy = jest.spyOn(wrapper.vm, 'hideModal');

      await wrapper.vm.selectRentType({ value: 'test' });
      expect(commitSpy).toBeCalled();
      expect(hideModalSpy).toBeCalled();

      commitSpy.mockRestore();
      hideModalSpy.mockRestore();
    });
  });
});
