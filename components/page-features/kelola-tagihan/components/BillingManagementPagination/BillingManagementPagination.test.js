import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import BillingManagementPagination from './BillingManagementPagination';
import billing from '~/components/page-features/kelola-tagihan/mixins/billing';
import localVueWithBuefy from '~/utils/addBuefy';

localVueWithBuefy.use(Vuex);
localVueWithBuefy.use(VueRouter);

const routesData = {
  routes: [
    { name: 'test1', path: '/test-1' },
    { name: 'test2', path: '/test-2' },
  ],
};

const storeData = {
  modules: {
    billingManagement: {
      namespaced: true,
      state: {
        pagination: {
          totalData: 1,
          limit: 1,
          pageNumber: 1,
          isLoading: false,
        },
      },
      actions: {
        getBillListInvoices: () => jest.fn(),
      },
    },
  },
};

localVueWithBuefy.use(Vuex);
const mountComponent = (adtMountData = {}) => {
  const mountData = {
    ...{
      mixins: [billing],
      localVue: localVueWithBuefy,
      store: new Vuex.Store(storeData),
      router: new VueRouter(routesData),
    },
    ...adtMountData,
  };
  return shallowMount(BillingManagementPagination, mountData);
};

describe('BillingManagementPagination.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should render component properly', () => {
      expect(wrapper.find('.bm-pagination').exists()).toBeTruthy();
    });

    it('should render component when total more than 1', () => {
      expect(wrapper.find('.pagination').exists()).toBeTruthy();
    });

    it('should not render component when total 0', async () => {
      wrapper.vm.$store.state.billingManagement.pagination.totalData = 0;
      await wrapper.vm.total;
      await expect(wrapper.find('.pagination').exists()).toBeFalsy();
    });
  });

  describe('should render computed value properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should compute total properly', async () => {
      wrapper.vm.$store.state.billingManagement.pagination.totalData = 1;
      await wrapper.vm.$nextTick();
      expect(wrapper.vm.total).toBe(1);
    });

    it('should compute limit properly', () => {
      expect(wrapper.vm.limit).toBe(1);
    });

    it('should compute route.query.page properly', async () => {
      await wrapper.vm.$router.replace({ name: 'test1', query: { page: 'unpaid' } });
      expect(wrapper.vm.page).toBe('unpaid');
    });

    it('should compute paginationLoading properly', () => {
      expect(wrapper.vm.paginationLoading).toBeFalsy();
    });
  });

  describe('should watch $route ', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });
    it('should set value when route changed', async () => {
      wrapper.vm.$router.push('test-2');
      await wrapper.vm.$nextTick();
      expect(wrapper.vm.current).toBe(1);
    });
  });

  describe('should run function properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should run function goTo properly', () => {
      const getBillListInvoicesSpy = jest.spyOn(wrapper.vm, 'getBillListInvoices');
      wrapper.vm.goTo();
      expect(getBillListInvoicesSpy).toBeCalled();
      getBillListInvoicesSpy.mockRestore();
    });
  });
});
