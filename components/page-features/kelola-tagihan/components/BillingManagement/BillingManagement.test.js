import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import BillingManagement from './BillingManagement.vue';
import localVueWithBuefy from '~/utils/addBuefy';
import { mockComponentWithAttrs } from '~/utils/mockComponent';

// mock event bus
jest.mock('~/components/event-bus/index.js', () => {
  const EventBus = {
    $on: jest.fn((eventName, cb) => {
      const mockCallbackData = {};
      cb && cb(mockCallbackData);
    }),
  };

  global.EventBus = EventBus;
  return global.EventBus;
});

const stubs = {
  BillingManagementSearchInput: mockComponentWithAttrs({ id: 'BillingManagementSearchInput' }),
  BillingManagementCard: mockComponentWithAttrs({ id: 'BillingManagementCard' }),
  BillingManagementPagination: mockComponentWithAttrs({ id: 'BillingManagementPagination' }),
  BillingManagementTabs: mockComponentWithAttrs({ id: 'BillingManagementTabs' }),
  BillingManagementList: mockComponentWithAttrs({ id: 'BillingManagementList' }),
  BillingManagementHeader: mockComponentWithAttrs({ id: 'BillingManagementHeader' }),
  BillingManagementFilterListModal: mockComponentWithAttrs({
    id: 'BillingManagementFilterListModal',
  }),
  BillingManagementReminderModal: mockComponentWithAttrs({ id: 'BillingManagementReminderModal' }),
};

const mocks = {
  $device: {
    isDesktop: true,
    isMobile: false,
  },
  $route: {
    query: {
      page: 'unpaid',
    },
  },
};

const storeData = {
  modules: {
    billingManagement: {
      namespaced: true,
      state: {
        filterOption: {
          sortBy: 'scheduled_date', // scheduled_date, room_number, tenant_name
        },
        pagination: {
          pageNumber: 1,
          data: [
            {
              invoice_id: 17691,
              invoice_is_overdue: true,
              invoice_calculated_amount: 1000000,
              invoice_scheduled_date: '2020-10-02',
              invoice_scheduled_label: 'Telat 3 Hari',
              room_number: '8',
              room_rent_type: 'Per Bulan',
              tenant_name: 'Test X',
              tenant_remindered: false,
            },
            {
              invoice_id: 19039,
              invoice_is_overdue: true,
              invoice_calculated_amount: 1000000,
              invoice_scheduled_date: '2020-10-04',
              invoice_scheduled_label: 'Telat 1 Hari',
              room_number: '1',
              room_rent_type: 'Per Bulan',
              tenant_name: 'Khazita Seiya Sadanti',
              tenant_remindered: false,
            },
          ],
        },
      },
    },
  },
};

localVueWithBuefy.use(Vuex);

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      stubs,
      mocks,
      localVue: localVueWithBuefy,
      store: new Vuex.Store(storeData),
    },
    ...adtMountData,
  };

  return shallowMount(BillingManagement, mountData);
};

describe('BillingManagement.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render properly', () => {
      expect(wrapper.find('.billing-management').exists()).toBeTruthy();
    });

    it('should call EventBus.$on after mounted', () => {
      const eventBusOnSpy = jest.spyOn(global.EventBus, '$on');

      wrapper.destroy();
      wrapper = mount();

      expect(eventBusOnSpy).toBeCalled();
      eventBusOnSpy.mockRestore();
    });
  });

  describe('should render computed value properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should compute isMobile value', () => {
      expect(wrapper.vm.isMobile).toBe(wrapper.vm.$device.isMobile);
    });

    it('should compute page value', () => {
      expect(wrapper.vm.page).toBe('unpaid');
    });

    it('should compute sortBy value', () => {
      expect(wrapper.vm.sortBy).toBe('Tanggal Jatuh Tempo');
    });

    it('should compute pageNumber value', () => {
      expect(wrapper.vm.pageNumber).toBe(1);
    });
  });

  describe('should call functions properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should return proper text when calling sortByText method', () => {
      expect(wrapper.vm.sortByText('scheduled_date')).toBe('Tanggal Jatuh Tempo');
    });

    it('should set isFilterListModalVisible value as true when clicking on due date link', async () => {
      const dueDateLink = wrapper.find('.billing-management__button__due-date');
      if (dueDateLink && dueDateLink.exists()) {
        await dueDateLink.trigger('click');

        expect(wrapper.vm.isFilterListModalVisible).toBeTruthy();
      }
    });
  });
});
