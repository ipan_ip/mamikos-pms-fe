export default (type) => {
  const rent = {
    all: 'Hitungan Sewa',
    day: 'Per Hari',
    week: 'Per Minggu',
    month: 'Per Bulan',
    '3_month': 'Per 3 Bulan',
    '6_month': 'Per 6 Bulan',
    year: 'Per Tahun',
  };
  return rent[type];
};
