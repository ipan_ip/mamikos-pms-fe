const emptyStateBasedOnPage = {
  unpaid: {
    title: 'Tidak Ada yang Belum Bayar',
    description: 'Semua tagihan sewa yang belum dibayar oleh penyewa akan muncul di halaman ini.',
  },
  paid: {
    title: 'Belum Ada Tagihan Terbayar',
    description:
      'Semua tagihan yang sudah dibayar oleh penyewa namun masih diproses Mamikos akan muncul di halaman ini.',
  },
  transferred: {
    title: 'Belum Ada yang Ditransfer',
    description: 'Semua tagihan yang sudah ditransfer oleh  Mamikos akan muncul di halaman ini',
  },
};

export default emptyStateBasedOnPage;
