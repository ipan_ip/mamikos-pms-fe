import _find from 'lodash/find';

export default {
  data() {
    return {
      isEmptyKos: false,
      isLoadingKosList: false,
    };
  },

  computed: {
    isEmptyBilling() {
      return this.isEmptyKos || this.kostList.length === 0;
    },
    kostList() {
      return this.$store.state.billingManagement.kostList;
    },
  },

  created() {
    this.updateSelectedKost({ songId: '', kostName: '' });
  },

  mounted() {
    this.initialPage();
  },

  methods: {
    resetKostData() {
      this.$store.commit('billingManagement/setKostList', []);
      this.updateSelectedKost({ songId: '', kostName: '' });
      this.$store.commit('billingManagement/setKostListPagination', {
        offset: 0,
        hasMore: true,
      });
    },
    async initialPage() {
      this.resetKostData();

      !this.$route.query.page && this.$router.replace('/billing-management?page=unpaid');

      const month = this.$route.query.month || this.$dayjs().format('MM');
      const year = this.$route.query.year || this.$dayjs().format('YYYY');
      const rentType = this.$route.query.rentType || 'all';

      this.$store.commit('billingManagement/updateFilterInitial', {
        month,
        year,
        rentType,
        status: 'unpaid',
        sortBy: 'scheduled_date',
        sortOrder: 'asc',
        tenantName: '',
      });

      await this.getOwnerRoom();
    },
    getBillingSummary(type) {
      this.$store.dispatch('billingManagement/getBillingSummaryPerStatus', type);
    },
    formatSelected(songId, kostName) {
      return {
        songId,
        kostName,
      };
    },
    updateSelectedKost({ songId, kostName }) {
      this.$store.commit(
        'billingManagement/updateSelectedKost',
        this.formatSelected(songId, kostName),
      );
    },
    getOwnerRoom() {
      this.isLoadingKosList = true;
      this.$api
        .getMamipayRooms({ booking_status: 'approve', offset: 0, limit: 200 })
        .then((response) => {
          const { status, data, has_more } = response.data;
          let kostList = [];

          if (status && data) {
            if (data.length) {
              kostList = data.map((item) => {
                return this.formatSelected(item.id, item.room_title);
              });
              const selectedKostSongId = this.$route.query.selectedKost;
              const selectedKostBasedSongId = selectedKostSongId
                ? _find(data, {
                    id: parseInt(selectedKostSongId),
                  })
                : null;
              const selectedKost = selectedKostBasedSongId || data[0];

              this.updateSelectedKost({
                songId: selectedKost.id,
                kostName: selectedKost.room_title,
              });

              this.$store.commit('billingManagement/setKostList', kostList);
              this.$store.commit('billingManagement/setKostListPagination', {
                offset: kostList.length,
                hasMore: has_more,
              });
              this.getBillingSummary('unpaid');
              this.getBillingSummary('paid');
              this.getBillingSummary('transferred');
            } else {
              this.$store.commit('billingManagement/setKostList', []);
              this.isEmptyKos = true;
            }
            this.isLoadingKosList = false;
          }
        });
    },
  },
};
