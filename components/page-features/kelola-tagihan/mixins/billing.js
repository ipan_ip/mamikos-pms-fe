export default {
  methods: {
    getBillingSummary(invoiceStatus) {
      this.$store.dispatch('billingManagement/getBillingSummaryPerStatus', invoiceStatus);
    },

    getAllBillingSummary() {
      this.$store.dispatch('billingManagement/clearBillingSummaryStatus');
      this.getBillingSummary('unpaid');
      this.getBillingSummary('paid');
      this.getBillingSummary('transferred');
    },

    getBillListInvoices({ invoiceStatus, pageNumber = 1 }) {
      this.$store.dispatch('billingManagement/getBillListInvoices', {
        invoiceStatus,
        pageNumber,
      });
    },
  },
};
