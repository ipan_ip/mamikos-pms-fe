import { shallowMount } from '@vue/test-utils';
import BenefitFromGp from '../BenefitFromGp';
import localVueWithBuefy from '~/utils/addBuefy';

const mocks = {
  $router: {
    push: jest.fn(),
  },
  $device: { isDesktop: true },
};

const mountComponent = () => {
  return shallowMount(BenefitFromGp, {
    localVue: localVueWithBuefy,
    mocks,
  });
};

describe('BenefitFromGp', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mountComponent();
  });

  describe('render componen benefitfromGP component', () => {
    it('should render back link with content Kembali', () => {
      const link = wrapper.find('.c-navigation');
      expect(link.text()).toBe('Kembali');
    });

    it('should render title BenefitFromGp correctly"', () => {
      expect(wrapper.find('.c-title__content')).toBeTruthy();
    });
  });

  describe('computed value in benefitFromGp component', () => {
    it('should return isDekstop when width of screen more than 1280', () => {
      expect(wrapper.vm.isDesktop).toBeTruthy();
    });

    it('should return illustration Type that corresponding with GP package status', () => {
      wrapper.vm.packageStatus = 'GoldPlus 1';
      expect(wrapper.vm.illustrationType).toBe('benefit-goldplus-1');
    });

    it('should return cashback that corresponding with GP package status', () => {
      wrapper.vm.packageStatus = 'GoldPlus 1';
      expect(wrapper.vm.cashbackPremium).toBe('59.000');
    });
  });

  describe('should execute method in BenefitFromGP', () => {
    it('should redirect to statisticPrice when back is cliked', () => {
      const backButton = wrapper.find('.c-navigation');
      backButton.trigger('click');
      expect(wrapper.vm.$router.push).toBeCalledWith({
        path: '/exit-gp-statistic/statistic-price',
      });
    });

    it('should redirect to offer extend GP when function when function called', () => {
      wrapper.vm.nextPage();
      expect(wrapper.vm.$router.push).toBeCalledWith({
        path: '/exit-gp-statistic/offer-extend-gp',
      });
    });
  });
});
