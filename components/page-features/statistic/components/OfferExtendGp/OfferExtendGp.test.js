import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import dayjs from 'dayjs';
import OfferExtendGp from '../OfferExtendGp';
import localVueWithBuefy from '~/utils/addBuefy';

const mocks = {
  $router: {
    push: jest.fn(),
  },
  $device: { isDesktop: true },
  $dayjs: dayjs,
  $tracker: {
    send: jest.fn(),
  },
};

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        user: {
          name: 'test Name',
          email: 'test@mail.com',
          phone_number: '083176408437',
        },

        isPremium: false,
      },
      getters: {
        user(state) {
          return state.user;
        },

        isPremium(state) {
          return state.isPremium;
        },
      },
    },
  },
};

localVueWithBuefy.use(Vuex);
const mounComponent = () => {
  return shallowMount(OfferExtendGp, {
    localVue: localVueWithBuefy,
    mocks,
    store: new Vuex.Store(storeData),
  });
};

describe('OfferExtendGP.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mounComponent();
    localStorage.setItem('packageStatus', 'GoldPlus 1');
  });

  describe('render component OfferExtendGP correctly', () => {
    it('should render back link with content "Kembali"', () => {
      const backLink = wrapper.find('.c-navigation');
      expect(backLink.text()).toBe('Kembali');
    });

    it('should render back link with content "Yuk, lanjut kembangkan bisnis kos Anda bersama GoldPlus"', () => {
      const title = wrapper.find('.c-title__content');
      expect(title.text()).toBe('Yuk, lanjut kembangkan bisnis kos Anda bersama GoldPlus');
    });
  });

  describe('computed value in OfferExtendGP component', () => {
    it('should return isDekstop when width of screen more than 1280', () => {
      expect(wrapper.vm.isDesktop).toBeTruthy();
    });

    window.localStorage.setItem('gpExpired', '2021-01-08');
    it('should return value with day month and year formated', () => {
      expect(wrapper.vm.expiredDate).toBe('08 Jan 2021');
    });
  });

  describe('execute method in OfferExtendGP component', () => {
    it('should goto benefit GP page when back link are clicked', () => {
      const backLink = wrapper.find('.c-navigation');
      backLink.trigger('click');
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-statistic/benefit-gp' });
    });

    it('should goto see other performance page when function called', () => {
      wrapper.vm.others();
      expect(wrapper.vm.$router.push).toBeCalledWith({
        path: '/exit-gp-statistic/see-other-performance',
      });
    });

    it('should goto invoice page  with id from localstorage', () => {
      wrapper.vm.invoiceId = 1;
      wrapper.vm.nextPage();
      expect(wrapper.vm.$router.push).toBeCalledWith({
        path: '/goldplus/payment/1',
      });
    });

    it('should send tracker "payment suggestion visited" with attribute already set in tracking spec', async () => {
      wrapper.vm.$device.isDesktop = false;
      wrapper.vm.$store.state.profile.user.name = null;
      wrapper.vm.sendVisitedTrackerEvent();
      await wrapper.vm.$nextTick(() => {
        expect(wrapper.vm.$device.isDesktop).toBeFalsy();
        expect(wrapper.vm.$tracker.send).toBeTruthy();
        expect(wrapper.vm.$store.state.profile.user.name).toBe(null);
      });
    });

    it('should send tracker "Bayar is click" with attribute already set in tracking spec', async () => {
      wrapper.vm.$device.isDesktop = false;
      wrapper.vm.$store.state.profile.user.name = null;
      wrapper.vm.sendCLickTrackerEvent();
      await wrapper.vm.$nextTick(() => {
        expect(wrapper.vm.$device.isDesktop).toBeFalsy();
        expect(wrapper.vm.$tracker.send).toBeTruthy();
        expect(wrapper.vm.$store.state.profile.user.name).toBe(null);
      });
    });
  });
});
