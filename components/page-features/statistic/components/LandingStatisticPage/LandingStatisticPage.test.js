import Vuex from 'vuex';
import dayjs from 'dayjs';
import { shallowMount } from '@vue/test-utils';
import LandingStatisticPage from '../LandingStatisticPage';
import localVueWithBuefy from '~/utils/addBuefy';

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        user: {
          name: 'test Name',
          email: 'test@mail.com',
          phone_number: '083176408437',
        },

        isPremium: false,
      },
      getters: {
        user(state) {
          return state.user;
        },

        isPremium(state) {
          return state.isPremium;
        },
      },
    },

    statistic: {
      namespaced: true,
      state: {
        statisticData: {},
        personalStatisticData: false,
        trackerRedirection: 'ownerDashboard',
      },

      getters: {
        redirectionHistory(state) {
          return state.trackerRedirection;
        },
      },

      mutations: {
        setPersonalStatisticData: jest.fn(),
        setStatisticData: jest.fn(),
        setPackageStatus: jest.fn(),
        setInvoiceId: jest.fn(),
        redirectionHistory: jest.fn(),
        setTrackerRedirection: jest.fn(),
      },
    },
  },
};

const mocks = {
  $dayjs: dayjs,
  $router: {
    push: jest.fn(),
  },
  $device: { isDesktop: true },
  $route: {
    query: {
      started_at: '2020-12-08',
      ended_at: '2021-01-08',
      gp_package: 'gp1',
      invoice_id: '2',
      expiry_time: '2021-01-18',
      has_statistic: '1',
    },
  },
  $api: {
    getStatisticData: jest.fn().mockResolvedValue({
      data: {
        status: true,
        meta: {
          response_code: 200,
          code: 200,
          severity: 'OK',
          message: 'test',
        },
        data: {
          id: 30665736,
          room_title: 'Kos test',
          price_title: {
            currency_symbol: 'Rp',
            price: 1000000,
            rent_type_unit: 'bulan',
          },
          data_price: {
            average: 9122610.075471697,
            min: 20000,
            max: 100000000,
          },
          gp_statistic: {
            favorite: {
              label: 'Peminat Kost',
              growth_type: 'up',
              value: '78%',
            },
          },
        },
      },
    }),
  },

  $tracker: {
    send: jest.fn(),
  },
};

localVueWithBuefy.use(Vuex);
const mountComponent = () => {
  return shallowMount(LandingStatisticPage, {
    localVue: localVueWithBuefy,
    mocks,
    store: new Vuex.Store(storeData),
  });
};

describe('LandingStatisticPage.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mountComponent();
  });

  describe('render component LandingStatisticPage', () => {
    it('should render back link with content Kembali', () => {
      const link = wrapper.find('.c-navigation');
      expect(link.text()).toBe('Kembali');
    });
    it('should render title with contet "test Name"', () => {
      const link = wrapper.find('.c-title__content');
      expect(link.text()).toBe('Halo, test Name!');
    });
  });

  describe('computed value in LandingStatisticPage Component', () => {
    it('should return isDekstop when width of screen more than 1280', () => {
      expect(wrapper.vm.isDesktop).toBeTruthy();
    });

    it('should return periode start GP in date format', () => {
      expect(wrapper.vm.periodeStartedAt).toBe('08 Dec 2020');
    });

    it('should return periode ended GP in date format', () => {
      expect(wrapper.vm.periodeEndedAt).toBe('08 Jan 2021');
    });

    it('should ownerRedirection return "owner dahsboard" when redirectionHistory in store is "ownerDasboard"', () => {
      expect(wrapper.vm.ownerRedirection).toBe('owner dashboard');
    });

    it('should ownerRedirection return "owner dahsboard" when redirectionHistory in store is "ownerDasboard"', () => {
      wrapper.vm.$store.state.statistic.trackerRedirection = 'notification';
      expect(wrapper.vm.ownerRedirection).toBe('notification');
    });
  });

  describe('execute method in LandingStatisticPage Component', () => {
    it('should return "GoldPlus 1" when param from scheme is gp1', () => {
      expect(wrapper.vm.checkPackageStatus).toBe('GoldPlus 1');
    });

    it('should return "GoldPlus 2" when param from scheme is gp2', () => {
      wrapper.vm.$route.query.gp_package = 'gp2';
      expect(wrapper.vm.checkPackageStatus).toBe('GoldPlus 2');
    });

    it('should get item from localStorage when package status is "alread set"', () => {
      wrapper.vm.$route.query.gp_package = 'already set';
      wrapper.vm.checkSchemeItem();
      expect(wrapper.vm.goldplusPackageStatus).toBe('GoldPlus 2');
    });

    it('should set load data status when data aleady set in store', () => {
      wrapper.vm.$store.state.statistic.personalStatisticData = true;
      wrapper.vm.getAllStatisticData();
      expect(wrapper.vm.isLoadStatisticData).toBeFalsy();
    });

    it('should get data to server when personal date in store are not be set ', () => {
      const getStatisticSpy = jest.spyOn(wrapper.vm.$api, 'getStatisticData');
      wrapper.vm.getAllStatisticData();
      expect(getStatisticSpy).toBeCalled();
      getStatisticSpy.mockRestore();
    });

    it('should set netx path to statistic price when statisticData is null', () => {
      wrapper.vm.checkStatistic('null');
      expect(wrapper.vm.nextPagePath).toBe('/exit-gp-statistic/statistic-price');
    });

    it('should redirect page to dashboard owner when function called', () => {
      const backLink = wrapper.find('.c-navigation');
      backLink.trigger('click');
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/' });
    });

    it('should redirect nextpage path when function are called', () => {
      wrapper.vm.checkStatistic('test');
      wrapper.vm.nextPage();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-statistic/review' });
    });

    it('should send tracking data with attribute that already set before', async () => {
      wrapper.vm.$device.isDesktop = false;
      await wrapper.vm.$nextTick(() => {
        wrapper.vm.sendTrackerEvent();
        expect(wrapper.vm.isDesktop).toBeFalsy();
        expect(wrapper.vm.$tracker.send).toBeTruthy();
      });
    });

    it('should send tracking data with  name is empty string and attribute that already set before', async () => {
      window.innerWidth = 1440;
      wrapper.vm.$store.state.profile.user.name = null;
      await wrapper.vm.$nextTick(() => {
        wrapper.vm.sendTrackerEvent();
        expect(wrapper.vm.user.name).toBe(null);
        expect(wrapper.vm.$tracker.send).toBeTruthy();
      });
    });
  });
});
