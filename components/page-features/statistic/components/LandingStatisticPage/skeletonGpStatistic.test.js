import { shallowMount } from '@vue/test-utils';
import skeletonGpStatistic from '../LandingStatisticPage/skeletonGpStatistic.vue';
import localVueWithBuefy from '~/utils/addBuefy';

const mountComponent = () => {
  return shallowMount(skeletonGpStatistic, {
    localVue: localVueWithBuefy,
  });
};

describe('skeletonGpStatistic.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mountComponent();
  });

  describe('render component skeletonGpStatistic', () => {
    it('should render skeleton properly', () => {
      expect(wrapper.find('stories-skeleton'));
    });
  });
});
