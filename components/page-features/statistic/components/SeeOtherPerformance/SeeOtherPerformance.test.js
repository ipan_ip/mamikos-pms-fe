import { shallowMount } from '@vue/test-utils';
import SeeOtherPerformance from '../SeeOtherPerformance';
import localVueWithBuefy from '~/utils/addBuefy';

const mocks = {
  $router: {
    push: jest.fn(),
  },
  $device: { isDesktop: true },
};

const mountComponent = () => {
  return shallowMount(SeeOtherPerformance, {
    localVue: localVueWithBuefy,
    mocks,
  });
};

describe('SeeOtherPerformance.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mountComponent();
  });

  describe('render component SeeOtherPerformance.vue', () => {
    it('should render back link with content "Ke Halaman Utama"', () => {
      const link = wrapper.find('.c-navigation');
      expect(link.text()).toBe('Ke Halaman Utama');
    });

    it('should render back title with content "Lihat performa kos lainnya di dashboard personal Anda"', () => {
      const title = wrapper.find('.c-title__content');
      expect(title.text()).toBe('Lihat performa kos lainnya di dashboard personal Anda');
    });
  });

  describe('execute Method SeeOtherPerformance components', () => {
    it('should redirect to dasboard page when "ke Halaman Utama" are clicked', () => {
      const backLink = wrapper.find('.c-navigation');
      backLink.trigger('click');
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/' });
    });

    it('should redirect to Offers Extend GP page when function to be called ', () => {
      wrapper.vm.prevPage();
      expect(wrapper.vm.$router.push).toBeCalledWith({
        path: '/exit-gp-statistic/offer-extend-gp',
      });
    });

    it('should redirect to GoldPlus page when function to be called ', () => {
      wrapper.vm.nextPage();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/goldplus' });
    });
  });
});
