import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import StatisticMenu from '../StatisticMenu';

const localVue = createLocalVue();
localVue.use(Vuex);

const $device = {
  isMobile: true,
  isDesktop: false,
};

const mockStore = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        user: { user_id: 10 },
        isNewOwner: false,
        isGPOwner: false,
        data: {
          kost_total: 6,
        },
      },
      getters: {
        user(state, getters) {
          return state.user;
        },
        isNewOwner(state, getters) {
          return state.isNewOwner;
        },
      },
    },
  },
};

const $router = { push: jest.fn() };

const $tracker = { send: jest.fn() };

const mountData = {
  localVue,
  store: new Vuex.Store(mockStore),
  mocks: {
    $device,
    $router,
    $tracker,
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(StatisticMenu, finalData);
};

describe('StatisticMenu.vue', () => {
  const wrapper = mount();

  it('should render statistic menu properly', () => {
    expect(wrapper.find('.statistic-menu').exists()).toBe(true);
    expect(wrapper.find('.statistic-menu__items').exists()).toBe(false);
  });

  it('should show empty state when user has no property', async () => {
    wrapper.vm.$store.state.profile.isNewOwner = true;
    await wrapper.vm.$nextTick();

    expect(wrapper.find('.statistic-menu__empty-state').exists()).toBe(true);
    expect(wrapper.find('.statistic-menu__items').exists()).toBe(false);
  });

  it('should handle goldplus statistic on click properly', () => {
    wrapper.vm.handleGoldPlusStatisticClicked();

    expect(wrapper.vm.$router.push).toBeCalled();
  });
});
