import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import InfoKosStatistic from '../InfoKosStatistic';
import statisticMixin from '../../mixins/statisticMixin';
import localVueWithBuefy from '~/utils/addBuefy';

const storeData = {
  modules: {
    statistic: {
      namespaced: true,
      state: {
        statisticData: {},
        personalStatisticData: false,
        currentStatisticPage: 'firstPge',
      },
      mutations: {
        setHistoryStatisticPage: jest.fn(),
        setPersonalStatisticData: jest.fn(),
        setStatisticData: jest.fn(),
        setPackageStatus: jest.fn(),
        setInvoiceId: jest.fn(),
      },
    },
  },
};

const mocks = {
  $router: {
    push: jest.fn(),
  },

  $device: { isDesktop: true },
  $api: {
    getStatisticData: jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: {
          id: 30665736,
          room_title: 'Kos test',
          price_title: {
            currency_symbol: 'Rp',
            price: 1000000,
            rent_type_unit: 'bulan',
          },
          data_price: {
            average: 9122610.075471697,
            min: 20000,
            max: 100000000,
          },
          gp_statistic: {
            favorite: {
              label: 'Peminat Kost',
              growth_type: 'up',
              value: '78%',
            },
          },
        },
      },
    }),
  },
};

localVueWithBuefy.use(Vuex);

const mountComponent = () => {
  return shallowMount(InfoKosStatistic, {
    localVue: localVueWithBuefy,
    mocks,
    mixins: [statisticMixin],
    store: new Vuex.Store(storeData),
  });
};

describe('InfoKosStatistic.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mountComponent();
  });

  describe('should render component InfoKosStatistic', () => {
    it('should render back link with content Kembali', () => {
      const link = wrapper.find('.c-navigation');
      expect(link.text()).toBe('Kembali');
    });
  });

  describe('compted value in InfoKosStatistic Component', () => {
    it('should return empty string when data not ready on store', () => {
      wrapper.vm.$store.state.statistic.statisticData = null;
      expect(wrapper.vm.storeStatistic).toBe('');
    });

    it('should return isDekstop when width of screen more than 1280', () => {
      expect(wrapper.vm.isDesktop).toBeTruthy();
    });

    it('should return data from store and title for list data at firstlist section when data not full complitely', () => {
      expect(wrapper.vm.firstPage).toBeTruthy();
    });

    it('should return data from store and title for list data at firstlist section', () => {
      wrapper.vm.$store.state.statistic.statisticData = {
        unique_visit: {
          label: 'test',
        },
        visit: {
          label: 'test',
        },
      };
      expect(wrapper.vm.firstPage).toBeTruthy();
    });

    it('should return data from store and title for list data at secondList section', () => {
      wrapper.vm.$store.state.statistic.statisticData = {
        chat: {
          label: 'test',
        },
        favorite: {
          label: 'test',
        },
      };
      expect(wrapper.vm.secondPage).toBeTruthy();
    });

    it('should return "not complite" when visit and unique_visit are return from server ', () => {
      expect(wrapper.vm.isDataFirstPageComplite).toBe('notComplite');
    });

    it('should return "complite" when chat and favorite are return from server ', () => {
      expect(wrapper.vm.isDataSecondPageComplite).toBe('complite');
    });

    it('should return "not complite" when chat and favorite are return from server ', () => {
      wrapper.vm.$store.state.statistic.statisticData = {
        favorite: {
          label: 'test',
        },
      };
      expect(wrapper.vm.isDataSecondPageComplite).toBe('notComplite');
    });
  });

  describe('execute method in InfoKosStatistic Compnent', () => {
    it('should set current page base on statistic data that complite will be render firstpage', () => {
      wrapper.vm.$store.state.statistic.statisticData = { test: 'test' };
      wrapper.vm.isDataReadyOnStore();
      expect(wrapper.vm.currentSectionStatus).toBe('firstPge');
    });

    it('should set previews page to statistic base on current section status', () => {
      wrapper.vm.currentSectionStatus = 'firstPage';
      wrapper.vm.routePrevComplitePage();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-statistic' });
    });

    it('should set previews page base on current section status', () => {
      wrapper.vm.currentSectionStatus = 'secondPage';
      wrapper.vm.routePrevComplitePage();
      expect(wrapper.vm.currentSectionStatus).toBe('firstPage');
    });

    it('should set next page to statistic base on current section status', () => {
      wrapper.vm.currentSectionStatus = 'firstPage';
      wrapper.vm.routeNextComplitePage();
      expect(wrapper.vm.$router.push).toBeCalledWith({
        path: '/exit-gp-statistic',
      });
    });

    it('should set next page base on current section status', () => {
      wrapper.vm.currentSectionStatus = 'secondPage';
      wrapper.vm.routeNextComplitePage();
      expect(wrapper.vm.currentSectionStatus).toBe('secondPage');
    });

    it('should redirect previews page to firstpage when page status is "complite"', () => {
      const routerSPY = jest.spyOn(wrapper.vm, 'routePrevComplitePage');
      wrapper.vm.prevPage('complite', 'complite');
      expect(routerSPY).toBeCalled();
      routerSPY.mockRestore();
    });

    it('should redirect previews page to statistic w when page status is "not complite"', () => {
      wrapper.vm.prevPage('complite', 'notComplite');
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-statistic' });
    });

    it('should redirect next page to firstpage when page status is "complite"', () => {
      const routerSPY = jest.spyOn(wrapper.vm, 'routeNextComplitePage');
      wrapper.vm.nextPage('complite', 'complite');
      expect(routerSPY).toBeCalled();
      routerSPY.mockRestore();
    });

    it('should redirect next page to statistic w when page status is "not complite"', () => {
      wrapper.vm.nextPage('complite', 'notComplite');
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-statistic' });
    });
  });
});
