import { shallowMount } from '@vue/test-utils';
import StatisticData from '../InfoKosStatistic/StatisticData.vue';
import localVueWithBuefy from '~/utils/addBuefy';

const propsData = {
  data: {
    firstList: {
      title: 'Kunjungan Iklan Kos',
      info: 'Kunjungan Iklan adalah berapa kali iklan kos Anda dilihat oleh para pencari kos.',
      type: true,
      value: true,
    },
    secondList: {
      title: 'Pengunjung Iklan Kos',
      info: 'Pengunjung iklan adalah pencari kos yang melihat iklan Anda.',
      type: true,
      value: true,
    },
  },
};

const mocks = { currentSectionStatus: 'firstPage' };
const mountComponent = () => {
  return shallowMount(StatisticData, {
    localVue: localVueWithBuefy,
    propsData,
    mocks,
  });
};
describe('StatisticData.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mountComponent();
  });

  describe('render component in StatisticData', () => {
    it('should render first chat title with content', () => {
      const title = wrapper.find('.l-statistic__title');
      expect(title.text()).toBe('Kunjungan Iklan Kos');
    });
  });

  describe('execute method in StatisticData', () => {
    it('should return "Naik"  when status is "up"', () => {
      expect(wrapper.vm.statisticGrowthType('up')).toBe('Naik');
    });
    it('should return "Turun"  when status is "down"', () => {
      expect(wrapper.vm.statisticGrowthType('down')).toBe('Turun');
    });
  });
});
