import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import StatisticPrice from '../StatisticPrice';
import localVueWithBuefy from '~/utils/addBuefy';

const mocks = {
  $router: { push: jest.fn() },
  $device: { isDesktop: true },
  toThousands: jest.fn(),
  $api: {
    getStatisticData: jest.fn().mockResolvedValue({
      data: {
        data: {
          status: true,
          gp_statistic: {
            favorite: {
              label: 'test',
            },
          },
        },
      },
    }),
  },
};

const storeData = {
  modules: {
    statistic: {
      namespaced: true,
      state: {
        statisticData: {},
        personalStatisticData: {},
      },
      mutations: {
        setPersonalStatisticData: jest.fn(),
        setStatisticData: jest.fn(),
        setPackageStatus: jest.fn(),
        setInvoiceId: jest.fn(),
      },

      getters: {
        storeTitlePrice: jest.fn(() => {
          return {
            currency_symbol: 'Rp',
            price: 10000,
            rent_type_unit: 'bulan',
          };
        }),
        storePriceData: jest.fn(() => {
          return {
            average: 9,
            min: 2,
            max: 4,
          };
        }),
      },
    },
  },
};

localVueWithBuefy.use(Vuex);
const mountComponent = () => {
  return shallowMount(StatisticPrice, {
    localVue: localVueWithBuefy,
    mocks,
    store: new Vuex.Store(storeData),
  });
};

describe('StatisticPrice.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mountComponent();
  });

  describe('render componen StatisticPrice.vue', () => {
    it('should render back button with content "Kembali"', () => {
      const backButton = wrapper.find('.c-navigation');
      expect(backButton.text()).toBe('Kembali');
    });

    it('should render back button with content "Cari tahu kos sekitar Anda dengan statistik lebih baik"', () => {
      const backButton = wrapper.find('.l-title__content');
      expect(backButton.text()).toBe('Cari tahu kos sekitar Anda dengan statistik lebih baik');
    });
  });

  describe('computed value in StatisticPrice component', () => {
    it('should return room price in thousand format', () => {
      expect(wrapper.vm.roomPrice).toBe('10.000');
    });

    it('should return rent type "bulan"from store ', () => {
      expect(wrapper.vm.rentTypeUnit).toBe('bulan');
    });

    it('should return average price from store ', () => {
      expect(wrapper.vm.averageUnitPrice).toBe('9');
    });

    it('should return top unit price from store ', () => {
      expect(wrapper.vm.topUnitPrice).toBe('4');
    });

    it('should return low unit price from store ', () => {
      expect(wrapper.vm.lowUnitPrice).toBe('2');
    });

    it('should return "true" when data ready set in store ', () => {
      expect(wrapper.vm.isDataStatisticReady).toBeTruthy();
    });
  });

  describe('execute method in StatisticProce Component', () => {
    it('should call data from server when data on store is not ready', () => {
      wrapper.vm.$store.state.statistic.personalStatisticData = false;
      const getApiSpy = jest.spyOn(wrapper.vm, 'getAllStatisticData');
      wrapper.vm.isDataReadyOnStore();
      expect(getApiSpy).toBeCalled();
      getApiSpy.mockRestore();
    });

    it('should check statistic and set previews path to GP statistic when status is "null"', () => {
      wrapper.vm.checkStatisticData('null');
      expect(wrapper.vm.prevPagePath).toBe('/exit-gp-statistic');
    });

    it('should redirect to previews page that url get from "prevPagePath"', () => {
      const backLink = wrapper.find('.c-navigation');
      backLink.trigger('click');
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-statistic/review' });
    });

    it('should redirect to BenefitGP page when function to be called ', () => {
      wrapper.vm.nextPage();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-statistic/benefit-gp' });
    });
  });
});
