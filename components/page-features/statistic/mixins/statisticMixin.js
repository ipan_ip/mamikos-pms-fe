import { mapMutations } from 'vuex';
export default {
  methods: {
    ...mapMutations('statistic', [
      'setPersonalStatisticData',
      'setPackageStatus',
      'setStatisticData',
    ]),
    getAllStatisticData() {
      this.$api.getStatisticData().then((response) => {
        this.setStatisticData(response.data.data.gp_statistic);
        this.setPersonalStatisticData(response.data.data);
      });
    },

    saveItemFromScheme(name, value) {
      localStorage.setItem(name, value);
    },
  },
};
