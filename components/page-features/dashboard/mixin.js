import { mapState, mapGetters } from 'vuex';
import ModalUpdateProperty from '~/components/page-features/dashboard/components/ModalUpdateProperty';
import ModalPremiumTrial from '~/components/page-features/dashboard/components/ModalPremiumTrial';
import EventBus from '~/components/event-bus/index.js';

export default {
  components: {
    ModalUpdateProperty,
    ModalPremiumTrial,
  },

  data() {
    return {
      displayModalPremiumTrial: false,
    };
  },

  computed: {
    ...mapState('dashboard', {
      bookingDashboardData: (state) => state.dashboardData,
      isLoadingBookingDashboardData: (state) => state.isLoading,
    }),
    ...mapGetters('profile', ['showModalPremiumTrial']),
    isShowWaitingWidget() {
      return (
        !this.isLoadingBookingDashboardData &&
        this.bookingDashboardData &&
        (this.bookingDashboardData.total_off_booking_pending > 0 ||
          this.bookingDashboardData.total_off_unpaid_invoice > 0 ||
          this.bookingDashboardData.total_off_contract_submission > 0)
      );
    },
    showUpdatePropertyModal: {
      get() {
        const { profile } = this.$store.state;
        return profile.data.activity_popover;
      },
      set() {
        // do nothing
      },
    },
  },

  created() {
    this.checkAddContractVisibility();
    this.fetchBookingDashboardData();
    if (this.showModalPremiumTrial) {
      this.toggleModalPremiumTrial(true);
    } else {
      this.isBbk && EventBus.$emit('onContractExists');
    }
  },

  methods: {
    checkAddContractVisibility() {
      this.$store.dispatch('contract/getContractAbTest');
    },
    fetchBookingDashboardData() {
      this.$store.dispatch('dashboard/getBookingDashboardData');
    },
    toggleModalPremiumTrial(isActive) {
      this.displayModalPremiumTrial = isActive;
    },
  },
};
