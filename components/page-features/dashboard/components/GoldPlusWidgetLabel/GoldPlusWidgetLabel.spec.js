import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusWidgetLabel from '../GoldPlusWidgetLabel';
import localVueWithBuefy from '~/utils/addBuefy';
import MkNumberCounter from '~/components/global/atoms/MkNumberCounter';

global.window = Object.create(window);

const $device = {
  isMobile: true,
  isDesktop: false,
};

const $tracker = { send: jest.fn() };

const stubs = {
  GoldPlusFeatureCard: MkNumberCounter,
};

const $router = {
  push: jest.fn(),
};

const propsData = {
  labelData: {
    label: 'Sedang di-review',
    value: 0,
    button_visible: false,
  },
};

localVueWithBuefy.use(Vuex);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus',
    },
    $device,
    $router,
    $tracker,
  },
  stubs,
  propsData,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusWidgetLabel, finalData);
};

describe('GoldPlusWidgetLabel.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-widget-label').exists()).toBeTruthy();
  });
});
