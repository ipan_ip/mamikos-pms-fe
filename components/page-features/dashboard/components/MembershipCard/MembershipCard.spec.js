import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';

import MembershipCard from './MembershipCard';
import MkActionCard from '~/components/global/molecules/MkActionCard';

jest.useFakeTimers();
jest.advanceTimersByTime(300);
const localVue = createLocalVue();
localVue.use(Vuex);
const stubs = {
  MkActionCard,
};

const mockStore = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        user: { user_id: 10 },
        data: {
          is_booking_all_room: false,
          is_mamipay_user: false,
          expired: false,
          status: 'Premium',
          total_mamipoin: 1000,
        },
      },
      getters: {
        user(state, getters) {
          return state.user;
        },
        isBbk(state, getters) {
          return getters.membership.is_mamipay_user && state.data.is_booking_all_room;
        },
        membership: () => {
          return {
            views: 10000,
          };
        },
        isPremium(state, getters) {
          return !getters.membership.expired && !!(getters.membership.status === 'Premium');
        },
      },
    },
  },
};

const $api = {
  getGoldPlusSubmissionStatus: jest.fn().mockResolvedValue({
    data: {
      status: true,
      gp_status: {
        key: 'review',
        label: 'sedang direview',
        invoice_id: '1234',
      },
    },
  }),
};

const $bugsnag = {
  notify: jest.fn(),
};

const $alert = jest.fn();

const $device = {
  isMobile: true,
  isDesktop: false,
};

const $router = { push: jest.fn() };

const $tracker = { send: jest.fn() };
const mountData = {
  localVue,
  store: new Vuex.Store(mockStore),
  mocks: {
    stubs,
    $api,
    $bugsnag,
    $alert,
    $device,
    $router,
    $tracker,
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(MembershipCard, finalData);
};

describe('MembershipCard.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount component properly', () => {
    expect(wrapper.find('.membership-card').exists()).toBeTruthy();
  });

  describe('Test the Methods', () => {
    const testGPStatusCases = [
      {
        key: 'review',
        label: 'sedang direview',
        invoice_id: '1234',
      },
      {
        key: 'waiting',
        label: 'menunggu pembayaran',
        invoice_id: '1234',
      },
      {
        key: 'register',
        label: 'daftar goldplus',
        invoice_id: null,
      },
      {
        key: 'nonGP',
        label: null,
        invoice_id: null,
      },
      {
        key: 'gp2',
        label: 'GoldPlus 2',
        invoice_id: null,
      },
    ];

    it('should show/hide the Red Dot', () => {
      testGPStatusCases.forEach((tc) => {
        wrapper.setData({
          gp_status: {
            key: tc.key,
            label: tc.label,
            invoice_id: tc.invoice_id,
          },
        });

        tc.key === 'waiting' || tc.key === 'review'
          ? expect(wrapper.vm.showRedDot).toBeTruthy()
          : expect(wrapper.vm.showRedDot).toBeFalsy();
      });
    });

    it('should redirect user depend on gp_status key', () => {
      testGPStatusCases.forEach((tc) => {
        wrapper.setData({
          gp_status: {
            key: tc.key,
            label: tc.label,
            invoice_id: tc.invoice_id,
          },
        });

        wrapper.vm.handleGoldPlusCardClicked();

        switch (tc.key) {
          case 'review':
            expect(wrapper.vm.$router.push).toBeCalledWith({
              name: 'goldplus-list',
              params: { filter_key: 2 },
            });
            break;
          case 'waiting':
            expect(wrapper.vm.$router.push).toBeCalledWith('/goldplus/payment/1234');
            break;
          case 'register':
            expect(wrapper.vm.$router.push).toBeCalledWith({
              name: 'goldplus-submission',
              params: { previousPage: 'Dashboard' },
            });
            break;
          default:
            expect(wrapper.vm.$router.push).toBeCalledWith({
              name: 'goldplus',
            });
        }
      });
    });

    it('should send tracker data function are called', () => {
      wrapper.vm.trackMamiPointClicked();
      expect(wrapper.vm.$tracker.send).toBeCalled();
    });

    it('should send tracker data function are called with data interface desktop when sceeen  more than 768', () => {
      window.innerWidth = 1440;
      wrapper.vm.$nextTick();
      wrapper.vm.$device.isMobile = false;
      wrapper.vm.trackMamiPointClicked();
      expect(wrapper.vm.$device.isMobile).toBeFalsy();
    });

    it('should send tracker goldplus  with data interface desktop when sceeen  more than 768', () => {
      window.innerWidth = 1440;
      wrapper.vm.$nextTick();
      wrapper.vm.$device.isMobile = false;
      wrapper.vm.trackEntryPointClicked();
      expect(wrapper.vm.$device.isMobile).toBeFalsy();
    });

    it('should send tracker goldplus  with data interface desktop when sceeen  more than 768 and key is "register"', () => {
      window.innerWidth = 1440;
      wrapper.vm.$nextTick();
      wrapper.vm.$device.isMobile = false;
      wrapper.vm.trackEntryPointClicked('register');
      expect(wrapper.vm.$device.isMobile).toBeFalsy();
    });

    it('should redirect to mamipoin page and triggered tracker event when function are called', () => {
      wrapper.vm.gotoMamipoinPage();
      const trackerSpy = jest.spyOn(wrapper.vm, 'trackMamiPointClicked');
      wrapper.vm.gotoMamipoinPage();
      expect(trackerSpy).toBeCalled();
      trackerSpy.mockRestore();
    });

    it('should return total point user in thousand format when poin more than 0', () => {
      wrapper.vm.$store.state.profile.data.total_mamipoin = 1000;
      wrapper.vm.$nextTick();
      expect(wrapper.vm.totalMamipoin).toBe('1.000');
    });

    it('should return title of mamipoin card is "Tukar Poin" when poin more than 0', () => {
      wrapper.vm.$store.state.profile.data.total_mamipoin = 1000;
      wrapper.vm.$nextTick();
      expect(wrapper.vm.titleMamipoin).toBe('Tukar Poin');
    });

    it('should return "true" when digit of point no more than 11', () => {
      expect(wrapper.vm.isLowPoint).toBeTruthy();
    });

    it('should return total point user from 0 when total is null or 0', () => {
      wrapper.vm.$store.state.profile.data.total_mamipoin = false;
      expect(wrapper.vm.totalMamipoin).toBe('0');
    });

    it('should return title mamipoin card "Pelajari" when total is null or 0', () => {
      wrapper.vm.$store.state.profile.data.total_mamipoin = false;
      expect(wrapper.vm.titleMamipoin).toBe('Pelajari');
    });

    it('should return GP1 status when return from server is gp1', () => {
      wrapper.vm.gp_status.key = 'gp1';
      expect(wrapper.vm.userGpPackage).toBe('gp1');
    });

    it('should run animation when fist time visit page dashboard', async () => {
      await wrapper.vm.handleAnimationMamipoin();
      expect(wrapper.vm.isFlipContent).toBeFalsy();
      jest.runTimersToTime(10000);
    });
  });
});
