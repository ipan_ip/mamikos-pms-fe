import { shallowMount } from '@vue/test-utils';
import ModalPremiumTrial from './ModalPremiumTrial';
import localVueWithBuefy from '~/utils/addBuefy';

import MkModal, { mixin as modalMixin } from '~/components/global/molecules/MkModal/MkModal';
import MkButton from '~/components/global/atoms/MkButton';

Object.defineProperty(window.location, 'reload', {
  configurable: true,
});
window.location.reload = jest.fn();

const propsData = {
  active: true,
};

const $api = {
  postPremiumTrial: jest
    .fn()
    .mockResolvedValueOnce({ data: { status: true } })
    .mockResolvedValueOnce({ data: { status: false, message: 'error' } })
    .mockRejectedValue(),
};

const $alert = jest.fn((messages) => messages);

const mount = () => {
  return shallowMount(ModalPremiumTrial, {
    localVue: localVueWithBuefy,
    mixins: [modalMixin],
    propsData,
    mocks: {
      $api,
      $alert,
      toggleModal: jest.fn(),
    },
    stubs: {
      MkModal,
      MkButton,
    },
  });
};

describe('ModalPremiumTrial', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.premium-trial-modal').exists()).toBeTruthy();
  });

  it('should submit trials', async () => {
    wrapper.find('.premium-trial-modal__ok-button').trigger('click');
    await wrapper.vm.$api.postPremiumTrial;
    await Promise.resolve;
    expect(wrapper.vm.isSendingData).toBeTruthy();

    // return false status
    wrapper.find('.premium-trial-modal__ok-button').trigger('click');
    await wrapper.vm.$api.postPremiumTrial;
    await Promise.resolve;
    await wrapper.vm.$alert;
    expect(wrapper.vm.isShow).toBeFalsy();
    expect(wrapper.vm.$alert).toBeCalled();
  });

  it('should close the modal properly', () => {
    wrapper.find('.premium-trial-modal__close').trigger('click');
    expect(wrapper.vm.isShow).toBeFalsy();
  });
});
