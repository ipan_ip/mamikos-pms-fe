import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import GreetingSection from '../GreetingSection';
import localVueWithBuefy from '~/utils/addBuefy';

global.window = Object.create(window);
const url = 'mamikos.com';

Object.defineProperty(window, 'location', {
  value: { href: url },
});

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        data: {
          gp_status: '',
          is_mamirooms: '',
        },
      },
      getters: {
        user: () => {
          return {
            name: '',
            phone_number: '',
            email: '',
          };
        },
        isBbk: () => jest.fn(),
        isPremium: () => jest.fn(),
      },
    },
  },
};

const store = new Vuex.Store(storeData);
const $tracker = { send: jest.fn() };
const $device = { isMobile: false };

const mountData = {
  localVue: localVueWithBuefy,
  propsData: {
    username: 'Jones',
  },
  store,
  mocks: { $tracker, $device },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GreetingSection, finalData);
};

describe('GreetingSection.vue', () => {
  let wrapper;

  process.env.MAMIKOS_URL = 'mamikos.com';

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.greeting-section').exists()).toBeTruthy();
  });

  it('Should redirect users to Owner Settings page properly', () => {
    wrapper.vm.toOwnerSettings();
    expect(window.location.href).toBe('mamikos.com/ownerpage/settings');
  });
});
