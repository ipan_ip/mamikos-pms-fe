import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import ManageKosCard from '../ManageKosCard';
import mockWindowProperty from '~/utils/mockWindowProperty';

const localVue = createLocalVue();
localVue.use(Vuex);
mockWindowProperty('window.location', { href: '' });
mockWindowProperty('Cookies', {
  get: jest.fn(),
});

const stubs = {
  MkActionCard: { template: `<div @click="$emit('click')"></div>` },
};

describe('ManageKosCard.vue', () => {
  const mockStore = {
    modules: {
      profile: {
        namespaced: true,
        state: { user: { kost_total: 50 } },
        getters: {
          user(state, getters) {
            return state.user;
          },
          userHasNoProperty(state, getters) {
            return getters.user.kost_total === 0;
          },
          isBbk() {
            return true;
          },
          isPremium() {
            return true;
          },
          activeKos(state, getters) {
            return getters.user.kost_total_active;
          },
        },
      },
    },
  };
  const $router = { push: jest.fn() };
  const $tracker = { send: jest.fn() };
  const $device = { isMobile: false };

  const $api = {
    getBbkStatus: jest.fn().mockResolvedValue({
      data: { status: true, bbk_status: { approve: 1, waiting: 2, reject: 1, other: 1 } },
    }),
  };

  const wrapper = shallowMount(ManageKosCard, {
    localVue,
    stubs,
    store: new Vuex.Store(mockStore),
    mocks: { $router, $tracker, $device, $api },
  });

  it('should getBbkStatus', async () => {
    await wrapper.vm.$api.getBbkStatus;
    await Promise.prototype.then;
    expect(wrapper.vm.bbkStatus).toBeTruthy();
  });

  it('should change the route to add tenant page', () => {
    wrapper
      .findAll(stubs.MkActionCard)
      .at(4)
      .trigger('click');
    expect($router.push).toBeCalledWith('/add-tenant/on-boarding');
  });

  it('should redirect page to help page', () => {
    wrapper
      .findAll(stubs.MkActionCard)
      .at(5)
      .trigger('click');
    expect(window.location.href).toBe('https://help.mamikos.com/pemilik');
  });
});
