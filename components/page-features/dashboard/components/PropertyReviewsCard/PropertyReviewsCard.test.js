import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import PropertyReviewsCard from '../PropertyReviewsCard';

const localVue = createLocalVue();
localVue.use(Vuex);

const $router = { push: jest.fn() };
const $tracker = { send: jest.fn() };
const $device = { isMobile: false };

const mockStore = {
  modules: {
    review: {
      namespaced: true,
      state: {
        hasMore: true,
      },
    },
    profile: {
      namespaced: true,
      state: {
        data: {
          gp_status: '',
          is_mamirooms: '',
        },
      },
      getters: {
        user: () => {
          return {
            name: '',
            phone_number: '',
            email: '',
          };
        },
        isBbk: () => jest.fn(),
        isPremium: () => jest.fn(),
      },
    },
  },
};

describe('PropertyReviewsCard.vue', () => {
  const wrapper = shallowMount(PropertyReviewsCard, {
    localVue,
    store: new Vuex.Store(mockStore),
    mocks: { $router, $tracker, $device },
  });

  it('Should mount properly', () => {
    expect(wrapper.find('.property-reviews-card').exists()).toBeTruthy();
  });

  it('Should redirect users to all reviews page properly', () => {
    wrapper.vm.goToAllReviews();
    expect($router.push).toBeCalledWith({ name: 'kos-reviews' });
  });
});
