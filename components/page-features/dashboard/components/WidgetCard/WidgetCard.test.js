import { shallowMount, createLocalVue } from '@vue/test-utils';
import WidgetCard from './WidgetCard';

const localVue = createLocalVue();

const mocks = {
  $device: {
    isMobile: false,
  },
};

describe('WidgetCard.vue', () => {
  const wrapper = shallowMount(WidgetCard, { localVue, mocks });

  it('should render widget card', () => {
    expect(wrapper.find('.widget-card').exists()).toBe(true);
  });

  it('should render empty card if no props given', () => {
    expect(wrapper.find('.widget-card__title').exists()).toBe(false);
    expect(wrapper.find('.widget-card__description').exists()).toBe(false);
  });

  it('should render data from props properly', async () => {
    const propsData = {
      title: 'Widget Card',
      description: 'this is widget card',
    };

    await wrapper.setProps({
      ...propsData,
    });

    expect(wrapper.find('.widget-card__title').exists()).toBe(true);
    expect(wrapper.find('.widget-card__title').text()).toBe(propsData.title);
    expect(wrapper.find('.widget-card__description').exists()).toBe(true);
    expect(wrapper.find('.widget-card__description').text()).toBe(propsData.description);
  });
});
