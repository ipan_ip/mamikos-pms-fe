import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import dayjs from 'dayjs';
import FinanceCard from '../FinanceCard';
import localVueWithBuefy from '~/utils/addBuefy';

const mockComponent = {
  template: '<div><slot /></div>',
};

const stubs = {
  MkCard: mockComponent,
  MkCardHeader: mockComponent,
  MkCardBody: mockComponent,
  MkCardTitle: mockComponent,
  MkCardFooter: mockComponent,
  WidgetCard: mockComponent,
  BgLink: { template: `<div @click="$emit('click')"><slot /></div>` },
  MonthSelectorModal: { template: "<div id='MonthSelectorModal'><slot /></div>" },
};

const storeData = {
  modules: {
    finance: {
      namespaced: true,
      state: {
        financeReport: {
          total: 10,
          monthly: {
            total: 10,
            loading: true,
          },
        },
      },
      actions: {
        getFinanceReport: () => jest.fn(),
      },
    },
    profile: {
      namespaced: true,
      getters: {
        isBbk() {
          return true;
        },
        isPremium() {
          return true;
        },
        user() {
          return {};
        },
      },
    },
  },
};

const mocks = {
  $tracker: { send: jest.fn() },
  $device: { isMobile: true },
  $router: { push: jest.fn() },
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.prototype.$dayjs = dayjs;

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      stubs,
      mocks,
      localVue: localVueWithBuefy,
      store: new Vuex.Store(storeData),
    },
    ...adtMountData,
  };

  return shallowMount(FinanceCard, mountData);
};

describe('FinanceCard.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render component properly', () => {
      expect(wrapper.find('.finance-card').exists()).toBeTruthy();
    });

    it('should render c-loader based on loading value', async () => {
      wrapper.vm.$store.state.finance.financeReport.monthly.loading = false;
      await wrapper.vm.$nextTick();
      expect(wrapper.find('.c-loader').exists()).toBeFalsy();

      wrapper.vm.$store.state.finance.financeReport.monthly.loading = true;
      await wrapper.vm.$nextTick();
      expect(wrapper.find('.c-loader').exists()).toBeTruthy();
    });
  });

  describe('should render computed values', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render incomeTotal properly', () => {
      expect(wrapper.vm.incomeTotal).toBe(10);
    });

    it('should render incomeMonthly properly', () => {
      expect(wrapper.vm.incomeMonthly).toBe(10);
    });

    it('should render loading properly', () => {
      expect(wrapper.vm.loading).toBeTruthy();
    });
  });

  describe('should execute functions properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should set financeModalActive as true when clicking on filterButton', async () => {
      const filterButton = wrapper.find('.finance-card__month-select');
      await filterButton.trigger('click');

      expect(wrapper.vm.financeModalActive).toBeTruthy();
    });

    it('should set selected month and call $store.dispatch when received optionClicked event from MonthSelectorModal', async () => {
      const dispatchSpy = jest.spyOn(wrapper.vm.$store, 'dispatch');
      const MonthSelectorModal = wrapper.find('#MonthSelectorModal');

      expect(MonthSelectorModal.exists()).toBe(true);

      await MonthSelectorModal.vm.$emit('optionClicked', 'Januari 2020');

      expect(wrapper.vm.financeModalActive).toBeFalsy();
      expect(wrapper.vm.selectedMonth).toBe('Jan 2020');
      expect(dispatchSpy).toBeCalledWith('finance/getFinanceReport', {
        month: 1,
        year: 2020,
      });
      dispatchSpy.mockRestore();
    });

    it('should handle tracker properly', async () => {
      const WidgetCard = wrapper.find(stubs.WidgetCard);
      const MonthOption = wrapper.find('.finance-card__month-select');

      await WidgetCard.vm.$emit('link-clicked');

      expect(mocks.$tracker.send).toBeCalledWith('moe', [
        '[Owner] OD - Laporan Keuangan Clicked',
        expect.any(Object),
      ]);

      await MonthOption.trigger('click');

      expect(mocks.$tracker.send).toBeCalledWith('moe', [
        '[Owner] OD Pendapatan - Month Option Clicked',
        expect.any(Object),
      ]);
    });
  });
});
