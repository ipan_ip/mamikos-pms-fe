import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusBanner from '../GoldPlusBanner';
import localVueWithBuefy from '~/utils/addBuefy';
import MkButton from '~/components/global/atoms/MkButton';

global.window = Object.create(window);
const url = 'mamikos.com';

Object.defineProperty(window, 'location', {
  value: { href: url },
});

const $device = { isMobile: false };
const $tracker = { send: jest.fn() };
const stubs = { MkButton };
const $router = { push: jest.fn() };

localVueWithBuefy.use(Vuex);

global.open = jest.fn(() => true);

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        isBannerDisplayed: true,
      },
      mutations: {
        closeBanner(state) {
          state.isBannerDisplayed = false;
        },
        setIsSubmissionFlowStartedFromEntryPoint: jest.fn(),
      },
    },
    profile: {
      namespaced: true,
      state: {
        data: {
          room_availability: {
            total: 5,
          },
        },
      },
      getters: {
        isBbk: () => false,
        isPremium: () => true,
        membership: () => {
          return {
            is_mamipay_user: true,
          };
        },
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      mocks: {
        $tracker,
        $device,
        $router,
      },
      stubs,
      store,
    },
    ...adtMountData,
  };

  return shallowMount(GoldPlusBanner, mountData);
};

describe('GoldPlusBanner.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-banner').exists()).toBeTruthy();
  });

  it('should handleLearnMore on Dekstop properly', () => {
    const button = wrapper.find('.goldplus-banner__learn-more');
    button.trigger('click');
    expect(global.open).toBeCalled();
    expect($tracker.send).toHaveBeenCalled();
  });

  it('should handleBtnClicked on Desktop properly', () => {
    const button = wrapper.find('.goldplus-banner__register');
    button.trigger('click');
    expect($tracker.send).toHaveBeenCalled();
    expect(global.open).toBeCalled();
    expect(wrapper.vm.$router.push).toBeCalledWith({
      name: 'goldplus-submission',
      params: { previousPage: 'Dashboard' },
    });
  });

  it('should handleLearnMore on Mobile properly', () => {
    wrapper.vm.$device.isMobile = true;
    const button = wrapper.find('.goldplus-banner__learn-more');
    button.trigger('click');
    expect(global.open).toBeCalled();
    expect($tracker.send).toHaveBeenCalled();
  });

  it('should handleBtnClicked on Mobile properly', () => {
    const button = wrapper.find('.goldplus-banner__register');
    button.trigger('click');
    expect($tracker.send).toHaveBeenCalled();
    expect(global.open).toBeCalled();
    expect(wrapper.vm.$router.push).toBeCalledWith({
      name: 'goldplus-submission',
      params: { previousPage: 'Dashboard' },
    });
  });
});
