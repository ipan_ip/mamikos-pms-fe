import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import TenantContractCard from '../TenantContractCard';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('TenantContractCard.vue', () => {
  const mockStore = {
    modules: {
      contract: {
        namespaced: true,
        state: {
          contractCardProperties: {
            total_off_contract_not_recorded: 0,
            total_off_contract: 10,
            total_off_contract_submission: 15,
          },
        },
      },
      profile: {
        namespaced: true,
        state: {
          user: { user_id: 10 },
          data: {
            is_booking_all_room: false,
            is_mamipay_user: false,
            expired: false,
            status: 'Premium',
          },
        },
        getters: {
          user(state, getters) {
            return state.user;
          },
          isBbk(state, getters) {
            return getters.membership.is_mamipay_user && state.data.is_booking_all_room;
          },
          membership(state) {
            const { membership = {} } = state.data;
            return membership;
          },
          isPremium(state, getters) {
            return !getters.membership.expired && !!(getters.membership.status === 'Premium');
          },
        },
      },
    },
  };
  const $router = { push: jest.fn() };
  const $tracker = { send: jest.fn() };
  const $device = { isMobile: false };

  const wrapper = shallowMount(TenantContractCard, {
    localVue,
    store: new Vuex.Store(mockStore),
    mocks: { $router, $tracker, $device },
  });

  it('should return the correct contract status label', () => {
    expect(wrapper.vm.contractLabel('approved')).toEqual('Kontrak Tercatat');
  });

  it('should open add contract onboarding page', () => {
    wrapper.vm.addTenantContract();
    expect($router.push).toBeCalledWith({ name: 'add-tenant-onboarding' });
  });

  it('should open contract request list page', () => {
    wrapper.vm.openContractList('waiting');
    expect($router.push).toBeCalledWith('/contract-request');
  });

  it('should send tracker properties', () => {
    wrapper.setProps({ isShowContractRequested: false });
    wrapper.vm.addTenantContract();
    expect($tracker.send).toBeCalled();
  });
});
