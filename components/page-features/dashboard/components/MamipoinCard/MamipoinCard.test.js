import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import _get from 'lodash/get';
import dayjs from 'dayjs';
import onboardingMixin from '@/components/page-features/mamipoin/mixins/onboarding';
import MamipoinCard from '../MamipoinCard';
import localVueWithBuefy from '~/utils/addBuefy';
import { toThousands } from '~/utils/number';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const mockComponent = {
  template: '<div />',
};

const $api = {
  getPointTotal: jest.fn(() => {
    return Promise.resolve({
      data: {
        status: true,
        point: 10,
        near_expired_point: 10,
        near_expired_date: '2020-08-20 08:09:10',
      },
    });
  }),
};

const $tracker = {
  send: jest.fn(),
};

const stubs = {
  MkCard: mockComponent,
  MkCardBody: mockComponent,
  MkCardFooter: mockComponent,
  MkButton: mockComponent,
  MkHighlight: mockComponent,
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('toThousands', toThousands);
localVueWithBuefy.prototype.$popupOrder = {
  next: jest.fn(),
};

const storeData = {
  modules: {
    mamipoin: {
      namespaced: true,
      state: {
        point: {
          total: 0,
        },
        mamipoinCard: {
          hasViewedOnboarding: false,
        },
      },
      mutations: {
        setPoint(state, point) {
          state.point = point;
        },
        setMamipoinCard(state, mamipoinCard = {}) {
          state.mamipoinCard = mamipoinCard;
        },
      },
    },
    profile: {
      namespaced: true,
      state: {
        data: {
          is_blacklist_mamipoin: false,
        },
      },
      getters: {
        isHighlightDailyAllocationShown: () => false,
        isBbk: () => true,
        isBooking: () => true,
        isPremium: () => true,
        membership: () => {
          return {
            is_mamipay_user: true,
          };
        },
        showModalVerifyPhone: () => false,
        showModalNeedPassword: () => false,
        showModalPremiumTrial: () => false,
        showModalSurveyPremium: () => false,
        showModalDailyBalanceAllocation: () => false,
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const $router = {
  push: jest.fn(),
};

const mountData = {
  localVue: localVueWithBuefy,
  mixins: [onboardingMixin],
  mocks: {
    $api,
    $route: {
      name: 'index',
    },
    $router,
    $tracker,
    $device,
    $dayjs: dayjs,
  },
  store,
  stubs,
  $refs: {
    mamipoinCard: {},
  },
  attachToDocument: true,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(MamipoinCard, finalData);
};

// mock window function
window.HTMLElement.prototype.scrollIntoView = jest.fn();
window.HTMLElement.prototype.scrollIntoViewIfNeeded = jest.fn();
window.screen = {
  availWidth: 900,
};
window.addEventListener = jest.fn();
window.removeEventListener = jest.fn();

describe('MamipoinCard.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  jest.useFakeTimers();

  describe('should render component properly', () => {
    it('should mount properly', () => {
      expect(wrapper.find('.mamipoin-card__card').exists()).toBeTruthy();
    });
  });

  describe('should call functions in the mounted block properly', () => {
    it('should call window.addEventListener after the component is fully mounted when $device.Desktop returns false', async () => {
      await wrapper.destroy();
      wrapper = mount({ mocks: { ...mountData.mocks, ...{ $device: { isDesktop: false } } } });

      expect(window.addEventListener).toBeCalled();
    });
  });

  describe('should call functions in the destroyed block properly', () => {
    it('should call window.removeEventListener after the component is fully destroyed when $device.Desktop returns false', async () => {
      wrapper.vm.$device.isDesktop = false;
      await wrapper.destroy();
      expect(window.removeEventListener).toBeCalled();
    });
  });

  describe('should render computed data properly', () => {
    it('should return isDesktop value from $device.isDesktop', () => {
      expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
    });

    it('should return isBlacklisted value from $store.state.profile.data.is_blacklist_mamipoin', () => {
      expect(wrapper.vm.isBlacklisted).toBe(
        wrapper.vm.$store.state.profile.data.is_blacklist_mamipoin,
      );
    });

    it('should return mamipoinCard value from mamipoin.mamipoinCard state', () => {
      expect(wrapper.vm.mamipoinCard).toBe(_get(storeData, 'modules.mamipoin.state.mamipoinCard'));
    });

    it('should compute isWideScreen value properly', () => {
      // isWideScreen is true when window.screen.availWidth >= 1920

      expect(wrapper.vm.isWideScreen).toBeFalsy();
    });

    it('should compute highlightPlacement value properly', () => {
      wrapper.vm.$device.isDesktop = true;
      expect(wrapper.vm.highlightPlacement).toBe('right');

      wrapper.vm.$device.isDesktop = false;
      expect(wrapper.vm.highlightPlacement).toBe('top');
    });

    it('should compute mamipoinCardWidth value properly', () => {
      if (wrapper.vm.$el.clientWidth) {
        expect(wrapper.vm.mamipoinCardWidth).toBe(wrapper.vm.$el.clientWidth - 15);
      } else {
        expect(wrapper.vm.mamipoinCardWidth).toBe(300);
      }
    });

    it('should return isHighlightShown as false when some of the requirements to show popover are not met', () => {
      /* Requirements: user has not viewed onboarding, none of the modals are shown */
      wrapper.vm.mamipoinCard = {
        hasViewedOnboarding: true,
      };

      expect(wrapper.vm.isHighlightShown).toBeFalsy();
    });
  });

  describe('should call functions properly', () => {
    it('should set  point data on successful api call', () => {
      expect(wrapper.vm.pointTotal).toBe(10);
      expect(wrapper.vm.nearExpiredPoint).toBe(10);
      expect(wrapper.vm.nearExpiredDate).toBe('20 Aug 2020');
    });

    it('should not set point data on failed api call', () => {
      const mockApi = {
        getPointTotal: jest.fn().mockResolvedValue({
          data: {
            status: false,
          },
        }),
      };

      wrapper = mount({ $api: mockApi });
      expect(wrapper.vm.pointTotal).toBe(0);
      expect(wrapper.vm.nearExpiredPoint).toBe(0);
      expect(wrapper.vm.nearExpiredDate).toBe('');
    });

    it('should set isLoading as false when catching api error failure', async () => {
      wrapper.vm.$api.getPointTotal = jest.fn().mockRejectedValue('error');
      await wrapper.vm.fetchPointApi();
      await wrapper.vm.$nextTick();
      expect(wrapper.vm.isLoading).toBeFalsy();
    });

    it('should set hasViewedOnboarding as false and remove has-highlight class from mamipoin card when calling closePopover function', async () => {
      const setDashboardOverflowSpy = jest.spyOn(wrapper.vm, 'setDashboardOverflow');
      const setBodyOverflowSpy = jest.spyOn(wrapper.vm, 'setBodyOverflow');

      // desktop view
      wrapper.vm.$device.isDesktop = true;
      await wrapper.vm.closeHighlight();
      expect(wrapper.vm.mamipoinCard).toEqual({
        hasViewedOnboarding: true,
      });
      expect(setDashboardOverflowSpy).toBeCalled();
      expect(wrapper.find('.mamipoin-card').classes()).not.toContain('--has-highlight');

      // mobile view
      wrapper.vm.$device.isDesktop = false;
      await wrapper.vm.closeHighlight();
      expect(setBodyOverflowSpy).toBeCalled();

      // restore
      setBodyOverflowSpy.mockRestore();
      setDashboardOverflowSpy.mockRestore();
    });

    it('should add has-highlight class to mamipoinCard and scroll the card when highlight is shown when calling handlePopupVisible function', async () => {
      const setBodyOverflowSpy = jest.spyOn(wrapper.vm, 'setBodyOverflow');
      const setDashboardOverflowSpy = jest.spyOn(wrapper.vm, 'setDashboardOverflow');

      wrapper.vm.scrollToThisCard = jest.fn();
      wrapper.vm.mamipoinCard = {
        hasViewedOnboarding: false,
      };

      // mobile view
      wrapper.vm.$device.isDesktop = false;
      await wrapper.vm.handlePopupVisible();
      expect(setBodyOverflowSpy).toBeCalled();
      wrapper.vm.isHighlightShown && expect(wrapper.vm.scrollToThisCard).toBeCalled();
      expect(wrapper.find('.mamipoin-card').classes()).toContain('--has-highlight');

      // desktop view
      wrapper.vm.$device.isDesktop = true;
      await wrapper.vm.handlePopupVisible();
      expect(setDashboardOverflowSpy).toBeCalled();

      // restore
      setBodyOverflowSpy.mockRestore();
      setDashboardOverflowSpy.mockRestore();
    });

    it('should call $tracker.send when sending tracker event', async () => {
      await wrapper.vm.sendTrackerEvent();

      expect($tracker.send).toHaveBeenCalled();
    });

    it('should call $tracker.send and $router.push when clicking on cta button', async () => {
      await wrapper.vm.handleCtaButtonClicked();

      expect($tracker.send).toHaveBeenCalled();
      expect($router.push).toHaveBeenCalled();
    });
  });
});
