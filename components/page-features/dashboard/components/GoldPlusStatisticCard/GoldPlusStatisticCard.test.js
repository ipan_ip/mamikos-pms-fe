import { shallowMount } from '@vue/test-utils';
import GoldPlusStatisticCard from '../GoldPlusStatisticCard';
import localVueWithBuefy from '~/utils/addBuefy';

const mockComponent = {
  template: '<div />',
};

const stubs = {
  MkCard: mockComponent,
  MkCardHeader: mockComponent,
  MkCardBody: mockComponent,
};

const propsData = {
  label: 'test',
  value: 2,
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      stubs,
      propsData,
    },
    ...adtMountData,
  };

  return shallowMount(GoldPlusStatisticCard, mountData);
};

describe('GoldPlusStatisticCard.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  describe('should render component', () => {
    it('should render component properly', () => {
      expect(wrapper.find('.goldplus-statistic-card').exists()).toBeTruthy();
    });
  });
});
