import { shallowMount } from '@vue/test-utils';
import dayjs from 'dayjs';
import LabelNumber from '../LabelNumber';
import BillingManagementWidget from './BillingManagementWidget';
import MkCard from '~/components/global/molecules/MkCard';
import MkCardFooter from '~/components/global/molecules/MkCard/components/MkCardFooter';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';
import MkCardHeader from '~/components/global/molecules/MkCard/components/MkCardHeader';
import MkButton from '~/components/global/atoms/MkButton';
import MkNumberCounter from '~/components/global/atoms/MkNumberCounter';
import mockComponent from '~/utils/mockComponent';
import localVueWithBuefy from '~/utils/addBuefy';

// mock EventBus
jest.mock('~/components/event-bus/index.js', () => {
  const EventBus = {
    $emit: jest.fn(),
  };

  global.EventBus = EventBus;
  return global.EventBus;
});

const stubs = {
  MkCard,
  MkButton,
  MkCardBody,
  LabelNumber,
  MkCardFooter,
  MkCardHeader,
  MkNumberCounter,
  MkCardTitle: mockComponent,
};

const mocks = {
  $dayjs: dayjs,
  $router: {
    push: jest.fn(),
  },
  $api: {
    getBillingSummary: jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: { unpaid_invoices: 2, paid_invoices: 2, transferred_invoices: 2 },
      },
    }),
  },
};

const mountComponent = (adtMountData = {}) => {
  const mountData = {
    ...{
      stubs,
      mocks,
      localVue: localVueWithBuefy,
    },
    ...adtMountData,
  };

  return shallowMount(BillingManagementWidget, mountData);
};

describe('BillingManagementWidget.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should render component properly ', () => {
      expect(wrapper.find('.manage-booking-card').exists()).toBeTruthy();
    });

    it('should render loader properly', async () => {
      await wrapper.setData({ isLoading: true });
      expect(wrapper.find('.c-loader').exists()).toBeTruthy();
    });

    it('should render label properly', async () => {
      await wrapper.setData({ isLoading: false });
      expect(wrapper.find('.label-number')).toBeTruthy();
    });
  });

  describe('should render computed value properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should compute getDate properly', () => {
      expect(wrapper.vm.getDate).toBe(dayjs().format('MMMM YYYY'));
    });
  });

  describe('should watch value', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should watch value isLoading properly', () => {
      const emitSpy = jest.spyOn(global.EventBus, '$emit');
      wrapper.vm.getSummary();
      expect(emitSpy).toBeCalled();
      emitSpy.mockRestore();
    });
  });

  describe('should run function', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mountComponent();
    });

    it('should run function goTo properly', () => {
      wrapper.vm.goTo('test');
      expect(wrapper.vm.$router.push).toBeCalled();
    });

    it('should run function getSummary when status true properly ', async () => {
      wrapper.vm.$api.getBillingSummary = jest.fn().mockResolvedValue({
        data: {
          status: true,
          data: { unpaid_invoices: '', paid_invoices: '', transferred_invoices: '' },
        },
      });
      await wrapper.vm.getSummary();
      expect(wrapper.vm.billListWidget.paidWidget.totalBilling).toBe(0);
    });

    it('should run function getSummary when status false properly', async () => {
      wrapper.vm.$api.getBillingSummary = jest.fn().mockResolvedValue({
        data: {
          status: false,
          data: { unpaid_invoices: 0, paid_invoices: 0, transferred_invoices: 0 },
        },
      });
      await wrapper.vm.getSummary();
      await wrapper.vm.$nextTick();
      await expect(wrapper.vm.billListWidget.transferredWidget.totalBilling).toBe(0);
    });

    it('should run  handleActionButtonClicked  when button clicked ', async () => {
      const mkButton = wrapper.find('.c-mk-button');
      if (mkButton && mkButton.exists()) {
        await mkButton.trigger('click');
        expect(wrapper.vm.$router.push).toBeCalled();
      }
    });
  });
});
