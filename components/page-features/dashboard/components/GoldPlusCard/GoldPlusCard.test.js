import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusCard from '../GoldPlusCard';
import localVueWithBuefy from '~/utils/addBuefy';
import MkButton from '~/components/global/atoms/MkButton';

global.window = Object.create(window);

const $device = { isMobile: false };
const $router = {
  push: jest.fn(),
};
const stubs = { MkButton };

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        statisticSummaries: [
          {
            label: 'Mock Label',
            value: 0,
          },
        ],
      },
      actions: {
        fetchWidgetBillingSummary() {
          return jest.fn().mockResolvedValue({
            data: {
              status: true,
              data: {
                unpaid_invoice: 0,
              },
            },
          });
        },
        fetchWidgetData() {
          return jest.fn().mockResolvedValue({
            data: {
              status: true,
              data: {},
            },
          });
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      mocks: {
        $device,
        $router,
        $bugsnag: {
          notify: jest.fn(),
        },
        $alert: jest.fn(),
      },
      stubs,
      store,
    },
    ...adtMountData,
  };

  return shallowMount(GoldPlusCard, mountData);
};

describe('GoldPlusCard.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  jest.mock(`~/assets/images/logo-goldplus.svg`, () => {
    return 'goldplus logo';
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-card').exists()).toBeTruthy();
  });

  it('should redirect to "/goldplus" route path when main button is clicked', () => {
    const button = wrapper.find('.goldplus-card__main-cta');
    button.trigger('click');

    expect($router.push).toBeCalledWith('/goldplus');
  });

  it('should call $bugsnag.notify and alert, when calling handleFetchWidgetData method and received error', async () => {
    wrapper.destroy();
    const mockStoreData = Object.assign(storeData);
    storeData.modules.goldplus.actions.fetchWidgetData = jest.fn().mockRejectedValue('error');

    wrapper = mount({ store: new Vuex.Store(mockStoreData) });
    await wrapper.vm.handleFetchWidgetData();

    expect(wrapper.vm.$bugsnag.notify).toBeCalled();
    expect(wrapper.vm.$alert).toBeCalled();
  });

  it('should return list feature ', () => {
    const mockStatisticSummary = {
      button_visible: true,
      key: 2,
      label: 'Sedang di-review',
      value: 1,
    };

    wrapper.vm.handleWidgetLabelClicked(mockStatisticSummary);
    expect(wrapper.vm.$router.push).toBeCalled();
  });

  it('should call handleUnpaidLabelClicked ', () => {
    wrapper.vm.handleUnpaidLabelClicked();
    expect(wrapper.vm.$router.push).toBeCalled();
  });

  it('should call $bugsnag.notify and alert, when calling handleFetchBillingSummary method and received error', async () => {
    wrapper.destroy();
    const mockStoreData = Object.assign(storeData);
    storeData.modules.goldplus.actions.fetchWidgetBillingSummary = jest
      .fn()
      .mockRejectedValue('error');

    wrapper = mount({ store: new Vuex.Store(mockStoreData) });
    await wrapper.vm.handleFetchBillingSummary();

    expect(wrapper.vm.$bugsnag.notify).toBeCalled();
    expect(wrapper.vm.$alert).toBeCalled();
  });
});
