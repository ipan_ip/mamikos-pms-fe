import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import ManageBookingCard from '../ManageBookingCard';
import localVueWithBuefy from '~/utils/addBuefy';
import MkButton from '~/components/global/atoms/MkButton';

global.window = Object.create(window);
const url = 'mamikos.com';

Object.defineProperty(window, 'location', {
  value: { href: url },
});

const $device = {
  isMobile: false,
};

const $tracker = {
  send: jest.fn(),
};

const $api = {
  getBbkStatus: jest.fn().mockResolvedValue({
    data: { status: true, bbk_status: { approved: 1, waiting: 1 } },
  }),
};

const stubs = {
  MkButton,
  NuxtLink: true,
};

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      getters: {
        isBbk: () => false,
        isPremium: () => true,
        user: () => {
          return {
            user_id: 1,
          };
        },
        membership: () => {
          return {
            is_mamipay_user: false,
          };
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      mocks: {
        $tracker,
        $device,
        $api,
      },
      stubs,
      store,
    },
    ...adtMountData,
  };

  return shallowMount(ManageBookingCard, mountData);
};

describe('ManageBookingCard.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.manage-booking-card').exists()).toBeTruthy();
  });

  it('should call $tracker.send when mamipay is false on Desktop', () => {
    const button = wrapper.find('button');
    button.trigger('click');
    expect($tracker.send).toHaveBeenCalled();
  });

  it('should call $tracker.send when mamipay is false on Mobile', () => {
    wrapper.vm.$device.isMobile = true;
    const button = wrapper.find('button');
    button.trigger('click');
    expect($tracker.send).toHaveBeenCalled();
  });

  it('should call $tracker.send when hasBbk is true on Mobile', () => {
    wrapper.setProps({ hasBbk: true });
    const button = wrapper.find('button');
    button.trigger('click');
    expect($tracker.send).toHaveBeenCalled();
  });

  it('should call $tracker.send when hasBbk is true on Desktop', () => {
    wrapper.vm.$device.isMobile = false;
    wrapper.setProps({ hasBbk: true });
    const button = wrapper.find('button');
    button.trigger('click');
    expect($tracker.send).toHaveBeenCalled();
  });
});
