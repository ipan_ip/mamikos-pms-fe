import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import WaitingCard from '../WaitingCard';
import mockWindowProperty from '~/utils/mockWindowProperty';

const localVue = createLocalVue();
localVue.use(Vuex);
mockWindowProperty('window.location', { href: '' });

describe('WaitingCard.vue', () => {
  const mocks = {
    $device: { isMobile: false },
    $tracker: { send: jest.fn() },
    $router: { push: jest.fn() },
    $mamikosUrl: 'mamikos.com',
  };

  const mockStore = {
    modules: {
      dashboard: {
        namespaced: true,
        state: { dashboardData: {}, isLoading: false },
      },
      profile: {
        namespaced: true,
        state: { user: { user_id: 12345 } },
        getters: {
          user(state) {
            return state.user;
          },
          isBbk() {
            return true;
          },
          isPremium() {
            return true;
          },
        },
      },
    },
  };

  const wrapper = shallowMount(WaitingCard, {
    localVue,
    mocks,
    store: new Vuex.Store(mockStore),
  });

  it('should load the component', () => {
    expect(wrapper.find('.widget-waiting').exists()).toBe(true);
  });

  it('should change the route to contract request page', () => {
    wrapper.vm.goToMenuPage(wrapper.vm.menus[2]);
    expect(mocks.$router.push).toBeCalledWith('/contract-request');
  });

  it('should redirect page to booking request list', () => {
    wrapper.vm.goToMenuPage(wrapper.vm.menus[0]);
    expect(window.location.href).toBe(
      'mamikos.com/ownerpage/manage/all/booking?status=need_confirmation',
    );
  });
});
