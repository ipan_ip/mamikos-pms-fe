import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import StateNoProperty from '../StateNoProperty';
import localVueWithBuefy from '~/utils/addBuefy';
import MkButton from '~/components/global/atoms/MkButton';

global.window = Object.create(window);
const url = 'mamikos.com';

Object.defineProperty(window, 'location', {
  value: { href: url },
});

const $device = {
  isMobile: false,
  isDesktop: true,
};

const $tracker = {
  send: jest.fn(),
};

const stubs = {
  MkButton,
};

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      getters: {
        isBbk: () => false,
        isPremium: () => true,
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      mocks: {
        $tracker,
        $device,
      },
      stubs,
      store,
    },
    ...adtMountData,
  };

  return shallowMount(StateNoProperty, mountData);
};

describe('StateNoProperty.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.no-property').exists()).toBeTruthy();
  });

  it('should call $tracker.send when sending tracker event on Desktop', () => {
    const button = wrapper.find('button');
    button.trigger('click');
    expect($tracker.send).toHaveBeenCalled();
  });

  it('should call $tracker.send when sending tracker event on Mobile', () => {
    wrapper.vm.$device.isMobile = true;
    const button = wrapper.find('button');
    button.trigger('click');
    expect($tracker.send).toHaveBeenCalled();
  });
});
