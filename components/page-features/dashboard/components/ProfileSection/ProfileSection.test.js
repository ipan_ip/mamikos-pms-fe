import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import ProfileSection from '../ProfileSection';
import dashboardTracker from '../../mixins/dashboardTracker';
import localVueWithBuefy from '~/utils/addBuefy';

global.window = Object.create(window);
const url = 'mamikos.com';

Object.defineProperty(window, 'location', {
  value: { href: url },
});

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        data: {
          gp_status: '',
          is_mamirooms: '',
        },
      },
      getters: {
        user: () => {
          return {
            name: '',
            phone_number: '',
            email: '',
          };
        },
        isBbk: () => jest.fn(),
        isPremium: () => jest.fn(),
      },
    },
  },
};

const store = new Vuex.Store(storeData);
const $tracker = { send: jest.fn() };
const $device = { isMobile: false };

const mountData = {
  localVue: localVueWithBuefy,
  mixins: [dashboardTracker],
  propsData: {
    avatar: '',
    username: '',
    notification: {
      count: 0,
    },
  },
  store,
  mocks: { $tracker, $device },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(ProfileSection, finalData);
};

describe('ProfileSection.vue', () => {
  let wrapper;

  process.env.MAMIKOS_URL = 'mamikos.com';

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.profile-section').exists()).toBeTruthy();
  });

  it('Should trim username properly', () => {
    const testCases = [
      {
        username: 'John So This Is A Mocked User Name That Exceeding Thirthy Characters Limit Doe',
        trimmed: 'John So This Is A Mocked User ...',
      },
      {
        username: 'Sasuke Uchiha',
        trimmed: 'Sasuke Uchiha',
      },
    ];

    testCases.forEach((tc) => {
      wrapper.setProps({ username: tc.username });
      expect(wrapper.vm.trimmedUserName).toBe(tc.trimmed);
    });
  });

  it('Should redirect users to Notification Center page properly', () => {
    wrapper.vm.toNotificationCenter();
    expect(window.location.href).toBe('mamikos.com/ownerpage/notification');
  });

  it('Should redirect users to Owner Settings page properly', () => {
    wrapper.vm.toOwnerSettings();
    expect(window.location.href).toBe('mamikos.com/ownerpage/settings');
  });
});
