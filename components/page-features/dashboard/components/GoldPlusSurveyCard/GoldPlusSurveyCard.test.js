import Vuex from 'vuex';
import dayjs from 'dayjs';
import { shallowMount } from '@vue/test-utils';
import GoldPlusSurveyCard from '../GoldPlusSurveyCard';
import localVueWithBuefy from '~/utils/addBuefy';

const mocks = {
  $router: { push: jest.fn() },
  $api: {
    getWidgetUnsubscribeStatus: jest
      .fn()
      .mockResolvedValue({ data: { data: { exit_gp_survey: 'test' } } }),
  },
  $dayjs: dayjs,
};

const storeData = {
  modules: {
    statistic: {
      namespaced: true,
      state: {
        storeWidgetData: { exit_gp_survey: true },
      },
      mutations: {
        setStoreWidgetData: jest.fn(),
        setTrackerRedirection: jest.fn(),
      },
      getters: {
        storeWidgetSurvey(state) {
          return state.storeWidgetData.exit_gp_survey;
        },
        storeSurveyWidgetGP: jest.fn(),
      },
    },
  },
};

localVueWithBuefy.use(Vuex);
const mountComponent = () => {
  return shallowMount(GoldPlusSurveyCard, {
    localVue: localVueWithBuefy,
    mocks,
    store: new Vuex.Store(storeData),
  });
};

describe('GoldPlusExpiredReminder.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mountComponent();
  });

  describe('render component GoldplusSurveyCard', () => {
    it('should render title with content ""', async () => {
      wrapper.vm.historyCloseAction = true;
      wrapper.vm.availableSurvey = true;
      await wrapper.vm.$nextTick();
      const title = wrapper.find('.c-main');
      expect(title.text()).toBe('Beritahu kami kesan Anda tentang GoldPlus  Isi Sekarang');
    });
  });

  describe('execute method in GoldplusSurveyCard', () => {
    it('should call checkSubmitStatus function when data ready on store', () => {
      const checkSubmitSpy = jest.spyOn(wrapper.vm, 'checkSubmitStatus');
      wrapper.vm.isDataReadyOnStore();
      expect(checkSubmitSpy).toBeCalled();
      checkSubmitSpy.mockRestore();
    });

    it('should call checkSubmitStatus function when data not ready on store', async () => {
      const getWidgetSpy = jest.spyOn(wrapper.vm, 'getWidgetStatus');
      wrapper.vm.$store.state.statistic.storeWidgetData.exit_gp_survey = false;
      await wrapper.vm.$nextTick();
      wrapper.vm.isDataReadyOnStore();
      expect(getWidgetSpy).toBeCalled();
      getWidgetSpy.mockRestore();
    });

    it('should set history action expired that more than 1 day', async () => {
      await wrapper.vm.$nextTick();
      const date = wrapper.vm.$dayjs().get('date');
      window.localStorage.setItem('closeWidgetHistory', date);
      wrapper.vm.isCardAvailableToDsiplay();
      expect(wrapper.vm.historyCloseAction).toBeFalsy();
    });

    it('should set date in localstorage with index "closeWidgetHistory"', () => {
      wrapper.vm.setExpiredVisibilityCard();
      expect(window.localStorage.getItem('closeWidgetHistory')).toBe(
        wrapper.vm
          .$dayjs()
          .get('date')
          .toString(),
      );
    });

    it('should set "available survey" when respon from server is "true"', async () => {
      wrapper.vm.$api.getWidgetUnsubscribeStatus = jest
        .fn()
        .mockResolvedValue({ data: { data: { exit_gp_survey: { is_visible: true } } } });

      wrapper.vm.checkSubmitStatus();
      await wrapper.vm.$nextTick();
      expect(wrapper.vm.availableSurvey).toBeTruthy();
    });

    it('should set "availableSurvey" to false when function called', () => {
      wrapper.vm.closeCard();
      expect(wrapper.vm.availableSurvey).toBeFalsy();
    });

    it('should redirect to exit gp survey when function called', () => {
      wrapper.vm.trackerGpStatus = 'gp1';
      wrapper.vm.gotoSurvey();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-survey?gp_package=gp1' });
    });
  });
});
