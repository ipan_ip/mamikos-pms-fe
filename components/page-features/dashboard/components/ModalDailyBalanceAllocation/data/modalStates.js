const content = {
  title: '',
  text: '',
  onConfirm: '',
  onCancel: '',
};
export default {
  activate() {
    content.title = 'Apakah Anda ingin mengaktifkan Saldo Harian?';
    content.text = 'Saldo iklan terpotong per hari sesuai Alokasi Saldo Iklan properti Anda.';
    content.onConfirm = 'Ya, Aktifkan';
    content.onCancel = 'Tidak';
    return content;
  },
  deactivate() {
    content.title = 'Apakah Anda ingin menonaktifkan Saldo Harian?';
    content.text = 'Anda akan menggunakan Saldo Iklan tanpa dibatasi alokasi saldo harian';
    content.onConfirm = 'Nonaktifkan';
    content.onCancel = 'Batal';
    return content;
  },
};
