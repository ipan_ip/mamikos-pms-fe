import Vuex from 'vuex';
import dayjs from 'dayjs';
import { shallowMount } from '@vue/test-utils';
import GoldPlusExpiredReminder from '../GoldPlusExpiredReminder';
import localVueWithBuefy from '~/utils/addBuefy';

const mocks = {
  $dayjs: dayjs,
  $router: { push: jest.fn() },
};
const storeData = {
  modules: {
    statistic: {
      namespaced: true,
      state: {
        storeWidgetData: {
          exit_gp_statistic: {
            key: 'test',
          },
        },
      },
      getters: {
        storeStatisticData(state) {
          return state.storeWidgetData.exit_gp_statistic;
        },
        storeStatisticWidgetGP(state) {
          return state.storeWidgetData.exit_gp_statistic.is_visible;
        },
      },
      mutations: {
        setTrackerRedirection: jest.fn(),
      },
    },
  },
};

localVueWithBuefy.use(Vuex);
const mountComponent = () => {
  return shallowMount(GoldPlusExpiredReminder, {
    localVue: localVueWithBuefy,
    mocks,
    store: new Vuex.Store(storeData),
  });
};

describe('GoldPlusExpiredReminder.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mountComponent();
  });

  describe('render component GoldPlusExpiredReminder', () => {
    it('should render title correctly', () => {
      expect(wrapper.find('.l-content__title')).toBeTruthy();
    });
  });

  describe('computed value in GoldPlusExpiredReminder component', () => {
    it('should return "true" when data ready in store', () => {
      expect(wrapper.vm.availableStatistic).toBeTruthy();
    });

    it('should watching change on availableStatistic data ', () => {
      wrapper.vm.$store.state.statistic.storeWidgetData = {
        exit_gp_statistic: {
          has_statistic: true,
          is_visible: true,
        },
      };
      expect(wrapper.vm.availableStatistic).toBeTruthy();
    });

    it('should watching change on availableStatistic data  tat visibility widget ', async () => {
      wrapper.vm.$store.state.statistic.storeWidgetData = {
        exit_gp_statistic: {
          is_visible: false,
        },
      };
      await wrapper.vm.$nextTick();
      expect(wrapper.vm.availableWidgetStatistic).toBeTruthy();
    });
  });

  describe('execute method in GoldPlusExpiredReminder', () => {
    it('should return GP status based on parameter ', () => {
      expect(wrapper.vm.checkPackageStatus('gp2')).toBe('GoldPlus 2');
    });

    it('should return date in day month and year format', () => {
      expect(wrapper.vm.formatedDate('2021-01-08')).toBe('08 Jan 2021');
    });

    it('should save item in localstorage based value in parameter', () => {
      wrapper.vm.saveItemStatistic('test', 1);
      expect(window.localStorage.getItem('test')).toBe('1');
    });

    it('should set info in localStorage based value on parameter', () => {
      const data = {
        invoice_id: 7575,
        started_at: '2020-12-22',
        ended_at: '2021-01-22',
        expiry_time: '2021-01-22',
        gp_package: 'gp3',
        has_statistic: false,
        is_visible: true,
      };
      wrapper.vm.setInfoStatisticGP(data);
      expect(window.localStorage.getItem('invoiceId')).toBe('7575');
    });

    it('should redirect to page statistic when function called', () => {
      wrapper.vm.startPeriodeGp = 1;
      wrapper.vm.endPeriodeGp = 1;
      wrapper.vm.gotoStatistic();
      expect(wrapper.vm.$router.push).toBeCalledWith({
        path: '/exit-gp-statistic?started_at=1&ended_at=1',
      });
    });

    it('should set available status when data ready on store', () => {
      wrapper.vm.isDataReadyOnStore();
      expect(wrapper.vm.availableWidgetStatistic).toBeFalsy();
    });

    it('should set available status when data not complite on store', () => {
      const storeData = {
        modules: {
          statistic: {
            namespaced: true,
            getters: {
              storeStatisticData: jest.fn(() => {
                return { status: 'test', is_visible: true };
              }),
              storeStatisticWidgetGP: jest.fn(() => {
                return false;
              }),
            },
          },
        },
      };

      const mountComponent2 = () => {
        return shallowMount(GoldPlusExpiredReminder, {
          localVue: localVueWithBuefy,
          mocks,
          store: new Vuex.Store(storeData),
        });
      };
      wrapper = mountComponent2();
      wrapper.vm.isDataReadyOnStore();
      expect(wrapper.vm.availableWidgetStatistic).toBeFalsy();
    });
  });
});
