import Vuex from 'vuex';
import { mount } from '@vue/test-utils';
import BannerCard from '../BannerCard';
import localVueWithBuefy from '~/utils/addBuefy';

global.window = Object.create(window);
const url = 'mamikos.com';

Object.defineProperty(window, 'location', {
  value: { href: url },
});

const $device = {
  isMobile: false,
};

const $tracker = {
  send: jest.fn(),
};

const $api = {
  getBannerEvent: jest.fn().mockResolvedValue({
    data: {
      status: true,
      homes: {
        posters: [
          {
            description: 'Solusi Kelola Untuk Maksimalkan Pendapatan Bersama Mamikos GoldPlus!',
            photo_url: {
              large:
                'https://static.mamikos.com/uploads/cache/data/event/2020-11-10/o7ea7Y82-540x720.jpg',
            },
            poster_id: 156,
            redirect_browser: 0,
            scheme:
              'https://mamikos.com/goldplus/?utm_medium=BannerEvent&utm_source=homebanner&utm_campaign=GoldPlus',
            title: 'Mamikos GoldPlus',
          },
        ],
      },
    },
  }),
};

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      getters: {
        isBbk: () => false,
        isPremium: () => true,
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const createComponent = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      mocks: {
        $tracker,
        $device,
        $api,
      },
      store,
    },
    ...adtMountData,
  };

  return mount(BannerCard, mountData);
};

describe('BannerCard.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = createComponent();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.banner-card').exists()).toBeTruthy();
  });

  it('should call $tracker.send when banner image is clicked', () => {
    const imgLink = wrapper.find('a');
    imgLink.trigger('click');
    expect($tracker.send).toHaveBeenCalled();
  });
});
