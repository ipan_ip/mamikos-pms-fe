import { mapGetters } from 'vuex';
import _get from 'lodash/get';

export default {
  computed: {
    ...mapGetters('profile', ['isBbk', 'isPremium', 'user']),
    interfaceLabel() {
      return this.$device.isMobile ? 'mobile' : 'desktop';
    },
    baseDashboardTrackerData() {
      return {
        owner_name: _get(this.user, 'name', ''),
        owner_phone_number: _get(this.user, 'phone_number', ''),
        owner_email: _get(this.user, 'email', ''),
        goldplus_status: _get(this.$store.state, 'profile.data.gp_status', ''),
        is_premium: !!this.isPremium,
        is_mamirooms: !!_get(this.$store.state, 'profile.data.is_mamirooms', false),
        is_booking: !!this.isBbk,
        interface: this.interfaceLabel,
      };
    },
  },
  methods: {
    sendDashboardTracker(eventName = '', payload = {}) {
      this.$tracker.send('moe', [
        eventName,
        {
          ...this.baseDashboardTrackerData,
          ...payload,
        },
      ]);
    },
  },
};
