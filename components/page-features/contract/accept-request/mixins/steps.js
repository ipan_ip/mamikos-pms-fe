import _get from 'lodash/get';
import { mapState, mapGetters, mapMutations, mapActions } from 'vuex';

import SelectRoom from '../components/SelectRoom';
import CustomPrice from '../components/CustomPrice';
import Navbar from '../components/Navbar';
import ContentTitle from '../components/ContentTitle';
import Skeleton from '../components/Skeleton';
import acceptMixin from '../../contract-request/mixins/accept';

export default {
  data() {
    return {
      mamikosUrl: process.env.MAMIKOS_URL,
    };
  },
  components: {
    SelectRoom,
    CustomPrice,
    Navbar,
    ContentTitle,
    Skeleton,
  },
  mixins: [acceptMixin],
  computed: {
    ...mapState('contract', {
      currentAcceptRequestStep: (state) => state.accept.currentAcceptRequestStep,
      isFetchingData: (state) => state.accept.isFetchingData,
    }),

    ...mapState('contract', ['contractRequestDetail', 'accept']),

    ...mapGetters('contract', ['acceptRequestSteps', 'availableAcceptRequestSteps']),

    totalSteps() {
      return this.availableAcceptRequestSteps.length;
    },
    currentStep() {
      return this.availableAcceptRequestSteps[this.currentAcceptRequestStep];
    },
    currentStepComponent() {
      return _get(this.currentStep, 'component', '');
    },
    currentStepTitle() {
      const title = _get(this.currentStep, 'title', '');
      const titleDekstop = _get(this.currentStep, 'titleDesktop', '');
      return this.$device.isDesktop ? titleDekstop || title : title;
    },
    stepProgress() {
      return this.isMoreThanOneStep ? (this.currentAcceptRequestStep + 1) / this.totalSteps : 0;
    },
    stepProgressLabel() {
      return this.isMoreThanOneStep
        ? `Langkah ${this.currentAcceptRequestStep + 1} dari ${this.totalSteps}`
        : '';
    },
    navStepTitle() {
      return this.isMoreThanOneStep
        ? `Langkah ${this.currentAcceptRequestStep + 1}: ${this.currentStepTitle}`
        : '';
    },
    requestId() {
      return this.$route.params.requestId;
    },
    isMoreThanOneStep() {
      return this.totalSteps > 1;
    },
    acceptParams() {
      const params = {
        contractId: this.contractRequestDetail.id,
        requestProps: {
          from: 'Detail Tenant',
          price_type: this.contractRequestDetail.is_revised_price ? 'revised' : 'original',
        },
      };

      if (this.accept.selectedRoom !== 0) {
        params.requestProps.designer_room_id = this.accept.selectedRoom;
      }

      return params;
    },
  },
  created() {
    this.initAcceptStep();
  },
  beforeDestroy() {
    this.resetAcceptRequestState();
  },
  methods: {
    ...mapMutations('contract', ['setCurrentAcceptRequestStep', 'setAcceptContractGetLoading']),
    ...mapActions('contract', ['getContractRequestDetail', 'resetAcceptRequestState']),
    async initAcceptStep() {
      if (
        (this.contractRequestDetail.id &&
          String(this.contractRequestDetail.id) !== String(this.requestId)) ||
        !this.contractRequestDetail.id
      ) {
        this.resetAcceptRequestState();
        this.setAcceptContractGetLoading(true);
        await this.getContractRequestDetail(this.requestId);
        this.setAcceptContractGetLoading(false);
        this.validatePage();
      } else {
        this.validatePage();
        this.setAcceptContractGetLoading(false);
      }
    },
    validatePage() {
      if (this.availableAcceptRequestSteps.length === 0) {
        this.goToRequestDetail();
      }
    },
    addStep(count) {
      const nextStep = this.currentAcceptRequestStep + count;
      const isLastStepNext = nextStep > this.availableAcceptRequestSteps.length - 1;
      const isFirstStepBack = nextStep < 0;

      if (isFirstStepBack) {
        this.goToRequestDetail();
      } else if (isLastStepNext) {
        this.onAcceptContract(this.acceptParams);
      } else {
        this.setCurrentAcceptRequestStep(nextStep);
      }
    },
    goToRequestDetail() {
      this.$router.replace({
        name: 'contract-request-detail',
        params: { id: this.requestId },
      });
    },
  },
};
