import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mobile from './mobile';
import { state, mutations, getters } from '~/store/contract.js';

const localVue = createLocalVue();
localVue.use(Vuex);

const flushPromises = () => {
  return new Promise((resolve) => setImmediate(resolve));
};

const store = {
  modules: {
    contract: {
      namespaced: true,
      state,
      mutations,
      getters,
      actions: {
        getContractRequestDetail: jest.fn().mockResolvedValue(true),
        resetAcceptRequestState: jest.fn(),
      },
    },
  },
};

const mockComponent = () => {
  return {
    template: '<div v-bind="$attrs"/>',
  };
};

const mocks = {
  $router: {
    replace: jest.fn(),
    push: jest.fn(),
    go: jest.fn(),
  },
  $route: {
    params: {
      requestId: 111,
    },
    query: {},
  },
  $device: {
    isDesktop: true,
    isMobile: false,
  },
};

const stubs = {
  Navbar: mockComponent(),
  ContentTitle: mockComponent(),
};

describe('mobile.vue', () => {
  const wrapper = shallowMount(mobile, { localVue, stubs, mocks, store: new Vuex.Store(store) });

  const setState = (state, value) => {
    wrapper.vm.$store.state.contract.accept[state] = value;
  };

  it('should render current step component properly', async () => {
    wrapper.vm.$store.commit('contract/updateContractRequestDetail', {
      is_empty_room: true,
      is_revised_price: true,
      room: {
        _id: 123,
      },
    });

    setState('isFetchingData', false);
    setState('isLoaded', true);
    await flushPromises();
    await wrapper.vm.$nextTick();

    // set all steps to be available
    wrapper.vm.$store.state.contract.contractRequestDetail.is_empty_room = true;
    wrapper.vm.$store.state.contract.contractRequestDetail.is_revised_price = true;

    // select first step
    setState('currentAcceptRequestStep', 0);

    await wrapper.vm.$nextTick();

    const expectedCurrentStep = wrapper.vm.$store.getters['contract/acceptRequestSteps'][0];
    const navbar = () => wrapper.find(stubs.Navbar);
    const contentTitle = () => wrapper.find(stubs.ContentTitle);

    expect(Object.keys(navbar().attributes()).includes('is-hide-progress-bar')).toBe(true);

    expect(navbar().attributes('right-text')).toBe(wrapper.vm.stepProgressLabel);

    expect(Object.keys(contentTitle().attributes()).includes('is-sticky')).toBe(true);
    expect(contentTitle().attributes('title')).toBe(expectedCurrentStep.title);
  });
});
