import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import desktop from './desktop';
import { state, mutations, getters } from '~/store/contract.js';
import mockWindowProperty from '~/utils/mockWindowProperty';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
  modules: {
    contract: {
      namespaced: true,
      state,
      mutations,
      getters,
      actions: {
        getContractRequestDetail: jest.fn().mockResolvedValue(true),
        resetAcceptRequestState: jest.fn(),
        acceptContractRequest: jest.fn().mockResolvedValue({ tenantId: 123 }),
      },
    },
  },
};

const mockComponent = () => {
  return {
    template: '<div v-bind="$attrs"/>',
  };
};

const stubs = {
  Navbar: mockComponent(),
  ContentTitle: mockComponent(),
  SelectRoom: mockComponent(),
  CustomPrice: mockComponent(),
};

const mocks = {
  $router: {
    replace: jest.fn(),
    push: jest.fn(),
    go: jest.fn(),
  },
  $route: {
    params: {
      requestId: 111,
    },
    query: {},
  },
  $device: {
    isDesktop: true,
  },
};

const mockLocationReplace = jest.fn();
mockWindowProperty('window.location', { replace: mockLocationReplace });

describe('desktop.vue', () => {
  const wrapper = shallowMount(desktop, {
    localVue,
    stubs,
    mocks,
    store: new Vuex.Store(store),
  });

  const setState = (state, value) => {
    wrapper.vm.$store.state.contract.accept[state] = value;
  };

  it('should render current step component properly', async () => {
    setState('isFetchingData', false);
    setState('isLoaded', true);
    await wrapper.vm.$nextTick();

    // set all steps to be available
    wrapper.vm.$store.commit('contract/updateContractRequestDetail', {
      is_empty_room: true,
      is_revised_price: true,
      room: {
        _id: 123,
      },
    });
    // select first step
    setState('currentAcceptRequestStep', 0);

    await wrapper.vm.$nextTick();

    const expectedCurrentStep = wrapper.vm.$store.getters['contract/acceptRequestSteps'][0];
    const container = wrapper.find('.contract-accept-request-desktop');
    const navbar = wrapper.find(stubs.Navbar);
    const contentTitle = wrapper.find(stubs.ContentTitle);

    expect(container.attributes().style).toBe(`width: ${expectedCurrentStep.width};`);

    expect(navbar.attributes('progress')).toBe('0.5');
    expect(navbar.attributes('title')).toBe(`Langkah 1: ${expectedCurrentStep.title}`);

    expect(contentTitle.attributes('title')).toBe(expectedCurrentStep.title);
  });

  it('should navigate step properly', async () => {
    setState('isFetchingData', false);
    setState('isLoaded', true);
    await wrapper.vm.$nextTick();

    const navbar = () => wrapper.find(stubs.Navbar);
    const selectRoom = () => wrapper.find(stubs.SelectRoom);
    const customPrice = () => wrapper.find(stubs.CustomPrice);

    const detailRouterParam = {
      name: 'contract-request-detail',
      params: { id: 111 },
    };
    // set all steps to be available
    wrapper.vm.$store.commit('contract/updateContractRequestDetail', {
      is_empty_room: true,
      is_revised_price: true,
      room: {
        id: 123,
      },
    });
    // select first step
    setState('currentAcceptRequestStep', 0);

    await wrapper.vm.$nextTick();

    // go back from first step
    navbar().vm.$emit('back');

    expect(mocks.$router.replace).toBeCalledWith(detailRouterParam);

    jest.clearAllMocks();

    // go next
    selectRoom().vm.$emit('next');
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.currentAcceptRequestStep).toBe(1);

    // go next from last step
    customPrice().vm.$emit('next');
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.currentAcceptRequestStep).toBe(1);
    // expect(mockLocationReplace).toBeCalled();

    // go back not from first step
    navbar().vm.$emit('back');

    expect(wrapper.vm.currentAcceptRequestStep).toBe(0);
  });
});
