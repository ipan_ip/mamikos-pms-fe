import { shallowMount, createLocalVue } from '@vue/test-utils';
import { BgModal, BgButton } from 'bangul-vue';
import RejectRevisedPriceModal from './RejectRevisedPriceModal';

const localVue = createLocalVue();

describe('RejectRevisedPriceModal.vue', () => {
  const propsData = {
    value: true,
  };

  const wrapper = shallowMount(RejectRevisedPriceModal, {
    localVue,
    propsData,
    stubs: { BgModal, BgButton },
  });

  it('should show the modal', () => {
    expect(wrapper.find('.reject-revised-price-modal').exists()).toBe(true);
  });

  it('should close the modal by emitting toggle with false value', () => {
    wrapper.vm.closeModal();
    expect(wrapper.emitted('toggle')[0][0]).toBe(false);
  });
});
