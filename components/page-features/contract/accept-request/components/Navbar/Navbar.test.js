import { shallowMount, createLocalVue } from '@vue/test-utils';
import { BgText } from 'bangul-vue';
import Navbar from './Navbar';

const localVue = createLocalVue();

const BgIcon = { template: '<img v-bind="$attrs" />' };

describe('Navbar.vue', () => {
  const wrapper = shallowMount(Navbar, {
    localVue,
    stubs: { BgIcon, BgText },
  });

  it('should render navbar', () => {
    expect(wrapper.find('.contract-accept-navbar').exists()).toBe(true);
  });

  it('should render title if given from props', async () => {
    await wrapper.setProps({ title: 'Nav Title' });
    const navTitle = wrapper.find('.contract-accept-navbar__title');

    expect(navTitle.exists()).toBe(true);
    expect(navTitle.text()).toBe('Nav Title');
  });

  it('should render right text if given from props', async () => {
    await wrapper.setProps({ rightText: 'Right Text' });
    const rightText = wrapper.find('.contract-accept-navbar__right');

    expect(rightText.exists()).toBe(true);
    expect(rightText.text()).toBe('Right Text');
  });

  it('should emit back when back icon clicked', async () => {
    const backIcon = wrapper.find('.contract-accept-navbar__back');
    await backIcon.trigger('click.native');

    expect(wrapper.emitted('back')[0]).toBeTruthy();
  });

  it('should render progress bar properly', async () => {
    await wrapper.setProps({ progress: 0.75 });
    const progressBar = wrapper.find('.contract-accept-navbar__progress-bar');

    expect(progressBar.exists()).toBe(true);
    expect(progressBar.attributes().style).toBe('width: 75%;');
  });
});
