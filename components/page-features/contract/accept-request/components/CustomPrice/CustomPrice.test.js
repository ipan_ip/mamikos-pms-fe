import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import CustomPrice from './CustomPrice';

const localVue = createLocalVue();
localVue.use(Vuex);
window.open = jest.fn();

describe('CustomPrice.vue', () => {
  const $device = { isMobile: false };

  const wrapper = shallowMount(CustomPrice, {
    localVue,
    mocks: { $device },
    store: new Vuex.Store({
      modules: {
        contract: {
          namespaced: true,
          state: {
            contractRequestDetail: {
              fullname: 'Chris Evans',
              id: 12345,
              room: { _id: 23456 },
            },
            accept: {
              selectedRoom: 1234,
            },
          },
        },
      },
    }),
  });

  it('should render the correct parameter for accept contract', () => {
    expect(wrapper.vm.acceptParams.contractId).toBe(12345);
    expect(wrapper.vm.acceptParams.requestProps.designer_room_id).toBe(1234);
  });

  it('should show the modal reject revised price', () => {
    wrapper.vm.toggleModalRevisedPrice(true);
    expect(wrapper.vm.isModalRejectRevisedPrice).toBe(true);
  });
});
