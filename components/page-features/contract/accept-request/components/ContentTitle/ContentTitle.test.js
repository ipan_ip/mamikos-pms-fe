import { shallowMount, createLocalVue } from '@vue/test-utils';
import { BgText } from 'bangul-vue';
import ContentTitle from './ContentTitle';

const localVue = createLocalVue();

describe('ContentTitle.vue', () => {
  const createWrapper = (props, mocks) =>
    shallowMount(ContentTitle, {
      localVue,
      mocks,
      propsData: { title: 'Content Title', ...props },
      stubs: { BgText },
    });

  it('should render content title', () => {
    const wrapper = createWrapper();

    expect(wrapper.find('.content-title').exists()).toBe(true);
    expect(wrapper.find('.content-title').text()).toBe('Content Title');
  });

  it('should not set sticky state when isSticky props is false', () => {
    const wrapper = createWrapper({ isSticky: false }, { $device: { isMobile: true } });

    const spySetSticky = jest.spyOn(wrapper.vm, 'setStickyState');
    wrapper.vm.setStickyTitleScrollListener();

    expect(spySetSticky).not.toBeCalled();
    spySetSticky.mockRestore();
  });

  it('should set sticky state when isSticky props is true', () => {
    const wrapper = createWrapper({ isSticky: true }, { $device: { isMobile: true } });

    const spySetSticky = jest.spyOn(wrapper.vm, 'setStickyState');
    wrapper.vm.setStickyTitleScrollListener();

    expect(spySetSticky).toBeCalled();
    spySetSticky.mockRestore();
  });

  it('should not set sticky state when isSticky props is true but scroll parent is not found', () => {
    const wrapper = createWrapper(
      { isSticky: true, scrollParentSelector: '.not-found-parent-selector' },
      { $device: { isMobile: true } },
    );

    const spySetSticky = jest.spyOn(wrapper.vm, 'setStickyState');
    wrapper.vm.setStickyTitleScrollListener();

    expect(spySetSticky).not.toBeCalled();
    spySetSticky.mockRestore();
  });

  it('should not set sticky state when event listener scroll removed', () => {
    const wrapper = createWrapper({ isSticky: true }, { $device: { isMobile: true } });

    const spySetSticky = jest.spyOn(wrapper.vm, 'setStickyState');
    wrapper.vm.setStickyTitleScrollListener(false);

    expect(spySetSticky).not.toBeCalled();
    spySetSticky.mockRestore();
  });

  it('should remove event listener scroll before component destroyed', () => {
    const wrapper = createWrapper({ isSticky: true }, { $device: { isMobile: true } });

    const removeEventListener = jest.fn();
    window.removeEventListener = removeEventListener;

    wrapper.vm.$destroy();
    expect(removeEventListener).toBeCalled();
  });
});
