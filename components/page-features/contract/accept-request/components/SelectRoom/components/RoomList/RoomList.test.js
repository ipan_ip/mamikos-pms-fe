import { shallowMount, createLocalVue } from '@vue/test-utils';
import { BgRadio, BgLabelPill } from 'bangul-vue';
import RoomList from './RoomList';

const localVue = createLocalVue();

const rooms = [
  {
    name: 'Kamar 1',
    floor: 'Lantai 1',
    is_gold_plus: true,
    id: 1,
    disable: false,
  },
  {
    name: 'Kamar 2',
    floor: 'Lantai 1',
    is_gold_plus: true,
    id: 2,
    disable: false,
  },
  {
    name: 'Kamar 3',
    floor: 'Lantai 2',
    is_gold_plus: false,
    id: 3,
    disable: false,
  },
  {
    name: 'Kamar 4',
    floor: 'Lantai 2',
    is_gold_plus: true,
    id: 4,
    disable: true,
  },
];

describe('RoomList.vue', () => {
  const wrapper = shallowMount(RoomList, {
    localVue,
    propsData: { value: 0, options: rooms },
    stubs: { BgRadio, BgLabelPill },
  });

  it('should render room list', () => {
    expect(wrapper.find('.room-list').exists()).toBe(true);
    expect(wrapper.findAll('.room-list__option').length).toBe(rooms.length);
  });

  it('should render disabled overlay when option is disabled and emit disabled-clicked when clicked', () => {
    const disabledOption = wrapper.findAll('.room-list__option').at(3);
    const disabledOptionOverlay = disabledOption.find('.room-list__disabled-overlay');

    expect(disabledOption.classes('room-list__option--disabled')).toBe(true);
    expect(disabledOptionOverlay.exists()).toBe(true);

    disabledOptionOverlay.trigger('click');

    expect(wrapper.emitted('disabled-clicked')[0]).toBeTruthy();
  });

  it('should set selected value when option selected', async () => {
    const roomOptionRadio = wrapper.findAll('.room-list__option input[type="radio"]').at(0);
    await roomOptionRadio.setChecked(true);

    expect(wrapper.emitted('input')[0][0]).toBe(rooms[0].id);
  });
});
