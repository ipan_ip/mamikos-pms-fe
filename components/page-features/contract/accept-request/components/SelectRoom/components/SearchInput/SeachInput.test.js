import { shallowMount, createLocalVue } from '@vue/test-utils';
import { BgInput } from 'bangul-vue';
import SearchInput from './SearchInput';

const localVue = createLocalVue();

describe('SearchInput.vue', () => {
  const wrapper = shallowMount(SearchInput, {
    localVue,
    propsData: { value: '' },
    stubs: { BgInput },
  });

  it('should render search input', () => {
    expect(wrapper.find('.search-input').exists()).toBe(true);
  });

  it('should emit input when input field inputted', async () => {
    const input = wrapper.find('input');
    await input.setValue('Test');

    expect(wrapper.find('input').element.value).toBe('Test');
    expect(wrapper.emitted('input')[0][0]).toBe('Test');
  });

  it('should reset input properly', async () => {
    const input = wrapper.find('input');
    await input.setValue('Kamar');

    const resetBtn = wrapper.find('.bg-c-input__icon--right');

    expect(wrapper.find('input').element.value).toBe('Kamar');
    expect(resetBtn.exists()).toBe(true);

    await resetBtn.trigger('click');

    expect(wrapper.find('input').element.value).toBe('');
  });
});
