import { shallowMount, createLocalVue } from '@vue/test-utils';
import { BgText, BgToast, BgButton } from 'bangul-vue';
import Vuex from 'vuex';
import SelectRoom from './SelectRoom';
import { state, mutations, getters } from '~/store/contract.js';

const localVue = createLocalVue();
localVue.use(Vuex);

const mockComponent = { template: '<div />' };

const flushPromises = () => {
  return new Promise((resolve) => setImmediate(resolve));
};

const stubs = {
  SearchInput: mockComponent,
  RoomList: mockComponent,
  BgText,
  BgToast,
  BgButton,
};

const rooms = [
  {
    name: 'Kamar 1',
    floor: 'Lantai 1',
    is_gold_plus: true,
    id: 1,
    disable: false,
  },
  {
    name: 'Kamar 2',
    floor: 'Lantai 1',
    is_gold_plus: true,
    id: 2,
    disable: false,
  },
  {
    name: 'Kamar 3',
    floor: 'Lantai 2',
    is_gold_plus: false,
    id: 3,
    disable: false,
  },
  {
    name: 'Kamar 4',
    floor: 'Lantai 2',
    is_gold_plus: true,
    id: 4,
    disable: true,
  },
];

const mocks = {
  $router: {
    push: jest.fn(),
  },
  $api: {
    getRoomAllotment: jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: {
          room_total: 4,
          room_available: 3,
          units: rooms,
        },
      },
    }),
  },
  $bugsnag: {
    notify: jest.fn(),
  },
  $alert: jest.fn(),
};

const store = () => ({
  modules: {
    contract: {
      namespaced: true,
      state: {
        ...state,
        accept: {
          roomList: [],
          isLoadedRoomList: false,
          roomAvailable: 0,
          selectedRoom: 0,
        },
        contractRequestDetail: {
          id: 123,
          is_revised_price: true,
          is_empty_room: true,
          room: {
            id: 123,
          },
        },
      },
      mutations,
      getters,
      actions: {
        getContractRequestDetail: jest.fn().mockResolvedValue(true),
      },
    },
  },
});

describe('SelectRoom.vue', () => {
  const createWrapper = async () => {
    const wrapper = shallowMount(SelectRoom, {
      localVue,
      stubs,
      mocks,
      store: new Vuex.Store(store()),
    });
    await flushPromises();
    await wrapper.vm.$nextTick();
    return wrapper;
  };

  it('should render select room', async () => {
    const wrapper = await createWrapper();
    expect(wrapper.find('.select-room').exists()).toBe(true);
  });

  it('should set room available properly', async () => {
    const wrapper = await createWrapper();
    await wrapper.vm.$nextTick();
    expect(wrapper.find('.select-room__room-available').text()).toBe('Sisa 3 Kamar');
  });

  it('should provide filtered room properly', async () => {
    const wrapper = await createWrapper();
    await wrapper.vm.$store.commit('contract/setRoomList', rooms);
    wrapper.find(stubs.SearchInput).vm.$emit('input', 'kamar 4');
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.searchedRoom).toEqual([{ ...rooms[3] }]);
  });

  it('should go to edit room page when edit room toast action clicked', async () => {
    const wrapper = await createWrapper();
    wrapper.find(stubs.BgToast).vm.$emit('click');

    expect(mocks.$router.push).toBeCalledWith(wrapper.vm.editRoomPath);
  });

  it('should emit next if button save clicked', async () => {
    const wrapper = await createWrapper();
    await wrapper.setData({ roomSelected: 1 });
    wrapper.find(stubs.BgButton).vm.$emit('click');

    expect(wrapper.emitted('next')[0]).toBeTruthy();
  });

  it('should handle request response false room allotment properly', async () => {
    mocks.$api.getRoomAllotment = jest.fn().mockResolvedValue({
      data: {
        status: false,
        data: null,
      },
    });
    const wrapper = await createWrapper();

    expect(wrapper.find('.select-room__room-empty').exists()).toBe(true);
  });

  it('should handle failed request room allotment properly', async () => {
    mocks.$api.getRoomAllotment = jest.fn().mockRejectedValue(new Error('error'));
    const wrapper = await createWrapper();

    expect(wrapper.find('.select-room__room-empty').exists()).toBe(true);
    expect(mocks.$bugsnag.notify).toBeCalled();
    expect(mocks.$alert).toBeCalledWith('is-danger', expect.any(String), expect.any(String));
  });
});
