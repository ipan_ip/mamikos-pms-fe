import { createLocalVue, shallowMount } from '@vue/test-utils';
import Skeleton from './Skeleton';

const localVue = createLocalVue();

describe('Skeleton.vue', () => {
  const createWrapper = (isMobile = false) =>
    shallowMount(Skeleton, { localVue, mocks: { $device: { isMobile } } });

  it('should render all skeleton properly on desktop', () => {
    const wrapper = createWrapper();

    expect(wrapper.findAll('.dbet-accept-skeleton > *').length).toBe(6);
    expect(
      wrapper
        .findAll('.dbet-accept-skeleton > *')
        .at(0)
        .attributes('width'),
    ).toBe('40%');
    expect(
      wrapper
        .findAll('.dbet-accept-skeleton > *')
        .at(1)
        .attributes('width'),
    ).toBe('80%');
  });

  it('should render all skeleton properly on mobile', () => {
    const wrapper = createWrapper(true);

    expect(wrapper.findAll('.dbet-accept-skeleton > *').length).toBe(6);
    expect(
      wrapper
        .findAll('.dbet-accept-skeleton > *')
        .at(0)
        .attributes('width'),
    ).toBe('60%');
    expect(
      wrapper
        .findAll('.dbet-accept-skeleton > *')
        .at(1)
        .attributes('width'),
    ).toBe('100%');
  });
});
