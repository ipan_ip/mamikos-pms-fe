import { shallowMount, createLocalVue } from '@vue/test-utils';
import ContractRequest from './ContractRequest';

const localVue = createLocalVue();

describe('ContractRequest.vue', () => {
  const wrapper = shallowMount(ContractRequest, {
    localVue,
    stubs: { 'nuxt-child': { template: '<div></div>' } },
  });

  it('should render the component', () => {
    expect(wrapper.find('.contract-request').exists()).toBe(true);
  });
});
