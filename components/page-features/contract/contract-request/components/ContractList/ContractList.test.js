import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import ContractList from './ContractList';
import localVueWithBuefy from '~/utils/addBuefy';

localVueWithBuefy.use(Vuex);

const stubs = {
  ContractRequestRejectModal: { template: '<div />' },
};

const flushPromises = () => {
  return new Promise((resolve) => setImmediate(resolve));
};

describe('ContractList.vue', () => {
  const roomOptions = [
    {
      id: 12345,
      song_id: 22222,
      name: 'Kos Singgah Sini',
      dbet_code: 'db123',
    },
    {
      id: 12346,
      song_id: 11111,
      name: 'Kos Mamirooms',
      dbet_code: 'db124',
    },
  ];

  const mockStore = {
    modules: {
      contract: {
        namespaced: true,
        state: {
          kosList: [...roomOptions],
          loading: false,
          kosListPagination: {
            total: 20,
            has_more: true,
          },
          contractRequest: {
            data: [],
          },
          rejectData: { fullname: 'Anak Kos' },
          rejectStatus: true,
        },
        mutations: {
          updateRejectStatus: jest.fn(),
        },
        actions: { getKosList: jest.fn().mockResolvedValue(), getContractRequestList: jest.fn() },
      },
    },
  };

  const $device = { isMobile: false };
  const $route = { meta: { pageTitle: '' }, query: {} };
  const $router = { replace: jest.fn() };
  const onRejectContract = jest.fn();
  const $bugsnag = { notify: jest.fn() };
  const $alert = jest.fn();
  const $api = {
    getKosListWithContractRequest: jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: roomOptions,
        pagination: {
          current_page: 1,
          has_more: true,
        },
      },
    }),
  };

  const mockElementFn = {
    addEventListener: jest.fn((_, fn) => fn()),
    removeEventListener: jest.fn((_, fn) => fn()),
    appendChild: jest.fn(),
    removeChild: jest.fn(),
  };

  const kosSelect = {
    $el: {
      querySelector: jest.fn(() => {
        return {
          clientHeight: 100,
          scrollTop: 200,
          scrollHeight: 300,
          ...mockElementFn,
        };
      }),
    },
  };

  const wrapper = shallowMount(ContractList, {
    localVue: localVueWithBuefy,
    store: new Vuex.Store(mockStore),
    mocks: { $device, $route, $router, $bugsnag, $api, $alert },
    stubs,
    methods: { onRejectContract },
  });

  it('should assign default selected kos', async () => {
    await flushPromises();
    expect(wrapper.vm.kosSelected).toBe(22222);
  });

  it('should set selected kos from query kos', async () => {
    wrapper.vm.$route.query.code = 'db124';
    wrapper.vm.initPage();
    await flushPromises();

    expect(wrapper.vm.kosSelected).toBe(11111);

    wrapper.setData({ kosSelected: 0 });
    wrapper.vm.$route.query.kos = 9999; // not exists
    wrapper.vm.initPage();
    await flushPromises();

    expect(wrapper.vm.kosSelected).toBe(11111);
  });

  it('should handle scrolled kos select properly', async () => {
    wrapper.vm.$refs.kosSelect = kosSelect;
    wrapper.vm.setKosSelectScrollAction();
    await wrapper.vm.$nextTick();

    expect(mockElementFn.addEventListener).toBeCalled();
    expect(mockElementFn.appendChild).toBeCalled();

    await flushPromises();
    expect(mockElementFn.removeChild).toBeCalled();
  });

  it('should set reject data properly when user want to reject', async () => {
    wrapper.vm.onRequestReject(123, 'List Penyewa');
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.rejectRequestId).toBe(123);
    expect(wrapper.vm.rejectFrom).toBe('List Penyewa');
  });

  it('should send reject request to the server', () => {
    wrapper.find(stubs.ContractRequestRejectModal).vm.$emit('reject');
    expect(onRejectContract).toBeCalled();
  });

  it('should remove scroll listener on kos select before destroyed', () => {
    jest.clearAllMocks();
    wrapper.vm.$refs.kosSelect = kosSelect;
    wrapper.vm.$destroy();

    expect(mockElementFn.removeEventListener).toBeCalled();
  });

  it('should set page value based on pagination active state', () => {
    wrapper.vm.setPaginationList(2);
    expect(wrapper.vm.contractListProperty.page).toBe(2);
  });
});
