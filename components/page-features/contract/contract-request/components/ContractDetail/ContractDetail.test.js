import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import ContractDetail from './ContractDetail';
import localVueWithBuefy from '~/utils/addBuefy';
import mockWindowProperty from '~/utils/mockWindowProperty';

localVueWithBuefy.use(Vuex);

mockWindowProperty('window.open', jest.fn());

describe('ContractDetail.vue', () => {
  const $route = { meta: { pageTitle: 'Penyewa Mengajukan Kontrak' }, params: { id: 5 } };
  const $device = { isMobile: false };
  const $router = { push: jest.fn() };
  const $tracker = { send: jest.fn() };

  const mockStore = {
    modules: {
      contract: {
        namespaced: true,
        state: {
          kosListData: {
            loadingKosList: false,
          },
          contractRequestDetail: {
            is_empty_room: true,
            is_revised_price: true,
          },
        },
        actions: {
          acceptContractRequest: jest.fn().mockResolvedValue({ tenantId: 123 }),
          getContractRequestDetail: jest.fn(),
        },
      },
      profile: {
        namespaced: true,
        state: {
          data: { user: { name: 'Mami Kos' } },
        },
      },
    },
  };

  const wrapper = shallowMount(ContractDetail, {
    localVue: localVueWithBuefy,
    store: new Vuex.Store(mockStore),
    mocks: { $route, $device, $router, $tracker },
  });

  it('should render the correct page title', () => {
    expect(wrapper.vm.pageTitle).toBe('Penyewa Mengajukan Kontrak');
  });

  it('should push the route to contract request accept page', () => {
    wrapper.vm.onRequestAccept();
    expect($router.push).toBeCalled();
  });
});
