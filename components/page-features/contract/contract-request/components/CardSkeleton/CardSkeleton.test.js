import { shallowMount } from '@vue/test-utils';
import CardSkeleton from './CardSkeleton';
import localVueWithBuefy from '~/utils/addBuefy';

describe('CardSkeleton.vue', () => {
  const $device = { isMobile: true };

  const wrapper = shallowMount(CardSkeleton, {
    localVue: localVueWithBuefy,
    mocks: { $device },
  });

  it('should render skeleton length for mobile screen', () => {
    expect(wrapper.vm.short).toBe('35%');
    expect(wrapper.vm.long).toBe('60%');
  });
});
