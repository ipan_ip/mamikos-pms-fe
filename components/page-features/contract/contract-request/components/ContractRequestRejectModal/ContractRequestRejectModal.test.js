import { createLocalVue, shallowMount } from '@vue/test-utils';
import { BgModal, BgButton } from 'bangul-vue';
import ContractRequestRejectModal from './ContractRequestRejectModal';

const localVue = createLocalVue();

describe('ContractRequestRejectModal.vue', () => {
  const wrapper = shallowMount(ContractRequestRejectModal, {
    localVue,
    propsData: {
      value: false,
      requestId: 0,
    },
    stubs: {
      BgModal,
      BgButton,
    },
  });

  it('should render contract request reject modal when value props is true and request id is given', async () => {
    await wrapper.setProps({ value: true, requestId: 123 });
    expect(wrapper.find('.contract-request-reject-modal').exists()).toBe(true);
  });

  it('should emit input with value false if modal closed', async () => {
    await wrapper.find(BgModal).vm.$emit('close');
    expect(wrapper.emitted('input')[0][0]).toBe(false);
  });

  it('should emit rejected and close modal when request successfully rejected', () => {
    jest.useFakeTimers();
    wrapper.find('.contract-request-reject-modal__action-confirm').trigger('click');

    jest.advanceTimersByTime(2000);
    jest.useRealTimers();

    expect(wrapper.emitted('reject')[0]).toBeTruthy();
  });
});
