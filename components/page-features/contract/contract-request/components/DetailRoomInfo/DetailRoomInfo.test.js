import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import DetailRoomInfo from './DetailRoomInfo';
import mockLazy from '~/utils/mocks/lazy';

const localVue = createLocalVue();
localVue.use(Vuex);
mockLazy(localVue);

describe('DetailRoomInfo.vue', () => {
  const mockStore = {
    modules: {
      contract: {
        namespaced: true,
        state: {
          contractRequestDetail: {
            room: { name: 'Kos SinggahSini', image: { small: 'http://mamikos.com' } },
          },
        },
      },
    },
  };

  const wrapper = shallowMount(DetailRoomInfo, { localVue, store: new Vuex.Store(mockStore) });

  it('should render the component', () => {
    expect(wrapper.find('.detail-room-info').exists()).toBe(true);
  });
});
