import { shallowMount, createLocalVue } from '@vue/test-utils';
import CardRequest from './CardRequest';

const localVue = createLocalVue();

describe('CardRequest.vue', () => {
  const propsData = {
    cardDetail: { fullname: 'Chris Evans', id: 123456 },
    kosId: 12345,
  };
  const wrapper = shallowMount(CardRequest, {
    localVue,
    propsData,
    stubs: {
      'router-link': {
        template: `<a />`,
      },
    },
    mocks: {
      $device: { isMobile: true },
    },
  });

  it('should render the correct component', () => {
    expect(wrapper.find('.card-request').exists()).toBe(true);
  });

  it('should return the correct accept params', () => {
    expect(wrapper.vm.acceptParams.contractId).toBe(123456);
    expect(wrapper.vm.acceptParams.requestProps.designer_room_id).toBe(12345);
  });
});
