import Cookies from 'js-cookie';
import { mapState, mapMutations } from 'vuex';
import _get from 'lodash/get';

export default {
  data() {
    return {
      rejectRequestId: 0,
      isShowRejectModal: false,
      isShowRejectToastSuccess: false,
      isShowRejectToastFailed: false,
      rejectFrom: '',
    };
  },

  computed: {
    ...mapState('contract', ['rejectLoading', 'rejectStatus', 'rejectData', 'contractRequest']),
    ...mapState('profile', { userData: (state) => state.data.user }),
  },

  watch: {
    isShowRejectModal(isShow) {
      if (!isShow) {
        this.rejectRequestId = 0;
        this.rejectFrom = '';
      }
    },
  },

  methods: {
    ...mapMutations('contract', ['updateRejectStatus']),
    onRequestReject(requestId = 0, rejectFrom) {
      this.rejectRequestId = requestId;
      this.rejectFrom = rejectFrom;
      this.isShowRejectModal = true;
    },
    onRejectContract() {
      this.$store.dispatch('contract/rejectContractRequest', this.rejectRequestId).then(() => {
        if (this.rejectStatus) {
          this.trackRejectContract();
          this.isShowRejectModal = false;
          this.redirectPage();
        } else {
          this.onFailedRejected();
        }
      });
    },
    redirectPage() {
      if (
        (this.$route.name !== 'contract-request-list' && this.contractRequest.data.length <= 1) ||
        this.contractRequest.data.length <= 1
      ) {
        Cookies.set('dbet-contract-reject', 'success');
        this.$router.push('/');
      } else if (
        this.$route.name === 'contract-request-list' &&
        this.contractRequest.data.length > 1
      ) {
        this.$router.go();
      } else {
        this.$router.replace({ name: 'contract-request-list' });
      }
    },
    onSuccessRejected() {
      this.isShowRejectToastFailed = false;
      this.isShowRejectToastSuccess = true;
      this.updateRejectStatus(false);
    },
    onFailedRejected() {
      this.isShowRejectToastSuccess = false;
      this.isShowRehectToastFailed = true;
    },
    trackRejectContract() {
      this.$tracker.send('moe', [
        '[Owner] DBET Tenant Options - Reject New Tenant',
        {
          property_type: 'kost',
          goldplus_status: _get(this.rejectData, 'room.is_goldplus', false),
          is_mamirooms: _get(this.rejectData, 'room.is_mamirooms', false),
          tenant_job: _get(this.rejectData, 'job', ''),
          tenant_name: _get(this.rejectData, 'fullname', ''),
          tenant_phone_number: _get(this.rejectData, 'phone', ''),
          tenant_email: _get(this.rejectData, 'tenant.email', ''),
          tenant_gender: _get(this.rejectData, 'gender', ''),
          property_id: _get(this.rejectData, 'room._id', ''),
          owner_name: _get(this.userData, 'name', ''),
          owner_phone_number: _get(this.userData, 'phone_number', ''),
          owner_email: _get(this.userData, 'email', ''),
          registration_id: _get(this.rejectData, 'id', 0),
          registration_date: new Date(this.rejectData.created_at),
          reject_from: 'Detail Tenant',
        },
      ]);
    },
  },
};
