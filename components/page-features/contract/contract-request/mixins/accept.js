import { mapState } from 'vuex';
import _get from 'lodash/get';
import _map from 'lodash/map';

export default {
  data() {
    return {
      isModalRejectRevisedPrice: false,
      isShowAcceptToast: false,
      mamikosUrl: process.env.MAMIKOS_URL,
    };
  },

  computed: {
    ...mapState('contract', [
      'contractRequestDetail',
      'acceptStatus',
      'acceptData',
      'acceptFailedMessage',
    ]),
    ...mapState('profile', { userData: (state) => state.data.user }),
  },

  methods: {
    toggleModalRevisedPrice(state) {
      this.isModalRejectRevisedPrice = state;
    },
    onAcceptContract(acceptParams) {
      this.toggleModalRevisedPrice(false);
      this.$store.dispatch('contract/acceptContractRequest', acceptParams).then(() => {
        if (this.acceptStatus) {
          this.trackAcceptContract(
            this.acceptData,
            acceptParams.requestProps.price_type,
            acceptParams.requestProps.from,
          );
          window.open(
            `${this.mamikosUrl}/ownerpage/billing-management/profile-tenant/${this.acceptData.contract_id}/payment-tenant?from=accept-contract`,
            '_self',
          );
        } else {
          this.toggleModalRevisedPrice(false);
          this.isShowAcceptToast = true;
        }
      });
    },
    trackAcceptContract(params, priceType, confirmFrom) {
      this.$tracker.send('moe', [
        '[Owner] DBET Tenant Options - Confirm New Tenant',
        {
          registration_id: _get(params, 'id', 0),
          property_id: _get(this.contractRequestDetail, 'room._id', 0),
          propery_type: 'kost',
          property_name: _get(params, 'room.name', ''),
          property_city: _get(params, 'room.city', ''),
          tenant_name: _get(params, 'fullname', ''),
          tenant_phone_number: _get(params, 'phone', ''),
          due_date: _get(params, 'due_date', ''),
          tenant_id_photo: _get(params, 'identity.medium', ''),
          tenant_gender: _get(params, 'gender', ''),
          tenant_job: _get(params, 'job', ''),
          tenant_email: _get(params, 'tenant.email', ''),
          room_id: _get(this.contractRequestDetail, 'room._id', 0),
          property_rent_type: _get(params, 'rent_count_type', ''),
          rent_price: priceType === 'original' ? params.original_price : params.revised_price,
          owner_phone_number: _get(this.userData, 'phone_number', ''),
          owner_email: _get(this.userData, 'email', ''),
          registration_date: new Date(params.created_at),
          confirm_from: confirmFrom,
          other_price_name: _map(params.additional_price, 'name'),
          other_price: _map(params.additional_price, 'price'),
          goldplus_status: _get(params, 'room.is_goldplus', false),
          is_mamirooms: _get(params, 'room.is_mamirooms', false),
          is_rent_price_owner: priceType === 'original',
        },
      ]);
    },
  },
};
