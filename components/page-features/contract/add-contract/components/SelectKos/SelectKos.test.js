import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import { BgCard, BgIcon, BgText } from 'bangul-vue';
import SelectKos from './SelectKos';
import localVueWithBuefy from '~/utils/addBuefy';

localVueWithBuefy.use(Vuex);
localVueWithBuefy.use({ BgCard, BgIcon, BgText });

describe('SelectKos.vue', () => {
  const mockStore = {
    modules: {
      contract: {
        namespaced: true,
        state: { kosSelected: {}, kosListData: { loadingKosList: false } },
        mutations: {
          updateKosSelected(state, kos) {
            state.kosSelected = kos;
          },
        },
        actions: {
          getKosList() {
            return jest.fn();
          },
        },
      },
    },
  };

  const $router = { push: jest.fn() };
  const $tracker = { send: jest.fn() };
  const $device = { isMobile: false };

  const wrapper = shallowMount(SelectKos, {
    localVue: localVueWithBuefy,
    store: new Vuex.Store(mockStore),
    mocks: { $router, $tracker, $device },
    stubs: { BgCard, BgIcon, BgText },
  });

  it('should save selected kost', () => {
    wrapper.vm.selectKos({ room_title: 'Kos Singgah Sini' });
    expect(mockStore.modules.contract.state.kosSelected.room_title).toBe('Kos Singgah Sini');
    expect($router.push).toBeCalledWith('/add-tenant/from-tenant/setting');
  });
});
