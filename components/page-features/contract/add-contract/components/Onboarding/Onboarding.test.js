import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import Onboarding from './Onboarding';
import localVueWithBuefy from '~/utils/addBuefy';

localVueWithBuefy.use(Vuex);

describe('Onboarding.vue', () => {
  const $router = { push: jest.fn() };

  const mockStore = {
    modules: {
      contract: {
        namespaced: true,
        state: {
          contractCardProperties: {
            dynamic_onboarding_label: 'Mamikos Aplikasi Pencarian Kos No. 1',
          },
        },
      },
    },
  };

  const wrapper = shallowMount(Onboarding, {
    localVue: localVueWithBuefy,
    store: new Vuex.Store(mockStore),
    mocks: { $router },
  });

  it('should go to the previous slide', () => {
    wrapper.vm.pageActive = 1;
    wrapper.vm.goPrevious();
    expect(wrapper.vm.pageActive).toBe(0);
  });

  it('should go to the next slide', () => {
    wrapper.vm.pageActive = 1;
    wrapper.vm.goNext();
    expect(wrapper.vm.pageActive).toBe(2);
  });

  it('should go to the next page', () => {
    wrapper.vm.pageActive = 4;
    wrapper.vm.goNext();
    expect($router.push).toBeCalledWith({
      name: 'add-tenant-select-method',
      params: { redirection_source: 'dashboard_kontrak_penyewa' },
    });
  });
});
