import { shallowMount } from '@vue/test-utils';
import { BgCard, BgDivider, BgIcon, BgText } from 'bangul-vue';
import ContractTypeBox from '../ContractTypeBox';
import localVueWithBuefy from '~/utils/addBuefy';

localVueWithBuefy.use({ BgCard, BgDivider, BgIcon, BgText });

describe('ContractTypeBox.vue', () => {
  const wrapper = shallowMount(ContractTypeBox, {
    localVue: localVueWithBuefy,
    propsData: {
      title: 'Minta penyewa untuk mengisi',
      content: 'Bagikan link dan minta penyewa Anda untuk mengisi data mereka dengan cara:',
    },
  });

  it('should emit an action when the box is clicked', () => {
    wrapper.vm.selectCard();
    expect(wrapper.emitted('select')).toBeTruthy();
  });
});
