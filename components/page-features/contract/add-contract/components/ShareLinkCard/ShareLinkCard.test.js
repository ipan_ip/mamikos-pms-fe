import { shallowMount } from '@vue/test-utils';
import ShareLinkCard from './ShareLinkCard';
import localVueWithBuefy from '~/utils/addBuefy';

window.open = jest.fn();

describe('ShareLinkCard.vue', () => {
  const shareUrl = 'mamikos.com/dbet-XXXX';
  const shareContent = 'Isi form ini yuk';

  const wrapper = shallowMount(ShareLinkCard, {
    localVue: localVueWithBuefy,
    propsData: { shareUrl, shareContent },
  });

  it('should open facebook share', () => {
    wrapper.vm.shareLink('facebook');
    expect(window.open).toBeCalledWith(
      'https://www.facebook.com/sharer/sharer.php?u=mamikos.com/dbet-XXXX',
      'facebook-popup',
    );
  });

  it('should open twitter share', () => {
    wrapper.vm.shareLink('twitter');
    expect(window.open).toBeCalledWith(
      'https://twitter.com/share?url=mamikos.com/dbet-XXXX',
      'twitter-popup',
    );
  });

  it('should open linkedin share', () => {
    wrapper.vm.shareLink('linkedin');
    expect(window.open).toBeCalledWith(
      'https://www.linkedin.com/cws/share?url=mamikos.com/dbet-XXXX',
    );
  });

  it('should open whatsapp share', () => {
    wrapper.vm.shareLink('linkedin');
    expect(window.open).toBeCalled();
  });
});
