import { shallowMount } from '@vue/test-utils';
import SelectKosSkeleton from './SelectKosSkeleton';
import localVueWithBuefy from '~/utils/addBuefy';

describe('SelectKosSkeleton.vue', () => {
  const wrapper = shallowMount(SelectKosSkeleton, {
    localVue: localVueWithBuefy,
  });

  it('should render skeleton for the kos box', () => {
    const titleSkeleton = wrapper.find('.select-kos-skeleton__content');
    expect(titleSkeleton.exists()).toBe(true);
  });
});
