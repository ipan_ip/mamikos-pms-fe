import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import ShareLink from './ShareLink';
import localVueWithBuefy from '~/utils/addBuefy';

localVueWithBuefy.use(Vuex);

const share = jest.fn();

global.document.getElementById = jest.fn().mockReturnValue({
  select: jest.fn().mockReturnValue([]),
});
global.document.execCommand = jest.fn().mockReturnValue(true);

describe('ShareLink.vue', () => {
  const $device = { isMobile: false };
  const $buefy = { toast: { open: jest.fn() } };
  const $tracker = { send: jest.fn() };
  const $router = { push: jest.fn() };

  const mockStore = {
    modules: {
      contract: {
        namespaced: true,
        state: {
          kosSelected: {
            room_title: 'Kos Singgah Sini',
          },
          kosLinkShareOptions: { code: 'XXXX' },
        },
      },
    },
  };

  const generateWrapper = ({ methodsData = {} } = {}) => {
    const mountProp = {
      localVue: localVueWithBuefy,
      store: new Vuex.Store(mockStore),
      mocks: { $buefy, $device, $tracker, $router },
      methods: { ...methodsData },
    };

    return shallowMount(ShareLink, mountProp);
  };

  it('should contain input with share link value', () => {
    const wrapper = generateWrapper();
    const linkElement = wrapper.find('#shareLinkAddContract');
    expect(linkElement.exists()).toBe(true);
  });

  it('should call the toast', () => {
    const wrapper = generateWrapper();
    wrapper.vm.copyLink();
    expect($buefy.toast.open).toBeCalled();
  });

  it('should copy the link', () => {
    const copyLink = jest.fn();
    global.navigator = 'undefined';
    $device.isMobile = true;
    const wrapper = generateWrapper({ methodsData: { copyLink } });
    wrapper.vm.shareAddContractLink();
    expect(copyLink).toBeCalled();
  });

  it('should open native share', () => {
    window.navigator.share = share;
    $device.isMobile = true;
    const wrapper = generateWrapper();
    wrapper.vm.shareAddContractLink();
    expect(navigator.share).toBeCalled();
  });

  it('should push the router to the main page of owner dashboard', () => {
    $device.isMobile = false;
    const wrapper = generateWrapper();
    wrapper.vm.shareAddContractLink();
    expect($router.push).toBeCalledWith('/');
  });
});
