import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import ContractType from '../ContractType';
import localVueWithBuefy from '~/utils/addBuefy';

localVueWithBuefy.use(Vuex);
global.window = Object.create(window);
const url = 'mamikos.com';

Object.defineProperty(window, 'location', {
  value: { href: url },
});

const $router = { push: jest.fn() };
const $tracker = { send: jest.fn() };
const $device = { isMobile: false };

describe('ContractType.vue', () => {
  const storeData = {
    modules: {
      contract: {
        namespaced: true,
        state: { contractType: '' },
        mutations: {
          updateContractType(state, type) {
            state.contractType = type;
          },
        },
      },
      profile: {
        namespaced: true,
        getters: {
          isBbk: () => false,
          isPremium: () => true,
          user: () => {
            return {
              user_id: 123,
            };
          },
          membership: () => {
            return {
              is_mamipay_user: true,
            };
          },
        },
      },
    },
  };

  const store = new Vuex.Store(storeData);
  const $route = { params: { redirection_source: 'deeplink' } };

  const generateWrapper = ({ methodsData = {} } = {}) => {
    const mountProp = {
      localVue: localVueWithBuefy,
      store,
      methods: { ...methodsData },
      mocks: { $router, $tracker, $device, $route },
    };

    return shallowMount(ContractType, mountProp);
  };

  it('should go to form contract fill by tenant', () => {
    const wrapper = generateWrapper();
    wrapper.vm.selectBox('tenant');
    expect($router.push).toBeCalledWith('/add-tenant/from-tenant/choose-kos');
  });

  it('should call function to handle form filling by owner', () => {
    const handleAddTenantByOwner = jest.fn();
    const wrapper = generateWrapper({ methodsData: { handleAddTenantByOwner } });

    wrapper.vm.selectBox('owner');
    expect(handleAddTenantByOwner).toBeCalled();
  });

  it('should call function to send tracker when owner choose to fill the form by himself', () => {
    const trackAddTenantByOwner = jest.fn();
    const wrapper = generateWrapper({ methodsData: { trackAddTenantByOwner } });

    wrapper.vm.handleAddTenantByOwner();
    expect(trackAddTenantByOwner).toBeCalled();
  });

  it('should open add tenant form filled by owner', () => {
    process.env.MAMIKOS_URL = 'mamikos.com';
    storeData.modules.profile.getters = {
      isBbk: () => true,
    };
    const wrapper = generateWrapper();

    wrapper.vm.handleAddTenantByOwner();
    expect(window.location.href).toBe('mamikos.com/ownerpage/manage/add-tenant/select-kost');
  });
});
