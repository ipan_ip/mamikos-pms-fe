import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import { BgButton, BgDivider, BgText, BgSelect, BgSwitch } from 'bangul-vue';
import FormSetting from './FormSetting';
import localVueWithBuefy from '~/utils/addBuefy';

localVueWithBuefy.use({ BgButton, BgDivider, BgText, BgSelect, BgSwitch });
localVueWithBuefy.use(Vuex);

describe('FormSetting.vue', () => {
  const $router = { push: jest.fn() };
  const $tracker = { send: jest.fn() };
  const $device = { isMobile: false };
  const $store = { commit: jest.fn() };
  const $api = {
    sendFormSetup: jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: {
          id: 1,
          code: 'A89LDX',
          designer_id: 4238423,
          is_required_identity: 1,
          is_required_due_date: 1,
          due_date: 1,
        },
      },
    }),
  };

  const mockStore = {
    modules: {
      contract: {
        namespaced: true,
        state: {
          kosSelected: { id: 4238423 },
          isCardActive: false,
          isDueDateActive: false,
          dueDate: 1,
        },
        mutations: {
          updateKosLinkShareOption: jest.fn(),
          updateCardState(state, isActive) {
            state.isCardActive = isActive;
          },
          updateDueDate(state, date) {
            state.dueDate = date;
          },
          updateDueDateState(state, isActive) {
            state.isDueDateActive = isActive;
          },
        },
      },
    },
  };

  const generateWrapper = () => {
    const mountProp = {
      localVue: localVueWithBuefy,
      store: new Vuex.Store(mockStore),
      mocks: { $router, $tracker, $device, $api, $store },
      stubs: { BgButton, BgDivider, BgText, BgSelect, BgSwitch },
    };

    return shallowMount(FormSetting, mountProp);
  };

  it('should show id card toggle section', () => {
    const wrapper = generateWrapper();
    expect(wrapper.find('.form-setting__id-card').exists()).toBe(true);
  });

  it('should go to the next share link page', () => {
    const wrapper = generateWrapper();
    wrapper.vm.sendFormSetup();
    expect($api.sendFormSetup).toBeCalled();
  });

  it('should turn error flag to true when api call return false status', async () => {
    const wrapper = generateWrapper();
    wrapper.vm.$api.sendFormSetup = jest.fn().mockResolvedValue({
      data: {
        status: false,
      },
    });
    await wrapper.vm.sendFormSetup();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isSubmitError).toBe(true);
  });

  it('should turn error flag to true when api call return error', async () => {
    const wrapper = generateWrapper();
    wrapper.vm.$api.sendFormSetup = jest.fn().mockRejectedValue('error');
    await wrapper.vm.sendFormSetup();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isSubmitError).toBe(true);
  });

  it('should update the id card state on store', () => {
    const wrapper = generateWrapper();
    wrapper.setData({ isIdCard: true });
    expect(mockStore.modules.contract.state.isCardActive).toBe(true);
  });

  it('should update the id due date state on store', () => {
    const wrapper = generateWrapper();
    wrapper.setData({ isDueDate: true });
    expect(mockStore.modules.contract.state.isDueDateActive).toBe(true);
  });

  it('should update the id due date value on store', () => {
    const wrapper = generateWrapper();
    wrapper.setData({ dueDateVal: 10 });
    expect(mockStore.modules.contract.state.dueDate).toBe(10);
  });
});
