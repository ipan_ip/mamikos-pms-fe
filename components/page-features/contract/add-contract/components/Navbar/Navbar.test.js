import { shallowMount } from '@vue/test-utils';
import Navbar from './Navbar';
import localVueWithBuefy from '~/utils/addBuefy';

describe('Navbar.vue', () => {
  process.env.MAMIKOS_URL = 'mamikos.com';

  const $device = {
    isMobile: false,
  };

  const $route = { meta: { pageTitle: '' }, name: 'add-tenant-onboarding' };
  const $router = { go: jest.fn() };

  const wrapper = shallowMount(Navbar, {
    localVue: localVueWithBuefy,
    mocks: { $device, $route, $router },
  });

  it('should have the correct attribute value on the logo', () => {
    const logo = wrapper.find('a');
    expect(logo.element.getAttribute('href')).toBe('mamikos.com');
  });

  it('should go to previous route', () => {
    wrapper.vm.backRoute();
    expect($router.go).toBeCalledWith(-1);
  });
});
