import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex, { mapState } from 'vuex';
import AddContract from './AddContract';

const localVue = createLocalVue();
localVue.use(Vuex);
global.mapState = mapState;

describe('AddContract.vue', () => {
  const mockStore = {
    modules: {
      contract: {
        namespaced: true,
        state: {
          kosSelected: {},
          contractCardProperties: {},
        },
        actions: {
          getContractAbTest: () => jest.fn(),
        },
      },
    },
  };

  const $route = { name: 'add-tenant-share-link' };
  const $router = { push: jest.fn() };

  const createComponent = () => {
    const mountData = {
      localVue,
      store: new Vuex.Store(mockStore),
      mocks: { $route, $router },
      stubs: { 'nuxt-child': { template: '<div></div>' } },
    };

    return shallowMount(AddContract, mountData);
  };

  it('should push the route to select add contract type page', () => {
    const wrapper = createComponent();
    expect(wrapper.find('.tenant-contract').exists()).toBe(true);
    expect($router.push).toBeCalledWith({ name: 'add-tenant-select-method' });
  });
});
