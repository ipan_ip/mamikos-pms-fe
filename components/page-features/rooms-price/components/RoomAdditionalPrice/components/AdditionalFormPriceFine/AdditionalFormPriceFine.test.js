import { shallowMount } from '@vue/test-utils';
import AdditionalFormPriceFine from './AdditionalFormPriceFine';
import localVueWithBuefy from '~/utils/addBuefy';
import MkButton from '~/components/global/atoms/MkButton';

describe('AdditionalFormPriceFine.vue', () => {
  const createWrapper = (props = {}) => {
    return shallowMount(AdditionalFormPriceFine, {
      localVue: localVueWithBuefy,
      propsData: { ...props, kostDetail: {} },
      stubs: { MkButton },
    });
  };

  it('should render form input fine', () => {
    const wrapper = createWrapper();
    expect(wrapper.find('.form-input-fine').exists()).toBe(true);
  });

  it('should set initial data if given from props', () => {
    const wrapper = createWrapper({
      value: {
        price: 1000,
        meta: [
          { key: 'fine_maximum_length', value: '1' },
          { key: 'fine_duration_type', value: 'day' },
        ],
      },
    });
    expect(wrapper.vm.fineVal).toBe(1000);
    expect(wrapper.vm.fineDuration).toBe('1');
    expect(wrapper.vm.fineCount).toBe('day');
  });

  it('should change fine count', () => {
    const wrapper = createWrapper();
    wrapper.vm.onChangeFineCount('week');
    expect(wrapper.vm.fineCount).toBe('week');
  });

  it('should set fine duration properly', () => {
    const wrapper = createWrapper();
    wrapper.vm.onChangeFineCount('week');
    wrapper.vm.onChangeFineDuration('50');
    expect(wrapper.vm.fineDuration).toBe('4');

    wrapper.vm.onChangeFineCount('month');
    wrapper.vm.onChangeFineDuration('0');
    expect(wrapper.vm.fineDuration).toBe('1');

    wrapper.vm.onChangeFineCount('daily');
    wrapper.vm.onChangeFineDuration('3');
    expect(wrapper.vm.fineDuration).toBe('3');
  });

  it('should emit submit when save button clicked', () => {
    const wrapper = createWrapper({
      value: {
        id: 1,
        price: 1000,
        meta: [
          { key: 'fine_maximum_length', value: '1' },
          { key: 'fine_duration_type', value: 'day' },
        ],
      },
      type: 'fine',
    });

    wrapper.find('button').trigger('click');
    expect(wrapper.emitted('submit')[0]).toEqual([
      'fine',
      { type: 'fine', price: 1000, fine_maximum_length: 1, fine_duration_type: 'day' },
      1,
    ]);
  });
});
