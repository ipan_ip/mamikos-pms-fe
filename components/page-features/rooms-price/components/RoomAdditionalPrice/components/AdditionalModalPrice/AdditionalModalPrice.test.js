import { shallowMount } from '@vue/test-utils';
import AdditionalModalPrice from './AdditionalModalPrice';
import localVueWithBuefy from '~/utils/addBuefy';

describe('AdditionalModalPrice.vue', () => {
  const wrapper = shallowMount(AdditionalModalPrice, {
    localVue: localVueWithBuefy,
    propsData: {
      active: true,
      cardTitle: 'Biaya Lain',
    },
  });

  it('should render the correct title', () => {
    const modalTitle = wrapper.find('.card-title');
    expect(modalTitle.text()).toBe('Biaya Lain');
  });

  it('should emit the close action when close icon is clicked', () => {
    const buttonClose = wrapper.find('.icon-close');
    buttonClose.trigger('click');
    expect(wrapper.emitted('close')).toBeTruthy();
  });
});
