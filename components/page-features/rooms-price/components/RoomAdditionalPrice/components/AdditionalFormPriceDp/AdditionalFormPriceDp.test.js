import { shallowMount } from '@vue/test-utils';
import AdditionalFormPriceDp from './AdditionalFormPriceDp';
import localVueWithBuefy from '~/utils/addBuefy';
import MkButton from '~/components/global/atoms/MkButton';

describe('AdditionalFormPriceFine.vue', () => {
  const createWrapper = (props = {}) => {
    return shallowMount(AdditionalFormPriceDp, {
      localVue: localVueWithBuefy,
      propsData: {
        ...props,
        kostDetail: {
          price_monthly: 1000000,
          price_3_month: 3000000,
          price_6_month: 6000000,
          price_yearly: 12000000,
          price_weekly: 250000,
        },
      },
      stubs: { MkButton },
    });
  };

  it('should assign down payment value', () => {
    const wrapper = createWrapper();
    wrapper.vm.onChangeDp(50);
    expect(wrapper.vm.dpVal).toBe(50);
  });

  it('should render form input dp', () => {
    const wrapper = createWrapper();
    expect(wrapper.find('.form-price-dp').exists()).toBe(true);
  });

  it('should set initial data if given from props', () => {
    const wrapper = createWrapper({
      value: {
        meta: [{ key: 'percentage', value: 20 }],
      },
    });
    expect(wrapper.vm.dpVal).toBe(20);
  });

  it('should emit submit when save button clicked', () => {
    const wrapper = createWrapper({
      value: {
        id: 5,
        meta: [{ key: 'percentage', value: 20 }],
      },
      type: 'dp',
    });

    wrapper.find('button').trigger('click');
    expect(wrapper.emitted('submit')[0]).toEqual(['dp', { type: 'dp', percentage: 20 }, 5]);
  });

  it('should assign focus state', () => {
    const wrapper = createWrapper();
    wrapper.vm.isFocus = false;
    wrapper.vm.handleFocus(true);
    expect(wrapper.vm.isFocus).toBe(true);
  });
});
