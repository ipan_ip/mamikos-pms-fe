import { shallowMount, createLocalVue } from '@vue/test-utils';
import AdditionalPriceModalDelete from './AdditionalPriceModalDelete';
import MkButton from '~/components/global/atoms/MkButton';

const localVue = createLocalVue();

describe('AdditionalPriceModalDelete.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(AdditionalPriceModalDelete, {
      localVue,
      stubs: {
        MkButton,
      },
    });
  });

  it('should render additional modal delete', () => {
    expect(wrapper.find('.additional-price-modal-delete').exists()).toBe(true);
  });

  it('could be closed when it is not loading', () => {
    wrapper.find('.additional-price-modal-delete__action-button-back').trigger('click');
    expect(wrapper.emitted('close')).toBeTruthy();
  });

  it('could not be closed when it is loading', () => {
    wrapper.setProps({ isLoading: true });
    wrapper.find('.additional-price-modal-delete__action-button-back').trigger('click');
    expect(wrapper.emitted('close')).toBeFalsy();
  });
});
