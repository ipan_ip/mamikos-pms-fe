import { shallowMount } from '@vue/test-utils';
import AdditionalPriceItem from './AdditionalPriceItem';
import localVueWithBuefy from '~/utils/addBuefy';

const data = [
  {
    id: 1,
    type: 'additional',
    name: 'Iuran Sampah',
    price: 30000,
    price_label: 'Rp30.000',
    meta: null,
  },
  {
    id: 2,
    type: 'additional',
    name: 'Iuran Air',
    price: 50000,
    price_label: 'Rp50.000',
    meta: null,
  },
];

describe('AdditionalPriceItem.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(AdditionalPriceItem, {
      localVue: localVueWithBuefy,
      propsData: {
        data,
        label: 'Biaya Tambahan',
        isActive: false,
        isMany: true,
        index: 0,
        kostId: 1,
        type: 'additional',
        kostDetail: {},
      },
      mocks: {
        $alert: jest.fn(),
        $buefy: { toast: { open: jest.fn() } },
        $bugsnag: { notify: jest.fn() },
        $api: {
          postPriceComponentActivation: jest
            .fn()
            .mockResolvedValueOnce({
              data: { status: true, meta: { message: 'OK' } },
            })
            .mockResolvedValueOnce({
              data: { status: false, meta: { message: 'FAIL' } },
            })
            .mockRejectedValueOnce(new Error('error')),
        },
      },
    });
  });

  it('should render additional price item', () => {
    expect(wrapper.find('.additional-price-item').exists()).toBe(true);
  });

  it('should set initial render properly', () => {
    expect(wrapper.vm.isActiveData).toBe(false);
    expect(wrapper.find('.additional-price-item__value').exists()).toBe(false);
    expect(wrapper.find('.additional-price-item__toggle-label').text()).toBe('Biaya Tambahan');
  });

  it('should render all additional price value if is active', async () => {
    wrapper.setProps({ isActive: true });
    await wrapper.vm.$nextTick();

    expect(wrapper.find('.additional-price-item__value').exists()).toBe(true);
    expect(wrapper.findAll('.additional-price-item__card').length).toBe(2);
  });

  it('should emit active when toggle active changed', () => {
    wrapper.setData({ isActiveComputed: false });
    expect(wrapper.emitted('active')[0][0]).toBe(false);

    wrapper.setData({ isActiveComputed: true });
    expect(wrapper.emitted('active')[1][0]).toBe(true);
  });

  it('should emit add when activating and no data exists', () => {
    wrapper.setProps({ data: [] });
    wrapper.setData({ isActiveComputed: true });

    expect(wrapper.emitted('active')[0][0]).toBe(true);
    expect(wrapper.emitted('add')[0]).toBeTruthy();
  });

  it('should emit edit when edit button clicked', async () => {
    await wrapper.setProps({ isActive: true });
    const firstItem = wrapper.findAll('.additional-price-item__card').at(0);
    const deleteButton = firstItem.find('button');
    deleteButton.trigger('click');

    expect(wrapper.emitted('edit')[0][0]).toEqual({ ...data[0], index: 0, key: 0 });
  });

  it('should emit delete when delete button clicked', async () => {
    await wrapper.setProps({ isActive: true });
    const firstItem = wrapper.findAll('.additional-price-item__card').at(0);
    const deleteButton = firstItem.find('.additional-price-item__action-separator + button');
    deleteButton.trigger('click');

    expect(wrapper.emitted('delete')[0][0]).toEqual({ ...data[0], index: 0, key: 0 });
  });

  it('should activation properly', async () => {
    const toast = jest.spyOn(wrapper.vm.$buefy.toast, 'open');
    const notify = jest.spyOn(wrapper.vm.$bugsnag, 'notify');
    const $alert = jest.spyOn(wrapper.vm, '$alert');

    wrapper.setProps({ isActive: false });

    // Response status true
    await wrapper.vm.handleActivation(true);
    expect(toast).toBeCalledWith(expect.objectContaining({ message: 'OK' }));

    // Response status false
    await wrapper.vm.handleActivation(false);
    expect(toast).toBeCalledWith(expect.objectContaining({ message: 'FAIL' }));

    // FAILED
    await wrapper.vm.handleActivation(false);
    expect(notify).toBeCalled();
    expect($alert).toBeCalled();

    [toast, notify, $alert].forEach((spy) => {
      spy.mockRestore();
    });
  });

  it('should set default data to empty array if no data props given', () => {
    const wrapper = shallowMount(AdditionalPriceItem, {
      localVue: localVueWithBuefy,
      propsData: {
        label: 'Biaya Tambahan',
        isActive: false,
        isMany: true,
        index: 0,
        kostId: 1,
        type: 'additional',
        kostDetail: {},
      },
      mocks: {
        $alert: jest.fn(),
        $buefy: { toast: { open: jest.fn() } },
        $bugsnag: { notify: jest.fn() },
        $api: {
          postPriceComponentActivation: jest.fn(),
        },
      },
    });

    expect(wrapper.vm.data).toEqual([]);
  });

  it('should set price label properly', async () => {
    const wrapper = shallowMount(AdditionalPriceItem, {
      localVue: localVueWithBuefy,
      propsData: {
        label: 'Dp',
        isRange: true,
        isActive: true,
        isMany: true,
        index: 0,
        kostId: 1,
        type: 'additional',
        kostDetail: {
          price_monthly: 1000000,
          price_3_month: 3000000,
          price_6_month: 6000000,
          price_yearly: 12000000,
          price_weekly: 250000,
        },
        data: [
          {
            id: 5,
            type: 'dp',
            name: 'DP 10%',
            price: 0,
            price_label: 'Rp0',
            meta: [
              {
                key: 'percentage',
                value: '10',
              },
            ],
          },
        ],
      },
      mocks: {
        $alert: jest.fn(),
        $buefy: { toast: { open: jest.fn() } },
        $bugsnag: { notify: jest.fn() },
        $api: {
          postPriceComponentActivation: jest.fn(),
        },
      },
    });

    const getPriceLabel = () => wrapper.findAll('.additional-price-item__info-price').at(0);
    let priceLabel = getPriceLabel();
    expect(priceLabel.text()).toBe('Rp25.000-Rp1.200.000');

    wrapper.setProps({
      kostDetail: {
        price_monthly: 0,
        price_3_month: 0,
        price_6_month: 0,
        price_yearly: 0,
        price_weekly: 250000,
      },
    });
    await wrapper.vm.$nextTick();
    priceLabel = getPriceLabel();
    expect(priceLabel.text()).toBe('Rp25.000');

    wrapper.setProps({
      kostDetail: {
        price_monthly: 0,
        price_3_month: 0,
        price_6_month: 0,
        price_yearly: 0,
        price_weekly: 0,
      },
    });
    await wrapper.vm.$nextTick();
    priceLabel = getPriceLabel();
    expect(priceLabel.text()).toBe('Rp0');
  });
});
