import { shallowMount } from '@vue/test-utils';
import AdditionalFormPriceDeposit from './AdditionalFormPriceDeposit';
import localVueWithBuefy from '~/utils/addBuefy';
import MkButton from '~/components/global/atoms/MkButton';

describe('AdditionalFormPriceDeposit.vue', () => {
  const createWrapper = (props = {}) => {
    return shallowMount(AdditionalFormPriceDeposit, {
      localVue: localVueWithBuefy,
      propsData: { ...props, kostDetail: {} },
      stubs: { MkButton },
    });
  };

  it('should render form input deposit', () => {
    const wrapper = createWrapper();
    expect(wrapper.find('.form-deposit-wrapper').exists()).toBe(true);
  });

  it('should set initial data if given from props', () => {
    const wrapper = createWrapper({ value: { price: 10000 } });
    expect(wrapper.vm.depositVal).toBe(10000);
  });

  it('should emit submit when save button clicked', () => {
    const wrapper = createWrapper({
      value: { id: 2, price: 10000 },
      type: 'deposit',
    });

    wrapper.find('button').trigger('click');
    expect(wrapper.emitted('submit')[0]).toEqual(['deposit', { type: 'deposit', price: 10000 }, 2]);
  });
});
