import { shallowMount } from '@vue/test-utils';
import AdditionalFormPriceOther from './AdditionalFormPriceOther';
import localVueWithBuefy from '~/utils/addBuefy';
import MkButton from '~/components/global/atoms/MkButton';

describe('AdditionalFormPriceOther.vue', () => {
  const createWrapper = (props = {}) => {
    return shallowMount(AdditionalFormPriceOther, {
      localVue: localVueWithBuefy,
      propsData: { ...props, kostDetail: {} },
      stubs: { MkButton },
    });
  };

  it('should render form input wrapper', () => {
    const wrapper = createWrapper();
    expect(wrapper.find('.form-input-wrapper').exists()).toBe(true);
  });

  it('should set initial data if given from props', () => {
    const wrapper = createWrapper({ value: { name: 'price name', price: 10000 } });
    expect(wrapper.vm.priceVal).toBe(10000);
    expect(wrapper.vm.priceName).toBe('price name');
  });

  it('should emit submit when save button clicked', () => {
    const wrapper = createWrapper({
      value: { id: 2, name: 'price name', price: 10000 },
      type: 'additional',
    });

    wrapper.find('button').trigger('click');
    expect(wrapper.emitted('submit')[0]).toEqual([
      'additional',
      { type: 'additional', name: 'price name', price: 10000 },
      2,
    ]);
  });
});
