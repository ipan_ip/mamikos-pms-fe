import { shallowMount, createLocalVue } from '@vue/test-utils';
import RoomAdditionalPrice from './RoomAdditionalPrice';
import MkButton from '~/components/global/atoms/MkButton';

const localVue = createLocalVue();

const additionalPrices = [
  {
    type: 'additional',
    label: 'Biaya Tambahan Lain',
    active: false,
    sort: 1,
    data: [
      {
        id: 1,
        type: 'additional',
        name: 'Iuran Sampah',
        price: 30000,
        price_label: 'Rp30.000',
        meta: null,
      },
      {
        id: 2,
        type: 'additional',
        name: 'Iuran Air',
        price: 50000,
        price_label: 'Rp50.000',
        meta: null,
      },
    ],
  },
  {
    type: 'fine',
    label: 'Biaya Denda',
    active: true,
    sort: 2,
    data: [
      {
        id: 3,
        type: 'fine',
        name: 'Denda',
        price: 30000,
        price_label: 'Rp30.000',
        meta: [
          {
            key: 'fine_maximum_lenght',
            value: '1',
          },
          {
            key: 'fine_duration_type',
            value: 'week',
          },
        ],
      },
    ],
  },
];

describe('RoomAdditionalPrice.vue', () => {
  const getPriceComponents = jest
    .fn()
    .mockResolvedValueOnce({
      data: {
        status: true,
        data: additionalPrices,
      },
    })
    .mockResolvedValueOnce({ data: { status: false, data: null } })
    .mockRejectedValueOnce(new Error('error'));

  const deletePriceComponent = jest
    .fn()
    .mockResolvedValueOnce({ data: { status: true, meta: { message: 'OK' } } })
    .mockResolvedValueOnce({ data: { status: false, meta: { message: 'FAIL' } } })
    .mockRejectedValueOnce(new Error('error'));

  const updatePriceComponent = jest
    .fn()
    .mockResolvedValueOnce({
      data: {
        status: true,
        data: {
          id: 55,
          type: 'additional',
          name: 'Iuran Sampah',
          price: 30000,
          price_label: 'Rp30.000',
          meta: null,
        },
        meta: {
          message: 'OK',
        },
      },
    })
    .mockResolvedValueOnce({
      data: { status: false, meta: { message: 'FAIL' } },
    })
    .mockRejectedValueOnce(new Error('error'));

  const postPriceComponent = jest
    .fn()
    .mockResolvedValueOnce({
      data: {
        status: true,
        data: {
          id: 56,
          type: 'additional',
          name: 'Iuran Sampah',
          price: 30000,
          price_label: 'Rp30.000',
          meta: null,
        },
        meta: {
          message: 'OK',
        },
      },
    })
    .mockResolvedValueOnce({
      data: { status: false, meta: { message: 'FAIL' } },
    })
    .mockRejectedValueOnce(new Error('error'));

  const wrapper = shallowMount(RoomAdditionalPrice, {
    localVue,
    stubs: {
      MkButton,
    },
    mocks: {
      $api: {
        getPriceComponents,
        deletePriceComponent,
        updatePriceComponent,
        postPriceComponent,
      },
      $alert: jest.fn(),
      $bugsnag: { notify: jest.fn() },
      $buefy: {
        toast: {
          open: jest.fn(),
        },
      },
    },
    propsData: {
      kostDetail: {
        _id: 1234,
      },
    },
  });

  it('should render room additional price', () => {
    expect(wrapper.find('.room-additional-price').exists()).toBe(true);
  });

  it('should render all additional price item', () => {
    expect(wrapper.findAll('additional-price-item-stub').length).toBe(2);
  });

  it('should handle show modal properly', () => {
    const data = additionalPrices[0];
    wrapper.vm.updateModalOptions(data, data.data[0]);
    expect(wrapper.vm.modalOptions).toEqual({
      type: data.type,
      label: data.label,
      isShow: true,
      value: data.data[0],
      isDelete: false,
      isLoading: false,
    });
  });

  it('should handle show modal delete properly', () => {
    const data = additionalPrices[0];
    wrapper.vm.updateModalOptions(data, data.data[0], true);
    expect(wrapper.vm.modalOptions).toEqual({
      type: data.type,
      label: data.label,
      isShow: true,
      value: data.data[0],
      isDelete: true,
      isLoading: false,
    });
    expect(wrapper.vm.deleteModalTitle).toContain(data.data[0].name);
  });

  it('should set activation to false if deleted price length is 0 left', async () => {
    const toastOpen = jest.spyOn(wrapper.vm.$buefy.toast, 'open');
    const data = additionalPrices[1];
    wrapper.vm.updateModalOptions(data, { ...data.data[0], key: 1, index: 0 }, true);
    await wrapper.vm.handleDeletePrice();
    expect(wrapper.vm.additionalPrices[0].active).toBe(false);
    expect(toastOpen).toBeCalled();
    toastOpen.mockRestore();
  });

  it('should cancel activation if owner try to activate price on empty price but close modal add', () => {
    wrapper.setData({
      additionalPrices: [
        ...additionalPrices,
        {
          type: 'dp',
          label: 'DP',
          active: true,
          sort: 3,
          data: [],
        },
      ],
    });
    const data = wrapper.vm.additionalPrices[2];
    wrapper.vm.updateModalOptions(data, { ...data.data[0], key: 2, index: 0 });
    wrapper.vm.handleCloseModal();
    expect(wrapper.vm.additionalPrices[2].active).toBe(false);
  });

  it('should add new item if store item success', async () => {
    wrapper.setData({
      additionalPrices: [...additionalPrices],
    });
    const data = wrapper.vm.additionalPrices[1];
    wrapper.vm.updateModalOptions(data, { key: 1 });
    await wrapper.vm.storeAdditionalPrice(data.type, { price: 1000 });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.additionalPrices[1].data[0]).toEqual(expect.objectContaining({ id: 56 }));
  });

  it('should update item if update item success', async () => {
    wrapper.setData({
      additionalPrices: [...additionalPrices],
    });
    const data = wrapper.vm.additionalPrices[0];
    wrapper.vm.updateModalOptions(data, { key: 0 });
    await wrapper.vm.storeAdditionalPrice(data.type, { price: 1000 }, 55);
    expect(wrapper.vm.additionalPrices[0].data[0]).toEqual(expect.objectContaining({ id: 55 }));
  });

  // API second call (status = false)
  it('should set additional price to empty and show alert when api return status false', async () => {
    const $alert = jest.spyOn(wrapper.vm, '$alert');
    await wrapper.vm.getAdditionalPrices();
    expect(wrapper.vm.additionalPrices).toEqual([]);
    expect($alert).toBeCalled();
    $alert.mockRestore();
  });

  it('should open toast with response message if delete price component return status false', async () => {
    const toastOpen = jest.spyOn(wrapper.vm.$buefy.toast, 'open');
    const data = additionalPrices[0];
    wrapper.vm.updateModalOptions(data, { ...data.data[0], key: 0, index: 0 }, true);
    await wrapper.vm.handleDeletePrice();
    expect(toastOpen).toBeCalledWith(expect.objectContaining({ message: 'FAIL' }));
    toastOpen.mockRestore();
  });

  it('should not add new item if store item success', async () => {
    const toastOpen = jest.spyOn(wrapper.vm.$buefy.toast, 'open');
    wrapper.setData({
      additionalPrices: [...additionalPrices],
    });
    const data = wrapper.vm.additionalPrices[1];
    wrapper.vm.updateModalOptions(data, { key: 1 });
    await wrapper.vm.storeAdditionalPrice(data.type, { price: 1000 });
    expect(wrapper.vm.additionalPrices[1].data.length).toBe(1);
    expect(toastOpen).toBeCalledWith(expect.objectContaining({ message: 'FAIL' }));
    toastOpen.mockRestore();
  });

  it('should not update item if update item success', async () => {
    const toastOpen = jest.spyOn(wrapper.vm.$buefy.toast, 'open');
    wrapper.setData({
      additionalPrices: [...additionalPrices],
    });
    const data = wrapper.vm.additionalPrices[0];
    wrapper.vm.updateModalOptions(data, { key: 0 });
    await wrapper.vm.storeAdditionalPrice(data.type, { price: 1000 }, 55);
    expect(toastOpen).toBeCalledWith(expect.objectContaining({ message: 'FAIL' }));
    toastOpen.mockRestore();
  });

  // API third call (failed)
  it('should call bugsnag notify if get additional prices failed', async () => {
    const notify = jest.spyOn(wrapper.vm.$bugsnag, 'notify');
    await wrapper.vm.getAdditionalPrices();
    expect(wrapper.vm.additionalPrices).toEqual([]);
    expect(notify).toBeCalled();
    notify.mockRestore();
  });

  it('should call bugsnag notify if delete price component failed', async () => {
    const notify = jest.spyOn(wrapper.vm.$bugsnag, 'notify');
    const data = additionalPrices[0];
    wrapper.vm.updateModalOptions(data, { ...data.data[0], key: 0, index: 0 }, true);
    await wrapper.vm.handleDeletePrice();
    expect(notify).toBeCalled();
    notify.mockRestore();
  });

  it('should call bugsnag notify if store item failed', async () => {
    const notify = jest.spyOn(wrapper.vm.$bugsnag, 'notify');
    wrapper.setData({
      additionalPrices: [...additionalPrices],
    });
    const data = wrapper.vm.additionalPrices[0];
    wrapper.vm.updateModalOptions(data, { key: 0 });
    await wrapper.vm.storeAdditionalPrice(data.type, { price: 1000 });
    expect(notify).toBeCalled();
    notify.mockRestore();
  });

  it('should call bugsnag notify if update item failed', async () => {
    const notify = jest.spyOn(wrapper.vm.$bugsnag, 'notify');
    wrapper.setData({
      additionalPrices: [...additionalPrices],
    });
    const data = wrapper.vm.additionalPrices[0];
    wrapper.vm.updateModalOptions(data, { key: 0 });
    await wrapper.vm.storeAdditionalPrice(data.type, { price: 1000 }, 55);
    expect(notify).toBeCalled();
    notify.mockRestore();
  });
});
