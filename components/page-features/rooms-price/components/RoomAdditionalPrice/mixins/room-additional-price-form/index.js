import _find from 'lodash/find';
import { toIDR } from '~/utils/currencyString';

export default {
  props: {
    value: {
      type: Object,
      default: null,
    },
    isLoading: {
      type: Boolean,
      default: false,
    },
    type: {
      type: String,
      default: '',
    },
  },
  methods: {
    _find,
    toIDR,
    getMetaValue(meta, key, defaultVal) {
      const metaData = this._find(meta, ['key', key]);
      return metaData ? metaData.value : defaultVal;
    },
    submit(payload) {
      this.$emit(
        'submit',
        this.type,
        { type: this.type, ...payload },
        this.value ? this.value.id : 0,
      );
    },
    roundPrice(price, percentage = 100) {
      return Math.round((percentage / 100) * price);
    },
  },
};
