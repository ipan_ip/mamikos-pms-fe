import { shallowMount, createLocalVue } from '@vue/test-utils';
import RoomPriceCollapseWrapper from './RoomPriceCollapseWrapper.vue';
import MkButton from '~/components/global/atoms/MkButton';
import mockWindowProperty from '~/utils/mockWindowProperty';

const localVue = createLocalVue();

describe('RoomPriceCollapseWrapper.vue', () => {
  const mainClass = '.room-price-collapse-wrapper';
  const slotSelector = mainClass + '__container > *';

  const mockElement = (prop, value) => {
    const el = document.createElement('div');
    !!prop &&
      Object.defineProperty(el, prop, {
        configurable: true,
        value,
      });
    return el;
  };

  let wrapper;

  jest.useFakeTimers();

  beforeEach(() => {
    jest.clearAllTimers();
    wrapper = shallowMount(RoomPriceCollapseWrapper, {
      localVue,
      props: {
        open: false,
      },
      stubs: { MkButton },
    });
  });

  it('should render wrapper', () => {
    expect(wrapper.find(mainClass).exists()).toBe(true);
    expect(wrapper.find(mainClass + '__container').exists()).toBe(true);
    expect(wrapper.find(mainClass + '__toggle-button').exists()).toBe(true);
  });

  it('should handle open transition properly', async () => {
    const priceContainer = mockElement('scrollHeight', 500);
    const slotElement = mockElement('clientHeight', 100);

    wrapper.vm.$refs.priceContainer = priceContainer;
    mockWindowProperty('document.querySelector', (selector) => {
      if (selector === slotSelector) {
        return slotElement;
      }
    });

    const spySetStyle = jest.spyOn(wrapper.vm, 'setStyle');
    await wrapper.setData({ isOpen: false });
    await wrapper.find('button').trigger('click');

    jest.advanceTimersByTime(wrapper.vm.transitionDuration);

    expect(wrapper.vm.isOpen).toBe(true);

    [
      [1, priceContainer, 'maxHeight', '100px'],
      [2, priceContainer, 'maxHeight', '500px'],
      [3, priceContainer, 'maxHeight', ''],
    ].forEach((args) => {
      expect(spySetStyle).toHaveBeenNthCalledWith(...args);
    });

    spySetStyle.mockRestore();
  });

  it('should handle close transition properly', async () => {
    const priceContainer = mockElement('clientHeight', 500);
    const slotElement = mockElement('clientHeight', 100);

    wrapper.vm.$refs.priceContainer = priceContainer;
    mockWindowProperty('document.querySelector', (selector) => {
      if (selector === slotSelector) {
        return slotElement;
      }
    });

    const spySetStyle = jest.spyOn(wrapper.vm, 'setStyle');
    wrapper.setData({ isOpen: true });
    await wrapper.find('button').trigger('click');

    jest.advanceTimersByTime(wrapper.vm.transitionDuration);

    expect(wrapper.vm.isOpen).toBe(false);

    [
      [1, priceContainer, 'maxHeight', '500px'],
      [2, priceContainer, 'maxHeight', '100px'],
      [3, priceContainer, 'maxHeight', ''],
    ].forEach((args) => {
      expect(spySetStyle).toHaveBeenNthCalledWith(...args);
    });

    spySetStyle.mockRestore();
  });

  it('should not do anything if there is no slot element', async () => {
    mockWindowProperty('document.querySelector', (selector) => {
      if (selector === slotSelector) {
        return null;
      }
    });

    const spySetStyle = jest.spyOn(wrapper.vm, 'setStyle');
    wrapper.setData({ isOpen: false });
    await wrapper.find('button').trigger('click');

    jest.advanceTimersByTime(wrapper.vm.transitionDuration);

    expect(wrapper.vm.isOpen).toBe(false);
    expect(spySetStyle).not.toBeCalled();

    spySetStyle.mockRestore();
  });

  it('should set element style properly', () => {
    const testElement = mockElement();
    wrapper.vm.setStyle(testElement, 'background', 'red');
    expect(testElement.style.background).toBe('red');
  });
});
