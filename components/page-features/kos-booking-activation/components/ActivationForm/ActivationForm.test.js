import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import ActivationForm from './ActivationForm';
import localVueWithBuefy from '~/utils/addBuefy';

const $api = {
  getBbkStatus: jest.fn().mockResolvedValue({
    data: { status: true, bbk_status: { reject: 1, waiting: 1, other: 1 } },
  }),
  getMamipayRooms: jest.fn().mockResolvedValue({
    data: {
      status: true,
      has_more: true,
      data: [
        {
          photo: {
            medium: 'path',
            room_title: 'title',
            area_formatted: 'area',
            room_available: 'available',
          },
        },
      ],
    },
  }),
  registerBbk: jest
    .fn()
    .mockResolvedValueOnce({ data: { status: true, meta: { message: 'message' } } })
    .mockResolvedValueOnce({ data: { status: false, meta: { message: 'message' } } })
    .mockRejectedValueOnce({ message: 'error' }),
};

localVueWithBuefy.use(Vuex);
const store = new Vuex.Store({
  modules: {
    profile: {
      namespaced: true,
      state: { isMamipayBookingAvailable: false },
      getters: {
        isBookingAvailable(state) {
          return state.membership;
        },
      },
    },
  },
});

const mount = (data = {}) => {
  const mountData = {
    localVue: localVueWithBuefy,
    mocks: {
      $store: { dispatch: jest.fn().mockResolvedValue({}) },
      $changes: { init: jest.fn(), update: jest.fn() },
      $api,
      $route: {
        query: { kos_name: 'kosName', room_count: 'roomCount', image: 'image' },
        params: { previousPage: 'Lengkapi Data Diri' },
      },
      $device: { isDesktop: true },
      $buefy: { toast: { open: jest.fn() } },
      $tracker: { send: jest.fn() },
    },
    store,
    ...data,
  };

  return shallowMount(ActivationForm, mountData);
};

describe('ActivationForm.vue', () => {
  let wrapper;
  const beforeEach = () => {
    wrapper = mount();
  };

  it('should mount properly', () => {
    beforeEach();
    expect(wrapper.find('.activation-form').exists()).toBeTruthy();
    expect(wrapper.vm.$api.getBbkStatus).toBeCalled();
  });

  it('should catch error when api getMamipayRooms is error', async () => {
    beforeEach();
    await Promise.resolve;

    expect(wrapper.vm.$api.getBbkStatus).toBeCalled();
    expect(wrapper.vm.$api.getMamipayRooms).toBeCalled();

    $api.getMamipayRooms = jest.fn().mockRejectedValue({});
    beforeEach();
    await Promise.resolve; // wait getBbkStatus api
    await Promise.reject; // wait getMamipayRooms api
    await Promise.prototype.catch; // wait getMamipayRooms api

    expect(wrapper.vm.$buefy.toast.open).toBeCalled();
  });

  it('should submit to server properly', async () => {
    // success response
    wrapper.vm.submitForm();
    await Promise.resolve;
    expect(wrapper.vm.$api.registerBbk).toBeCalled();

    // failed response
    wrapper.vm.submitForm();
    await Promise.resolve;
    expect(wrapper.vm.$buefy.toast.open).toBeCalled();

    // error response
    wrapper.vm.submitForm();
    await Promise.resolve;
    expect(wrapper.vm.$buefy.toast.open).toBeCalled();
  });
});
