import { shallowMount } from '@vue/test-utils';
import TncBookingLangsung from './TncBookingLangsung';
import localVueWithBuefy from '~/utils/addBuefy';

describe('TncBookingLangsung.vue', () => {
  const wrapper = shallowMount(TncBookingLangsung, {
    localVue: localVueWithBuefy,
  });
  it('should mount properly', () => {
    expect(wrapper.find('.tnc-booking-langsung').exists()).toBeTruthy();
  });

  it('should emit event when reach the bottom of this component', () => {
    wrapper.find('.tnc-booking-langsung__content').trigger('scroll');
    expect(wrapper.emitted('scrolled')[0]).toEqual([true]);
  });
});
