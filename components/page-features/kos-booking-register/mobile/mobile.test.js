import { shallowMount } from '@vue/test-utils';
import mobile from '../mobile';

describe('mobile.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(mobile);
  });

  it('should mount properly', () => {
    expect(wrapper.find('.booking-register-wrapper').exists()).toBeTruthy();
  });

  it('should check currentStep properly', () => {
    expect(wrapper.vm.currentStep).toBe(0);
    wrapper.vm.next();
    expect(wrapper.vm.currentStep).toBe(1);
  });
});
