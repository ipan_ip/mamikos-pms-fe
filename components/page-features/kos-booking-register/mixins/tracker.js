export default {
  computed: {
    redirectionSource() {
      const { previousPage } = this.$route.params; // from pms
      const queryPrevious = this.$route.query['previous-page']; // from mami53

      if (previousPage !== undefined) return previousPage;
      else if (queryPrevious !== undefined) return queryPrevious;
      else return 'Else';
    },
  },
};
