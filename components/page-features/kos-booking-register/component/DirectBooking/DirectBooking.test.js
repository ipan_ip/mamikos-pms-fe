import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import DirectBooking from '../DirectBooking';
import trackerMixin from '../../mixins/tracker';
import mockLazy from '~/utils/mocks/lazy';
import localVueWithBuefy from '~/utils/addBuefy';

mockLazy(localVueWithBuefy);

const $api = {
  getBbkStatus: jest.fn().mockResolvedValue({
    data: { status: true, bbk_status: { reject: 1, waiting: 1, other: 1 } },
  }),
  registerBbk: jest
    .fn()
    .mockResolvedValueOnce({ data: { status: true, meta: { message: 'message' } } })
    .mockResolvedValueOnce({ data: { status: false, meta: { message: 'message' } } })
    .mockRejectedValueOnce({ message: 'error' }),
};

localVueWithBuefy.use(Vuex);
const store = new Vuex.Store({
  modules: {
    profile: {
      namespaced: true,
      state: { isMamipayBookingAvailable: false },
      getters: {
        membership(state) {
          return state.data.is_mamipay_user;
        },
        isBookingAvailable(state) {
          return state.membership;
        },
      },
      actions: {
        getMamipayBookingAvailable: jest.fn(),
      },
    },
  },
});

const mount = (data = {}) => {
  const mountData = {
    localVue: localVueWithBuefy,
    mixins: [trackerMixin],
    mocks: {
      $store: { dispatch: jest.fn().mockResolvedValue({}) },
      $changes: { init: jest.fn(), update: jest.fn() },
      $api,
      $route: {
        query: { kos_name: 'kosName', room_count: 'roomCount', image: 'image' },
        params: { previousPage: 'Dashboard' },
      },
      $router: {
        replace: jest.fn(),
      },
      $device: { isDesktop: true },
      $buefy: { toast: { open: jest.fn() } },
      $tracker: { send: jest.fn() },
    },
    store,
    ...data,
  };

  return shallowMount(DirectBooking, mountData);
};

const urlMamikos = 'https://owner.mamikos.com/';

describe('DirectBooking.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.direct-booking').exists()).toBeTruthy();
    expect(wrapper.vm.$api.getBbkStatus).toBeCalled();

    return wrapper.vm.$store.dispatch('profile/getMamipayBookingAvailable');
  });

  it('should next & go to edit data pribadi properly', () => {
    wrapper.vm.nextStep();
    expect(wrapper.emitted().next).toBeTruthy();

    wrapper.vm.$route.query = { 'edit-data': true };
    wrapper.vm.nextStep();
    expect(wrapper.emitted().next).toBeTruthy();
  });

  it('should skip form data diri', () => {
    wrapper.setData({ isSkipForm: true });
    wrapper.vm.nextStep();
    expect(wrapper.vm.$router.replace).toBeCalled();
    wrapper.setData({ isSkipForm: false });
  });

  it('should follow autobbk flow properly', () => {
    wrapper.vm.$route.query = { autobbk: true };
    wrapper.vm.nextStep();
    expect(wrapper.emitted().next).toBeTruthy();
  });

  it('should follow notification reject flow properly', async () => {
    // isWaitingBBK = true and success response
    wrapper.vm.$route.query = { 'rejected-notif': true };
    wrapper.vm.nextStep();
    await Promise.resolve;
    expect(wrapper.vm.$api.registerBbk).toBeCalled();

    // failed response
    wrapper.vm.nextStep();
    await Promise.resolve;
    expect(wrapper.vm.$buefy.toast.open).toBeCalled();

    // error response
    wrapper.vm.nextStep();
    await Promise.resolve;
    expect(wrapper.vm.$buefy.toast.open).toBeCalled();

    // isWaitingBBK = false
    wrapper.setData({ isWaitingBBK: false });
    wrapper.vm.nextStep();
    expect(wrapper.emitted().next).toBeTruthy();
  });

  it('should handle success button on modal success properly', () => {
    // without query params redirect_url
    wrapper.vm.handleSuccessButton();
    window.history.pushState({}, '', '/newPathname');

    // with query params redirect_url
    wrapper.vm.$route.query = { redirect_url: urlMamikos };
    global.window = Object.create(window);
    Object.defineProperty(window, 'location', {
      value: {
        href: urlMamikos,
      },
    });
    wrapper.vm.handleSuccessButton();
    expect(window.location.href).toEqual(urlMamikos);
  });
});
