export default [
  {
    icon: require(`~/assets/images/booking/badge-item-direct-booking.svg`),
    title: 'Okupansi',
    description:
      'Calon penyewa gampang untuk mulai sewa dan bayar kos secara online tanpa harus janji bertemu dengan Anda.',
  },
  {
    icon: require(`~/assets/images/booking/badge-item-reminder.svg`),
    title: 'Reminder Pembayaran',
    description:
      'Mamikos otomatis mengirimkan notifikasi tagihan ke penyewa. Anda juga diberi tahu jika sudah ditransfer.',
  },
  {
    icon: require(`~/assets/images/booking/badge-item-manage-financial.svg`),
    title: 'Tinjauan Keuangan',
    description: 'Lihat overview keuangan kos Anda langsung dari dashboard.',
  },
  {
    icon: require(`~/assets/images/booking/badge-item-cost.svg`),
    title: 'Hanya 7% Harga Sewa',
    description:
      'Anda hanya perlu menyisihkan 7% dari harga sewa, ketika terjadi transaksi booking, di pembayaran pertama saja.',
  },
  {
    icon: require(`~/assets/images/booking/badge-item-disbursement-booking.svg`),
    title: 'Pencairan Dana 1x24 Jam',
    description:
      'Untuk keamanan transaksi, uang sewa akan diteruskan pada Anda 1 hari setelah penyewa check-in.',
  },
];
