import { shallowMount } from '@vue/test-utils';
import desktop from '../desktop';

describe('desktop.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(desktop);
  });

  it('should mount properly', () => {
    expect(wrapper.find('.booking-register-wrapper').exists()).toBeTruthy();
  });

  it('should check currentStep properly', () => {
    expect(wrapper.vm.currentStep).toBe(0);
    wrapper.vm.next();
    expect(wrapper.vm.currentStep).toBe(1);
  });
});
