export default {
  methods: {
    navigate(path, type, isBbkRestricted = false) {
      const base = type === 'mami53' ? this.$mamikosUrl : '';

      if (isBbkRestricted) {
        this.$router.push('/kos/booking/register');
      } else if (base) {
        window.location.href = `${base}${path}`;
      } else {
        this.$router.push(path);
      }
    },
  },
};
