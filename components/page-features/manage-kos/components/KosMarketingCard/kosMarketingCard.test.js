import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import KosMarketingCard from '../KosMarketingCard';
import mockWindowProperty from '~/utils/mockWindowProperty';

const localVue = createLocalVue();
localVue.use(Vuex);
mockWindowProperty('window.location', { href: '' });

const mockStore = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        user: {
          user_id: 12345,
          name: '',
          phone_number: '',
          email: '',
        },
      },
      getters: {
        user(state, getters) {
          return state.user;
        },
        isBbk(state, getters) {
          return getters.membership.is_mamipay_user && state.data.is_booking_all_room;
        },
        membership: () => {
          return {
            views: 10000,
          };
        },
        isPremium(state, getters) {
          return !getters.membership.expired && !!(getters.membership.status === 'Premium');
        },
      },
    },
  },
};

describe('KosMarketingCard.vue', () => {
  const $router = { push: jest.fn() };
  const $device = { isMobile: true };
  const $mamikosUrl = 'mamikos.com';
  const $tracker = { send: jest.fn() };

  const wrapper = shallowMount(KosMarketingCard, {
    mocks: { $router, $device, $mamikosUrl, $tracker },
    store: new Vuex.Store(mockStore),
    localVue,
  });

  it('should change the route to premium pages', () => {
    wrapper.vm.goToPage(wrapper.vm.marketingMenus[0]);
    expect(window.location.href).toBe('');
  });
});
