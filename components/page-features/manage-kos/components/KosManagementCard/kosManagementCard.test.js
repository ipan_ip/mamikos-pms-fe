import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import KosManagementCard from '../KosManagementCard';
import mockWindowProperty from '~/utils/mockWindowProperty';

const localVue = createLocalVue();
localVue.use(Vuex);

mockWindowProperty('window.location', { href: '' });

describe('KosManagementCard.vue', () => {
  const mockStore = {
    modules: {
      profile: {
        namespaced: true,
        state: { user: { name: 'username' } },
        getters: {
          user(state, getters) {
            return state.user;
          },
          userHasNoProperty(state, getters) {
            return getters.user.kost_total === 0;
          },
          isBbk() {
            return true;
          },
          isPremium() {
            return true;
          },
        },
      },
    },
  };

  const $router = { push: jest.fn() };
  const $tracker = { send: jest.fn() };
  const $device = { isMobile: true };
  const $mamikosUrl = 'mamikos.com';

  const wrapper = shallowMount(KosManagementCard, {
    localVue,
    store: new Vuex.Store(mockStore),
    mocks: { $router, $tracker, $device, $mamikosUrl },
  });

  it('should change the route to manage page', () => {
    wrapper.vm.goToPage(wrapper.vm.managementMenus[0]);
    expect(window.location.href).toBe(`mamikos.com/ownerpage/manage/all/booking`);
  });

  it('should change the route to kelola tagihan page', () => {
    wrapper.vm.goToPage(wrapper.vm.managementMenus[2]);
    expect($router.push).toBeCalledWith('/billing-management');
  });
});
