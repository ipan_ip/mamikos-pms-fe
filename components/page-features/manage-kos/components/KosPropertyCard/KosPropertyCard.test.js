import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import KosPropertyCard from '../KosPropertyCard';
import mockWindowProperty from '~/utils/mockWindowProperty';

const localVue = createLocalVue();
localVue.use(Vuex);
mockWindowProperty('window.location', { href: '' });

describe('KosPropertyCard.vue', () => {
  const $router = { push: jest.fn() };
  const $device = { isMobile: true };
  const $mamikosUrl = 'mamikos.com';
  const $tracker = { send: jest.fn() };

  const mockStore = {
    modules: {
      profile: {
        namespaced: true,
        state: { user: { user_id: 12345 } },
        getters: {
          user(state) {
            return state.user;
          },
          isBbk() {
            return true;
          },
          isPremium() {
            return true;
          },
        },
      },
    },
  };

  const wrapper = shallowMount(KosPropertyCard, {
    localVue,
    mocks: { $router, $device, $mamikosUrl, $tracker },
    store: new Vuex.Store(mockStore),
  });

  it('should change the route to kos page', () => {
    wrapper.vm.goToPage(wrapper.vm.propertyMenus[0]);
    expect(window.location.href).toBe('mamikos.com/ownerpage/kos');
  });

  it('should change the route to apartemen page', () => {
    wrapper.vm.goToPage(wrapper.vm.propertyMenus[1]);
    expect(window.location.href).toBe('mamikos.com/ownerpage/apartment');
  });

  it('should change the route to lowongan kerja page', () => {
    wrapper.vm.goToPage(wrapper.vm.propertyMenus[2]);
    expect(window.location.href).toBe('mamikos.com/ownerpage/vacancy');
  });
});
