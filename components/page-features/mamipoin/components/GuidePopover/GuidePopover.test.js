import { shallowMount } from '@vue/test-utils';
import { VPopover } from 'v-tooltip';
import GuidePopover from '../GuidePopover';
import localVueWithBuefy from '~/utils/addBuefy';

const stubs = {
  VPopover,
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      stubs,
    },
    ...adtMountData,
  };

  return shallowMount(GuidePopover, mountData);
};

describe('GuidePopover.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.guide-popover').exists()).toBeTruthy();
  });

  it('should render popover-button text properly', async () => {
    wrapper.setProps({
      isFinished: false,
      nextButtonText: 'selanjutnya',
    });
    await wrapper.vm.$nextTick();
    expect(wrapper.find('.popover-button').text()).toBe('selanjutnya');

    wrapper.setProps({
      isFinished: true,
      finishedButtonText: 'oke',
    });
    await wrapper.vm.$nextTick();
    expect(wrapper.find('.popover-button').text()).toBe('oke');
  });

  it('should emit click:popover-button event when calling onClickPopover', async () => {
    wrapper.vm.$emit = jest.fn();

    await wrapper.vm.onClickPopover();
    expect(wrapper.vm.$emit).toBeCalled();
  });
});
