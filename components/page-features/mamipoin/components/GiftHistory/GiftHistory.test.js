import { shallowMount } from '@vue/test-utils';
import GiftHistory from '../GiftHistory';

import localVueWithBuefy from '~/utils/addBuefy';
import { toThousands } from '~/utils/number';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const mockComponent = {
  template: `<div />`,
};

const stubs = {
  GiftCard: mockComponent,
  MamipoinPagination: mockComponent,
  MamipoinPlaceholder: mockComponent,
};

const $api = {
  getRedeemList: jest.fn().mockResolvedValue({
    data: {
      status: true,
      current_page: 1,
      has_next: true,
      total_pages: 10,
      data: [
        {
          id: '1',
          reward: {
            name: 'reward name',
          },
          status: 'onprocess',
        },
        {
          id: '2',
          reward: {
            name: 'reward name 2',
          },
          status: 'failed',
        },
      ],
    },
  }),
};

localVueWithBuefy.filter('toThousands', toThousands);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'mamipoin',
    },
    $api,
    $device,
  },
  stubs,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GiftHistory, finalData);
};

describe('GiftHistory.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.gift-history').exists()).toBeTruthy();
  });

  it('should return isDesktop value from $device.isDesktop', () => {
    expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
  });

  it('should set pagination limit on create', () => {
    // desktop view
    expect(wrapper.vm.pagination.limit).toBe(8);

    // mobile view
    wrapper = mount({
      mocks: {
        ...mountData.mocks,
        ...{
          $device: {
            isDesktop: false,
          },
        },
      },
    });
    expect(wrapper.vm.pagination.limit).toBe(4);
  });

  it('should set redeem target and pagination page when calling setTarget', async () => {
    await wrapper.vm.setTarget();

    expect(wrapper.vm.target).toBe('all');
    expect(wrapper.vm.pagination.page).toBe(1);
    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should set redeem data on successful getRewardList api request', async () => {
    wrapper.setData({
      pagination: {
        page: 1,
        limit: 8,
        hasNext: false,
        total: 10,
        totalPages: 1,
      },
    });
    await wrapper.vm.fetchRedeemList();
    expect(wrapper.vm.redeems.length).toBe(2);
    expect(wrapper.vm.pagination.page).toBe(1);
    expect(wrapper.vm.pagination.hasNext).toBeTruthy();
    expect(wrapper.vm.pagination.totalPages).toBe(10);
    expect(wrapper.vm.pagination.total).toBe(80);

    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should set redeem data as [] when getting null/undefined on successful getRewardList api request', async () => {
    wrapper.vm.$api.getRedeemList = jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: null,
      },
    });
    await wrapper.vm.fetchRedeemList();
    expect(wrapper.vm.redeems.length).toBe(0);
    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should set isLoading as false on failed getRedeemList api request', async () => {
    wrapper.vm.$api.getRedeemList = jest.fn().mockRejectedValue('error');
    await wrapper.vm.fetchRedeemList();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should call fetchRedeemList on target change', async () => {
    wrapper.vm.fetchRedeemList = jest.fn();
    wrapper.setData({ target: 'all' });
    await wrapper.vm.$nextTick();
    wrapper.setData({ target: 'failed' });
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.fetchRedeemList).toBeCalled();
  });
});
