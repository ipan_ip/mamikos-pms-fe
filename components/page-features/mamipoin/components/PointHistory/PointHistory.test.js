import { shallowMount } from '@vue/test-utils';
import dayjs from 'dayjs';
import PointHistory from '.';
import localVueWithBuefy from '~/utils/addBuefy';

const $api = {
  getPointHistory: jest.fn().mockResolvedValue({
    data: {
      status: true,
      data: [
        {
          title: 'test',
          date: '2020-09-10 08:09:10',
          point: 10,
        },
      ],
      current_page: 1,
      has_next: true,
      total_pages: 10,
    },
  }),
};

const $buefy = {
  toast: {
    open: jest.fn(),
  },
};

const $device = {
  isDesktop: true,
};

const mockComponent = {
  template: `<div />`,
};

const stubs = {
  MamipoinPagination: mockComponent,
  MamipoinPlaceholder: mockComponent,
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $api,
    $buefy,
    $dayjs: dayjs,
    $device,
  },
  stubs,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(PointHistory, finalData);
};

describe('PointHistory.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.mamipoin-history').exists()).toBeTruthy();
  });

  it('should return isDesktop value properly', () => {
    expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
  });

  it('should render c-loader when isLoading is true', async () => {
    await wrapper.setData({ isLoading: true });
    expect(wrapper.find('.c-loader').exists()).toBeTruthy();
  });

  it('should render history when isLoading is false', async () => {
    await wrapper.setData({ isLoading: false });
    expect(wrapper.find('.mamipoin-history__content').exists()).toBeTruthy();
  });

  it('should set history data and isLoading as false on successful api request', async () => {
    await wrapper.vm.fetchPointHistory();
    expect(wrapper.vm.history.length).toBe(1);
    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should set isLoading as false on failed api request', async () => {
    wrapper.vm.$api.getPointHistory = jest.fn().mockRejectedValue('error');
    await wrapper.vm.fetchPointHistory();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should call fetchPointHistory function on pagination.page change', async () => {
    wrapper.vm.fetchPointHistory = jest.fn();
    wrapper.vm.pagination.page = 1;
    await wrapper.vm.$nextTick();
    wrapper.vm.pagination.page = 2;
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.fetchPointHistory).toBeCalled();
  });
});
