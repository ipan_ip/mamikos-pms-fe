import { shallowMount } from '@vue/test-utils';
import MamipoinPagination from '../MamipoinPagination';
import LoadingModal from '../LoadingModal';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const stubs = { LoadingModal };

const propsData = {
  hasNext: true,
  current: 1,
  isLoading: false,
  total: 10,
  totalPages: 5,
  limit: 10,
  showModalOnLoading: false,
  order: 'is-centered',
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'mamipoin',
    },
    $device,
  },
  propsData,
  stubs,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(MamipoinPagination, finalData);
};

describe('MamipoinPagination.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render mamipoin-pagination properly', () => {
    expect(wrapper.find('.mamipoin-pagination').exists()).toBeTruthy();

    // desktop view
    expect(wrapper.find('.mamipoin-pagination').classes()).toContain('--desktop');
  });

  it('should return isDesktop value from $device.isDesktop', () => {
    expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
  });

  it('should return paginationCurrent value from current props', () => {
    wrapper.setProps({ current: 2 });

    expect(wrapper.vm.paginationCurrent).toBe(2);
  });

  it('should return paginationClass value properly', async () => {
    await wrapper.setProps({ totalPages: 5, current: 1 });
    expect(wrapper.vm.paginationClass).toBe('');
  });

  it('should set isLoadingModalShown value on isLoading value change', async () => {
    await wrapper.setProps({
      showModalOnLoading: true,
      isLoading: true,
    });

    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isLoadingModalShown).toBeTruthy();

    wrapper.setProps({
      showModalOnLoading: false,
      isLoading: false,
    });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isLoadingModalShown).toBeFalsy();
  });

  it('should set emit current value when calling handlePaginationChange method and isLoading is false', async () => {
    wrapper.vm.$emit = jest.fn();
    wrapper.setProps({ isLoading: false });

    await wrapper.vm.handlePaginationChange(1);
    expect(wrapper.vm.$emit).toBeCalled();
    expect(wrapper.vm.$emit.mock.calls[0][0]).toBe('update:current');
    expect(wrapper.vm.$emit.mock.calls[0][1]).toBe(1);
  });

  it('should set default current pagination value as 1 if the function param is not provided', async () => {
    wrapper.vm.$emit = jest.fn();
    wrapper.setProps({ isLoading: false });

    await wrapper.vm.handlePaginationChange();
    expect(wrapper.vm.$emit).toBeCalled();
    expect(wrapper.vm.$emit.mock.calls[0][1]).toBe(1);
  });

  it('should set emit current value on b-pagination current sync value change', () => {
    wrapper.vm.$emit = jest.fn();
    const bPagination = wrapper.find('.pagination-list-all');

    if (bPagination && bPagination.exists()) {
      bPagination.vm.$emit('update:current', 2);

      expect(wrapper.vm.$emit).toBeCalled();
      expect(wrapper.vm.$emit.mock.calls[0][0]).toBe('update:current');
      expect(wrapper.vm.$emit.mock.calls[0][1]).toBe(2);
    }
  });
});
