import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GiftCard from '../GiftCard';
import MkCard from '~/components/global/molecules/MkCard';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';
import MkCardFooter from '~/components/global/molecules/MkCard/components/MkCardFooter';
import localVueWithBuefy from '~/utils/addBuefy';
import { toThousands } from '~/utils/number';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const stubs = {
  MkCard,
  MkCardBody,
  MkCardFooter,
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('toThousands', toThousands);

const storeData = {
  modules: {
    mamipoin: {
      namespaced: true,
      state: {
        redeemDetailPage: {
          prevPath: {},
        },
      },
      mutations: {
        setRedeemDetailPage(state, redeemDetailPage = {}) {
          state.redeemDetailPage = redeemDetailPage;
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const $router = {
  push: jest.fn(),
};

const mockRedeemData = {
  id: '',
  className: '',
  displayStatus: '',
  reward: {
    image: {
      medium: '',
      small: '',
    },
  },
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'mamipoin',
    },
    $router,
    $device,
  },
  propsData: {
    redeem: mockRedeemData,
  },
  store,
  stubs,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GiftCard, finalData);
};

describe('GiftCard.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.gift-card').exists()).toBeTruthy();
  });

  it('should return isDesktop value from $device.isDesktop', () => {
    expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
  });

  it('should call viewDetail method on gift-button click and call router push in desktop view', () => {
    const giftButton = wrapper.find('.gift-button');
    if (giftButton && giftButton.exists()) {
      giftButton.trigger('click');
      wrapper.vm.$nextTick();

      // desktop view
      expect($router.push).toBeCalled();
    }
  });

  it('should call viewDetail method on gift-button click and set redeemDetail data in mobile view', () => {
    const mocks = mountData.mocks;
    mocks.$device = {
      isDesktop: false,
      isMobile: true,
    };

    wrapper = mount({ mocks });

    const giftButton = wrapper.find('.gift-button');
    wrapper.vm.$emit = jest.fn();
    if (giftButton && giftButton.exists()) {
      giftButton.trigger('click');

      // mobile view
      expect(wrapper.vm.$store.state.mamipoin.redeemDetailPage).toEqual({
        prevPath: {
          name: 'mamipoin',
          path: '/mamipoin',
        },
      });
      expect($router.push).toBeCalled();
    }
  });
});
