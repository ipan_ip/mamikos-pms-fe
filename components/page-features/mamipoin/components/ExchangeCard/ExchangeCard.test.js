import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import _get from 'lodash/get';
import ExchangeCard from '../ExchangeCard';
import onboardingMixin from '../../mixins/onboarding';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const mockComponent = {
  template: '<div />',
};

const $tracker = {
  send: jest.fn(),
};

const stubs = {
  MkCard: mockComponent,
  MkCardBody: mockComponent,
  GuidePopover: mockComponent,
};

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    mamipoin: {
      namespaced: true,
      state: {
        point: {
          total: 0,
        },
        mamipoinPage: {
          hasViewedOnboarding: false,
          currentOnboardingStep: 'exchangeCard',
        },
      },
      mutations: {
        setMamipoinPage(state, mamipoinPage = {}) {
          state.mamipoinPage = mamipoinPage;
        },
      },
    },
    profile: {
      namespaced: true,
      state: {
        data() {
          return {
            total_mamipoin: 0,
          };
        },
      },
      getters: {
        isHighlightDailyAllocationShown: () => false,
        isBooking: () => true,
        isPremium: () => true,
        membership: () => {
          return {
            is_mamipay_user: true,
          };
        },
        showModalVerifyPhone: () => false,
        showModalNeedPassword: () => false,
        showModalPremiumTrial: () => false,
        showModalSurveyPremium: () => false,
        showModalDailyBalanceAllocation: () => false,
      },
    },
  },
};

const store = new Vuex.Store(storeData);
const $router = {
  push: jest.fn(),
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      mixins: [onboardingMixin],
      mocks: {
        $route: {
          name: 'mamipoin',
        },
        $router,
        $tracker,
        $device,
      },
      stubs,
      store,
    },
    ...adtMountData,
  };

  return shallowMount(ExchangeCard, mountData);
};

describe('ExchangeCard.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should return isDesktop value from $device.isDesktop', () => {
    expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
  });

  it('should return mamipoinPage value from mamipoin.mamipoinPage state', () => {
    expect(wrapper.vm.mamipoinPage).toBe(_get(storeData, 'modules.mamipoin.state.mamipoinPage'));
  });

  it('should return isPopoverShown as true when all requirements to show popover are met', () => {
    /* Requirements: user has not viewed onboarding, the current onboarding step is exchangeCard, none of the modals are shown */

    expect(wrapper.vm.isPopoverShown).toBeTruthy();
  });

  it('should call $route.push when calling goToRewardsPage method', () => {
    const exchangeCard = wrapper.find('.exchange-card__card');
    if (exchangeCard) {
      exchangeCard.trigger('click');
      expect($router.push).toBeCalled();

      const calls = $router.push.mock.calls || [[]];
      expect(calls[0][0]).toBe('/mamipoin/rewards');
    }
  });

  it('should set mamipoinPage data when calling closePopover method', async () => {
    await wrapper.vm.closePopover();

    expect(wrapper.vm.mamipoinPage).toEqual({
      hasViewedOnboarding: true,
      currentOnboardingStep: 'exchangeCard',
    });
  });

  it('should call tracker.send when calling sendTrackerEvent method', async () => {
    await wrapper.vm.sendTrackerEvent();

    expect($tracker.send).toHaveBeenCalled();
  });
});
