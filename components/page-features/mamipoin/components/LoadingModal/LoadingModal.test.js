import { mount } from '@vue/test-utils';
import LoadingModal from './LoadingModal';
import localVueWithBuefy from '~/utils/addBuefy';
import MkModal, { mixin as modalMixin } from '~/components/global/molecules/MkModal/MkModal';
import MkCard from '~/components/global/molecules/MkModal';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';

const mountComponent = () =>
  mount(LoadingModal, {
    localVue: localVueWithBuefy,
    mixins: [modalMixin],
    stubs: {
      MkModal,
      MkCard,
      MkCardBody,
    },
  });

describe('LoadingModal', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mountComponent();
  });

  it('mount properly', async () => {
    await wrapper.setProps({ active: true });
    expect(wrapper.find('.loading-modal').exists()).toBe(true);

    await wrapper.setProps({ active: false });
    expect(wrapper.find('.loading-modal').exists()).toBe(false);
  });

  it('hide or show loader', async () => {
    await wrapper.setProps({ active: true });
    expect(wrapper.vm.isShowCard).toBe(true);

    await wrapper.setProps({ active: false });
    expect(wrapper.vm.isShowCard).toBe(false);
  });
});
