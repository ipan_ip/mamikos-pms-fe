import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import _get from 'lodash/get';
import _cloneDeep from 'lodash/cloneDeep';
import dayjs from 'dayjs';
import MamipoinCard from '../MamipoinCard';
import onboardingMixin from '../../mixins/onboarding';
import GuidePopover from '../GuidePopover';
import localVueWithBuefy from '~/utils/addBuefy';
import { toThousands } from '~/utils/number';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const mockParentComponent = {
  template: `<div ref="mamipoinPage">
    <div ref="mamipoinHeader" />
  </div>`,
  $refs: {
    mamipoinPage: '',
  },
};

const $api = {
  getPointTotal: jest.fn(() => {
    return Promise.resolve({
      data: {
        status: true,
        point: 10,
        near_expired_point: 10,
        near_expired_date: '2020-08-20 08:09:10',
      },
    });
  }),
};

const stubs = {
  GuidePopover,
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('toThousands', toThousands);

const storeData = {
  modules: {
    mamipoin: {
      namespaced: true,
      state: {
        mamipoinPage: {
          hasViewedOnboarding: false,
          currentOnboardingStep: 'exchangeCard',
        },
      },
      mutations: {
        setMamipoinPage(state, mamipoinPage = {}) {
          state.mamipoinPage = mamipoinPage;
        },
      },
    },
    profile: {
      namespaced: true,
      getters: {
        isHighlightDailyAllocationShown: () => false,
        isBooking: () => true,
        showModalVerifyPhone: () => false,
        showModalNeedPassword: () => false,
        showModalPremiumTrial: () => false,
        showModalSurveyPremium: () => false,
        showModalDailyBalanceAllocation: () => false,
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const $router = {
  push: jest.fn(),
};

const $bugsnag = {
  notify: jest.fn(),
};

const mountData = {
  localVue: localVueWithBuefy,
  mixins: [onboardingMixin],
  mocks: {
    $api,
    $route: {
      name: 'mamipoin',
    },
    $router,
    $device,
    $bugsnag,
    $dayjs: dayjs,
  },
  store,
  provide() {
    return {
      parent: mockParentComponent,
    };
  },
  stubs,
  attachToDocument: true,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(MamipoinCard, finalData);
};

describe('MamipoinCard.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.mamipoin-card__card').exists()).toBeTruthy();
  });

  it('should return isDesktop value from $device.isDesktop', () => {
    expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
  });

  it('should return mamipoinPage value from mamipoin.mamipoinPage state', () => {
    expect(wrapper.vm.mamipoinPage).toBe(_get(storeData, 'modules.mamipoin.state.mamipoinPage'));
  });

  it('should return isPopoverShown as false when some of the requirements to show popover are not met', () => {
    /* Requirements: user has not viewed onboarding, the current onboarding step is 'mamipoinCard', none of the modals are shown */

    expect(wrapper.vm.isPopoverShown).toBeFalsy();
  });

  it('should return isPopoverShown as true when all of the requirements to show popover are met', () => {
    /* Requirements: user has not viewed onboarding, the current onboarding step is 'mamipoinCard', none of the modals are shown */

    wrapper.vm.mamipoinPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'mamipoinCard',
    };

    expect(wrapper.vm.isPopoverShown).toBeTruthy();
  });

  it('should set  point data on successful api call', () => {
    expect(wrapper.vm.pointTotal).toBe(10);
    expect(wrapper.vm.nearExpiredPoint).toBe(10);
    expect(wrapper.vm.nearExpiredDate).toBe('20 Aug 2020');
  });

  it('should set point data on successful api call', async () => {
    wrapper.vm.$api.getPointTotal = jest.fn().mockResolvedValue({
      data: {
        status: true,
        point: null,
        near_expired_point: null,
        near_expired_date: null,
      },
    });

    await wrapper.vm.fetchPoinApi();

    expect(wrapper.vm.pointTotal).toBe(0);
    expect(wrapper.vm.nearExpiredPoint).toBe(0);
    expect(wrapper.vm.nearExpiredDate).toBe('');
  });

  it('should not set point data on failed api call', () => {
    const mockApi = {
      getPointTotal: jest.fn().mockRejectedValue('error'),
    };

    wrapper = mount({ $api: mockApi });
    expect(wrapper.vm.pointTotal).toBe(0);
    expect(wrapper.vm.nearExpiredPoint).toBe(0);
    expect(wrapper.vm.nearExpiredDate).toBe('');
  });

  it('should call openTnc method and emitted event when clicking on infoButton in desktop view', () => {
    const infoButton = wrapper.find('.info-button');

    if (infoButton && infoButton.exists()) {
      wrapper.vm.$emit = jest.fn();
      infoButton.trigger('click');
      expect(wrapper.vm.$emit).toBeCalled();
    }
  });

  it('should call openTnc method and $router.push event when clicking on infoButton in mobile view', async () => {
    const mockDevice = {
      isDesktop: false,
      isMobile: true,
    };
    const mocks = _cloneDeep({
      ...mountData.mocks,
      ...{
        $device: mockDevice,
      },
    });
    wrapper = mount({ mocks });

    const infoButton = wrapper.find('.info-button');
    if (infoButton && infoButton.exists()) {
      wrapper.vm.$emit = jest.fn();
      await infoButton.trigger('click');
      expect($router.push).toBeCalled();
    }
  });

  it('should set mamipoinCardHeight when calling setMamipoinCardHeight method', () => {
    wrapper.vm.setMamipoinCardHeight();
    wrapper.vm.$nextTick();

    expect(wrapper.vm.mamipoinCardHeight).not.toBe(0);
  });

  it('should call closePopover button on guide popover close-popover event emit', () => {
    const guidePopover = wrapper.find('.mamipoin-card__popover');
    if (guidePopover && guidePopover.exists()) {
      guidePopover.vm.$emit('click:popover-button');

      expect(wrapper.vm.mamipoinPage).toEqual({
        hasViewedOnboarding: false,
        currentOnboardingStep: 'giftHistory',
      });
    }
  });

  it('should call bugsnag.notify on failed getPointTotal api call', async () => {
    wrapper.vm.$api.getPointTotal = jest.fn().mockRejectedValue('error');

    await wrapper.vm.fetchPoinApi();
    await wrapper.vm.$nextTick();
    expect($bugsnag.notify).toBeCalled();
  });

  it('should call setBodyOverflow when isPopoverShown returns true and in mobile view', () => {
    wrapper.vm.mamipoinPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'mamipoinCard',
    };

    const { isPopoverShown, isDesktop } = wrapper.vm;
    wrapper.vm.setBodyOverflow = jest.fn();

    if (isPopoverShown && !isDesktop) {
      expect(wrapper.vm.setBodyOverflow).toBeCalled();
    }
  });

  it('should redirect to owner Dashboard when function called', () => {
    wrapper.vm.backToDashboard();

    expect(wrapper.vm.$router.push).toBeCalledWith({ name: 'index' });
  });
});
