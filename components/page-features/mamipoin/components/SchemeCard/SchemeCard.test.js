import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import SchemeCard from '../SchemeCard';
import localVueWithBuefy from '~/utils/addBuefy';

const mockComponent = {
  template: '<div />',
};

const stubs = {
  MkCard: mockComponent,
  MkCardBody: mockComponent,
};

const $device = {
  isDesktop: true,
};

const $router = {
  push: jest.fn(),
};

const $tracker = {
  send: jest.fn(),
};

localVueWithBuefy.use(Vuex);

const store = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        data: {},
      },
      getters: {
        isBooking: () => false,
        isPremium: () => false,
        membership: () => {
          return {
            is_mamipay_user: true,
          };
        },
      },
    },
  },
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      stubs,
      mocks: {
        $router,
        $device,
        $tracker,
      },
      store: new Vuex.Store(store),
    },
    ...adtMountData,
  };

  return shallowMount(SchemeCard, mountData);
};

describe('SchemeCard.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.scheme-card-wrapper').exists()).toBeTruthy();
  });

  it('should render isDesktop value from $device.isDesktop', () => {
    expect(wrapper.vm.isDesktop).toBe(wrapper.vm.$device.isDesktop);
  });

  it('should call $router.push & $tracker.send when calling handleSchemeCardButtonClick method', async () => {
    await wrapper.vm.handleSchemeCardButtonClick();

    expect(wrapper.vm.$router.push).toBeCalled();
    expect(wrapper.vm.$tracker.send).toBeCalled();
  });
});
