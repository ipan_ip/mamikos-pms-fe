import { shallowMount } from '@vue/test-utils';
import MamipoinPlaceholder from '../MamipoinPlaceholder';
import localVueWithBuefy from '~/utils/addBuefy';

global.require = jest.fn((text) => {
  return JSON.stringify(text);
});

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'mamipoin',
    },
  },
};
const mount = (adtMountData = {}) => {
  const finalData = {
    ...mountData,
    ...adtMountData,
  };

  return shallowMount(MamipoinPlaceholder, finalData);
};

describe('MamipoinPlaceholder.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.mamipoin-placeholder').exists()).toBeTruthy();
  });

  it('should render title & desc properly', async () => {
    wrapper.setProps({
      title: 'test',
      desc: 'test',
    });
    await wrapper.vm.$nextTick();

    expect(wrapper.find('.mamipoin-placeholder__title').text()).toBe('test');
    expect(wrapper.find('.mamipoin-placeholder__desc').text()).toBe('test');
  });

  it('should return placeholderImage properly', () => {
    // no image
    expect(wrapper.vm.placeholderImage).toBe('');

    // has image
    // mock the image
    jest.mock('~/assets/images/mamipoin/empty-rewards-placeholder.svg', () => {
      return 'empty-rewards-placeholder';
    });
    wrapper.setProps({
      image: 'empty-rewards-placeholder',
    });
    expect(wrapper.vm.placeholderImage).toBeTruthy();
  });
});
