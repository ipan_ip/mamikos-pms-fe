import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import _get from 'lodash/get';
import MamipoinTabs from '../MamipoinTabs';
import onboardingMixin from '../../mixins/onboarding';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const mockComponent = {
  template: '<div />',
};

const mockParentComponent = {
  template: `<div ref="mamipoinPage" />`,
  $refs: {
    mamipoinPage: '',
  },
};

const stubs = {
  GuidePopover: mockComponent,
  GiftHistory: mockComponent,
  PointHistory: mockComponent,
  Guideline: mockComponent,
};

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    mamipoin: {
      namespaced: true,
      state: {
        mamipoinPage: {
          hasViewedOnboarding: false,
          currentOnboardingStep: 'giftHistory',
        },
      },
      mutations: {
        setMamipoinPage(state, mamipoinPage = {}) {
          state.mamipoinPage = mamipoinPage;
        },
      },
    },
    profile: {
      namespaced: true,
      state: {
        data: {
          total_mamipoin: 10,
        },
      },
      getters: {
        isHighlightDailyAllocationShown: () => false,
        isBooking: () => false,
        isPremium: () => false,
        membership: () => {
          return {
            is_mamipay_user: true,
          };
        },
        showModalVerifyPhone: () => false,
        showModalNeedPassword: () => false,
        showModalPremiumTrial: () => false,
        showModalSurveyPremium: () => false,
        showModalDailyBalanceAllocation: () => false,
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const $router = {
  push: jest.fn(),
};

const $route = {
  name: 'mamipoin',
  query: {
    tab: '1',
  },
};

const $tracker = {
  send: jest.fn(),
};

const mountData = {
  localVue: localVueWithBuefy,
  mixins: [onboardingMixin],
  mocks: {
    $route,
    $router,
    $device,
    $tracker,
  },
  store,
  provide() {
    return {
      parent: mockParentComponent,
    };
  },
  stubs,
  attachToDocument: true,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(MamipoinTabs, finalData);
};

describe('MamipoinTabs.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  jest.useFakeTimers();

  it('should mount properly', () => {
    expect(wrapper.find('.mamipoin-tabs').exists()).toBeTruthy();
  });

  it('should return isDesktop value from $device.isDesktop', () => {
    expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
  });

  it('should set active tab when $route.query.tab exists', () => {
    expect(wrapper.vm.activeTab).toBe(1);
  });

  it('should set active tab as 0 when received invalid tab value', () => {
    wrapper = mount({
      mocks: {
        ...mountData.mocks,
        ...{
          $route: {
            query: {
              tab: '23456789',
            },
          },
        },
      },
    });
    expect(wrapper.vm.activeTab).toBe(0);
  });

  it('should return mamipoinPage value from mamipoin.mamipoinPage state', () => {
    expect(wrapper.vm.mamipoinPage).toBe(_get(storeData, 'modules.mamipoin.state.mamipoinPage'));
  });

  it('should return isPopoverShown as false when some of the requirements to show popover are not met', () => {
    /* Requirements: user has not viewed onboarding, the current onboarding step is 'giftHistory', 'pointHistory', or 'guideline', none of the modals are shown */

    wrapper.vm.mamipoinPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'exchangeCard',
    };

    expect(wrapper.vm.isPopoverShown).toBeFalsy();
  });

  it('should return isPopoverShown as true when all of the requirements to show popover are met', () => {
    /* Requirements: user has not viewed onboarding, the current onboarding step is either 'giftHistory', 'pointHistory', or 'guideline', none of the modals are shown */

    jest.advanceTimersByTime(1000);
    wrapper.vm.mamipoinPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'giftHistory',
    };

    expect(wrapper.vm.isPopoverShown).toBeTruthy();
  });

  it('should set mamipoinPage onboarding data when calling navigatePopover method', async () => {
    // show onboarding for other tabs
    wrapper.setData({
      currentStep: 0,
    });
    await wrapper.vm.navigatePopover();
    expect(wrapper.vm.currentStep).toBe(1);
    expect(wrapper.vm.mamipoinPage).toEqual({
      hasViewedOnboarding: false,
      currentOnboardingStep: 'pointHistory',
    });

    // show onboarding for exchange card
    wrapper.setData({
      currentStep: 2,
    });
    await wrapper.vm.navigatePopover();
    expect(wrapper.vm.currentStep).toBe(2);
    expect(wrapper.vm.mamipoinPage).toEqual({
      hasViewedOnboarding: false,
      currentOnboardingStep: 'exchangeCard',
    });
  });

  it('should set activeTab and call $tracker when clicking on tab-item__link', () => {
    const tabLinkItem = wrapper.findAll('.tab-item__link');
    if (tabLinkItem && tabLinkItem.exists() && tabLinkItem.length >= 3) {
      const mamipoinTncTab = tabLinkItem.at(2);
      mamipoinTncTab.trigger('click');

      expect(wrapper.vm.activeTab).toBe(2);
      expect(wrapper.vm.$tracker.send).toBeCalled();
    }
  });
});
