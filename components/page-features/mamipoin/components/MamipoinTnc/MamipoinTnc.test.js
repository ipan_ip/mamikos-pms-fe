import { shallowMount } from '@vue/test-utils';
import MamipoinTnc from '../MamipoinTnc';
import localVueWithBuefy from '~/utils/addBuefy';

const $api = {
  getTnc: jest.fn(() => {
    return Promise.resolve({
      data: {
        status: true,
        tnc: 'tnc',
      },
    });
  }),
};

const $buefy = {
  toast: {
    open: jest.fn(),
  },
};

const mockComponent = {
  template: '<div />',
};

const stubs = {
  SchemeCard: mockComponent,
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $api,
    $buefy,
  },
  stubs,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(MamipoinTnc, finalData);
};

describe('MamipoinTnc.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.mamipoin-tnc').exists()).toBeTruthy();
  });

  it('should render c-loader when isLoading is true', async () => {
    await wrapper.setData({ isLoading: true });
    expect(wrapper.find('.c-loader').exists()).toBeTruthy();
  });

  it('should render tnc when isLoading is false', async () => {
    await wrapper.setData({ isLoading: false });
    expect(wrapper.find('.mamipoin-tnc__content').exists()).toBeTruthy();
  });

  it('should set tnc data and isLoading as false on successful api request', () => {
    expect(wrapper.vm.tnc).toBe('tnc');
    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should set tnc data as empty string when received falsy tnc on successful api request', () => {
    const mockApi = {
      getTnc: jest.fn().mockResolvedValueOnce({
        data: {
          status: true,
          tnc: null,
        },
      }),
    };

    wrapper = mount({ mocks: { $api: mockApi, $buefy } });
    expect(wrapper.vm.tnc).toBe('');
  });

  it('should set isLoading as false on failed api request', async () => {
    const mockApi = {
      getTnc: jest.fn().mockRejectedValueOnce(),
    };

    wrapper = mount({ mocks: { $api: mockApi, $buefy } });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isLoading).toBeFalsy();
  });
});
