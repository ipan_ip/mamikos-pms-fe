import Vuex, { mapState } from 'vuex';
import { shallowMount } from '@vue/test-utils';
import RewardContent from '../RewardContent';

import localVueWithBuefy from '~/utils/addBuefy';
import { toThousands } from '~/utils/number';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const mockComponent = {
  template: `<div />`,
};

const stubs = {
  MkCard: mockComponent,
  MkCardBody: mockComponent,
  MamipoinPlaceholder: mockComponent,
  RewardHeader: mockComponent,
  RewardCard: mockComponent,
  'router-link': mockComponent,
};

window.addEventListener = jest.fn();
window.removeEventListener = jest.fn();

const $api = {
  getRewardList: jest.fn().mockResolvedValue({
    data: {
      status: true,
      is_all_redeemed: false,
      data: [
        {
          remaining_quota: {
            total: 1,
            daily: 1,
            total_user: 1,
            daily_user: 1,
          },
        },
        {
          remaining_quota: {
            total: 0,
            daily: 0,
            total_user: 1,
            daily_user: 1,
          },
        },
      ],
    },
  }),
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('toThousands', toThousands);

window.HTMLElement.prototype.scrollIntoView = jest.fn();
window.HTMLElement.prototype.getBoundingClientRect = jest.fn(() => {
  return {
    top: -10,
  };
});

const storeData = {
  modules: {
    mamipoin: {
      namespaced: true,
      state: {
        rewardsPage: {
          firstRedeemedRewardId: '1',
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'mamipoin',
    },
    mapState,
    $api,
    $device,
    $refs: {
      rewardContentBody: `<div />`,
    },
  },
  store,
  stubs,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(RewardContent, finalData);
};

describe('RewardContent.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.reward-content').exists()).toBeTruthy();
  });

  it('should call addEventListener method in mobile view', () => {
    wrapper = mount({
      mocks: {
        ...mountData.mocks,
        ...{
          $device: {
            isDesktop: false,
          },
        },
      },
    });

    expect(window.addEventListener).toBeCalled();
  });

  it('should call removeEventListener method in destroyed life cycle', () => {
    wrapper = mount({
      mocks: {
        ...mountData.mocks,
        ...{
          $device: {
            isDesktop: false,
          },
        },
      },
    }).destroy();

    expect(window.removeEventListener).toBeCalled();
  });

  it('should set hasScrolledMobile as true when domPosition.top is less than n or equal to -10', async () => {
    wrapper = mount({
      mocks: {
        ...mountData.mocks,
        ...{
          $device: {
            isDesktop: false,
          },
        },
      },
    });
    wrapper.vm.$refs.rewardContent = {
      template: `<div />`,
      getBoundingClientRect: jest.fn(() => {
        return {
          top: -10,
        };
      }),
    };
    await wrapper.vm.listenToMobileScrollEvent();
    expect(wrapper.vm.hasScrolledMobile).toBeTruthy();
  });

  it('should set reward data on successful getRewardList api request', async () => {
    await wrapper.vm.fetchRewardList();
    expect(wrapper.vm.rewards.length).toBe(2);
    expect(wrapper.vm.isAllRedeemed).toBeFalsy();
    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should set point data as 0 when getting null point on successful getRewardList api request', async () => {
    wrapper.vm.$api.getRewardList = jest.fn().mockResolvedValue({
      data: {
        status: true,
        is_all_redeemed: true,
        data: null,
      },
    });
    await wrapper.vm.fetchRewardList();
    expect(wrapper.vm.rewards.length).toBe(0);
    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should set isLoading as false on failed getRewardList api request', async () => {
    wrapper.vm.$api.getRewardList = jest.fn().mockRejectedValue('error');
    await wrapper.vm.fetchRewardList();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isLoading).toBeFalsy();
  });
});
