import { shallowMount } from '@vue/test-utils';
import GeneralQuestions from '../GeneralQuestions';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      mocks: {
        $route: {
          name: 'mamipoin',
        },
        $device,
      },
    },
    ...adtMountData,
  };

  return shallowMount(GeneralQuestions, mountData);
};

describe('GeneralQuestions.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should return isDesktop value from $device.isDesktop', () => {
    expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
  });
});
