import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import _get from 'lodash/get';
import dayjs from 'dayjs';
import RewardCard from '../RewardCard';
import onboardingMixin from '~/components/page-features/mamipoin/mixins/onboarding';

import GuidePopover from '~/components/page-features/mamipoin/components/GuidePopover';
import MkCard from '~/components/global/molecules/MkCard';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';
import localVueWithBuefy from '~/utils/addBuefy';
import { toThousands } from '~/utils/number';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const mockParentComponent = {
  template: `<div />`,
  $refs: {
    rewardContent: '',
  },
};

const stubs = {
  GuidePopover,
  MkCard,
  MkCardBody,
  'router-link': {
    template: `<a />`,
  },
};

const $tracker = {
  send: jest.fn(),
};

const $buefy = {
  toast: {
    open: jest.fn(),
  },
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('toThousands', toThousands);
localVueWithBuefy.filter('shortenText', (text) => {
  return text && text.length > 40 ? text.substring(0, 40) + '...' : text;
});

window.HTMLElement.prototype.scrollIntoView = jest.fn();

const storeData = {
  modules: {
    mamipoin: {
      namespaced: true,
      state: {
        point: {
          total: 0,
        },
        rewardsPage: {
          hasViewedOnboarding: false,
          currentOnboardingStep: 'firstReward',
        },
        redeemDetailPage: {},
      },
      mutations: {
        setRewardsPage(state, rewardsPage = {}) {
          state.rewardsPage = rewardsPage;
        },
        setRedeemDetailPage(state, redeemDetailPage = {}) {
          state.redeemDetailPage = redeemDetailPage;
        },
      },
    },
    profile: {
      namespaced: true,
      state: {
        data() {
          return {
            total_mamipoin: 10,
          };
        },
      },
      getters: {
        membership: () => {
          return {
            is_mamipay_user: false,
          };
        },
        isHighlightDailyAllocationShown: () => false,
        isPremium: () => true,
        isBooking: () => true,
        showModalVerifyPhone: () => false,
        showModalNeedPassword: () => false,
        showModalPremiumTrial: () => false,
        showModalSurveyPremium: () => false,
        showModalDailyBalanceAllocation: () => false,
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mockRewardData = {
  id: 1,
  hasBeenRedeemed: true,
  name: '',
  redeem_value: 0,
  imageIcon: '',
  icon: '',
  redeem: [
    {
      id: 10,
    },
  ],
  index: 0,
  status: '',
};

const $router = {
  push: jest.fn(),
};

const mountData = {
  localVue: localVueWithBuefy,
  mixins: [onboardingMixin],
  mocks: {
    $route: {
      name: 'mamipoin-rewards',
    },
    $buefy,
    $router,
    $tracker,
    $device,
    $dayjs: dayjs,
  },
  propsData: {
    reward: mockRewardData,
  },
  store,
  provide() {
    return {
      parent: mockParentComponent,
    };
  },
  stubs,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(RewardCard, finalData);
};

jest.useFakeTimers();
describe('RewardCard.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.reward-card').exists()).toBeTruthy();
  });

  it('should return totalMamipoin value from profile.data.total_mamipoin', async () => {
    wrapper.vm.$store.state.profile.data.total_mamipoin = 10;
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.totalMamipoin).toBe(10);

    wrapper.vm.$store.state.profile.data = {};
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.totalMamipoin).toBe(0);
  });

  it('should call $tracker.send when calling sendTrackerEvent method', async () => {
    await wrapper.vm.sendTrackerEvent();

    expect($tracker.send).toHaveBeenCalled();
  });

  it('should return isDesktop value from $device.isDesktop', () => {
    expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
  });

  it('should return rewardsPage value from mamipoin.rewardsPage state', () => {
    expect(wrapper.vm.rewardsPage).toBe(_get(storeData, 'modules.mamipoin.state.rewardsPage'));
  });

  it('should return isPopoverShown as true when all of the requirements to show popover are met', () => {
    /* Requirements: user has not viewed onboarding, the current onboarding step is either 'firstReward' or 'firstRedeem', none of the modals are shown */

    wrapper.vm.rewardsPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'firstReward',
    };

    expect(wrapper.vm.isPopoverShown).toBeTruthy();
  });

  it('should return isPopoverShown as false when some of the requirements to show popover are not met', () => {
    /* Requirements: user has not viewed onboarding, the current onboarding step is either 'firstReward' or 'firstRedeem', none of the modals are shown */

    wrapper.vm.rewardsPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'pointCount',
    };

    expect(wrapper.vm.isPopoverShown).toBeFalsy();
  });

  it("should return popoverPlacement as 'bottom-left' in desktop view", () => {
    expect(wrapper.vm.popoverPlacement).toBe('bottom-left');
  });

  it('should return popoverPlacement value as bottom-left in mobile view', () => {
    const mocks = mountData.mocks;
    mocks.$device = {
      isDesktop: false,
    };

    wrapper = mount({ mocks });
    expect(wrapper.vm.popoverPlacement).toBe('bottom-left');
  });

  it('should return popoverOffset value as 0 if firstRewardPopover is shown and in desktop view', () => {
    const mocks = mountData.mocks;
    mocks.$device = {
      isDesktop: true,
    };
    wrapper = mount({ mocks });
    wrapper.vm.rewardsPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'firstReward',
    };

    expect(wrapper.vm.popoverOffset).toBe(0);
  });

  it('should return popoverText based on current onboarding step', () => {
    wrapper.vm.rewardsPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'firstReward',
    };

    expect(wrapper.vm.popoverText).toEqual({
      title: 'Hadiah Bisa Ditukar',
      message: 'Anda dapat menukar poin Anda sesuai dengan jumlah yang dibutuhkan hadiah terkait.',
    });
  });

  it('should return popoverText as {} when current onboarding step is neither firstRedeem nor firstReward', () => {
    wrapper.vm.rewardsPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'faq',
    };

    expect(wrapper.vm.popoverText).toEqual({});
  });

  it('should call closePopover function and set rewardsPage data when isFirstRewardPopoverShown value returns true', async () => {
    wrapper.vm.rewardsPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'firstReward',
    };

    await wrapper.vm.closePopover();
    expect(wrapper.vm.rewardsPage).toEqual({
      hasViewedOnboarding: false,
      currentOnboardingStep: 'faq',
    });
  });

  // it('should call $tracker.send when sending tracker event', async () => {
  //   await wrapper.vm.sendTrackerEvent();

  //   expect($tracker.send).toHaveBeenCalled();
  // });
});
