import { shallowMount } from '@vue/test-utils';
import dayjs from 'dayjs';
import ExchangeDetail from '../ExchangeDetail';
import redeemDictionary from '../../json/redeemDictionary.json';
import localVueWithBuefy from '~/utils/addBuefy';

import { toThousands } from '~/utils/number';
import { shortenText } from '~/utils/string';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const mockRedeemData = {
  status: 'onprocess',
  history: [
    {
      status: 'failed',
      date: '2020-08-20 20:03:00',
    },
    {
      status: 'redemeed',
      date: null,
    },
    { status: 'onprocess' },
    {
      status: 'success',
    },
    { status: 'redeemed' },
    { status: 'onprocess' },
  ],
  notes: 'test',
  date: '2020-08-20 20:03:00',
};
const $api = {
  getRedeemDetail: jest.fn().mockResolvedValue({
    data: {
      status: true,
      data: mockRedeemData,
    },
  }),
};

localVueWithBuefy.filter('toThousands', toThousands);
localVueWithBuefy.filter('shortenText', shortenText);

window.scrollTo = jest.fn();
global.require = jest.fn((text) => {
  return JSON.stringify(text);
});

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'mamipoin-redeem-detail',
      params: {
        redeem_id: 1,
      },
    },
    $device,
    $api,
    $dayjs: dayjs,
    redeemDictionary,
  },
};
const mount = (adtMountData = {}) => {
  const finalData = {
    ...mountData,
    ...adtMountData,
  };

  return shallowMount(ExchangeDetail, finalData);
};

describe('ExchangeDetail.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  // mock svg file
  jest.mock(`~/assets/images/mamipoin/gift-icon.svg`, () => {
    return 'gift-icon';
  });
  jest.mock(`~/assets/images/mamipoin/clock-icon-white-filled.svg`, () => {
    return 'clock-icon-white-filled';
  });

  it('should return isDesktop value from $device.isDesktop', () => {
    expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
  });

  it('should return redeemId value from $route.params.redeem_id', () => {
    expect(wrapper.vm.redeemId).toBe(1);
  });

  it('should set redeem data on successful api request', async () => {
    await wrapper.vm.fetchRedeemDetail();

    expect(wrapper.vm.redeem).not.toBe({});
  });

  it('should map redeem history properly', () => {
    const history = wrapper.vm.mapRedeemHistory({
      onprocess: [
        {
          date: '2012-08-10 08:09:10',
          status: 'onprocess',
        },
      ],
      redeemed: [
        {
          date: null,
          status: 'redeemed',
        },
      ],
    });

    expect(history[0].dateString).toBe('10 Aug 2012 08:09');
    expect(history[1].dateString).toBe('');
  });

  it('should concat onprocess status properly', () => {
    const prevSteps = [
      {
        status: 'redeemed',
        dateString: '10 Aug 2020 08:09',
      },
    ];

    // steps contain a step with "redeemed" status
    let steps = wrapper.vm.concatOnProcessStatus(prevSteps);
    expect(steps).toEqual([
      ...prevSteps,
      {
        title: 'Diproses',
        status: 'onprocess',
        image: 'clock-icon-white-filled',
        order: 1,
        className: 'is-previous --done',
        dateString: '10 Aug 2020 08:09',
      },
    ]);

    // steps didn't contain any step with "redeemed" status
    steps = wrapper.vm.concatOnProcessStatus([]);
    expect(steps).toEqual([
      {
        title: 'Diproses',
        status: 'onprocess',
        image: 'clock-icon-white-filled',
        order: 1,
        className: 'is-previous --done',
        dateString: undefined,
      },
    ]);
  });

  it('should concat finished status properly', () => {
    const prevSteps = [
      {
        status: 'redeemed',
        dateString: '10 Aug 2020 08:09',
      },
    ];
    const steps = wrapper.vm.concatOnProcessStatus(prevSteps);

    expect(steps).toEqual([
      ...prevSteps,
      {
        title: 'Diproses',
        status: 'onprocess',
        image: 'clock-icon-white-filled',
        order: 1,
        className: 'is-previous --done',
        dateString: '10 Aug 2020 08:09',
      },
    ]);
  });

  it('should filter duplicated status properly', () => {
    const prevSteps = [
      {
        status: 'success',
      },
      {
        status: 'failed',
      },
    ];

    // success
    wrapper.setData({
      redeem: {
        status: 'success',
      },
    });
    let steps = wrapper.vm.filterDuplicateFinishedStatus(prevSteps);
    expect(steps).toEqual([
      {
        status: 'success',
      },
    ]);

    // failed
    wrapper.setData({
      redeem: {
        status: 'failed',
      },
    });
    steps = wrapper.vm.filterDuplicateFinishedStatus(prevSteps);
    expect(steps).toEqual([
      {
        status: 'failed',
      },
    ]);
  });

  it('should set isLoading as false when catching api request failure', async () => {
    wrapper.vm.$api.getRedeemDetail = jest.fn().mockRejectedValue('error');
    await wrapper.vm.fetchRedeemDetail();
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.isLoading).toBeFalsy();
  });
});
