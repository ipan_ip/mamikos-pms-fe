import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import _get from 'lodash/get';
import RewardHeader from '../RewardHeader';
import onboardingMixin from '~/components/page-features/mamipoin/mixins/onboarding';

import GuidePopover from '~/components/page-features/mamipoin/components/GuidePopover';
import localVueWithBuefy from '~/utils/addBuefy';
import { toThousands } from '~/utils/number';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const mockParentComponent = {
  template: `<div />`,
  $refs: {
    rewardContent: '',
    rewardContentBody: '',
  },
};

const stubs = {
  GuidePopover,
};

const $api = {
  getPointTotal: jest.fn().mockResolvedValue({
    data: {
      status: true,
      point: '100',
    },
  }),
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('toThousands', toThousands);

window.HTMLElement.prototype.scrollIntoView = jest.fn();
window.scroll = jest.fn();
window.scrollTo = jest.fn();

const storeData = {
  modules: {
    mamipoin: {
      namespaced: true,
      state: {
        rewardsPage: {
          hasViewedOnboarding: false,
          currentOnboardingStep: 'firstReward',
        },
      },
      mutations: {
        setRewardsPage(state, rewardsPage = {}) {
          state.rewardsPage = rewardsPage;
        },
      },
    },
    profile: {
      namespaced: true,
      getters: {
        isHighlightDailyAllocationShown: () => false,
        isBooking: () => true,
        showModalVerifyPhone: () => false,
        showModalNeedPassword: () => false,
        showModalPremiumTrial: () => false,
        showModalSurveyPremium: () => false,
        showModalDailyBalanceAllocation: () => false,
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mixins: [onboardingMixin],
  mocks: {
    $route: {
      name: 'mamipoin',
    },
    $api,
    $device,
  },
  store,
  provide() {
    return {
      parent: mockParentComponent,
    };
  },
  stubs,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(RewardHeader, finalData);
};

describe('RewardHeader.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.reward-header').exists()).toBeTruthy();
  });

  it('should return isDesktop value from $device.isDesktop', () => {
    expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
  });

  it('should return rewardsPage value from mamipoin.rewardsPage state', () => {
    expect(wrapper.vm.rewardsPage).toBe(_get(storeData, 'modules.mamipoin.state.rewardsPage'));
  });

  it('should return isPopoverShown as true when all of the requirements to show popover are met', () => {
    /* Requirements: user has not viewed onboarding, the current onboarding step is either 'pointCount', or 'faq', none of the modals are shown */

    wrapper.vm.rewardsPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'pointCount',
    };

    expect(wrapper.vm.isPopoverShown).toBeTruthy();
  });

  it('should return isPopoverShown as false when some of the requirements to show popover are not met', () => {
    /* Requirements: user has not viewed onboarding, the current onboarding step is either 'firstReward' or 'firstRedeem', none of the modals are shown */

    wrapper.vm.rewardsPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'firstRedeem',
    };

    expect(wrapper.vm.isPopoverShown).toBeFalsy();
  });

  it('should call setBodyOverflow function on isPopoverShown value change in mobile view', async () => {
    wrapper.vm.$device.isDesktop = false;
    wrapper.vm.setBodyOverflow = jest.fn();
    wrapper.vm.rewardsPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'firstRedeem',
    };
    await wrapper.vm.$nextTick();
    wrapper.vm.rewardsPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'pointCount',
    };
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.setBodyOverflow).toBeCalled();
  });

  it('should return popoverText based on current onboarding step', () => {
    wrapper.vm.rewardsPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'pointCount',
    };

    expect(wrapper.vm.popoverText).toEqual({
      title: 'Poin Anda',
      message: 'Pastikan poin Anda cukup untuk ditukarkan dengan hadiah yang Anda inginkan.',
    });
  });

  it('should return popoverText as {} when current onboarding step is neither pointCount nor faq', () => {
    wrapper.vm.rewardsPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'firstRedeem',
    };

    expect(wrapper.vm.popoverText).toEqual({});
  });

  it('should set rewardsPage data when calling showRewardOnboarding function', async () => {
    await wrapper.vm.showRewardOnboarding();
    expect(wrapper.vm.rewardsPage).toEqual({
      hasViewedOnboarding: false,
      currentOnboardingStep: 'pointCount',
    });
  });

  it('should call closePopover function and set rewardsPage data when current onboarding step is pointCount', () => {
    wrapper.vm.rewardsPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'pointCount',
    };

    const guidePopover = wrapper.find('.guide-popover');
    if (guidePopover && guidePopover.exists()) {
      guidePopover.vm.$emit('click:popover-button');
      expect(wrapper.vm.rewardsPage).toEqual({
        hasViewedOnboarding: false,
        currentOnboardingStep: 'firstReward',
      });
    }
  });

  it('should set point data on successful getPointTotal api request', async () => {
    await wrapper.vm.fetchPoint();
    expect(wrapper.vm.point).toBe(100);
  });

  it('should set point data as 0 when getting null point on successful getPointTotal api request', async () => {
    wrapper.vm.$api.getPointTotal = jest.fn().mockResolvedValue({
      data: {
        status: true,
        point: null,
      },
    });
    await wrapper.vm.fetchPoint();
    expect(wrapper.vm.point).toBe(0);
  });

  it('should call closePopover function and set rewardsPage data when current onboarding step is faq', () => {
    wrapper.vm.rewardsPage = {
      hasViewedOnboarding: false,
      currentOnboardingStep: 'faq',
    };

    const guidePopover = wrapper.find('.guide-popover');
    if (guidePopover && guidePopover.exists()) {
      guidePopover.vm.$emit('click:popover-button');
      expect(wrapper.vm.rewardsPage).toEqual({
        hasViewedOnboarding: true,
        currentOnboardingStep: 'faq',
      });
    }
  });

  it('should call viewRedeemDetail function and emitted an event when clicking on "lihat status" link on redeemed reward card in desktop view', async () => {
    wrapper.vm.$emit = jest.fn();
    wrapper.vm.$device.isDesktop = true;

    const statusLink = wrapper.find('.status-link.--redeemed');
    if (statusLink && statusLink.exists()) {
      await statusLink.trigger('click');

      expect(wrapper.vm.$emit).toBeCalled();
    }
  });
});
