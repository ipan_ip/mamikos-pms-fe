import { shallowMount } from '@vue/test-utils';
import mobile from '../mobile';
import localVueWithBuefy from '~/utils/addBuefy';

const mockComponent = {
  template: '<div />',
};

const stubs = {
  ExchangeForm: mockComponent,
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      stubs,
    },
    ...adtMountData,
  };

  return shallowMount(mobile, mountData);
};

describe('mobile.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.reward-exchange-page.--mobile').exists()).toBeTruthy();
  });
});
