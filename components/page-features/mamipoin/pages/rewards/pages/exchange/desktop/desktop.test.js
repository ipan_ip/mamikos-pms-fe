import { shallowMount } from '@vue/test-utils';
import desktop from '../desktop';
import localVueWithBuefy from '~/utils/addBuefy';

const mockComponent = {
  template: '<div />',
};

const stubs = {
  ExchangeForm: mockComponent,
  RouterLink: {
    template: '<a />',
  },
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      stubs,
      mocks: {
        $route: {
          params: {
            reward_id: '1',
          },
        },
      },
    },
    ...adtMountData,
  };

  return shallowMount(desktop, mountData);
};

describe('desktop.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.reward-exchange.--desktop').exists()).toBeTruthy();
  });

  it('should compute rewardId properly', () => {
    // has reward_id params
    expect(wrapper.vm.rewardId).toBe('1');

    // has no params
    wrapper.vm.$route.params = {};
    expect(wrapper.vm.rewardId).toBe('');
  });

  it('should compute rewardDetailUrl properly', () => {
    expect(wrapper.vm.rewardDetailUrl).toBe('/mamipoin/rewards/1');
  });
});
