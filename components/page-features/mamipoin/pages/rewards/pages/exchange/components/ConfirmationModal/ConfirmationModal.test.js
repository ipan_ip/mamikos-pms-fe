import { shallowMount } from '@vue/test-utils';
import ConfirmationModal from '../ConfirmationModal';
import localVueWithBuefy from '~/utils/addBuefy';

import MkModal, { mixin as modalMixin } from '~/components/global/molecules/MkModal/MkModal';
import MkCard from '~/components/global/molecules/MkCard';
import MkCardHeader from '~/components/global/molecules/MkCard/components/MkCardHeader';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';
import MkCardFooter from '~/components/global/molecules/MkCard/components/MkCardFooter';

import MkButton from '~/components/global/atoms/MkButton';

const stubs = {
  MkModal,
  MkCard,
  MkCardBody,
  MkCardFooter,
  MkCardHeader,
  MkButton,
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      mixins: [modalMixin],
      mocks: {
        $route: {
          name: 'mamipoin',
        },
      },
      stubs,
    },
    ...adtMountData,
  };

  return shallowMount(ConfirmationModal, mountData);
};

describe('ConfirmationModal.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should set isShowCard value and emit an event when calling hideModal function', async () => {
    wrapper.vm.$emit = jest.fn();
    await wrapper.vm.hideModal(false);

    expect(wrapper.vm.isShowCard).toBeFalsy();
    expect(wrapper.vm.$emit).toBeCalled();
  });

  it('should emit an event when calling emitConfirmation function', async () => {
    wrapper.vm.$emit = jest.fn();
    await wrapper.vm.emitConfirmation(true);

    expect(wrapper.vm.$emit).toBeCalled();
    expect(wrapper.vm.$emit.mock.calls[0][0]).toBe('submit');
    expect(wrapper.vm.$emit.mock.calls[0][1]).toBe(true);
  });

  it('should emit an event and the event value must be false when calling emitConfirmation function without param', async () => {
    wrapper.vm.$emit = jest.fn();
    await wrapper.vm.emitConfirmation();

    expect(wrapper.vm.$emit).toBeCalled();
    expect(wrapper.vm.$emit.mock.calls[0][0]).toBe('submit');
    expect(wrapper.vm.$emit.mock.calls[0][1]).toBe(false);
  });

  it('should set the isShowCard value on active value change', async () => {
    await wrapper.setProps({ active: true });
    expect(wrapper.vm.isShowCard).toBe(true);

    await wrapper.setProps({ active: false });
    expect(wrapper.vm.isShowCard).toBe(false);
  });
});
