import { shallowMount } from '@vue/test-utils';
import Vuex, { mapGetters } from 'vuex';
import dayjs from 'dayjs';
import Vuelidate, { validationMixin } from 'vuelidate';
import { required, numeric, minLength, maxLength } from 'vuelidate/lib/validators';
import ExchangeForm from '../ExchangeForm';
import ConfirmationModal from '../../../exchange/components/ConfirmationModal';
import localVueWithBuefy from '~/utils/addBuefy';

import { toThousands } from '~/utils/number';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const stubs = {
  ConfirmationModal,
};

const $api = {
  getPointTotal: jest.fn().mockResolvedValue({
    data: {
      status: true,
      point: 10,
    },
  }),
  getRewardDetail: jest.fn().mockResolvedValue({
    data: {
      status: true,
      data: {
        name: 'nama reward',
        remaining_quota: {
          total: 1,
          daily: 1,
          total_user: 1,
          daily_user: 1,
        },
        redeem_value: '10',
        start_date: '2020-08-20 08:09:10',
        end_date: '2020-08-21 08:09:10',
      },
    },
  }),
  redeemReward: jest.fn().mockResolvedValue({
    data: {
      status: true,
      redeem_id: 1000,
    },
  }),
};

const $buefy = {
  toast: {
    open: jest.fn(),
  },
};

const $router = {
  push: jest.fn(),
};

const $tracker = {
  send: jest.fn(),
};

const storeData = {
  modules: {
    mamipoin: {
      namespaced: true,
      state: {
        redeemDetailPage: {},
      },
      mutations: {
        setRedeemDetailPage(state, redeemDetailPage) {
          state.redeemDetailPage = redeemDetailPage;
        },
      },
    },
    profile: {
      namespaced: true,
      getters: {
        membership() {
          return {
            is_mamipay_user: true,
          };
        },
        isBooking: () => true,
        isPremium: () => true,
        user() {
          return {
            name: 'user',
            phone_number: '081234567890',
          };
        },
      },
    },
  },
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.use(Vuelidate);
localVueWithBuefy.filter('toThousands', toThousands);

window.scrollTo = jest.fn();

const store = new Vuex.Store(storeData);
const mountData = {
  localVue: localVueWithBuefy,
  mixins: [validationMixin],
  mocks: {
    $route: {
      name: 'mamipoin-rewards-exchange',
      path: '/mamipoin/rewards/1/exchange',
      params: {
        reward_id: '1',
      },
    },
    $api,
    $router,
    $device,
    $tracker,
    $buefy,
    stubs,
    $dayjs: dayjs,
    mapGetters,
    required,
    numeric,
    minLength,
    maxLength,
  },
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(ExchangeForm, finalData);
};

describe('ExchangeForm.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.exchange-form').exists()).toBeTruthy();

    // desktop view
    expect(wrapper.find('.exchange-form__form.--desktop').exists()).toBeTruthy();
  });

  it('should return isDesktop value from $device.isDesktop', () => {
    expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
  });

  it('should return rewardId properly', () => {
    expect(wrapper.vm.rewardId).toBe('1');
  });

  it('should return errorMessages properly', () => {
    expect(wrapper.vm.errorMessages).toEqual({
      name: 'Nama wajib diisi',
      rElectricityNumber: 'Mohon masukkan nomor token listrik yg valid',
      rPhoneNumber: 'Mohon masukkan nomor HP yg valid',
      rAddress: 'Alamat pengiriman wajib diisi dan tidak boleh lebih dari 90 karakter',
      phoneNumber: 'Mohon masukkan nomor HP yg valid',
      note: 'Catatan tidak boleh lebih dari 90 karakter',
    });
  });

  it('should return isFormFieldInvalid properly', () => {
    // required
    wrapper.vm.hasFocusedOnce.name = true;
    expect(wrapper.vm.isFormFieldInvalid.name).toBe(false);

    // required
    wrapper.vm.hasFocusedOnce.phoneNumber = true;
    expect(wrapper.vm.isFormFieldInvalid.phoneNumber).toBe(false);

    // only required when reward.type === virtual_goods_pln
    wrapper.vm.hasFocusedOnce.rElectricityNumber = true;
    expect(wrapper.vm.isFormFieldInvalid.rElectricityNumber).toBe(true);

    // only required when reward.type === virtual_goods_pulsa
    wrapper.vm.hasFocusedOnce.rPhoneNumber = true;
    expect(wrapper.vm.isFormFieldInvalid.rPhoneNumber).toBe(true);

    // only required when reward.type === shipping_goods
    wrapper.vm.hasFocusedOnce.rAddress = true;
    expect(wrapper.vm.isFormFieldInvalid.rAddress).toBe(true);

    // not required
    wrapper.vm.hasFocusedOnce.note = true;
    expect(wrapper.vm.isFormFieldInvalid.note).toBe(false);
  });

  it('should reset receiver phone number on isReceiverSameAsOwner data change and the reward type is virtual_goods_pulsa', async () => {
    wrapper.vm.reward.type = 'virtual_goods_pulsa';
    wrapper.vm.formData.isReceiverSameAsOwner = true;
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.formData.rPhoneNumber).toBe('081234567890');

    wrapper.vm.formData.isReceiverSameAsOwner = false;
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.formData.rPhoneNumber).toBe('');
  });

  it('should return requiredPoint properly', async () => {
    await wrapper.setData({
      reward: {
        redeem_value: 10,
      },
      point: 100,
    });
    expect(wrapper.vm.requiredPoint).toBe(-90);

    await wrapper.setData({
      reward: {},
      point: 100,
    });
    expect(wrapper.vm.requiredPoint).toBe(-90);
  });

  it('should return mapGetters result from profile properly', () => {
    expect(wrapper.vm.user).toEqual({
      name: 'user',
      phone_number: '081234567890',
    });
  });

  it('should set point data on successful getPointTotal api request', async () => {
    await wrapper.vm.fetchPoint();
    expect(wrapper.vm.point).toBe(10);
  });

  it('should set reward data on successful getRewardDetail api request', async () => {
    await wrapper.vm.fetchRewardDetail();
    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should set isLoading as false on failed getRewardDetail api request', async () => {
    const mocks = {
      ...mountData.mocks,
      ...{
        $api: {
          ...$api,
          ...{
            getRewardDetail: jest.fn().mockRejectedValue('error'),
          },
        },
      },
    };

    wrapper = mount({ mocks });

    await wrapper.vm.fetchRewardDetail();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should set isWaitingConfirmation and isConfirmationDialogShown value as false when calling handleConfirmationDialogSubmission without params', async () => {
    await wrapper.vm.handleConfirmationDialogSubmission();

    expect(wrapper.vm.isConfirmationDialogShown).toBeFalsy();
    expect(wrapper.vm.isWaitingConfirmation).toBeFalsy();
  });

  it('should set isWaitingConfirmation and isConfirmationDialogShown value as false when calling showConfirmationDialog', async () => {
    await wrapper.vm.showConfirmationDialog();

    expect(wrapper.vm.isConfirmationDialogShown).toBeTruthy();
    expect(wrapper.vm.isWaitingConfirmation).toBeTruthy();
  });

  it('should hide confirmation modal when user chose to close the modal', async () => {
    await wrapper.vm.handleConfirmationDialogSubmission(false);

    expect(wrapper.vm.isConfirmationDialogShown).toBeFalsy();
  });

  it('should call submitForm when calling handleConfirmationDialogSubmission and received isConfirmed param as true', async () => {
    wrapper.vm.submitForm = jest.fn();
    await wrapper.vm.handleConfirmationDialogSubmission(true);

    expect(wrapper.vm.submitForm).toBeCalled();
  });

  it('should return generated request properly', () => {
    // no type
    let request = wrapper.vm.generateRequest();
    expect(request).toEqual({
      id: 1,
      recipient_name: 'user',
      recipient_phone: '081234567890',
    });

    // type = virtual_goods_pln
    wrapper.vm.reward.type = 'virtual_goods_pln';
    wrapper.vm.formData.rElectricityNumber = '1234567890';
    request = wrapper.vm.generateRequest();
    expect(request).toEqual({
      id: 1,
      recipient_data: '1234567890',
      recipient_name: 'user',
      recipient_phone: '081234567890',
    });

    // type = virtual_goods_pulsa
    wrapper.vm.reward.type = 'virtual_goods_pulsa';
    wrapper.vm.formData.rPhoneNumber = '081234567891';
    request = wrapper.vm.generateRequest();
    expect(request).toEqual({
      id: 1,
      recipient_data: '081234567891',
      recipient_name: 'user',
      recipient_phone: '081234567890',
    });

    // type = shipping_goods
    wrapper.vm.reward.type = 'shipping_goods';
    wrapper.vm.formData.rAddress = 'test';
    wrapper.vm.formData.note = 'test';

    request = wrapper.vm.generateRequest();
    expect(request).toEqual({
      id: 1,
      recipient_data: 'test',
      recipient_notes: 'test',
      recipient_name: 'user',
      recipient_phone: '081234567890',
    });
  });

  it('should call $buefy.toast.open and redirect user to redeem detail page on successful redeem request in mobile view on successful redeem api request', async () => {
    const mocks = {
      ...mountData.mocks,
      ...{
        $device: {
          isDesktop: false,
        },
      },
    };

    wrapper = mount({ mocks });
    wrapper.vm.rewardsPage = {
      hasViewedFirstRedeem: true,
    };

    await wrapper.vm.submitForm();
    await wrapper.vm.$nextTick();
    expect($buefy.toast.open).toBeCalled();
    expect(wrapper.vm.$router.push).toBeCalled();

    const calls = wrapper.vm.$router.push.mock.calls || [[]];
    expect(calls[0][0]).toBe('/mamipoin/redeem/1000');
  });

  it('should call $buefy.toast.open on failed redeem api request', async () => {
    const mocks = {
      ...mountData.mocks,
      ...{
        $api: {
          ...mountData.mocks.$api,
          ...{
            redeemReward: jest.fn().mockRejectedValue('error'),
          },
        },
      },
    };

    wrapper = mount({ mocks });
    await wrapper.vm.submitForm();
    await wrapper.vm.$nextTick();
    expect($buefy.toast.open).toBeCalled();
  });

  it('should redirect user to mamipoin rewards page on successful api request when not receiving redeem id', async () => {
    const mocks = {
      ...mountData.mocks,
      ...{
        $api: {
          ...mountData.mocks.$api,
          ...{
            redeemReward: jest.fn().mockResolvedValue({
              data: {
                status: true,
              },
            }),
          },
        },
      },
    };

    wrapper = mount({ mocks });

    await wrapper.vm.submitForm();
    await wrapper.vm.$nextTick();

    expect($buefy.toast.open).toBeCalled();
    expect(wrapper.vm.$router.push).toBeCalled();
    const calls = wrapper.vm.$router.push.mock.calls || [[]];
    expect(calls[1][0]).toBe('/mamipoin/rewards');
  });

  it('should call $buefy.toast.open on successful api request when received status: false', async () => {
    const mocks = {
      ...mountData.mocks,
      ...{
        $api: {
          ...mountData.mocks.$api,
          ...{
            redeemReward: jest.fn().mockResolvedValue({
              data: {
                status: false,
              },
            }),
          },
        },
      },
    };

    wrapper = mount({ mocks });

    await wrapper.vm.submitForm();
    await wrapper.vm.$nextTick();

    expect($buefy.toast.open).toBeCalled();
  });
});
