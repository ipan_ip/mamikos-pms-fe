import { shallowMount } from '@vue/test-utils';
import mobile from '../mobile';
import localVueWithBuefy from '~/utils/addBuefy';

const mockComponent = {
  template: '<div />',
};

const stubs = {
  ActionCard: mockComponent,
  DetailCard: mockComponent,
  RouterLink: {
    template: '<a />',
  },
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      stubs,
    },
    ...adtMountData,
  };

  return shallowMount(mobile, mountData);
};

describe('mobile.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.reward-detail-page.--mobile').exists()).toBeTruthy();
  });

  it('should show c-loader when either one of detail or action card is loading', () => {
    wrapper.setData({
      isLoadingActionCard: true,
    });

    expect(wrapper.find('.c-loader').exists()).toBeTruthy();
  });
});
