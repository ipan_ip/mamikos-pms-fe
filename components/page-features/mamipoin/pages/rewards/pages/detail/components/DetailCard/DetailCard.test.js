import { shallowMount } from '@vue/test-utils';
import DetailCard from '../DetailCard';
import localVueWithBuefy from '~/utils/addBuefy';

import MkCard from '~/components/global/molecules/MkCard';
import MkCardHeader from '~/components/global/molecules/MkCard/components/MkCardHeader';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const stubs = {
  MkCard,
  MkCardHeader,
  MkCardBody,
};

const $api = {
  getRewardDetail: jest.fn().mockResolvedValue({
    data: {
      status: true,
      data: {
        name: 'nama reward',
        description: 'desc',
        tnc: 'tnc',
        howto: 'howto',
        remaining_quota: {
          total: 1,
          daily: 1,
          total_user: 1,
          daily_user: 1,
        },
        image: {
          medium: 'http://mamikos.com/imageUrl.jpg',
        },
        redeem_value: '10',
        start_date: '2020-08-20 08:09:10',
        end_date: '2020-08-21 08:09:10',
      },
    },
  }),
};

const $router = {
  push: jest.fn(),
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'mamipoin-rewards-detail',
      path: '/mamipoin/rewards/1',
      params: {
        reward_id: '1',
      },
    },
    $api,
    $router,
    $device,
    stubs,
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(DetailCard, finalData);
};

describe('DetailCard.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.detail-card').exists()).toBeTruthy();

    // desktop view
    expect(wrapper.find('.detail-card').classes()).toContain('--desktop');
    expect(wrapper.find('.card-content').classes()).toContain('--desktop');
  });

  it('should return isDesktop value from $device.isDesktop', () => {
    expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
  });

  it('should return rewardId properly', () => {
    expect(wrapper.vm.rewardId).toBe('1');
  });

  it('should set reward data on successful getRewardDetail api request', async () => {
    await wrapper.vm.fetchRewardDetail();
    expect(wrapper.vm.reward.description).toBe('desc');
    expect(wrapper.vm.reward.tnc).toBe('tnc');
    expect(wrapper.vm.reward.howto).toBe('howto');
    expect(wrapper.vm.reward.image.medium).toBe('http://mamikos.com/imageUrl.jpg');

    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should set isLoading as false on failed getRewardDetail api request', async () => {
    wrapper.vm.$api.getRewardDetail = jest.fn().mockRejectedValue('error');
    await wrapper.vm.fetchRewardDetail();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isLoading).toBeFalsy();
  });
});
