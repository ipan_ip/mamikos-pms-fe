import { shallowMount } from '@vue/test-utils';
import Vuex, { mapGetters } from 'vuex';
import dayjs from 'dayjs';
import ActionCard from '../ActionCard';
import ConfirmationModal from '../../../exchange/components/ConfirmationModal';
import localVueWithBuefy from '~/utils/addBuefy';
import MkCard from '~/components/global/molecules/MkCard';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';
import { toThousands } from '~/utils/number';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const stubs = {
  ConfirmationModal,
  MkCard,
  MkCardBody,
};

const $api = {
  getPointTotal: jest.fn().mockResolvedValue({
    data: {
      status: true,
      point: 10,
    },
  }),
  getRewardDetail: jest.fn().mockResolvedValue({
    data: {
      status: true,
      data: {
        name: 'nama reward',
        remaining_quota: {
          total: 1,
          daily: 1,
          total_user: 1,
          daily_user: 1,
        },
        redeem_value: '10',
        start_date: '2020-08-20 08:09:10',
        end_date: '2020-08-21 08:09:10',
      },
    },
  }),
  redeemReward: jest.fn().mockResolvedValue({
    data: {
      status: true,
      redeem_id: 1000,
    },
  }),
};

const $buefy = {
  toast: {
    open: jest.fn(),
  },
};

const $router = {
  push: jest.fn(),
};

const $tracker = {
  send: jest.fn(),
};

const storeData = {
  modules: {
    mamipoin: {
      namespaced: true,
      state: {
        point: {
          total: 0,
        },
        rewardsPage: {
          hasViewedOnboarding: true,
          currentOnboardingStep: 'faq',
        },
      },
      mutations: {
        setPoint(state, point) {
          state.point = point;
        },
        setRewardsPage(state, rewardsPage) {
          state.rewardsPage = rewardsPage;
        },
      },
    },
    profile: {
      namespaced: true,
      getters: {
        membership() {
          return {
            is_mamipay_user: true,
          };
        },
        isBooking: () => true,
        isPremium: () => true,
        user() {
          return {
            name: 'user',
            phone_number: '081234567890',
          };
        },
      },
    },
  },
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('toThousands', toThousands);

window.scrollTo = jest.fn();

const store = new Vuex.Store(storeData);
const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'mamipoin-rewards-detail',
      path: '/mamipoin/rewards/1',
      params: {
        reward_id: '1',
      },
    },
    $api,
    $router,
    $device,
    $tracker,
    $buefy,
    stubs,
    $dayjs: dayjs,
    mapGetters,
  },
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(ActionCard, finalData);
};

describe('ActionCard.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.action-card').exists()).toBeTruthy();

    // desktop view
    expect(wrapper.find('.action-card').classes()).toContain('--desktop');
  });

  it('should return isDesktop value from $device.isDesktop', () => {
    expect(wrapper.vm.isDesktop).toBe($device.isDesktop);
  });

  it('should return rewardsPage value from $store.state.rewardsPage', () => {
    expect(wrapper.vm.rewardsPage).toBe(storeData.modules.mamipoin.state.rewardsPage);
  });

  it('should return exchangeUrl properly', () => {
    expect(wrapper.vm.exchangeUrl).toBe('/mamipoin/rewards/1/exchange');
  });

  it('should return rewardId properly', () => {
    expect(wrapper.vm.rewardId).toBe('1');
  });

  it('should return requiredPoint properly', () => {
    wrapper.setData({
      reward: {
        redeem_value: 10,
      },
      point: 100,
    });
    expect(wrapper.vm.requiredPoint).toBe(-90);
  });

  it('should return mapGetters result from profile properly', () => {
    expect(wrapper.vm.user).toEqual({
      name: 'user',
      phone_number: '081234567890',
    });
  });

  it('should set point data on successful getPointTotal api request', async () => {
    await wrapper.vm.fetchPoint();
    expect(wrapper.vm.point).toBe(10);
  });

  it('should set reward data on successful getRewardDetail api request', async () => {
    await wrapper.vm.fetchRewardDetail();
    expect(wrapper.vm.reward.endDateString).toBe('21 Aug 2020');
    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should handle empty reward and its redeem_value on sucessful getRewardDetail api request', async () => {
    const mocks = {
      ...mountData.mocks,
      ...{
        $api: {
          ...$api,
          ...{
            getRewardDetail: jest.fn().mockResolvedValue({
              data: {
                status: true,
              },
            }),
          },
        },
      },
    };

    wrapper = mount({ mocks });

    await wrapper.vm.fetchRewardDetail();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.reward.redeem_value).toBe(0);
  });

  it('should set isLoading as false on failed getRewardDetail api request', async () => {
    const mocks = {
      ...mountData.mocks,
      ...{
        $api: {
          ...$api,
          ...{
            getRewardDetail: jest.fn().mockRejectedValue('error'),
          },
        },
      },
    };

    wrapper = mount({ mocks });

    await wrapper.vm.fetchRewardDetail();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should call redeemModal function when user chose to confirm the modal', async () => {
    wrapper.vm.redeemReward = jest.fn();
    await wrapper.vm.handleConfirmationDialogSubmission(true);

    expect(wrapper.vm.redeemReward).toBeCalled();
  });

  it('should set the default isConfirmed value as false when calling handleConfirmationDialogSubmission without params', async () => {
    await wrapper.vm.handleConfirmationDialogSubmission();

    expect(wrapper.vm.isConfirmationDialogShown).toBeFalsy();
  });

  it('should hide confirmation modal when user chose to close the modal', async () => {
    await wrapper.vm.handleConfirmationDialogSubmission(false);

    expect(wrapper.vm.isConfirmationDialogShown).toBeFalsy();
  });

  it('should call $buefy.toast.open, set rewardsPage store data and redirect user to redeem page on successful redeem request in desktop view', async () => {
    await wrapper.vm.redeemReward();
    expect($buefy.toast.open).toBeCalled();
    expect($router.push).toBeCalled();
    expect($router.push.mock.calls[0][0]).toBe('/mamipoin/redeem/1000');
    expect(wrapper.vm.isSubmitting).toBeFalsy();
  });

  it('should call $buefy.toast.open, and redirect user to redeem detail page on successful redeem request when user has viewed first redeem onboarding in mobile view on successful redeem api request', async () => {
    const mocks = {
      ...mountData.mocks,
      ...{
        $device: {
          isDesktop: false,
        },
      },
    };

    wrapper = mount({ mocks });
    wrapper.vm.rewardsPage = {
      hasViewedFirstRedeem: true,
    };

    await wrapper.vm.redeemReward();
    expect($buefy.toast.open).toBeCalled();
    expect($router.push).toBeCalled();
    expect($router.push.mock.calls[1][0]).toBe('/mamipoin/redeem/1000');
  });

  it('should call $buefy.toast.open and set isSubmitting as false on succesful redeem api request when received falsy status', async () => {
    const mocks = {
      ...mountData.mocks,
      ...{
        $api: {
          ...mountData.mocks.$api,
          ...{
            redeemReward: jest.fn().mockResolvedValue({
              data: {
                status: false,
              },
            }),
          },
        },
      },
    };

    wrapper = mount({ mocks });
    await wrapper.vm.redeemReward();
    await wrapper.vm.$nextTick();

    expect($buefy.toast.open).toBeCalled();
    expect(wrapper.vm.isSubmitting).toBeFalsy();
  });

  it('should call $buefy.toast.open and set isSubmitting as false on failed redeem api request', async () => {
    const mocks = {
      ...mountData.mocks,
      ...{
        $api: {
          ...mountData.mocks.$api,
          ...{
            redeemReward: jest.fn().mockRejectedValue('error'),
          },
        },
      },
    };

    wrapper = mount({ mocks });
    await wrapper.vm.redeemReward();
    await wrapper.vm.$nextTick();

    expect($buefy.toast.open).toBeCalled();
    expect(wrapper.vm.isSubmitting).toBeFalsy();
  });

  it('should go to exchange page when the reward type is either virtual_goods_pln,virtual_goods_pulsa, or shipping_goods on action button click', async () => {
    wrapper.setData({
      reward: {
        type: 'shipping_goods',
      },
    });

    await wrapper.vm.handleActionButtonClick();

    expect($router.push).toBeCalled();
    expect($router.push.mock.calls[2][0]).toBe(wrapper.vm.exchangeUrl);
  });

  it('should show confirmation dialog when the reward type is neither virtual_goods_pln,virtual_goods_pulsa, nor shipping_goods on action button click', async () => {
    wrapper.setData({
      reward: {
        type: 'others',
      },
    });

    await wrapper.vm.handleActionButtonClick();

    expect(wrapper.vm.isConfirmationDialogShown).toBeTruthy();
  });

  it('should call $tracker.send when sending tracker event', async () => {
    await wrapper.vm.sendConfirmRedeemTrackerEvent();

    expect($tracker.send).toBeCalled();
  });

  it('should call $tracker.send when sending tracker event', async () => {
    await wrapper.vm.sendRedeemPointTrackerEvent();

    expect($tracker.send).toBeCalled();
  });

  it('should redirect user to mamipoin rewards page on succesful api request when not receiving redeem id', async () => {
    const mocks = {
      ...mountData.mocks,
      ...{
        $api: {
          ...mountData.mocks.$api,
          ...{
            redeemReward: jest.fn().mockResolvedValue({
              data: {
                status: true,
                redeem_id: null,
              },
            }),
          },
        },
      },
    };

    wrapper = mount({ mocks });

    wrapper.vm.redeemReward();
    await wrapper.vm.$nextTick();
    expect($router.push).toBeCalled();
    expect($router.push.mock.calls[3][0]).toBe('/mamipoin/rewards');
  });
});
