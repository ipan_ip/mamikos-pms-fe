import { shallowMount } from '@vue/test-utils';
import SchemeContent from '../SchemeContent';
import localVueWithBuefy from '~/utils/addBuefy';

const mockComponent = {
  template: '<div />',
};

const stubs = {
  DesktopScheme: mockComponent,
  MobileScheme: mockComponent,
};

const $device = {
  isDesktop: true,
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      mocks: {
        $device,
        $route: {
          name: 'mamipoin-scheme',
        },
      },
      stubs,
    },
    ...adtMountData,
  };

  return shallowMount(SchemeContent, mountData);
};

describe('SchemeContent.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.scheme-content').exists()).toBeTruthy();
  });

  it('should return isDesktop value from $device.isDesktop', () => {
    wrapper.vm.$device.isDesktop = true;
    expect(wrapper.vm.isDesktop).toBeTruthy();

    wrapper.vm.$device.isDesktop = false;
    expect(wrapper.vm.isDesktop).toBeFalsy();
  });
});
