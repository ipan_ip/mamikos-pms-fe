import { shallowMount } from '@vue/test-utils';
import Vuex, { mapGetters } from 'vuex';
import _zip from 'lodash/zip';
import _flatMap from 'lodash/flatMap';
import DesktopScheme from '../DesktopScheme';
import localVueWithBuefy from '~/utils/addBuefy';
import { toThousands } from '~/utils/number';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const mockSchemeData = [
  {
    title: 'test',
    type: 'test',
    is_active: true,
    activities: [
      {
        title: 'test',
        description: 'test',
        point_details: null,
      },
    ],
  },
  {
    title: 'test_2',
    type: 'test_2',
    activities: [
      {
        title: 'test_2',
        description: 'test_2',
        point_details: {
          min_point: 0,
          max_point: 10,
          limit_type: 'hari',
        },
      },
    ],
  },
];

const $api = {
  getPointScheme: jest.fn().mockResolvedValue({
    data: {
      status: true,
      data: mockSchemeData,
    },
  }),
};

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      getters: {
        membership() {
          return {
            is_mamipay_user: true,
          };
        },
        isBooking: () => true,
        isPremium: () => true,
        user() {
          return {
            name: 'user',
            phone_number: '081234567890',
          };
        },
      },
    },
  },
};

const formatPoint = (point) => {
  return parseInt(point) === 0 ? '-' : point;
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('toThousands', toThousands);
localVueWithBuefy.filter('formatPoint', formatPoint);

const $router = {
  push: jest.fn(),
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'mamipoin-scheme',
      path: '/mamipoin/scheme',
    },
    $api,
    $router,
    $device,
    mapGetters,
    _zip,
    _flatMap,
  },
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(DesktopScheme, finalData);
};

describe('DesktopScheme.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.desktop-scheme').exists()).toBeTruthy();
  });

  it('should set schemes data and isActive as false on successful getPointScheme api call', async () => {
    await wrapper.vm.fetchPointScheme();

    expect(wrapper.vm.isLoading).toBeFalsy();
    expect(wrapper.vm.schemes).toEqual(mockSchemeData);
  });

  it('should set isLoading as false on failed getPointScheme api call', async () => {
    wrapper.vm.$api.getPointScheme = jest.fn().mockRejectedValue('error');
    await wrapper.vm.fetchPointScheme();
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should return formatted point correctly', () => {
    const point = formatPoint('0');
    expect(point).toBe('-');
  });
});
