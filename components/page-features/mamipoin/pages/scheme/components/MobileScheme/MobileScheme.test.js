import { shallowMount } from '@vue/test-utils';
import Vuex, { mapGetters, mapState } from 'vuex';
import _zip from 'lodash/zip';
import _flatMap from 'lodash/flatMap';
import MobileScheme from '../MobileScheme';
import localVueWithBuefy from '~/utils/addBuefy';
import { toThousands } from '~/utils/number';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const $tracker = {
  send: jest.fn(),
};

const mockSchemeData = [
  {
    title: 'test',
    type: 'test',
    is_active: true,
    activities: [
      {
        title: 'test',
        description: 'test',
        point_details: null,
      },
    ],
  },
  {
    title: 'test_2',
    type: 'test_2',
    activities: [
      {
        title: 'test_2',
        description: 'test_2',
        point_details: {
          min_point: 0,
          max_point: 10,
          limit_type: 'hari',
        },
      },
    ],
  },
  {
    title: 'test_3',
    type: 'test_3',
    activities: [
      {
        title: 'test_3',
        description: 'test_3',
        point_details: {
          min_point: 0,
          max_point: 10,
          limit_type: 'bulan',
        },
      },
    ],
  },
];

const $api = {
  getPointScheme: jest.fn().mockResolvedValue({
    data: {
      status: true,
      data: mockSchemeData,
    },
  }),
};

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        data: {},
      },
      getters: {
        membership() {
          return {
            is_mamipay_user: true,
          };
        },
        isBooking: () => true,
        isPremium: () => true,
        user() {
          return {
            name: 'user',
            phone_number: '081234567890',
          };
        },
      },
    },
  },
};

const formatPoint = (point) => {
  return parseInt(point) === 0 ? '-' : point;
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('toThousands', toThousands);
localVueWithBuefy.filter('formatPoint', formatPoint);

const $router = {
  push: jest.fn(),
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'mamipoin-scheme',
      path: '/mamipoin/scheme',
    },
    $api,
    $router,
    $device,
    $tracker,
    mapState,
    mapGetters,
    _zip,
    _flatMap,
  },
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(MobileScheme, finalData);
};

describe('MobileScheme.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.mobile-scheme').exists()).toBeTruthy();
  });

  it('should set schemes data and isActive as false on successful getPointScheme api call', async () => {
    await wrapper.vm.fetchPointScheme();

    expect(wrapper.vm.isLoading).toBeFalsy();
    expect(wrapper.vm.schemes).toEqual(mockSchemeData);
  });

  it('should set main & comparison selectedPackage value on successful getPointScheme api call properly ', async () => {
    /*
      Rules: 
      active_package_index < schemes.length - 1 => main: active_package, comparison: active_package+1

      active_package_index === schemes.length - 1 => main: active_package-1 , comparison: active_package
    */

    // active package = 0
    await wrapper.vm.fetchPointScheme();
    expect(wrapper.vm.selectedPackage.main).toBe('0');
    expect(wrapper.vm.selectedPackage.comparison).toBe('2');

    // active package = 1
    wrapper.vm.$api.getPointScheme = jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: [
          {
            type: 'test',
          },
          {
            type: 'test_2',
            is_active: true,
          },
          {
            type: 'test_3',
          },
        ],
      },
    });
    await wrapper.vm.fetchPointScheme();
    expect(wrapper.vm.selectedPackage.main).toBe('1');
    expect(wrapper.vm.selectedPackage.comparison).toBe('2');

    // active package = 2
    wrapper.vm.$api.getPointScheme = jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: [
          {
            type: 'test',
          },
          {
            type: 'test_2',
          },
          {
            type: 'test_3',
            is_active: true,
          },
        ],
      },
    });
    await wrapper.vm.fetchPointScheme();
    expect(wrapper.vm.selectedPackage.main).toBe('1');
    expect(wrapper.vm.selectedPackage.comparison).toBe('2');
  });

  it('should set isLoading as false on failed getPointScheme api call', async () => {
    wrapper.vm.$api.getPointScheme = jest.fn().mockRejectedValue('error');
    await wrapper.vm.fetchPointScheme();
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should return point details properly when accessing selectedPackagePointDetails method', () => {
    // empty detail
    let pointDetails = wrapper.vm.selectedPackagePointDetails(mockSchemeData[0], 0);
    expect(pointDetails).toEqual({});

    // has detail
    pointDetails = wrapper.vm.selectedPackagePointDetails(mockSchemeData[1], 0);
    expect(pointDetails).toEqual({
      min_point: 0,
      max_point: 10,
      limit_type: 'hari',
    });
  });

  it('should set comparison package properly when calling setComparisonPackage method', async () => {
    // prev comparison exists
    await wrapper.setData({
      schemes: mockSchemeData,
      selectedPackage: {
        main: 0,
        comparison: '1',
      },
    });
    await wrapper.vm.setComparisonPackage();
    expect(wrapper.vm.selectedPackage.comparison).toBe('1');

    // prev comparison doesnt exist
    await wrapper.setData({
      selectedPackage: {
        main: 0,
        comparison: '10',
      },
    });
    await wrapper.vm.setComparisonPackage();
    expect(wrapper.vm.selectedPackage.comparison).toBe(1);
  });

  it('should compute packages properly', async () => {
    wrapper.vm.$api.getPointScheme = jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: [
          {
            title: 'test',
            type: 'test',
            activities: [],
          },
        ],
      },
    });

    await wrapper.vm.fetchPointScheme();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.packages).toEqual([
      {
        id: 0,
        type: 'test',
        label: 'test',
      },
    ]);
  });

  it('should return formatted point correctly', () => {
    const point = formatPoint('0');
    expect(point).toBe('-');
  });

  it('should call $tracker.send when calling sendTrackerEvent method', async () => {
    await wrapper.vm.sendTrackerEvent();

    expect(wrapper.vm.$tracker.send).toBeCalled();
  });

  it('should call setComparisonPackage and sendTrackerEvent when calling handleMainPackageChange method', async () => {
    const sendTrackerEventSpy = jest.spyOn(wrapper.vm, 'sendTrackerEvent');
    const setComparisonPackageSpy = jest.spyOn(wrapper.vm, 'setComparisonPackage');

    await wrapper.vm.handleMainPackageChange();

    expect(sendTrackerEventSpy).toBeCalled();
    expect(setComparisonPackageSpy).toBeCalled();

    sendTrackerEventSpy.mockRestore();
    setComparisonPackageSpy.mockRestore();
  });
});
