import { shallowMount } from '@vue/test-utils';
import Vuex, { mapGetters } from 'vuex';
import ActivityGuide from '../ActivityGuide';
import localVueWithBuefy from '~/utils/addBuefy';
import { toThousands } from '~/utils/number';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const mockSchemeData = [
  {
    title: 'test',
    type: 'test',
    is_active: true,
    activities: [
      {
        title: 'test',
        description: 'test',
        point_details: null,
      },
    ],
  },
  {
    title: 'test_2',
    type: 'test_2',
    activities: [
      {
        title: 'test_2',
        description: 'test_2',
        point_details: {
          min_point: 0,
          max_point: 10,
          limit_type: 'hari',
        },
      },
    ],
  },
];

const $api = {
  getPointScheme: jest.fn().mockResolvedValue({
    data: {
      status: true,
      data: mockSchemeData,
    },
  }),
};

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        data: {},
      },
      getters: {
        membership() {
          return {
            is_mamipay_user: true,
          };
        },
        isBooking: () => true,
        isPremium: () => true,
        user() {
          return {
            name: 'user',
            phone_number: '081234567890',
          };
        },
      },
    },
  },
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('toThousands', toThousands);
localVueWithBuefy.filter('formatPoint', (point) => {
  return parseInt(point) === 0 ? '-' : point;
});

const $router = {
  push: jest.fn(),
};

const $tracker = {
  send: jest.fn(),
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'mamipoin-scheme',
      path: '/mamipoin/scheme',
    },
    $api,
    $router,
    $device,
    $tracker,
    mapGetters,
  },
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(ActivityGuide, finalData);
};

describe('ActivityGuide.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.activity-guide').exists()).toBeTruthy();
  });

  it('should set active package when calling setActivePackage method', async () => {
    // profile.isBooking: true -> activePackage: regular
    await wrapper.vm.setActivePackage();
    expect(wrapper.vm.activePackage).toBe('regular');

    // profile.isBooking: false -> activePackage: ""
    const { state, getters, namespaced } = storeData.modules.profile;
    const mockStoreData = {
      modules: {
        profile: {
          namespaced,
          state,
          getters: {
            ...getters,
            ...{
              isBooking: () => false,
            },
          },
        },
      },
    };
    wrapper.destroy();
    wrapper = mount({
      store: new Vuex.Store(mockStoreData),
      methods: {
        fetchPointScheme: jest.fn(),
      },
    });
    expect(wrapper.vm.activePackage).toBe('');
  });

  it('should set schemes data and isActive as false on successful getPointScheme api call', async () => {
    await wrapper.vm.fetchPointScheme();

    expect(wrapper.vm.schemes).toEqual(mockSchemeData);
    expect(wrapper.vm.activePackage).toBe(0);
  });

  it('should set isLoading as false on failed getPointScheme api call', async () => {
    wrapper.vm.$api.getPointScheme = jest.fn().mockRejectedValue('error');
    await wrapper.vm.fetchPointScheme();
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should call $tracker.send on package change', async () => {
    const dropdown = wrapper.find('.activity-dropdown');
    if (dropdown && dropdown.exists()) {
      await dropdown.vm.$emit('change');

      await wrapper.vm.$nextTick();
      expect(wrapper.vm.$tracker.send).toBeCalled();
    }
  });

  it('should compute selectedPackageActivities properly', async () => {
    wrapper.vm.$api.getPointScheme = jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: mockSchemeData,
      },
    });

    await wrapper.vm.fetchPointScheme();

    expect(wrapper.vm.selectedPackageActivities).toEqual([
      {
        title: 'test',
        description: 'test',
        point_details: null,
      },
    ]);
  });
});
