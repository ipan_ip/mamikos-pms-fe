import { shallowMount } from '@vue/test-utils';
import desktop from '../desktop';
import localVueWithBuefy from '~/utils/addBuefy';

const mockComponent = {
  template: '<div />',
};

const stubs = {
  ExchangeDetail: mockComponent,
  GeneralQuestions: mockComponent,
  RouterLink: {
    template: '<a />',
  },
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      stubs,
    },
    ...adtMountData,
  };

  return shallowMount(desktop, mountData);
};

describe('desktop.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.redeem-detail.--desktop').exists()).toBeTruthy();
  });
});
