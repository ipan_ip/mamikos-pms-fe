import { send } from '~/plugins/tracker';

const mobileScreenBreakpoint = 768;
const isDesktop = window.innerWidth >= mobileScreenBreakpoint;

export function trackMamipoinPage({ userData }) {
  const moeData = {
    interface: isDesktop ? 'desktop' : 'mobile',
    redirection_source: `[Owner] ${document.title}`,
    is_owner: true,
    is_mamipay: userData.membership.is_mamipay_user,
    is_booking: userData.is_booking_all_room,
    is_premium_owner: userData.membership.expired && userData.membership.status === 'Premium',
    point_total: userData.total_mamipoin,
  };
  send('moe', ['[Owner] MamiPoin Page Visited', moeData]);
}
