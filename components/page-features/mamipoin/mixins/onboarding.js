export default {
  data() {
    return {
      isNotificationConfirmed: false,
    };
  },
  methods: {
    setDashboardOverflow(isOverflow = true) {
      const dashboardBody = document.getElementsByClassName('dashboard-desktop__body');

      if (dashboardBody && dashboardBody[0]) {
        dashboardBody[0].style.overflow = isOverflow ? 'auto' : 'hidden';
      }
    },
    setBodyOverflow(isAllowedToScroll = true) {
      const body = document.querySelector('body');
      body.style.overflowY = isAllowedToScroll ? '' : 'hidden';
      body.style.height = isAllowedToScroll ? '' : (window.innerHeight || 1000) + 'px';
    },
    listenToNotificationPermissionChange() {
      const base = this;
      const confirmations = ['granted', 'denied'];

      if ('permissions' in navigator) {
        navigator.permissions.query({ name: 'notifications' }).then(function(notificationPerm) {
          notificationPerm.onchange = function() {
            base.isNotificationConfirmed = confirmations.includes(notificationPerm.state);
          };
        });
      }
    },
    getVisibility(el, holder) {
      holder = holder || document.body;
      if (el && holder) {
        const { top, bottom, height } = el.getBoundingClientRect();
        const holderRect = holder.getBoundingClientRect();

        return top <= holderRect.top
          ? holderRect.top - top <= height
          : bottom - holderRect.bottom <= height;
      }
    },
    setNotificationConfirmationState() {
      const confirmations = ['granted', 'denied'];

      if ('Notification' in window && Notification) {
        this.isNotificationConfirmed = confirmations.includes(Notification.permission);
      } else {
        this.isNotificationConfirmed = true;
      }
    },
  },
  computed: {
    isDailyAllocationHighlightShown() {
      return (
        !!this.$route.name === 'index' &&
        !!this.$store.getters['profile/isHighlightDailyAllocationShown']
      );
    },
    isProfileModalShown() {
      const isModalShown =
        this.$store.getters['profile/showModalVerifyPhone'] ||
        this.$store.getters['profile/showModalNeedPassword'] ||
        this.$store.getters['profile/showModalUpdateProperty'] ||
        this.$store.getters['profile/showModalPremiumTrial'] ||
        this.$store.getters['profile/showModalSurveyPremium'] ||
        (this.$store.getters['profile/showModalDailyBalanceAllocation'] &&
          this.$route.name === 'index');

      return isModalShown;
    },
  },
};
