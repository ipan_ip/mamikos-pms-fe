import { shallowMount } from '@vue/test-utils';
import desktop from '../desktop';
import localVueWithBuefy from '~/utils/addBuefy';

const mockComponent = {
  template: '<div />',
};

const stubs = {
  MamipoinCard: mockComponent,
  ExchangeCard: mockComponent,
  MamipoinTabs: mockComponent,
};

const mocks = {
  $router: {
    replace: jest.fn(),
    push: jest.fn(),
    go: jest.fn(),
  },
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      stubs,
      mocks,
    },
    ...adtMountData,
  };

  return shallowMount(desktop, mountData);
};

describe('desktop.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should render properly', () => {
    expect(wrapper.find('.mamipoin-page.--desktop').exists()).toBeTruthy();
  });

  it('should redirect to owner Dashboard when function called', () => {
    wrapper.vm.backToDashboard();

    expect(wrapper.vm.$router.push).toBeCalledWith({ name: 'index' });
  });
});
