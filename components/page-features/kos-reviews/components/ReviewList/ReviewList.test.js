import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import ReviewList from '../ReviewList';

const localVue = createLocalVue();
localVue.use(Vuex);

const mocks = {
  $bugsnag: {
    notify: jest.fn(),
  },
  $alert: jest.fn(),
};

const mockStore = {
  modules: {
    review: {
      namespaced: true,
      state: {
        isLoading: false,
        reviewList: [],
      },
      actions: {
        fetchReviews: () => jest.fn(),
        resetReviewItems: () => jest.fn(),
      },
    },
  },
};

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue,
      mocks,
      store: new Vuex.Store(mockStore),
    },
    ...adtMountData,
  };

  return shallowMount(ReviewList, mountData);
};

describe('ReviewList.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('Should mount properly', () => {
    expect(wrapper.find('.review-list').exists()).toBeTruthy();
  });

  it('Should call fetchReviews request propertly', async () => {
    const fetchReviewsSpy = jest.spyOn(wrapper.vm, 'fetchReviews');
    await wrapper.vm.handleFetchReviews();
    expect(fetchReviewsSpy).toBeCalled();
  });

  it('Should handle fetchReviews request error propertly', async () => {
    mockStore.modules.review.actions.fetchReviews = jest.fn().mockRejectedValue('error');

    wrapper = mount({ store: new Vuex.Store(mockStore) });
    await wrapper.vm.handleFetchReviews();

    expect(wrapper.vm.$bugsnag.notify).toBeCalled();
    expect(wrapper.vm.$alert).toBeCalled();
  });

  it('Should reset review store items on destroyed', () => {
    const resetReviewItemsSpy = jest.spyOn(wrapper.vm, 'resetReviewItems');
    wrapper.destroy();
    expect(resetReviewItemsSpy).toBeCalled();
  });
});
