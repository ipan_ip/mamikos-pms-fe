import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import RatingCard from '.';
import localVueWithBuefy from '~/utils/addBuefy';
import mockLazy from '~/utils/mocks/lazy';
import mockWindowProperty from '~/utils/mockWindowProperty';

mockLazy(localVueWithBuefy);
mockWindowProperty('window.location', { href: '' });

localVueWithBuefy.use(Vuex);

const $tracker = { send: jest.fn() };
const $device = { isMobile: false };

const mockStore = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        data: {
          gp_status: '',
          is_mamirooms: '',
        },
      },
      getters: {
        user: () => {
          return {
            name: '',
            phone_number: '',
            email: '',
          };
        },
        isBbk: () => jest.fn(),
        isPremium: () => jest.fn(),
      },
    },
  },
};

const mountData = {
  localVue: localVueWithBuefy,
  propsData: {
    propertyId: 1,
    propertyPhoto: '',
    propertyName: 'Kos Tes',
    ratingValue: 5,
    reviewCount: 10,
  },
  store: new Vuex.Store(mockStore),
  mocks: {
    $mamikosUrl: 'mamikos.com',
    $tracker,
    $device,
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(RatingCard, finalData);
};

describe('RatingCard.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('Should mount properly', () => {
    expect(wrapper.find('.rating-card').exists()).toBeTruthy();
  });

  it('Should return proper fixed average rating value', () => {
    const testCases = [
      {
        value: 1,
        shownValue: '1.0',
      },
      {
        value: 2.5,
        shownValue: 2.5,
      },
    ];

    testCases.forEach((tc) => {
      wrapper.setProps({ ratingValue: tc.value });
      expect(wrapper.vm.fixedRatingValue).toEqual(tc.shownValue);
    });
  });

  it('Should redirect users to individual Property Review Detail page', () => {
    wrapper.vm.goToReviewDetail();
    expect(window.location.href).toBe(
      `mamikos.com/ownerpage/statistics/${wrapper.vm.propertyId}#review`,
    );
  });
});
