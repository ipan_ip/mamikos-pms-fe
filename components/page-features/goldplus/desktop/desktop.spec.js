import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import desktop from '../desktop';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const mockComponent = {
  template: `<div />`,
};

const stubs = {
  GoldPlusHeader: mockComponent,
  GoldPlusFeatures: mockComponent,
};

localVueWithBuefy.use(Vuex);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-statistic',
    },
    $device,
  },
  stubs,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(desktop, finalData);
};

describe('desktop.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-dashboard').exists()).toBeTruthy();
  });
});
