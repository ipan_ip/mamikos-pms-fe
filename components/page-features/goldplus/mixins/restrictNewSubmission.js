export default {
  created() {
    this.checkIsFromDeepLink();
  },
  methods: {
    async checkIsFromDeepLink() {
      const isGPSubmissionStatusFetched = this.$store.state.goldplus.isGPSubmissionStatusFetched;

      if (!isGPSubmissionStatusFetched) {
        this.$store.commit('goldplus/setIsGPSubmissionStatusFetched', true);
        await this.$store.dispatch('profile/getProfile');
      }

      this.handleRedirect();
    },
    handleRedirect() {
      const isNewGPOwner = this.$store.state.goldplus.isGoldPlusActive;
      const isFromEntryPoint = this.$store.state.goldplus.isSubmissionFlowStartedFromEntryPoint;

      if (isNewGPOwner) if (!isFromEntryPoint) this.$router.replace({ name: 'goldplus' });
    },
  },
};
