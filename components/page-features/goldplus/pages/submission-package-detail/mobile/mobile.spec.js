import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import mobile from '../mobile';
import localVueWithBuefy from '~/utils/addBuefy';
import { toIDR } from '~/utils/currencyString';
import restrictNewSubmission from '~/components/page-features/goldplus/mixins/restrictNewSubmission';

const $device = {
  isMobile: true,
  isDesktop: false,
};

const $router = {
  replace: jest.fn(),
  push: jest.fn(),
};

const $tracker = { send: jest.fn() };

const $alert = jest.fn((messages) => messages);

const $bugsnag = {
  notify: jest.fn(),
};

const mockComponent = {
  template: `<div />`,
};

const stubs = {
  GoldPlusSubmissionPackageConfirmationModal: mockComponent,
  GoldPlusSubmissionPackageRoomList: mockComponent,
  MkAlert: mockComponent,
  MkButton: mockComponent,
  GoldPlusHeader: mockComponent,
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('currencyIDR', toIDR);

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        selectedPackage: null,
        selectedRoomsForSubmission: [1, 2],
        isGoldPlusActive: true,
      },
      mutations: {
        setSelectedRoomsForSubmission(state, data = []) {
          state.selectedRoomsForSubmission = data;
        },
        setSelectedPackage(state, data = null) {
          state.selectedPackage = data;
        },
      },
    },
    profile: {
      namespaced: true,
      getters: {
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
      actions: {
        getProfile() {
          return jest.fn();
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const $api = {
  fetchAllPropertyRooms: jest
    .fn()
    .mockResolvedValue({
      data: {
        status: true,
        data: [
          {
            id: 1,
            room_title: 'room 1',
            address: 'Jalan jalan men',
            area_formatted: 'Somewhere in my heart',
            gender: 0,
            photo: '',
            room_count: 10,
          },
          {
            id: 2,
            room_title: 'room 2',
            address: 'Jalan jalan mun',
            area_formatted: 'Someone in my heart',
            gender: 1,
            photo: '',
            room_count: 5,
          },
        ],
      },
    })
    .mockResolvedValueOnce({ data: { status: false, message: 'failed' } })
    .mockRejectedValueOnce(),
};

const mountData = {
  localVue: localVueWithBuefy,
  mixins: [restrictNewSubmission],
  mocks: {
    $route: {
      name: 'goldplus-submission-package-code',
      params: {
        code: '1',
      },
    },
    $device,
    $api,
    $router,
    $bugsnag,
    $alert,
    $tracker,
    $store: { dispatch: jest.fn().mockResolvedValue({}) },
  },
  stubs,
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(mobile, finalData);
};

describe('mobile.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should redirect to landingpage when selectedPackage is null', () => {
    expect($router.replace).toBeCalled();
  });

  it('should mount properly', async () => {
    await store.commit('goldplus/setSelectedPackage', {
      id: 1,
      code: 'gp1',
      name: 'GoldPlus 2',
      price: 1000,
      unit_type: 'all',
    });
    expect(wrapper.find('.goldplus-submission-package-detail-mweb').exists()).toBeTruthy();
  });

  it('should handle method to show TnC', () => {
    wrapper.vm.showTnC();
    expect(wrapper.vm.isShowModalWrapper).toBeTruthy();
  });

  it('should handle selectedRooms method properly', () => {
    wrapper.vm.setSelectedRooms(null);
    expect(wrapper.vm.selectedRooms).toBe(null);
  });

  it('Should return purchaseDescription properly', async () => {
    wrapper.setData({ isLoadingRoomList: true });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.purchaseDescription).toEqual(
      '<p style="color: #949494;">Belum ada kos yang dipilih</p>',
    );

    wrapper.setData({
      isLoadingRoomList: false,
      selectedRooms: [
        {
          id: 2,
        },
      ],
    });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.purchaseDescription).toEqual(
      `<strong>Paket ${wrapper.vm.selectedPackage.name}</strong> untuk <strong>${wrapper.vm.selectedRooms.length} Kos</strong>`,
    );
  });

  it('should handle submitRequest method properly', () => {
    wrapper.vm.submitRequest();
    expect(wrapper.vm.isShowConfirmationModal).toBeTruthy();
  });

  it('should return selectedRooms correctly', async () => {
    const mockRoomList = [
      {
        address: 'mock Address',
        id: 23,
        area_formatted: 'mock Area Formatted',
        gender: 2,
        room_count: 5,
        room_title: 'mock Kost',
      },
    ];
    wrapper.setData({
      roomList: mockRoomList,
    });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.selectedRooms).toEqual([mockRoomList[0].id]);
  });

  it('should handle initRoomList method properly', () => {
    const mockRoomList = [
      {
        address: 'mock Address',
        _id: 23,
        area_formatted: 'mock Area Formatted',
        gender: 2,
        room_count: 5,
        room_title: 'mock Kost',
        photo_url: 'mock_large_1.jpeg',
      },
    ];

    wrapper.vm.initRoomList(mockRoomList);
    expect(wrapper.vm.roomList.length).toBe(1);
  });

  it('Should handle fetchAllPropertyRooms api properly', async () => {
    wrapper.vm.handleFetchRoomList();
    await Promise.resolve;
    expect(wrapper.vm.$api.fetchAllPropertyRooms).toBeCalled();

    wrapper.vm.handleFetchRoomList();
    await Promise.resolve;
    expect($bugsnag.notify).toBeCalled();
  });
});
