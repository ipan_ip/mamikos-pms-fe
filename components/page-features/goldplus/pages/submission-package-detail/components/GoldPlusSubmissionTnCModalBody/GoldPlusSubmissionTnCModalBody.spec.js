import { shallowMount } from '@vue/test-utils';
import GoldPlusSubmissionTnCModalBody from '../GoldPlusSubmissionTnCModalBody';

const mockStub = '<div></div>';
const stubs = {
  GoldPlusSubmissionTnCContent: mockStub,
  GoldPlusSubmissionTnCFooter: mockStub,
  GoldPlusHeader: mockStub,
};

const mount = () => {
  const mountData = {
    stubs,
  };

  return shallowMount(GoldPlusSubmissionTnCModalBody, mountData);
};

describe('GoldPlusSubmissionTnCGoldPlusSubmissionTnCModalBody.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('Should mount properly', () => {
    expect(wrapper.find('.goldplus-submission-tnc-modal-body').exists()).toBeTruthy();
  });
  it('Should enable checkbox when TnC content scroll position is at bottom', () => {
    const mockScrollElement = {
      target: {
        scrollHeight: 10,
        clientHeight: 10,
        scrollTop: 0,
      },
    };

    wrapper.vm.handleTnCScrolling(mockScrollElement);

    expect(wrapper.vm.isCheckboxDisabled).toBe(false);
  });
});
