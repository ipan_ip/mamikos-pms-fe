import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusSubmissionModalWrapper from '../GoldPlusSubmissionModalWrapper';
import localVueWithBuefy from '~/utils/addBuefy';
import MkModal, { mixin as modalMixin } from '~/components/global/molecules/MkModal/MkModal';

// mock event bus
jest.mock('~/components/event-bus/index.js', () => {
  const EventBus = {
    $on: jest.fn((eventName, cb) => {
      const mockCallbackData = {};
      cb && cb(mockCallbackData);
    }),
  };

  global.EventBus = EventBus;
  return global.EventBus;
});

localVueWithBuefy.use(Vuex);

const $device = {
  isMobile: true,
  isDesktop: false,
};
const $router = {
  push: jest.fn(),
};
const stubs = {
  MkModal,
};
const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      actions: {
        getActiveStatus: jest.fn(),
      },
    },
  },
};
const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-submission-package-code',
    },
    $device,
    $router,
  },
  stubs,
  store,
  mixins: [modalMixin],
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusSubmissionModalWrapper, finalData);
};

describe('GoldPlusSubmissionModalWrapper.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should handle back to dashboard method properly', async () => {
    const dispatchSpy = jest.spyOn(wrapper.vm.$store, 'dispatch');

    await wrapper.vm.backToDashboard();

    expect(dispatchSpy).toBeCalled();
    expect($router.push).toBeCalled();
  });

  it('should compute showModal value', async () => {
    // get
    await wrapper.setProps({
      active: true,
    });
    expect(wrapper.vm.showModal).toBeTruthy();

    // set
    const emitSpy = jest.spyOn(wrapper.vm, '$emit');
    wrapper.vm.showModal = false;
    expect(emitSpy).toBeCalled();
    emitSpy.mockRestore();
  });

  it('closeModal method called properly depends on isShowSuccessModal', () => {
    const backToDashboardSpy = jest.spyOn(wrapper.vm, 'backToDashboard');
    const toggleModalSpy = jest.spyOn(wrapper.vm, 'toggleModal');

    wrapper.setData({ isShowSuccessModal: false });
    wrapper.vm.closeModal();
    expect(toggleModalSpy).toBeCalled();

    wrapper.setData({ isShowSuccessModal: true });
    wrapper.vm.closeModal();
    expect(backToDashboardSpy).toBeCalled();
  });

  it('should call EventBus.$on after mounted', () => {
    const eventBusOnSpy = jest.spyOn(global.EventBus, '$on');

    wrapper.destroy();
    wrapper = mount();

    expect(eventBusOnSpy).toBeCalled();
    eventBusOnSpy.mockRestore();
  });
});
