import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusSubmissionPackageConfirmationModal from '../GoldPlusSubmissionPackageConfirmationModal';
import localVueWithBuefy from '~/utils/addBuefy';
import MkModal, { mixin as modalMixin } from '~/components/global/molecules/MkModal/MkModal';
import MkButton from '~/components/global/atoms/MkButton';

const $device = {
  isMobile: true,
  isDesktop: false,
};

const stubs = {
  MkModal,
  MkButton,
};

localVueWithBuefy.use(Vuex);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-submission-tnc',
    },
    $device,
  },
  stubs,
  mixins: [modalMixin],
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusSubmissionPackageConfirmationModal, finalData);
};

describe('GoldPlusSubmissionPackageConfirmationModal.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should handle submit method properly', async () => {
    wrapper.vm.$emit = jest.fn();

    await wrapper.vm.handleSubmit();
    expect(wrapper.vm.$emit).toBeCalled();
  });

  it('should handle close modal when clicked properly', () => {
    wrapper.setMethods({ toggleModal: jest.fn() });
    wrapper.vm.closeModal();
    expect(wrapper.vm.toggleModal).toBeCalledWith(false);
  });

  it('should compute showModal value', async () => {
    // get
    await wrapper.setProps({
      active: true,
    });
    expect(wrapper.vm.showModal).toBeTruthy();

    // set
    const emitSpy = jest.spyOn(wrapper.vm, '$emit');
    wrapper.vm.showModal = false;
    expect(emitSpy).toBeCalled();
    emitSpy.mockRestore();
  });
});
