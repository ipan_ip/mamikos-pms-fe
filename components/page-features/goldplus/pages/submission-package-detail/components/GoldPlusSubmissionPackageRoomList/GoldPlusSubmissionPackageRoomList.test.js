import { shallowMount } from '@vue/test-utils';
import GoldPlusSubmissionPackageRoomList from '../GoldPlusSubmissionPackageRoomList';
import localVueWithBuefy from '~/utils/addBuefy';

global.window = Object.create(window);

const mount = () => {
  const mountData = {
    localVue: localVueWithBuefy,
    propsData: {
      roomList: [
        {
          id: 1,
          room_title: 'room 1',
          address: 'Jalan jalan men',
          area_formatted: 'Somewhere in my heart',
          gender: 0,
          photo: '',
          room_count: 10,
        },
      ],
      packageCode: 'gp3',
    },
  };

  return shallowMount(GoldPlusSubmissionPackageRoomList, mountData);
};

describe('GoldPlusSubmissionPackageRoomList.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('Should mount properly', () => {
    expect(wrapper.find('.goldplus-submission-package-room-list').exists()).toBeTruthy();
  });

  it('Should handle select all rooms toggling properly', async () => {
    wrapper.setData({ isSelectAll: true });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.selectedRooms.length).toEqual(wrapper.vm.roomList.length);

    wrapper.setData({ isSelectAll: false });
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.selectedRooms.length).toEqual(0);
  });

  it('Should set isSelectAll to true if selected rooms equal room list', async () => {
    const mockRooms = [];

    wrapper.vm.roomList.forEach((room) => {
      mockRooms.push(room.id);
    });

    wrapper.setData({
      selectedRooms: mockRooms,
    });

    await wrapper.vm.$nextTick();

    expect(wrapper.vm.isSelectAll).toBe(true);
  });

  it("Should emitted 'setSelectedRooms' when there are changes on selectedRooms data", async () => {
    wrapper.setData({ selectedRooms: [1] });

    await wrapper.vm.$nextTick();

    expect(wrapper.emitted('setSelectedRooms')).toBeTruthy();
  });
});
