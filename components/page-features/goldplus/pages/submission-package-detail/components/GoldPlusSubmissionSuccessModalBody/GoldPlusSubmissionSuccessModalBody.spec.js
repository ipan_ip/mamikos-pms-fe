import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusSubmissionSuccessModalBody from '../GoldPlusSubmissionSuccessModalBody';
import localVueWithBuefy from '~/utils/addBuefy';
import MkModal, { mixin as modalMixin } from '~/components/global/molecules/MkModal/MkModal';
import MkButton from '~/components/global/atoms/MkButton';

localVueWithBuefy.use(Vuex);

const $device = {
  isMobile: true,
  isDesktop: false,
};
const $router = {
  push: jest.fn(),
};
const stubs = {
  MkModal,
  MkButton,
};
const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      actions: {
        getActiveStatus: jest.fn(),
      },
    },
  },
};
const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-submission-package-code',
    },
    $device,
    $router,
  },
  stubs,
  store,
  mixins: [modalMixin],
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusSubmissionSuccessModalBody, finalData);
};

describe('GoldPlusSubmissionSuccessModalBody.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  jest.mock(`~/assets/images/goldplus/submission_success.svg`, () => {
    return 'success_icon';
  });

  it('should handle back to dashboard method properly', async () => {
    const dispatchSpy = jest.spyOn(wrapper.vm.$store, 'dispatch');

    await wrapper.vm.backToDashboard();

    expect(dispatchSpy).toBeCalled();
    expect($router.push).toBeCalled();
  });
});
