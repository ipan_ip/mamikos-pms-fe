import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusSubmissionTnCFooter from '../GoldPlusSubmissionTnCFooter';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: true,
  isDesktop: false,
};

const $tracker = { send: jest.fn() };

const $api = {
  sendGoldPlusSubmissionRequest: jest
    .fn()
    .mockResolvedValueOnce({ data: { status: true, message: 'success' } })
    .mockResolvedValueOnce({ data: { status: false, message: 'failed' } })
    .mockRejectedValueOnce('error'),
  createBillingContractOrder: jest
    .fn()
    .mockResolvedValueOnce({ data: { status: true, property_contract_id: 696 } }),
};
const $bugsnag = { notify: jest.fn() };

const propsData = {
  isCheckboxDisabled: true,
};

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        selectedPackage: {
          id: 1,
          code: 'gp1',
          name: 'GoldPlus 2',
          price: 1000,
          unit_type: 'all',
        },
        selectedRoomsForSubmission: [1, 2],
        billingInvoiceData: {
          invoice_id: 123,
          invoice_number: 'GP1/20201014/00000012/0001',
        },
      },
    },
    profile: {
      namespaced: true,
      getters: {
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mount = () => {
  const mountData = {
    localVue: localVueWithBuefy,
    mocks: {
      $device,
      $api,
      $alert: jest.fn(),
      $buefy: { toast: { open: jest.fn() } },
      $bugsnag,
      $route: {
        params: {
          listing_count: 3,
        },
        push: jest.fn(),
      },
      $tracker,
    },
    store,
    propsData,
  };

  return shallowMount(GoldPlusSubmissionTnCFooter, mountData);
};

describe('GoldPlusSubmissionTnCFooter.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('Should mount properly', () => {
    expect(wrapper.find('.goldplus-submission-tnc-footer').exists()).toBeTruthy();
  });

  it('should handle redirect to Billing', async () => {
    wrapper.vm.handleRedirectToBilling();
    await wrapper.vm.$nextTick();

    await wrapper.vm.$api.createBillingContractOrder;

    if ($api.createBillingContractOrder.data) {
      expect(
        wrapper.vm.$store.commit('goldplus/setBillingInvoiceData', wrapper.vm.data),
      ).toBeCalled();
    } else {
      expect(wrapper.vm.$bugsnag.notify).toBeCalled();
    }
  });

  it('Should show warning toast when checkbox is clicked while disabled', () => {
    expect(wrapper.vm.isCheckboxDisabled).toBe(true);

    wrapper.vm.handleOnInputCheckbox();

    expect(wrapper.vm.$buefy.toast.open).toBeCalled();
  });

  it('Should handle send submission request properly', async () => {
    wrapper.vm.submitRequest();
    await Promise.resolve;
    expect(wrapper.vm.$api.sendGoldPlusSubmissionRequest).toBeCalled();

    wrapper.vm.submitRequest();
    await Promise.resolve;
    expect($bugsnag.notify).toBeCalled();
  });
});
