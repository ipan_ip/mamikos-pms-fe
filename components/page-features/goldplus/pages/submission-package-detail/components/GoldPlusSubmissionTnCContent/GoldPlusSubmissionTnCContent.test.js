import { shallowMount } from '@vue/test-utils';
import GoldPlusSubmissionTnCContent from '../GoldPlusSubmissionTnCContent';
import localVueWithBuefy from '~/utils/addBuefy';

global.window = Object.create(window);

const $api = {
  getGoldPlusSubmissionTnCContent: jest.fn().mockResolvedValue({
    data: {
      status: true,
      html_content: '<!DOCTYPE html><html></html>',
    },
  }),
};

const $bugsnag = {
  notify: jest.fn(),
};

const mount = () => {
  const mountData = {
    localVue: localVueWithBuefy,
    mocks: {
      $api,
      $bugsnag,
    },
  };

  return shallowMount(GoldPlusSubmissionTnCContent, mountData);
};

describe('GoldPlusSubmissionTnCContent.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('Should mount properly', () => {
    expect(wrapper.find('.goldplus-submission-tnc-content').exists()).toBeTruthy();
  });

  it("Should emit 'scrolled' when scrolled", async () => {
    wrapper.find('.goldplus-submission-tnc-content').trigger('scroll');

    await wrapper.vm.$nextTick();

    expect(wrapper.emitted('scrolled')).toBeTruthy();
  });

  it('should call bugsnag.notify on failed getGoldPlusSubmissionTnCContent api call', async () => {
    wrapper.vm.$api.getGoldPlusSubmissionTnCContent = jest.fn().mockRejectedValue('error');

    await wrapper.vm.fetchTnCContent();
    await wrapper.vm.$nextTick();
    expect($bugsnag.notify).toBeCalled();
  });
});
