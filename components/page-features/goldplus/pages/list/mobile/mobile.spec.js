import { shallowMount } from '@vue/test-utils';
import mobile from './mobile';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: true,
  isDesktop: false,
};

const mockComponent = '<div></div>';

const stubs = {
  GoldPlusRoomListKostContent: mockComponent,
  GoldPlusHeader: mockComponent,
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $device,
  },
  stubs,
};

global.open = jest.fn(() => true);

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(mobile, finalData);
};

describe('mobile.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-room-list-mweb').exists()).toBeTruthy();
  });
});
