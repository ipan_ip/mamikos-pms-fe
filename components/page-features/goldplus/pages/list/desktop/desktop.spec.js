import { shallowMount } from '@vue/test-utils';
import desktop from './desktop';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const $router = {
  replace: jest.fn(),
};

const mockComponent = '<div></div>';

const stubs = {
  GoldPlusRoomListKostContent: mockComponent,
  GoldPlusHeader: mockComponent,
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $device,
    $router,
  },
  stubs,
};

global.open = jest.fn(() => true);

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(desktop, finalData);
};

describe('mobile.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-room-list').exists()).toBeTruthy();
  });

  it('Should handle on clicked back button', () => {
    wrapper.vm.handleBackClicked();

    expect(wrapper.vm.$router.replace).toBeCalled();
  });
});
