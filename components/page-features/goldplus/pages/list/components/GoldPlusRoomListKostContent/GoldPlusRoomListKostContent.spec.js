import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import dayjs from 'dayjs';
import GoldPlusRoomListKostContent from './GoldPlusRoomListKostContent';
import localVueWithBuefy from '~/utils/addBuefy';

localVueWithBuefy.use(Vuex);

const $device = {
  isMobile: true,
  isDesktop: false,
};

const $bugsnag = {
  notify: jest.fn(),
};

const $tracker = { send: jest.fn() };

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      getters: {
        user: () => {
          return {
            user_id: 1,
            name: 'Joni',
            phone_number: '08123456789',
          };
        },
      },
    },
    goldplus: {
      namespaced: true,
      state: {
        billingAlertPending: {
          closest_date: '10 Desember 2020',
          has_pending_first_transaction: true,
        },
      },
      actions: {
        fetchBillingAlertPending() {
          return $api.getBillingAlertPending();
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const $api = {
  fetchGoldPlusSubmissionRoomList: jest.fn().mockResolvedValue({
    data: {
      offset: 0,
      limit: 15,
      status: true,
      data: [
        {
          id: 1,
          room_tittle: 'Kost Test 1',
          photo: '',
          gp_status: {
            key: 10,
            value: 'Goldplus 3',
          },
          membership_status: {
            key: 1,
            value: 'Aktif',
          },
          room_count: 12,
          address: 'Alamat test 1',
          new_gp: true,
        },
        {
          id: 2,
          room_tittle: 'Kost Test 2',
          photo: '',
          gp_status: {
            key: null,
            value: 'Goldplus 2',
          },
          membership_status: {
            key: 2,
            value: 'Sedang direview',
          },
          room_count: 12,
          address: 'Alamat test 2',
          new_gp: false,
        },
      ],
      button: true,
    },
  }),
  getGoldPlusActiveContract: jest
    .fn()
    .mockResolvedValueOnce({
      data: {
        status: true,
        data: {
          contract: {
            name: 'mock package name',
          },
        },
      },
    })
    .mockRejectedValueOnce(),
};

const $router = {
  push: jest.fn((path) => {
    return path;
  }),
};

const $alert = jest.fn((messages) => messages);

const mockComponent = '<div></div>';

const stubs = {
  BgModal: {
    template: mockComponent,
    props: ['illustrationName'],
  },
  GoldPlusHeader: mockComponent,
  GoldPlusRoomListFilters: mockComponent,
  GoldPlusRoomCard: mockComponent,
  MkButton: mockComponent,
  EmptyState: mockComponent,
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      params: {
        filter_key: 0,
      },
    },
    $dayjs: dayjs,
    $device,
    $tracker,
    $api,
    $router,
    $bugsnag,
    $alert,
  },
  store,
  stubs,
};

global.open = jest.fn(() => true);

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusRoomListKostContent, finalData);
};

describe('mobile.vue', () => {
  let wrapper;
  const handleActionButtonClickedSpy = jest.spyOn(
    GoldPlusRoomListKostContent.methods,
    'handleActionButtonClicked',
  );
  const togglePackageChangesModalSpy = jest.spyOn(
    GoldPlusRoomListKostContent.methods,
    'togglePackageChangesModal',
  );

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-room-list-content').exists()).toBeTruthy();
  });

  it('should call bugsnag notify on failed fetchGoldPlusSubmissionRoomList api call', async () => {
    wrapper.vm.$api.fetchGoldPlusSubmissionRoomList = jest.fn().mockRejectedValue('error');

    await wrapper.vm.handleFetchGPSubmissionRoomList([]);

    await wrapper.vm.$nextTick();

    expect($bugsnag.notify).toBeCalled();
  });

  it('should call bugsnag notify on failed handleFetchPendingAlert api call', async () => {
    wrapper.vm.$api.fetchBillingAlertPending = jest.fn().mockRejectedValue('error');

    await wrapper.vm.handleFetchPendingAlert([]);

    await wrapper.vm.$nextTick();

    expect($bugsnag.notify).toBeCalled();
  });

  it('should get closest date for billing alert pending', () => {
    expect(wrapper.vm.closestDate).toBe('10 Desember 2020');
  });

  it('should have alert description', () => {
    expect(wrapper.vm.alertDescription).toBe(
      'Mohon selesaikan pembayaran sebelum 10 Desember 2020 agar registrasi GoldPlus Anda tidak dibatalkan.',
    );
  });

  it('should return date according date formated (D MMM YYYY)', () => {
    expect(wrapper.vm.dateTimeFormat('2020-12-19')).toBe('19 Dec 2020');
  });

  it('should handle selected filter', () => {
    const key = {
      filter: {
        key: 0,
      },
    };

    wrapper.vm.$api.fetchGoldPlusSubmissionRoomList = jest.fn().mockResolvedValue();

    const handleFetchGPSubmissionRoomListSpy = jest.spyOn(
      wrapper.vm,
      'handleFetchGPSubmissionRoomList',
    );

    wrapper.vm.handleSelectFilter(key.filter.key);
    expect(handleFetchGPSubmissionRoomListSpy).toBeCalled();
  });

  it('should show GP button, if owner flagged as old_GP or owner never submit their new_GP request until success', async () => {
    wrapper.vm.$api.fetchGoldPlusSubmissionRoomList = jest.fn().mockResolvedValue();

    await wrapper.vm.handleFetchGPSubmissionRoomList([]);

    await wrapper.vm.$nextTick();

    if (wrapper.vm.roomList.some((room) => !room.new_gp)) {
      expect(wrapper.vm.showGPButton).toBeTruthy();
    } else {
      expect(wrapper.vm.showGPButton).toBeFalsy();
    }
  });

  it('Should handle action button clicked properly', () => {
    const testCases = ['new', 'change', null];

    testCases.forEach((tc) => {
      wrapper.setData({
        currentActiveAction: tc,
      });
      wrapper.vm.handleActionButtonClicked();

      if (tc === 'new') expect($router.push).toBeCalledWith('/goldplus/submission');
      else if (tc === 'change') expect(togglePackageChangesModalSpy).toBeCalled();
      else expect(handleActionButtonClickedSpy).toHaveReturnedWith(null);
    });
  });

  it('Should return proper action button text', () => {
    const testCases = [
      {
        action: 'new',
        text: 'Beli Paket GoldPlus',
      },
      {
        action: 'change',
        text: 'Ajukan Ganti Paket',
      },
      {
        action: null,
        text: null,
      },
    ];

    testCases.forEach((tc) => {
      wrapper.setData({
        currentActiveAction: tc.action,
      });

      expect(wrapper.vm.actionButtonText).toBe(tc.text);
    });
  });

  it('Should handle get active contract request properly', async () => {
    wrapper.vm.handleGetActiveContract();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.$api.getGoldPlusActiveContract).toBeCalled();
    expect(wrapper.vm.currentActivePackage).toBe('mock package name');

    wrapper.vm.handleGetActiveContract();
    await wrapper.vm.$nextTick();
    expect($bugsnag.notify).toBeCalled();
  });

  it('Should handle package changes request properly', () => {
    const mamikosNumber = '6281325111171';
    const preText =
      'Mohon dibantu proses penggantian paket GoldPlus dengan detail sebagai berikut:';
    let detailText = [
      `Nama: ${wrapper.vm.user.name}`,
      `Nomor handphone: ${wrapper.vm.user.phone_number}`,
      `Paket yang sedang aktif: ${wrapper.vm.currentActivePackage}`,
      'Paket pengganti yang ingin diaktifkan:',
    ];

    detailText = detailText.join('%0a');

    const whatsappUrl = `https://api.whatsapp.com/send?phone=${mamikosNumber}&text=Halo,%0a%0a${preText}%0a%0a${detailText}`;

    wrapper.vm.requestPackageChanges();
    expect(global.open).toBeCalledWith(whatsappUrl);
  });

  it('should route to goldplus billing list', () => {
    const path = '/goldplus/payment';
    wrapper.vm.handleGoToBillingList();

    expect(wrapper.vm.$router.push(path)).toBe('/goldplus/payment');
  });
});
