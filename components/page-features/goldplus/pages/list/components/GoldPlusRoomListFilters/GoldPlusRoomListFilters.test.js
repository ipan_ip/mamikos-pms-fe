import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusRoomListFilters from './GoldPlusRoomListFilters';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: true,
  isDesktop: false,
};

const $bugsnag = {
  notify: jest.fn(),
};

localVueWithBuefy.use(Vuex);

const $api = {
  getGoldPlusSubmissionRoomListFilters: jest.fn().mockResolvedValue({
    data: {
      status: true,
      data: [
        {
          key: 0,
          value: 'Semua',
        },
        {
          key: 1,
          value: 'Aktif',
        },
        {
          key: 2,
          value: 'Sedang direview',
        },
      ],
    },
  }),
};

const $route = {
  params: {
    filter_key: 0,
  },
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $api,
    $device,
    $route,
    $bugsnag,
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusRoomListFilters, finalData);
};

describe('GoldPlusRoomListFilters.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-room-list-filters').exists()).toBeTruthy();
  });

  it('should emitted filter value parameter', async () => {
    const filter = wrapper.vm.selectedFilter;

    wrapper.vm.selectFilter(filter);
    wrapper.vm.$emit('filter');

    await wrapper.vm.$nextTick();
    expect(wrapper.emitted().filter).toBeTruthy();
  });

  it('should handle method and if filter clicked the component emitted event to mobile view', () => {
    const filterClickable = wrapper.find('.goldplus-room-list-filters_item');

    if (filterClickable && filterClickable.exists()) {
      wrapper.vm.$emit = jest.fn();
      filterClickable.trigger('click');
      expect(wrapper.vm.$emit).toBeCalled();
    }

    expect(wrapper.vm.selectedFilter).toBe('Semua');
  });

  it('should handle filter from widget', () => {
    const mockSelectedFilter = {
      key: 0,
      value: 'Semua',
    };

    wrapper.vm.handleFilterFromWidget(mockSelectedFilter.key);
    expect(wrapper.vm.selectedFilter).toBe(mockSelectedFilter.value);
  });

  it('should call bugsnag.notify on failed getGoldPlusSubmissionRoomListFilters api call', async () => {
    wrapper.vm.$api.getGoldPlusSubmissionRoomListFilters = jest.fn().mockRejectedValue('error');

    await wrapper.vm.handleFetchFilters();
    await wrapper.vm.$nextTick();
    expect($bugsnag.notify).toBeCalled();
  });
});
