import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mobile from '../mobile';
import GoldPlusRoomCard from '../../../components/GoldPlusRoomCard';
import EmptyState from '~/components/page-features/goldplus/components/GoldPlusRoomsEmptyState';
import GoldPlusHeader from '~/components/page-features/goldplus/components/GoldPlusHeader';
import MkThreeDotsLoading from '~/components/global/atoms/MkThreeDotsLoading';
import MkButton from '~/components/global/atoms/MkButton';
import mockWindowProperty from '~/utils/mockWindowProperty';
import localVueWithBuefy from '~/utils/addBuefy';
import mockLazy from '~/utils/mocks/lazy';

const $device = {
  isMobile: true,
  isDesktop: false,
};

const mockComponent = {
  template: `<div />`,
};

const stubs = {
  GoldPlusStatisticRoomListFilters: mockComponent,
  GoldPlusRoomCard,
  GoldPlusHeader,
  MkThreeDotsLoading,
  MkButton,
  EmptyState,
};

mockLazy(localVueWithBuefy);

const dataAPI = [
  {
    id: 123,
    room_title: 'mocked title 1',
    area_formatted: 'mock area 1',
    address: 'mock address 1',
    photo: {
      real: 'mock_real_1.jpeg',
      small: 'mock_small_1.jpeg',
      medium: 'mock_medium_1.jpeg',
      large: 'mock_large_1.jpeg',
    },
    gender: 0,
    gp_status: {
      key: 9,
      value: 'Goldplus 1',
    },
  },
  {
    id: 456,
    room_title: 'mocked title 2',
    area_formatted: 'mock area 2',
    address: 'mock address 2',
    photo: {
      real: 'mock_real_2.jpeg',
      small: 'mock_small_2.jpeg',
      medium: 'mock_medium_2.jpeg',
      large: 'mock_large_2.jpeg',
    },
    gender: 0,
    gp_status: {
      key: 7,
      value: 'Goldplus 2',
    },
  },
];

const $api = {
  fetchGoldPlusStatisticRoomList: jest.fn().mockResolvedValue({
    data: {
      status: true,
      has_more: false,
      data: dataAPI,
    },
  }),
};

const $bugsnag = {
  notify: jest.fn(),
};

const $router = {
  push: jest.fn(),
};

const $tracker = { send: jest.fn() };

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      getters: {
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-statistic',
    },
    $router,
    $device,
    $api,
    $bugsnag,
    $tracker,
  },
  stubs,
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(mobile, finalData);
};

describe('mobile.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  jest.mock(`~/assets/images/goldplus/empty_state_v1.png`, () => {
    return 'empty_state_v1';
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-statistic').exists()).toBeTruthy();
  });

  it('should handle load more properly', () => {
    const handleFetchGoldPlusRoomListSpy = jest.spyOn(wrapper.vm, 'handleFetchGoldPlusRoomList');

    wrapper.vm.handleLoadMore();

    expect(handleFetchGoldPlusRoomListSpy).toBeCalled();
    expect(wrapper.vm.rooms).toEqual(dataAPI);
    expect(wrapper.vm.roomsFiltered).toEqual(dataAPI);
  });

  it('should change option selected', () => {
    const option = {
      key: 0,
      value: 'Goldplus 1',
    };
    wrapper.vm.handleOptionFilter(option);
    expect(wrapper.vm.optionSelected.value).toEqual(option.value);
  });

  it('should call handleFetchGoldPlusRoomList when load more clicked', async () => {
    wrapper.vm.handleFetchGoldPlusRoomList = jest.fn();
    const option = {
      key: 0,
      value: 'Goldplus 1',
    };
    wrapper.vm.handleOptionFilter(option);
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.handleFetchGoldPlusRoomList).toBeCalled();
  });

  it('should change isScrolled when window is scrolled', () => {
    mockWindowProperty('pageYOffset', 200);

    wrapper.vm.checkCurrentScrollWindow();
    expect(wrapper.vm.isScrolled).toBe(true);

    mockWindowProperty(
      'addEventListener',
      jest.fn((event, cb) => {
        if (event === 'scroll') return cb();
      }),
    );
    wrapper.vm.scrollWindow();
    expect(wrapper.vm.isScrolled).toBe(true);
    mockWindowProperty('pageYOffset', 10);
    wrapper.vm.scrollWindow();
    expect(wrapper.vm.isScrolled).toBe(false);
  });

  it('should change empty state message properly ', () => {
    wrapper.setData({ optionSelected: { key: 0, value: 'Semua' } });
    expect(wrapper.vm.emptyMessage).toEqual('Tidak ditemukan kosan Anda yang menggunakan Goldplus');

    wrapper.setData({ optionSelected: { key: 9, value: 'GoldPlus 1' } });
    expect(wrapper.vm.emptyMessage).toEqual(
      'Tidak ditemukan kosan Anda yang menggunakan GoldPlus 1',
    );
  });

  it('should call bugsnag.notify on failed fetchGoldPlusRoomList api call', async () => {
    wrapper.vm.$api.fetchGoldPlusStatisticRoomList = jest.fn().mockRejectedValue('error');

    await wrapper.vm.handleFetchGoldPlusRoomList([0, 2, 0]);
    await wrapper.vm.$nextTick();
    expect($bugsnag.notify).toBeCalled();
  });

  it('Should redirect to detail statistic path properly', () => {
    const mockRoomId = dataAPI[0].id;

    wrapper.vm.goToDetailPage(mockRoomId);

    expect($router.push).toBeCalledWith({
      name: 'goldplus-statistic-id',
      params: { id: mockRoomId },
    });
  });

  it('should route to statistic detail page', () => {
    wrapper.vm.goToDetailPage(456);
    expect($router.push).toBeCalled();
  });
});
