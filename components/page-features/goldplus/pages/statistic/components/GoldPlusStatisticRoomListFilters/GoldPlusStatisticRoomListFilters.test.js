import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusStatisticRoomListFilters from '.';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: true,
  isDesktop: false,
};

const $bugsnag = {
  notify: jest.fn(),
};

localVueWithBuefy.use(Vuex);

const $api = {
  getGoldPlusStatisticRoomListFilters: jest.fn().mockResolvedValue({
    data: {
      status: true,
      data: [
        {
          key: 0,
          value: 'Semua',
        },
        {
          key: 9,
          value: 'Goldplus 1',
        },
        {
          key: 7,
          value: 'Goldplus 2',
        },
        {
          key: 10,
          value: 'Goldplus 3',
        },
        {
          key: 101,
          value: 'Goldplus 4',
        },
      ],
    },
  }),
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-statistic',
    },
    $api,
    $device,
    $bugsnag,
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusStatisticRoomListFilters, finalData);
};

describe('GoldPlusStatisticRoomListFilters.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-statistic-room-list-filters').exists()).toBeTruthy();
  });

  it('should handle method and emitted event when clicking option in mobile view', () => {
    const optionClickable = wrapper.find('.goldplus-statistic-room-list-filter__item');

    if (optionClickable && optionClickable.exists()) {
      wrapper.vm.$emit = jest.fn();
      optionClickable.trigger('click');
      expect(wrapper.vm.$emit).toBeCalled();
    }

    expect(wrapper.vm.optionSelected).toBe('Semua');
  });

  it('should call bugsnag.notify on failed getGoldPlusStatisticRoomListFilters api call', async () => {
    wrapper.vm.$api.getGoldPlusStatisticRoomListFilters = jest.fn().mockRejectedValue('error');

    await wrapper.vm.fetchFilterApi();
    await wrapper.vm.$nextTick();
    expect($bugsnag.notify).toBeCalled();
  });
});
