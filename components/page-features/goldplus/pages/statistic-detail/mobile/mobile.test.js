import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import mobile from '../mobile';
import localVueWithBuefy from '~/utils/addBuefy';

const $router = {
  push: jest.fn(),
};

const $alert = jest.fn((messages) => messages);

const $bugsnag = {
  notify: jest.fn(),
};

const $device = {
  isMobile: true,
  isDesktop: false,
};

const $tracker = { send: jest.fn() };

const mockComponent = {
  template: `<div />`,
};

const stubs = {
  GoldPlusHeader: mockComponent,
  GoldPlusRoomCard: mockComponent,
  GoldPlusStatisticChart: mockComponent,
};

localVueWithBuefy.use(Vuex);

const mockRoomDetail = {
  id: 1,
  room_title: 'room 1',
  address: 'Jalan jalan men',
  area_formatted: 'Somewhere in my heart',
  gender: 0,
  photo: '',
  room_count: 10,
  gp_status: { key: 9, value: 'Goldplus 1' },
};

const mockStatisticFilter = {
  key: 'today',
  value: 'hari ini',
  periode_description: 'kyou',
};

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        roomDetail: mockRoomDetail,
        selectedVisitStatisticFilter: mockStatisticFilter,
        selectedUniqueVisitStatisticFilter: mockStatisticFilter,
        selectedChatStatisticFilter: mockStatisticFilter,
        selectedFavoriteStatisticFilter: mockStatisticFilter,
      },
      getters: {
        selectedStatisticFilter: () => {
          return mockStatisticFilter;
        },
        statisticData: () => {
          return jest.fn(() => {
            return {
              title: 'Title Chart',
              description: 'Title Chart description',
              selectedFilter: mockStatisticFilter,
              latestValue: 0,
              labels: [1, 2, 3],
              data: [3, 2, 1],
              pointRadius: [0, 0, 0],
              xAxisLineWidth: [0, 0, 0],
              growthStatus: {
                growth_type: null,
                tooltip: 'hi there im tooltip',
              },
              isEmptyData: false,
            };
          });
        },
      },
      mutations: {
        setRoomDetail: jest.fn(),
        setStatisticRoomId: jest.fn(),
      },
      actions: {
        getStatistics() {
          return jest.fn();
        },
        resetStatisticFilters() {
          return jest.fn();
        },
      },
    },
    profile: {
      namespaced: true,
      getters: {
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const $api = {
  getGoldPlusRoomDetail: jest
    .fn()
    .mockResolvedValueOnce({
      data: {
        status: true,
        data: mockRoomDetail,
      },
    })
    .mockResolvedValueOnce({ data: { status: false, message: 'failed' } })
    .mockRejectedValueOnce(),
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-statistic-detail',
      params: {
        id: '1',
      },
    },
    $api,
    $router,
    $bugsnag,
    $alert,
    $device,
    $tracker,
  },
  stubs,
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(mobile, finalData);
};

describe('mobile.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-statistic-detail-mweb').exists()).toBeTruthy();
  });

  it('Should handle get room detail request properly', async () => {
    wrapper.vm.handleGetRoomDetail();
    await Promise.resolve;
    expect(wrapper.vm.$api.getGoldPlusRoomDetail).toBeCalled();

    wrapper.vm.handleGetRoomDetail();
    await Promise.resolve;
    expect($router.push).toBeCalledWith({ name: 'goldplus-statistic' });

    wrapper.vm.handleGetRoomDetail();
    await Promise.reject;
    expect($bugsnag.notify).toBeCalled();
  });

  it('Should re-fetch statistic data when filter is changed', () => {
    // 'unique-visit' statistics is temporarily hidden until further updates.

    const handleGetChatStatisticsSpy = jest.spyOn(wrapper.vm, 'handleGetChatStatistics');
    const handleGetVisitStatisticsSpy = jest.spyOn(wrapper.vm, 'handleGetVisitStatistics');
    // const handleGetUniqueVisitStatisticsSpy = jest.spyOn(
    //   wrapper.vm,
    //   'handleGetUniqueVisitStatistics',
    // );
    const handleGetFavoriteStatisticsSpy = jest.spyOn(wrapper.vm, 'handleGetFavoriteStatistics');

    const newMockStatisticFilter = {
      key: 'minggu ke-1',
      value: 'first week',
      periode_description: 'saisho no shuu',
    };

    const testCases = [
      {
        statisticType: 'Chat',
        method: handleGetChatStatisticsSpy,
      },
      {
        statisticType: 'Visit',
        method: handleGetVisitStatisticsSpy,
      },
      // {
      //   statisticType: 'UniqueVisit',
      //   method: handleGetUniqueVisitStatisticsSpy,
      // },
      {
        statisticType: 'Favorite',
        method: handleGetFavoriteStatisticsSpy,
      },
    ];

    testCases.forEach(async (tc) => {
      // eslint-disable-next-line standard/computed-property-even-spacing
      wrapper.vm.$store.state.goldplus[
        `selected${tc.statisticType}StatisticFilter`
      ] = newMockStatisticFilter;

      await wrapper.vm.$nextTick();

      expect(tc.method).toBeCalled();
    });
  });

  it('Should call resetStatisticFilter when the component is destroyed', async () => {
    const resetStatisticFiltersSpy = jest.spyOn(wrapper.vm, 'resetStatisticFilters');

    await wrapper.destroy();

    expect(resetStatisticFiltersSpy).toBeCalled();
  });
});
