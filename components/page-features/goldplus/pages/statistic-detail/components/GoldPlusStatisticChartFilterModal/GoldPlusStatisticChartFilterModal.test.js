import { shallowMount } from '@vue/test-utils';
import Vuex, { mapGetters, mapMutations, mapActions } from 'vuex';
import GoldPlusStatisticChartFilterModal from '../GoldPlusStatisticChartFilterModal';
import localVueWithBuefy from '~/utils/addBuefy';

import MkModal, { mixin as modalMixin } from '~/components/global/molecules/MkModal/MkModal';
import MkButton from '~/components/global/atoms/MkButton';

const stubs = {
  MkModal,
  MkButton,
};

const propsData = {
  statisticType: {
    pascalValue: 'Visit', // Visit, UniqueVisit, Chat, Favorite
  },
  active: true,
};

const mocks = {
  $device: { isMobile: true },
  $emit: jest.fn(),
  $route: {
    params: {
      id: 'test',
    },
  },
  $bugsnag: {
    notify: jest.fn(),
  },
  $alert: jest.fn(),
  $tracker: { send: jest.fn() },
};

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        roomDetail: {
          gp_status: {
            key: 9,
            value: 'Goldplus 1',
          },
        },
      },
      getters: {
        statisticFilters: () =>
          jest.fn().mockReturnValue({ 0: { key: 'key_0' }, 1: { key: 'key_1' } }),
        selectedStatisticFilter: () => jest.fn().mockReturnValue({ key: 'test' }),
      },
      mutations: {
        setSelectedVisitStatisticFilter: jest.fn(),
        setSelectedUniqueVisitStatisticFilter: jest.fn(),
        setSelectedChatStatisticFilter: jest.fn(),
        setSelectedFavoriteStatisticFilter: jest.fn(),
      },
      actions: {
        getStatisticFilters: jest.fn().mockResolvedValue({ data: { status: true, data: {} } }),
      },
    },
    profile: {
      namespaced: true,
      getters: {
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
    },
  },
};

localVueWithBuefy.use(Vuex);

global.mapActions = mapActions;
global.mapMutations = mapMutations;
global.mapGetters = mapGetters;

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      mocks,
      stubs,
      propsData,
      localVue: localVueWithBuefy,
      mixins: [modalMixin],
      store: new Vuex.Store(storeData),
    },
    ...adtMountData,
  };

  return shallowMount(GoldPlusStatisticChartFilterModal, mountData);
};

describe('GoldPlusStatisticChartFilterModal.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render component properly', () => {
      expect(wrapper.find('.goldplus-statistic-chart-filter-modal').exists()).toBeTruthy();
    });
  });

  describe('should render computed value properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should compute showModal value', async () => {
      // get
      await wrapper.setProps({
        active: true,
      });
      expect(wrapper.vm.showModal).toBeTruthy();

      // set
      const emitSpy = jest.spyOn(wrapper.vm, '$emit');
      wrapper.vm.showModal = false;
      expect(emitSpy).toBeCalled();
      emitSpy.mockRestore();
    });

    it('should compute statisticFilterArray value', () => {
      expect(wrapper.vm.statisticFilterArray).toEqual([{ key: 'key_0' }, { key: 'key_1' }]);
    });

    it('should compute selectedFilterObject value', async () => {
      await wrapper.setData({
        selectedFilterKey: 'key_0',
      });
      expect(wrapper.vm.selectedFilterObject).toEqual({
        key: 'key_0',
      });
    });
  });

  describe('should execute functions in the watch block', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should call handleGetStatisticFilters on active value change, if the modal is not active and statisticFilterArray is empty', async () => {
      wrapper.destroy();
      const mockStoreData = Object.assign(storeData);
      storeData.modules.goldplus.getters.statisticFilters = () => jest.fn().mockReturnValue([]);
      wrapper = mount({ store: new Vuex.Store(mockStoreData) });

      const handleGetStatisticFiltersSpy = jest.spyOn(wrapper.vm, 'handleGetStatisticFilters');
      await wrapper.setProps({ active: false });
      await wrapper.setProps({ active: true });

      expect(handleGetStatisticFiltersSpy).toBeCalled();
      handleGetStatisticFiltersSpy.mockRestore();
    });
  });

  describe('should execute functions properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should call $bugsnag.notify and alert, when calling handleGetStatisticFilters method and received error', async () => {
      wrapper.destroy();
      const mockStoreData = Object.assign(storeData);
      storeData.modules.goldplus.getters.selectedStatisticFilter = () =>
        jest.fn().mockReturnValue(undefined);
      storeData.modules.goldplus.actions.getStatisticFilters = jest.fn().mockRejectedValue('error');

      wrapper = mount({ store: new Vuex.Store(mockStoreData) });
      await wrapper.vm.handleGetStatisticFilters();

      expect(wrapper.vm.$bugsnag.notify).toBeCalled();
      expect(wrapper.vm.$alert).toBeCalled();
      expect(wrapper.vm.isLoadingFilters).toBeFalsy();
    });

    it('should call toggleModal when calling closeModal method', async () => {
      const toggleModalSpy = jest.spyOn(wrapper.vm, 'toggleModal');
      const closeButton = wrapper.find('.goldplus-statistic-chart-filter-modal__close-button');
      if (closeButton && closeButton.exists()) {
        await closeButton.trigger('click');

        expect(toggleModalSpy).toBeCalled();
        toggleModalSpy.mockRestore();
      }
    });

    it('should call setSelected...StatisticFilter function when calling handleSetFilter method', async () => {
      const setSelectedVisitStatisticFilterSpy = jest.spyOn(
        wrapper.vm,
        'setSelectedVisitStatisticFilter',
      );
      await wrapper.setProps({
        statisticType: {
          pascalValue: 'Visit',
        },
      });
      await wrapper.vm.handleSetFilter();
      expect(setSelectedVisitStatisticFilterSpy).toBeCalled();
      setSelectedVisitStatisticFilterSpy.mockRestore();
    });
  });
});
