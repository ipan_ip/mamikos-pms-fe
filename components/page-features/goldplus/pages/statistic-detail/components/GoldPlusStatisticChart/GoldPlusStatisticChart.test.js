import { shallowMount } from '@vue/test-utils';
import Chart from 'chart.js';
import GoldPlusStatisticChart from '../GoldPlusStatisticChart';
import localVueWithBuefy from '~/utils/addBuefy';

const mockComponent = {
  template: '<div><slot /></div>',
};

const stubs = {
  GoldPlusStatisticChartFilterModal: mockComponent,
  GoldPlusStatisticChartDescription: mockComponent,
  GoldPlusStatisticChartTipsCard: {
    template: '<div id="GoldPlusStatisticChartTipsCard" />',
  },
};

const propsData = {
  isLoading: false,
  title: 'test',
  description: 'test',
  selectedFilter: {
    value: 'test',
    key: 'today',
  },
  latestValue: 1,
  chartData: {
    id: 'test',
    isEmptyData: true,
    growthStatus: {
      growth_type: 'growth_type',
    },
  },
};

global.Chart = Chart;
global.initAlwaysShowLastTooltipPlugin = jest.fn();
global.document.getElementById = jest.fn().mockReturnValue({
  getContext: jest.fn().mockReturnValue([]),
});

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      stubs,
      propsData,
    },
    ...adtMountData,
  };

  return shallowMount(GoldPlusStatisticChart, mountData);
};

describe('GoldPlusStatisticChart.vue', () => {
  describe('should render component', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should render component properly', () => {
      expect(wrapper.find('.goldplus-statistic-chart').exists()).toBeTruthy();
    });

    it("should render GoldPlusStatisticChartTipsCard if growth type equals to 'down'", async () => {
      await wrapper.setProps({
        chartData: {
          growthStatus: {
            growth_type: 'down',
          },
        },
      });
      expect(wrapper.find('#GoldPlusStatisticChartTipsCard').exists()).toBeTruthy();
    });
  });

  describe('should execute functions in the watch block', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should call updateChartElement on isLoading value change, and the value returns false', async () => {
      const updateChartElementSpy = jest.spyOn(wrapper.vm, 'updateChartElement');
      await wrapper.setProps({
        isLoading: true,
      });
      await wrapper.setProps({
        isLoading: false,
      });

      expect(updateChartElementSpy).toBeCalled();
      updateChartElementSpy.mockRestore();
    });
  });

  describe('should render computed value properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should compute footerNote properly', async () => {
      // footer note is empty
      await wrapper.setProps({
        chartData: {
          isEmptyData: true,
          growthStatus: {
            growth_type: 'growth_type',
          },
        },
      });
      expect(wrapper.vm.footerNote).toBe('Statistik belum bisa menampilkan data test');

      // footer note not empty
      await wrapper.setProps({
        selectedFilter: {
          value: 'test',
          key: 'today',
        },
        chartData: {
          id: 'test',
          isEmptyData: false,
          growthStatus: {
            growth_type: 'growth_type',
          },
        },
      });
      expect(wrapper.vm.footerNote).toBe(
        'Persentase di atas menunjukkan pertumbuhan dari sebelum gabung GoldPlus.',
      );

      await wrapper.setProps({
        chartData: {
          isEmptyData: false,
          growthStatus: {
            growth_type: null,
          },
        },
      });
      expect(wrapper.vm.footerNote).toBe(null);
    });

    it('should compute chartOptions properly', () => {
      const chartOptions = wrapper.vm.chartOptions;

      expect(chartOptions.tooltips.callbacks.title({}, { labelTitle: 'test' })).toBe('test');
      expect(chartOptions.tooltips.callbacks.label({}, { growthStatus: { tooltip: 'test' } })).toBe(
        'test',
      );
    });
  });

  describe('should execute functions properly', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = mount();
    });

    it('should set isShowFilterModal  as true when clicking on filter button', async () => {
      const filterButton = wrapper.find('.goldplus-statistic-chart__filter-button');
      if (filterButton && filterButton.exists()) {
        await filterButton.trigger('click');

        expect(wrapper.vm.isShowFilterModal).toBeTruthy();
      }
    });
  });
});
