import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusStatisticChartTipsCard from '../GoldPlusStatisticChartTipsCard';
import localVueWithBuefy from '~/utils/addBuefy';

global.window = Object.create(window);
const url = 'mamikos.com';

Object.defineProperty(window, 'location', {
  value: { href: url },
});

const $device = {
  isMobile: true,
  isDesktop: false,
};

const $tracker = { send: jest.fn() };

const propsData = {
  statisticType: {
    key: 'visit',
  },
};

localVueWithBuefy.use(Vuex);

global.open = jest.fn(() => true);

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      getters: {
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
    },
    goldplus: {
      namespaced: true,
      state: {
        roomDetail: {
          gp_status: {
            key: 9,
            value: 'Goldplus 1',
          },
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

global.open = jest.fn(() => true);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-statistic-id',
    },
    $device,
    $tracker,
  },
  propsData,
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusStatisticChartTipsCard, finalData);
};

describe('GoldPlusStatisticChartTipsCard.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-statistic-chart-tips-card').exists()).toBeTruthy();
  });

  it('should mount properly', () => {
    expect(wrapper.vm.tipsLink).toBe(
      'https://mamikos.com/info/fasilitas-yang-banyak-dicari-oleh-pencari-kost-milenial-pemilik-kost-wajib-tahu-nih/',
    );

    wrapper.setProps({
      statisticType: {
        key: 'unique_visit',
      },
    });
    expect(wrapper.vm.tipsLink).toBe(
      'https://mamikos.com/info/fasilitas-yang-banyak-dicari-oleh-pencari-kost-milenial-pemilik-kost-wajib-tahu-nih/',
    );

    wrapper.setProps({
      statisticType: {
        key: 'chat',
      },
    });
    expect(wrapper.vm.tipsLink).toBe(
      'https://mamikos.com/info/tips-jitu-agar-kost-anda-dilirik-oleh-mahasiswa-baru/',
    );

    wrapper.setProps({
      statisticType: {
        key: 'favorite',
      },
    });
    expect(wrapper.vm.tipsLink).toBe(
      'https://mamikos.com/info/tips-jitu-agar-kost-anda-dilirik-oleh-mahasiswa-baru/',
    );

    wrapper.setProps({
      statisticType: {
        key: '',
      },
    });
    expect(wrapper.vm.tipsLink).toBe('');
  });

  it('should goToLink on Mobile properly', () => {
    const button = wrapper.find('.goldplus-statistic-chart-tips-card__footer');
    button.trigger('click');
    expect(global.open).toBeCalled();
  });
});
