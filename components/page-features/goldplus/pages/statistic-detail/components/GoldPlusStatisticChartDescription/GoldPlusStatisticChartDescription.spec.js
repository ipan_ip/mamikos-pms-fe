import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import dayjs from 'dayjs';
import GoldPlusStatisticChartDescription from '../GoldPlusStatisticChartDescription';
import localVueWithBuefy from '~/utils/addBuefy';
import mockLazy from '~/utils/mocks/lazy';

const $device = {
  isMobile: true,
  isDesktop: false,
};

mockLazy(localVueWithBuefy);

localVueWithBuefy.use(Vuex);

const mockTitle = 'mocked title';
const mockDescription = 'mocked description';
const mockDateRange = 'mocked data range';
const mockLatestValue = 'mocked latest value';
const mockGrowthType = 'up';

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-statistic-id',
    },
    $dayjs: dayjs,
    $device,
  },
  propsData: {
    title: mockTitle,
    description: mockDescription,
    dateRange: mockDateRange,
    latestValue: mockLatestValue,
    growthType: mockGrowthType,
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusStatisticChartDescription, finalData);
};

describe('GoldPlusStatisticChartDescription.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  jest.mock(`~/assets/images/goldplus/ic_statistic_growth_up.svg`, () => {
    return 'up';
  });

  jest.mock(`~/assets/images/goldplus/ic_statistic_growth_down.svg`, () => {
    return 'down';
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-statistic-chart-description').exists()).toBeTruthy();
  });

  it('should return menu icon properly ', () => {
    expect(wrapper.vm.growthIcon).toEqual('up');

    wrapper.setProps({
      growthType: 'down',
    });

    expect(wrapper.vm.growthIcon).toEqual(wrapper.vm.growthType);
  });

  it('should return dateRangePlaceholder properly', () => {
    expect(wrapper.vm.dateRangePlaceholder).toEqual('mocked data range');

    wrapper.setProps({
      dateRange: '',
    });
    const date = new Date();
    const yesterday = date.setDate(date.getDate() - 1);
    expect(wrapper.vm.dateRangePlaceholder).toEqual(dayjs(yesterday).format('DD MMM YYYY'));
  });
});
