import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import dayjs from 'dayjs';
import mobile from '../mobile';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: true,
  isDesktop: false,
};

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        pendingBillingList: {
          total: 123,
          offset: 0,
          limit: 10,
          billings: [
            {
              invoice_id: 123,
              name: 'GoldPlus 2',
              description: 'Semua Properti',
              start_date: '2020-11-20',
              end_date: '2020-12-19',
              amount: 200000,
              status: 'unpaid',
              due_date: '2020-12-19',
              paid_at: null,
              expired_at: null,
            },
          ],
        },
        doneBillingList: {
          total: 123,
          offset: 0,
          limit: 10,
          billings: [
            {
              invoice_id: 123,
              name: 'GoldPlus 2',
              description: 'Semua Properti',
              start_date: '2020-11-20',
              end_date: '2020-12-19',
              amount: 200000,
              status: 'paid',
              due_date: '2020-12-19',
              paid_at: '2020-12-19',
              expired_at: '2020-12-20',
            },
          ],
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-payment',
    },
    $dayjs: dayjs,
    $device,
    $router: {
      push: jest.fn(),
    },
  },
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(mobile, finalData);
};

describe('mobile.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-payment-mobile').exists()).toBeTruthy();
  });

  it('should route to goldplus payment details by id', () => {
    const id = 1;
    wrapper.vm.handleViewDetails(id);

    expect(wrapper.vm.$router.push).toBeCalled();
  });
});
