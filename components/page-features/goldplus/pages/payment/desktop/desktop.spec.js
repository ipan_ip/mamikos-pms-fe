import Vuex from 'vuex';
import dayjs from 'dayjs';
import { shallowMount } from '@vue/test-utils';
import desktop from '../desktop';
import localVueWithBuefy from '~/utils/addBuefy';
import { toIDR } from '~/utils/currencyString';

const $device = {
  isMobile: false,
  isDesktop: true,
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('currencyIDR', toIDR);

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        pendingBillingList: {
          total: 123,
          offset: 0,
          limit: 10,
          billings: [
            {
              invoice_id: 123,
              name: 'GoldPlus 2',
              description: 'Semua Properti',
              start_date: '2020-11-20',
              end_date: '2020-12-19',
              amount: 200000,
              status: 'unpaid',
              due_date: '2020-12-19',
              paid_at: null,
              expired_at: null,
            },
          ],
        },
        doneBillingList: {
          total: 123,
          offset: 0,
          limit: 10,
          billings: [
            {
              invoice_id: 123,
              name: 'GoldPlus 2',
              description: 'Semua Properti',
              start_date: '2020-11-20',
              end_date: '2020-12-19',
              amount: 200000,
              status: 'paid',
              due_date: '2020-12-19',
              paid_at: '2020-12-19',
              expired_at: '2020-12-20',
            },
          ],
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-payment',
    },
    $dayjs: dayjs,
    $device,
    $router: {
      push: jest.fn(),
    },
  },
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(desktop, finalData);
};

describe('desktop.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-payment-desktop').exists()).toBeTruthy();
  });

  it('should handle dateYearFormat method', () => {
    expect(wrapper.vm.dateYearFormat('2020-12-19')).toBe('19 Dec 2020');
  });

  it('should handle dateFormat method', () => {
    expect(wrapper.vm.dateFormat('2020-12-19')).toBe('19 Dec');
  });

  it('should route to goldplus payment details by id', () => {
    const id = 1;
    wrapper.vm.handleViewDetails(id);

    expect(wrapper.vm.$router.push).toBeCalled();
  });

  it('should handle go back', () => {
    wrapper.vm.handleGoBack();

    expect(wrapper.vm.$router.push).toBeCalled();
  });
});
