import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusPaymentEmptyList from '../GoldPlusPaymentEmptyList';
import localVueWithBuefy from '~/utils/addBuefy';
import { toIDR } from '~/utils/currencyString';

const $device = {
  isMobile: true,
  isDesktop: false,
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('currencyIDR', toIDR);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-payment',
    },
    $device,
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusPaymentEmptyList, finalData);
};

describe('GoldPlusPaymentEmptyList.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-payment-empty-list').exists()).toBeTruthy();
  });

  it('should show content', () => {
    expect(wrapper.find('.goldplus-payment-empty-list__content').exists()).toBeTruthy();
  });
});
