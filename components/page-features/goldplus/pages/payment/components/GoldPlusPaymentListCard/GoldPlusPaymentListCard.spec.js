import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import dayjs from 'dayjs';
import GoldPlusPaymentListCard from '../GoldPlusPaymentListCard';
import localVueWithBuefy from '~/utils/addBuefy';
import { toIDR } from '~/utils/currencyString';

const $device = {
  isMobile: true,
  isDesktop: false,
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('currencyIDR', toIDR);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-payment',
    },
    $dayjs: dayjs,
    $device,
  },
  propsData: {
    listData: {
      invoice_id: 123,
      name: 'GoldPlus 2',
      description: 'Semua Properti',
      start_date: '2020-11-20',
      end_date: '2020-12-19',
      amount: 200000,
      status: 'unpaid',
      due_date: '2020-12-19',
      paid_at: {},
      expired_at: {},
    },
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusPaymentListCard, finalData);
};

describe('GoldPlusPaymentListCard.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-payment-list-card').exists()).toBeTruthy();
  });

  it('should show due date', () => {
    expect(wrapper.find('.goldplus-payment-list-card__muted').text()).toEqual(
      'Jatuh tempo 19 Dec 2020',
    );
  });

  it('should handle dateTimeFormat method', () => {
    expect(wrapper.vm.dateTimeFormat('2020-12-19')).toBe('19 Dec 2020, 12.00');
  });

  it('should handle dateFormat method', () => {
    expect(wrapper.vm.dateFormat('2020-12-19')).toBe('19 Dec');
  });

  it('should handle dateYearFormat method', () => {
    expect(wrapper.vm.dateYearFormat('2020-12-19')).toBe('19 Dec 2020');
  });
});
