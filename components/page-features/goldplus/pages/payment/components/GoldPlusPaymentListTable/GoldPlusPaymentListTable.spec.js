import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusPaymentListTable from '../GoldPlusPaymentListTable';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: true,
  isDesktop: false,
};

localVueWithBuefy.use(Vuex);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-payment',
    },
    $device,
  },
  propsData: {
    tableHead: ['Tanggal Jatuh Tempo', 'Nama Tagihan', 'Periode', 'Total Tagihan', 'Aksi'],
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusPaymentListTable, finalData);
};

describe('GoldPlusPaymentListTable.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-payment-list-table').exists()).toBeTruthy();
  });
});
