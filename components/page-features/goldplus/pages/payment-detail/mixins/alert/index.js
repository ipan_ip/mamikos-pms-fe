import { mapState } from 'vuex';
import _get from 'lodash/get';

export default {
  computed: {
    ...mapState('goldplus', ['billingContractDetail']),
    isShowAlert() {
      return _get(this.billingContractDetail, 'is_show_alert', false);
    },
    alertReminder() {
      const { expiry_time } = this.billingContractDetail;
      const formattedExpiryTime = this.$dayjs(expiry_time).format('D MMM YYYY, hh:mm');
      const title = 'Tagihan Ini Lewat Jatuh Tempo';
      const description = `Agar langganan GoldPlus Anda tidak dibatalkan oleh sistem, yuk segera bayar sebelum <strong>${formattedExpiryTime}</strong>.`;
      return {
        title,
        description,
      };
    },
  },
};
