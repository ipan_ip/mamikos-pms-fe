import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import dayjs from 'dayjs';
import GoldplusPaymentBillingDetail from '../GoldplusPaymentBillingDetail';
import localVueWithBuefy from '~/utils/addBuefy';
import { toIDR } from '~/utils/currencyString';

const $device = {
  isMobile: true,
  isDesktop: false,
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('currencyIDR', toIDR);

const billingContractDetail = {
  order_id: 123,
  order_name: 'Pembayaran GP',
  order_amount: 20000,
  order_status: 'pending',
  order_time_label: null,
  order_time_value: null,
  contract_start_date: '2020-05-05',
  contract_end_date: '2020-05-05',
  is_due_date: true,
  expiry_time: '2020-11-23 12:12:00',
  package: {
    package_name: 'Paket Goldplus 1',
    package_type: 'goldplus_1',
    package_description: 'Paket Goldplus 1 untuk semua kos Anda',
    package_price: 20000,
  },
  property_detail: null,
};

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        billingContractDetail,
        billingInvoiceData: {
          invoice_id: 123,
          invoice_number: 'GP1/20201014/00000012/0001',
        },
        billingInvoicePayment: {
          invoice_number: 'GP1/20201014/00000012/0001',
          status: 'unpaid',
          amount: 0,
          order_detail_url: 'https://pay.mamikos.com/invoice/123',
        },
      },
      mutations: {
        setBillingContractDetail(state, payload) {
          state.billingContractDetail = payload;
        },
      },
      actions: {
        fetchBillingContractDetail() {
          return $api.getBillingContractDetail();
        },
        fetchBillingInvoice() {
          return $api.getBillingInvoice();
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const $route = {
  name: 'goldplus-payment-id',
  params: {
    id: 1,
  },
};
const $bugsnag = { notify: jest.fn() };
const $api = {
  getBillingContractDetail: jest.fn(),
  getBillingInvoice: jest.fn(),
};
const $alert = jest.fn();

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $dayjs: dayjs,
    $alert,
    $route,
    $device,
    $bugsnag,
    $api,
  },
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldplusPaymentBillingDetail, finalData);
};

describe('GoldplusPaymentBillingDetail.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-billing-detail').exists()).toBeTruthy();
  });

  it('should return correct invoice id', () => {
    expect(wrapper.vm.invoiceId).toBe(1);
  });

  it('should return correct total amount', () => {
    expect(wrapper.vm.totalAmount).toBe('Rp20.000');
  });

  it('should hide order time section when order_time_label or order_time_value is null', () => {
    expect(wrapper.vm.hasOrderTime).toBeFalsy();
  });

  it('should show order time section when order_time_label or order_time_value is not null', async () => {
    const payload = {
      ...billingContractDetail,
      order_time_label: 'Jatuh Tempo',
      order_time_value: 'Jumat, 04 Desember 2020',
    };
    await store.commit('goldplus/setBillingContractDetail', payload);

    expect(wrapper.vm.hasOrderTime).toBeTruthy();
  });
});
