import Vuex from 'vuex';
import dayjs from 'dayjs';
import { shallowMount } from '@vue/test-utils';
import GoldplusPaymentActionButton from '../GoldplusPaymentActionButton';
import localVueWithBuefy from '~/utils/addBuefy';
import { toIDR } from '~/utils/currencyString';

const $device = {
  isMobile: true,
  isDesktop: false,
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('currencyIDR', toIDR);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-payment-id',
    },
    $dayjs: dayjs,
    $device,
  },
  propsData: {
    orderAmount: 20000,
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldplusPaymentActionButton, finalData);
};

describe('GoldplusPaymentActionButton.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-action-button').exists()).toBeTruthy();
  });

  it('should set button text correctly', () => {
    expect(wrapper.vm.actionButtonText).toBe('Bayar Sekarang Rp20.000');
  });

  it('should emit action when button clicked', () => {
    wrapper.vm.handleActionPay();

    expect(wrapper.emitted()['action-pay']).toBeTruthy();
  });
});
