import { shallowMount } from '@vue/test-utils';
import dayjs from 'dayjs';
import GoldplusPaymentPackageDetail from '.';
import localVueWithBuefy from '~/utils/addBuefy';
import mockLazy from '~/utils/mocks/lazy';
import { toIDR } from '~/utils/currencyString';

mockLazy(localVueWithBuefy);
localVueWithBuefy.filter('currencyIDR', toIDR);

const $device = {
  isMobile: true,
  isDesktop: false,
};

const billingContractDetail = {
  order_id: 123,
  order_name: 'Pembayaran GP',
  order_amount: 20000,
  order_status: 'pending',
  order_time_label: 'Jatuh Tempo',
  order_time_value: '2020-05-05 00:00:00',
  contract_start_date: '2020-05-05',
  contract_end_date: '2020-05-05',
  is_due_date: true,
  expiry_time: '2020-11-23 12:12:00',
  package: {
    package_name: 'Paket Goldplus X',
    package_type: 'goldplus_3',
    package_description: 'Paket Goldplus X untuk semua kos Anda',
    package_price: 20000,
  },
  property_detail: {
    property_name: 'Kost Rumah Kamang Residence Satu Dua Tiga Haha',
    property_image_url: {
      medium: 'https://placekitten.com/300/300',
    },
    property_rooms_count: '31',
    property_address: 'Jl. Satu Dua Tiga',
  },
};

const goldplusOneData = {
  ...billingContractDetail,
  package: {
    package_name: 'Paket Goldplus 1',
    package_type: 'goldplus_1',
    package_description: 'Paket Goldplus 1 untuk semua kos Anda',
    package_price: 20000,
  },
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-payment-id',
    },
    $dayjs: dayjs,
    $device,
  },
  propsData: {
    orderDetail: billingContractDetail,
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldplusPaymentPackageDetail, finalData);
};

describe('GoldplusPaymentPackageDetail.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-package-detail').exists()).toBeTruthy();
  });

  it('[mobile] should set gridColumn correctly on normal package type', async () => {
    await wrapper.setProps({
      orderDetail: goldplusOneData,
    });

    expect(wrapper.vm.gridColumn).toEqual({ left: 4, right: 0 });
  });

  it('[mobile] should set gridColumn correctly on goldplus3', () => {
    expect(wrapper.vm.gridColumn).toEqual({ left: 3, right: 1 });
  });

  it('[desktop] should set gridColumn correctly on normal package type ', async () => {
    const wrapperDesktop = mount({
      mocks: {
        $route: {
          name: 'goldplus-payment-id',
        },
        $dayjs: dayjs,
        $device: {
          isMobile: false,
          isDesktop: true,
        },
      },
    });
    await wrapperDesktop.setProps({
      orderDetail: goldplusOneData,
    });

    expect(wrapperDesktop.vm.gridColumn).toEqual({ left: 12, right: 0 });
  });

  it('[desktop] should set gridColumn correctly on goldplus3', () => {
    const wrapperDesktop = mount({
      mocks: {
        $route: {
          name: 'goldplus-payment-id',
        },
        $dayjs: dayjs,
        $device: {
          isMobile: false,
          isDesktop: true,
        },
      },
    });

    expect(wrapperDesktop.vm.gridColumn).toEqual({ left: 8, right: 4 });
  });

  it('should hide field kamar on gp3 if property_room_count is null', async () => {
    const goldplusThreeData = {
      ...billingContractDetail,
      property_detail: {
        property_name: 'Kost Rumah Kamang Residence Satu Dua Tiga Haha',
        property_image_url: {
          medium: 'https://placekitten.com/300/300',
        },
        property_rooms_count: null,
        property_address: 'Jl. Satu Dua Tiga',
      },
      package: {
        package_name: 'Paket Goldplus 3',
        package_type: 'goldplus_3',
        package_description: 'Paket Goldplus 3 untuk semua kos Anda',
        package_price: 20000,
      },
    };

    await wrapper.setProps({
      orderDetail: goldplusThreeData,
    });

    expect(wrapper.vm.packageDescription).toBe('');
    expect(wrapper.find('.goldplus-package-detail_room-count').exists()).toBeFalsy();
  });

  it('should show field kamar on gp3 if property_room_count is not null', async () => {
    const goldplusThreeData = {
      ...billingContractDetail,
      property_detail: {
        property_name: 'Kost Rumah Kamang Residence Satu Dua Tiga Haha',
        property_image_url: {
          medium: 'https://placekitten.com/300/300',
        },
        property_rooms_count: 3,
        property_address: 'Jl. Satu Dua Tiga',
      },
      package: {
        package_name: 'Paket Goldplus 3',
        package_type: 'goldplus_3',
        package_description: 'Paket Goldplus 3 untuk semua kos Anda',
        package_price: 20000,
      },
    };

    await wrapper.setProps({
      orderDetail: goldplusThreeData,
    });

    expect(wrapper.vm.packageDescription).toBe('3 Kamar');
    expect(wrapper.find('.goldplus-package-detail_room-count').exists()).toBeTruthy();
  });
});
