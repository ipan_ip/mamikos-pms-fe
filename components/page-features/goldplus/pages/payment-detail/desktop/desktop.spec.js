import Vuex from 'vuex';
import dayjs from 'dayjs';
import { shallowMount } from '@vue/test-utils';
import desktop from '../desktop';
import localVueWithBuefy from '~/utils/addBuefy';
import { toIDR } from '~/utils/currencyString';

const $device = {
  isMobile: true,
  isDesktop: false,
};

localVueWithBuefy.use(Vuex);
localVueWithBuefy.filter('currencyIDR', toIDR);

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        billingContractDetail: {
          order_id: 123,
          order_name: 'Pembayaran GP',
          order_amount: 20000,
          order_status: 'pending',
          order_time_label: 'Jatuh Tempo',
          order_time_value: 'Jumat, 04 Desember 2020, 21:09 WIB',
          contract_start_date: '2020-05-05',
          contract_end_date: '2020-05-05',
          is_due_date: true,
          expiry_time: '2020-11-23 12:12:00',
          package: {
            package_name: 'Paket Goldplus 1',
            package_type: 'goldplus_1',
            package_description: 'Paket Goldplus 1 untuk semua kos Anda',
            package_price: 20000,
          },
          property_detail: null,
        },
        billingInvoiceData: {
          invoice_id: '',
          invoice_number: 'GP1/20201014/00000012/0001',
        },
        billingInvoicePayment: {
          invoice_number: 'GP1/20201014/00000012/0001',
          status: 'unpaid',
          amount: 0,
          order_detail_url: 'https://pay.mamikos.com/invoice/123',
        },
      },
      mutations: {
        setBillingInvoiceData(state, payload) {
          state.billingInvoiceData = payload;
        },
      },
      actions: {
        fetchBillingContractDetail() {
          return $api.getBillingContractDetail();
        },
        fetchBillingInvoice() {
          return $api.getBillingInvoice();
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const $route = {
  name: 'goldplus-payment-id',
  params: {
    id: 5,
  },
  query: {
    submission: true,
  },
};
const $bugsnag = { notify: jest.fn() };
const $api = {
  getBillingContractDetail: jest.fn(),
  getBillingInvoice: jest.fn(),
};
const $alert = jest.fn();

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $dayjs: dayjs,
    $route,
    $device,
    $bugsnag,
    $api,
    $alert,
  },
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(desktop, finalData);
};

describe('desktop.vue', () => {
  let wrapper;

  delete window.location;
  window.location = { reload: jest.fn() };

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-detail-bill-desktop').exists()).toBeTruthy();
  });

  it('should return invoice id from params if billingInvoiceData.invoice_id is empty', () => {
    expect(wrapper.vm.invoiceId).toBe(5);
  });

  it('should return invoice id from billingInvoiceData.invoice_id by default', async () => {
    await store.commit('goldplus/setBillingInvoiceData', {
      invoice_id: 123,
      invoice_number: 'GP1/20201014/00000012/0001',
    });

    expect(wrapper.vm.invoiceId).toBe(123);
  });

  it('should call bugsnag.notify on failed handleFetchBillingData api call', async () => {
    wrapper.vm.$api.getBillingInvoice = jest.fn().mockRejectedValue('error');
    await wrapper.vm.handleFetchBillingData();

    expect($bugsnag.notify).toBeCalled();
    expect($alert).toBeCalled();
  });

  it('should redirect to invoice page when click Bayar Sekarang', () => {
    wrapper.vm.handleActionPay();

    global.window = Object.create(window);
    const invoice_url = 'https://pay.mamikos.com/invoice/select-payment/1';
    Object.defineProperty(window, 'location', {
      value: {
        href: invoice_url,
      },
    });

    expect(window.location.href).toEqual(invoice_url);
    expect(wrapper.vm.isActionButtonLoading).toBeTruthy();
  });

  it('should reload if query submission is true', () => {
    wrapper.vm.handleReloadPage();

    const url = '/goldplus/payment/5';
    Object.defineProperty(window, 'location', {
      value: {
        href: url,
      },
    });
    expect(window.location.href).toEqual(url);
  });

  it('should reload if query submission is false', () => {
    const wrapperFalseSubmission = mount({
      mocks: {
        $dayjs: dayjs,
        $route: {
          query: {
            submission: false,
          },
        },
        $device,
        $bugsnag,
        $api,
        $alert,
      },
    });
    wrapperFalseSubmission.vm.handleReloadPage();

    const url = '/goldplus/payment/5';
    Object.defineProperty(window, 'location', {
      value: {
        href: url,
      },
    });
    expect(window.location.href).toEqual(url);
  });
});
