import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import desktop from '../desktop';
import mockComponent from '~/utils/mockComponent';
import localVueWithBuefy from '~/utils/addBuefy';
import restrictNewSubmission from '~/components/page-features/goldplus/mixins/restrictNewSubmission';

const stubs = {
  GoldPlusSubmissionOnBoardingContent: mockComponent,
  GoldPlusSubmissionOnBoardingFooter: mockComponent,
  GoldPlusHeader: mockComponent,
};

const $router = {
  replace: jest.fn(),
};

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      mutations: {
        setIsGPSubmissionStatusFetched: jest.fn(),
      },
    },
    profile: {
      namespaced: true,
      actions: {
        getProfile() {
          return jest.fn();
        },
      },
    },
  },
};

const $api = {
  fetchGoldPlusSubmissionOnboardingContent: jest
    .fn()
    .mockResolvedValue({
      data: {
        status: true,
        title: 'Manfaat GoldPlus',
        data: [
          {
            title: 'Title 1',
            body: 'Body 1',
          },
        ],
      },
    })
    .mockResolvedValueOnce({ data: { status: false, message: 'failed' } })
    .mockRejectedValueOnce(),
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mixins: [restrictNewSubmission],
  mocks: {
    $router,
    $store: { dispatch: jest.fn().mockResolvedValue({}) },
    $api,
    $alert: jest.fn(),
    $bugsnag: {
      notify: jest.fn(),
    },
  },
  stubs,
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(desktop, finalData);
};

describe('desktop.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('Should mount properly', () => {
    expect(wrapper.find('.goldplus-submission-onboarding').exists()).toBeTruthy();
  });

  it('Should redirect to dashboard when close button is clicked', () => {
    wrapper.vm.backToDashboard();
    expect($router.replace).toBeCalledWith('/');
  });
});
