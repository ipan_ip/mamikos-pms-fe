import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mobile from '../mobile';
import mockComponent from '~/utils/mockComponent';
import localVueWithBuefy from '~/utils/addBuefy';
import restrictNewSubmission from '~/components/page-features/goldplus/mixins/restrictNewSubmission';

const stubs = {
  GoldPlusSubmissionOnBoardingContent: mockComponent,
  GoldPlusSubmissionOnBoardingFooter: mockComponent,
  GoldPlusHeader: mockComponent,
};

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      mutations: {
        setIsGPSubmissionStatusFetched: jest.fn(),
      },
    },
    profile: {
      namespaced: true,
      actions: {
        getProfile() {
          return jest.fn();
        },
      },
    },
  },
};

const $api = {
  fetchGoldPlusSubmissionOnboardingContent: jest
    .fn()
    .mockResolvedValue({
      data: {
        status: true,
        title: 'Manfaat GoldPlus',
        data: [
          {
            title: 'Title 1',
            body: 'Body 1',
          },
        ],
      },
    })
    .mockResolvedValueOnce({ data: { status: false, message: 'failed' } })
    .mockRejectedValueOnce(),
};

const store = new Vuex.Store(storeData);

const mountData = {
  mixins: [restrictNewSubmission],
  localVue: localVueWithBuefy,
  mocks: {
    $store: { dispatch: jest.fn().mockResolvedValue({}) },
    $api,
    $alert: jest.fn(),
    $bugsnag: {
      notify: jest.fn(),
    },
  },
  stubs,
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(mobile, finalData);
};

describe('mobile.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-submission-onboarding-mweb').exists()).toBeTruthy();
  });
});
