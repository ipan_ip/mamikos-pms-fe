export default {
  data() {
    return {
      isLoadingContent: false,
      onboardingTitle: '',
      onboardingContents: [],
    };
  },
  created() {
    this.handleFetchContent();
  },
  methods: {
    handleFetchContent() {
      this.isLoadingContent = true;

      this.$api
        .fetchGoldPlusSubmissionOnboardingContent()
        .then((response) => {
          if (response.data.status) {
            this.onboardingTitle = response.data.title;
            this.onboardingContents = response.data.data;
          } else {
            this.$alert('is-danger', 'Gagal', 'Terjadi galat, silakan coba lagi.');
          }
        })
        .catch((error) => {
          this.$bugsnag.notify(error);
          this.$alert('is-danger', 'Gagal', 'Terjadi galat, silakan coba lagi.');
        })
        .finally(() => {
          this.isLoadingContent = false;
        });
    },
  },
};
