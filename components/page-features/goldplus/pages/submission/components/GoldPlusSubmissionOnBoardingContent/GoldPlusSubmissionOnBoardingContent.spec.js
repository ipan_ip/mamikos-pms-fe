import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusSubmissionOnBoardingContent from '.';
import MkButton from '~/components/global/atoms/MkButton';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: true,
  isDesktop: false,
};

const stubs = {
  MkButton,
};

const $tracker = { send: jest.fn() };

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      getters: {
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $tracker,
    $device,
    $route: {
      query: { redirect_url: 'www.mamikos.com/blog' },
      params: { previousPage: 'Dashboard' },
    },
  },
  stubs,
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusSubmissionOnBoardingContent, finalData);
};

describe('GoldPlusSubmissionOnBoardingContent.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  jest.mock(`~/assets/images/goldplus/ic_subtract.svg`, () => {
    return 'ic_subtract';
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-submission-onboarding-content').exists()).toBeTruthy();
  });

  it('should call $tracker.send on Mobile', () => {
    wrapper.vm.trackPageVisited();
    expect(wrapper.vm.$tracker.send).toBeCalled();
  });

  it('should call $tracker.send on Desktop', () => {
    wrapper.vm.$device.isMobile = false;
    wrapper.vm.$device.isDesktop = true;
    wrapper.vm.trackPageVisited();

    expect(wrapper.vm.$tracker.send).toBeCalled();
  });
});
