import { shallowMount } from '@vue/test-utils';
import GoldPlusSubmissionOnBoardingFooter from '.';
import MkButton from '~/components/global/atoms/MkButton';
import localVueWithBuefy from '~/utils/addBuefy';

const stubs = {
  MkButton,
};

const $router = {
  push: jest.fn(),
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $router,
  },
  stubs,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusSubmissionOnBoardingFooter, finalData);
};

describe('GoldPlusSubmissionOnBoardingFooter.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-submission-onboarding-footer').exists()).toBeTruthy();
  });

  it('should route to statistic page', () => {
    wrapper.vm.handleButtonClicked();
    expect($router.push).toBeCalledWith('/goldplus/submission/steps');
  });
});
