import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusPackageModal from './GoldPlusPackageModal';
import localVueWithBuefy from '~/utils/addBuefy';

localVueWithBuefy.use(Vuex);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    goldPlusPackagesList: [],
  },
  propsData: {
    active: true,
    modalContent: 'Mock Modal Content',
    packageName: 'Mock Package Name',
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusPackageModal, finalData);
};

describe('GoldPlusPackageModal.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('Should mount properly', () => {
    expect(wrapper.find('.goldplus-package-modal').exists()).toBeTruthy();
  });

  it('should show Modal', () => {
    expect(wrapper.props('active')).toBeTruthy();
  });

  it('should close modal', () => {
    wrapper.find('.goldplus-package-modal__close-button').trigger('click');

    wrapper.vm.toggleModal = jest.fn();

    wrapper.vm.closeModal();

    expect(wrapper.vm.toggleModal).toBeCalled();
  });

  it('should emit selected Package', () => {
    wrapper.find('.goldplus-package-modal__footer-button').trigger('click');

    wrapper.vm.$emit = jest.fn();

    wrapper.vm.selectPackage();

    expect(wrapper.vm.$emit).toBeCalled();
  });

  it('should compute showModal value', async () => {
    // get
    await wrapper.setProps({
      active: true,
    });
    expect(wrapper.vm.showModal).toBeTruthy();

    // set
    const emitSpy = jest.spyOn(wrapper.vm, '$emit');
    wrapper.vm.showModal = false;
    expect(emitSpy).toBeCalled();
    emitSpy.mockRestore();
  });
});
