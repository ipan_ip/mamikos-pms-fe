import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusPackageContent from './GoldPlusPackageContent';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: true,
  isDesktop: false,
};

const $bugsnag = {
  notify: jest.fn(),
};

const $tracker = { send: jest.fn() };

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      getters: {
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const $router = {
  push: jest.fn((path) => {
    return path;
  }),
};

const $api = {
  getGoldPlusPackages: jest
    .fn()
    .mockResolvedValueOnce({
      data: {
        status: true,
        packages: [
          {
            id: 3,
            code: 'gp3',
            name: 'Goldplus 3',
            description: 'Goldplus 2 + Bantuan Account Manager',
            price_description: 'Rp595,000/bulan untuk setiap kos yang dipilih',
            detail_package_html:
              '<!DOCTYPE html><html><h5>Paket GoldPlus 3</h5><br><b>GoldPlus 2 + Account Manager</b><br><p><span>&#8226; </span>Mendapatkan semua keuntungan GoldPlus 1: pengelolaan sistem booking kos, manajemen tagihan kos, dan pengelolaan bisnis kos.</p><br><b>Bisnis kos Anda dibantu oleh Account Manager dari Mamikos, dengan dukungan berupa:</b><br><p><span>&#8226; </span>Laporan keuangan dapat diakses oleh pemilik kos pada dasbor/halaman pemilik.</p><p><span>&#8226; </span>Berkoordinasi dengan penjaga kos untuk membantu proses check-in dan check-out dari penyewa kos.</p><p><span>&#8226; </span>Memastikan bahwa kualitas kamar cocok dengan daftar iklan.</p><p><span>&#8226; </span>Menangani pengingat pembayaran dan membantu penghuni kos membayar sewa.</p><p><span>&#8226; </span>Menangani penagihan kos.</p><p><span>&#8226; </span>Mengelola chat, menerima booking, serta menetapkan penjatahan kamar</p><p><span>&#8226; </span>Membantu untuk mendaftarkan kos Anda ke fitur Premium, hingga membantu mengisi saldo Pre mium.</p><p><span>&#8226; </span>Mengelola permintaan/komplain di kos Anda menjadi prioritas CS kami</p><br><b>Memaksimalkan Pemasaran Kos Anda</b><br><p><span>&#8226; </span>Dapat memanfaatkan sejumlah produk pendukung untuk meningkatkan tampilan detail kos. BONUS: 30% top up saldo Premium + professional photo &amp; video.</p><br><b>Bonus</b><br><p><span>&#8226; </span>Cashback saldo Premium senilai Rp595.000 setiap bulan.</p></html>',
          },
        ],
      },
    })
    .mockResolvedValueOnce({ data: { status: false, message: 'failed' } })
    .mockRejectedValueOnce(),
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $api,
    $router,
    $device,
    $bugsnag,
    $tracker,
  },
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusPackageContent, finalData);
};

describe('GoldPlusPackageContent.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('Should mount properly', () => {
    expect(wrapper.find('.goldplus-package-content').exists()).toBeTruthy();
  });

  it('should call bugsnag notify on failed fetch goldplus package properly', async () => {
    wrapper.vm.$api.getGoldPlusPackages = jest.fn().mockResolvedValue();

    await wrapper.vm.handleFetchGoldPlusPackageList();

    await wrapper.vm.$nextTick();

    expect($bugsnag.notify).toBeCalled();
  });

  it('should handle track page visited on Mobile', () => {
    wrapper.vm.trackPageVisited();
    expect(wrapper.vm.$tracker.send).toBeCalled();
  });

  it('should handle track page visited on Desktop', () => {
    wrapper.vm.$device.isMobile = false;
    wrapper.vm.$device.isDesktop = true;
    wrapper.vm.trackPageVisited();

    expect(wrapper.vm.$tracker.send).toBeCalled();
  });
});
