import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusPackageCard from '.';
import mockLazy from '~/utils/mocks/lazy';
import MkCard from '~/components/global/molecules/MkCard';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';
import MkCardHeader from '~/components/global/molecules/MkCard/components/MkCardHeader';
import localVueWithBuefy from '~/utils/addBuefy';

global.window = Object.create(window);

const $device = {
  isMobile: true,
  isDesktop: false,
};

const stubs = {
  MkCard,
  MkCardBody,
  MkCardHeader,
};

mockLazy(localVueWithBuefy);
localVueWithBuefy.use(Vuex);

global.open = jest.fn(() => true);

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      getters: {
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const $router = {
  push: jest.fn((path) => {
    return path;
  }),
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $router,
    $device,
  },
  stubs,
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusPackageCard, finalData);
};

describe('GoldPlusPackageCard.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-package-card').exists()).toBeTruthy();
  });

  it('should show modal when "pelajari lebih lanjut" is clicked', () => {
    wrapper.vm.showModal();

    expect(wrapper.vm.isModalActive).toBeTruthy();
  });

  it('should handle select package properly', () => {
    const setSelectedPackageSpy = jest.spyOn(wrapper.vm, 'setSelectedPackage');
    const path = '/goldplus/submission/packages/gp3';

    wrapper.vm.handleSelectPackage();

    expect(setSelectedPackageSpy).toBeCalled();
    expect(wrapper.vm.$router.push(path)).toBe('/goldplus/submission/packages/gp3');
  });

  it('should handle select package properly', () => {
    expect(wrapper.vm.packageDetail.isEmpty).toBeFalsy();

    const mockPackageDetail = {
      id: 3,
      code: 'gp3',
      name: 'Goldplus 3',
      description: 'Goldplus 2 + Bantuan Account Manager',
      price_description: 'Rp595,000/bulan untuk setiap kos yang dipilih',
      detail_package_html:
        '<!DOCTYPE html><html><h5>Paket GoldPlus 3</h5><br><b>GoldPlus 2 + Account Manager</b><br><p><span>&#8226; </span>Mendapatkan semua keuntungan GoldPlus 1: pengelolaan sistem booking kos, manajemen tagihan kos, dan pengelolaan bisnis kos.</p><br><b>Bisnis kos Anda dibantu oleh Account Manager dari Mamikos, dengan dukungan berupa:</b><br><p><span>&#8226; </span>Laporan keuangan dapat diakses oleh pemilik kos pada dasbor/halaman pemilik.</p><p><span>&#8226; </span>Berkoordinasi dengan penjaga kos untuk membantu proses check-in dan check-out dari penyewa kos.</p><p><span>&#8226; </span>Memastikan bahwa kualitas kamar cocok dengan daftar iklan.</p><p><span>&#8226; </span>Menangani pengingat pembayaran dan membantu penghuni kos membayar sewa.</p><p><span>&#8226; </span>Menangani penagihan kos.</p><p><span>&#8226; </span>Mengelola chat, menerima booking, serta menetapkan penjatahan kamar</p><p><span>&#8226; </span>Membantu untuk mendaftarkan kos Anda ke fitur Premium, hingga membantu mengisi saldo Pre mium.</p><p><span>&#8226; </span>Mengelola permintaan/komplain di kos Anda menjadi prioritas CS kami</p><br><b>Memaksimalkan Pemasaran Kos Anda</b><br><p><span>&#8226; </span>Dapat memanfaatkan sejumlah produk pendukung untuk meningkatkan tampilan detail kos. BONUS: 30% top up saldo Premium + professional photo &amp; video.</p><br><b>Bonus</b><br><p><span>&#8226; </span>Cashback saldo Premium senilai Rp595.000 setiap bulan.</p></html>',
    };

    wrapper.setProps({ packageDetail: mockPackageDetail });
    expect(wrapper.vm.packageDetail).toEqual(mockPackageDetail);
  });
});
