import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import Mobile from './mobile';
import localVueWithBuefy from '~/utils/addBuefy';
import restrictNewSubmission from '~/components/page-features/goldplus/mixins/restrictNewSubmission';

localVueWithBuefy.use(Vuex);

const $router = {
  replace: jest.fn(),
};

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        isGoldPlusActive: true,
      },
      actions: {
        getActiveStatus() {
          return jest.fn();
        },
      },
      mutations: {
        setIsGPSubmissionStatusFetched: jest.fn(),
      },
    },
    profile: {
      namespaced: true,
      actions: {
        getProfile() {
          return jest.fn();
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $store: { dispatch: jest.fn().mockResolvedValue({}) },
    $router,
  },
  mixins: [restrictNewSubmission],
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(Mobile, finalData);
};

describe('mobile.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('Should mount properly', () => {
    expect(wrapper.find('.goldplus-packages-mweb').exists()).toBeTruthy();
  });
});
