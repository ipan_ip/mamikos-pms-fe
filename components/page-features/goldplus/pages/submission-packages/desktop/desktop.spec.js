import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import Desktop from './desktop';
import localVueWithBuefy from '~/utils/addBuefy';
import restrictNewSubmission from '~/components/page-features/goldplus/mixins/restrictNewSubmission';

localVueWithBuefy.use(Vuex);

const $router = {
  replace: jest.fn(),
};

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        isGoldPlusActive: true,
      },
      actions: {
        getActiveStatus() {
          return jest.fn();
        },
      },
      mutations: {
        setIsGPSubmissionStatusFetched: jest.fn(),
      },
    },
    profile: {
      namespaced: true,
      actions: {
        getProfile() {
          return jest.fn();
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $store: { dispatch: jest.fn() },
    $router,
  },
  mixins: [restrictNewSubmission],
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(Desktop, finalData);
};

describe('desktop.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('Should mount properly', () => {
    expect(wrapper.find('.goldplus-packages').exists()).toBeTruthy();
  });

  it('Should handle on clicked back button', () => {
    wrapper.vm.handleBackClicked();

    expect(wrapper.vm.$router.replace).toBeCalled();
  });
});
