import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mobile from '../mobile';
import mockComponent from '~/utils/mockComponent';
import localVueWithBuefy from '~/utils/addBuefy';
import restrictNewSubmission from '~/components/page-features/goldplus/mixins/restrictNewSubmission';

localVueWithBuefy.use(Vuex);

const stubs = {
  GoldPlusSubmissionOnBoardingContent: mockComponent,
  GoldPlusSubmissionOnBoardingFooter: mockComponent,
  GoldPlusHeader: mockComponent,
};

const $router = {
  replace: jest.fn(),
};

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        isGoldPlusActive: true,
      },
      actions: {
        getActiveStatus() {
          return jest.fn();
        },
      },
      mutations: {
        setIsGPSubmissionStatusFetched: jest.fn(),
      },
    },
    profile: {
      namespaced: true,
      actions: {
        getProfile() {
          return jest.fn();
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $store: { dispatch: jest.fn().mockResolvedValue({}) },
    $router,
  },
  mixins: [restrictNewSubmission],
  stubs,
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(mobile, finalData);
};

describe('mobile.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-submission-stepper-mweb').exists()).toBeTruthy();
  });
});
