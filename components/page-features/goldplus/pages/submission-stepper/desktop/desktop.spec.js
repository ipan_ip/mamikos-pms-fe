import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import desktop from '../desktop';
import mockComponent from '~/utils/mockComponent';
import localVueWithBuefy from '~/utils/addBuefy';
import restrictNewSubmission from '~/components/page-features/goldplus/mixins/restrictNewSubmission';

localVueWithBuefy.use(Vuex);

const stubs = {
  GoldPlusSubmissionOnBoardingContent: mockComponent,
  GoldPlusSubmissionOnBoardingFooter: mockComponent,
  GoldPlusHeader: mockComponent,
};

const $router = {
  replace: jest.fn(),
};

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        isGoldPlusActive: true,
      },
      actions: {
        getActiveStatus() {
          return jest.fn();
        },
      },
      mutations: {
        setIsGPSubmissionStatusFetched: jest.fn(),
      },
    },
    profile: {
      namespaced: true,
      actions: {
        getProfile() {
          return jest.fn();
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mixins: [restrictNewSubmission],
  mocks: {
    $router,
    $store: { dispatch: jest.fn().mockResolvedValue({}) },
  },
  stubs,
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(desktop, finalData);
};

describe('desktop.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-submission-stepper').exists()).toBeTruthy();
  });

  it('Should redirect user back to onboarding page when back button is clicked', () => {
    wrapper.vm.goToPreviousState();
    expect(wrapper.vm.$router.replace).toBeCalledWith('/goldplus/submission/onboarding');
  });

  it('Should handle on clicked back button', () => {
    wrapper.vm.handleBackClicked();

    expect(wrapper.vm.$router.replace).toBeCalled();
  });
});
