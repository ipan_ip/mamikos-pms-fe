import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import GoldPlusSubmissionStepperContent from '.';
import MkButton from '~/components/global/atoms/MkButton';
import localVueWithBuefy from '~/utils/addBuefy';
import mockWindowProperty from '~/utils/mockWindowProperty';

mockWindowProperty('window.location', {
  href: '',
});

const stubs = {
  MkButton,
};

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      getters: {
        membership: () => {
          return {
            is_mamipay_user: false,
          };
        },
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
    },
  },
};

localVueWithBuefy.use(Vuex);

const $router = {
  push: jest.fn(),
};

const $device = {
  isMobile: false,
  isDesktop: true,
};

const $tracker = { send: jest.fn() };

const mount = (adtMountData = {}) => {
  const mountData = {
    localVue: localVueWithBuefy,
    stubs,
    mocks: {
      $router,
      $device,
      $tracker,
    },
    store: new Vuex.Store(storeData),
  };

  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusSubmissionStepperContent, finalData);
};

describe('GoldPlusSubmissionOnBoardingContent.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('Should mount properly', () => {
    expect(wrapper.find('.goldplus-submission-stepper-content').exists()).toBeTruthy();
  });

  it('Should handle next step actions propertly', () => {
    const testCases = ['register_bbk', 'purchase_gp'];

    testCases.forEach((tc) => {
      wrapper.vm.startStep(tc);

      if (tc === 'register_bbk') {
        expect(wrapper.vm.$router.push).toBeCalledWith({
          name: 'kos-booking-register',
          query: { gp_submission: true },
        });
      } else {
        expect(wrapper.vm.$router.push).toBeCalledWith({ name: 'goldplus-packages' });
      }
    });
  });
});
