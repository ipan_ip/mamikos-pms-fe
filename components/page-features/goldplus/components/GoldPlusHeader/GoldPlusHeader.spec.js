import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusHeader from '../GoldPlusHeader';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: true,
  isDesktop: false,
};

localVueWithBuefy.use(Vuex);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $router: {
      name: 'goldplus-statistic',
      back: jest.fn(),
      replace: jest.fn(),
    },
    $device,
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusHeader, finalData);
};

describe('GoldPlusHeader.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-header').exists()).toBeTruthy();
  });

  it('should back to previous page when back button clicked', () => {
    wrapper.vm.handleBackClicked();

    expect(wrapper.vm.$router.back).toBeCalled();
  });
});
