import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusFeatureCard from '../GoldPlusFeatureCard';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: true,
  isDesktop: false,
};

const mockComponent = {
  template: `<div />`,
};

const stubs = {
  MkCard: mockComponent,
  MkCardBody: mockComponent,
  MkCardTitle: mockComponent,
  MkCardHeader: mockComponent,
};

const mockIcon = 'ic_mamicoin';

const mockTitle = 'mocked title';

const mockDescription = 'mocked description';

const mockFeatureName = 'list';

const storeData = {
  modules: {
    goldplus: {
      namespaced: true,
      state: {
        billingSummary: {
          unpaid_invoice: 0,
        },
      },

      actions: {
        fetchWidgetBillingSummary() {
          return jest.fn().mockResolvedValue({
            data: {
              status: true,
              data: {
                unpaid_invoice: 0,
              },
            },
          });
        },
        fetchWidgetData() {
          return jest.fn().mockResolvedValue({
            data: {
              status: true,
              data: {},
            },
          });
        },
      },
    },
  },
};

localVueWithBuefy.use(Vuex);

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-statistic',
    },
    $router: {
      push: jest.fn(),
      replace: jest.fn(),
    },
    $bugsnag: {
      notify: jest.fn(),
    },
    $alert: jest.fn(),
    $device,
  },
  propsData: {
    icon: mockIcon,
    title: mockTitle,
    description: mockDescription,
    featureName: mockFeatureName,
  },
  stubs,
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusFeatureCard, finalData);
};

describe('GoldPlusFeatureCard.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  jest.mock(`~/assets/images/goldplus/ic_mamicoin.svg`, () => {
    return 'ic_mamicoin';
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-feature').exists()).toBeTruthy();
  });

  it('should return menu icon properly ', () => {
    expect(wrapper.vm.menuIcon).toEqual(mockIcon);
  });

  it('should handle method and emitted event when clicking on the card in mobile view', async () => {
    wrapper.vm.$emit = jest.fn();

    await wrapper.vm.handleLinkClicked();
    expect(wrapper.vm.$emit).toBeCalled();
  });

  it('should return goldplus list feature ', () => {
    expect(wrapper.vm.isListCard).toBeTruthy();
  });

  it('should call $bugsnag.notify and alert, when calling handleFetchBillingSummary method and received error', async () => {
    wrapper.destroy();
    const mockStoreData = Object.assign(storeData);
    storeData.modules.goldplus.actions.fetchWidgetBillingSummary = jest
      .fn()
      .mockRejectedValue('error');

    wrapper = mount({ store: new Vuex.Store(mockStoreData) });
    await wrapper.vm.handleFetchBillingSummary();

    expect(wrapper.vm.$bugsnag.notify).toBeCalled();
    expect(wrapper.vm.$alert).toBeCalled();
  });

  it('should go to page payment list - unpaid invoice when card widget payment clicked', () => {
    wrapper.vm.handleWidgetPaymentClicked();

    expect(wrapper.vm.$router.push).toBeCalledWith({
      name: 'goldplus-payment',
    });
  });

  it('should call $bugsnag.notify and alert, when calling handleFetchWidgetData method and received error', async () => {
    wrapper.destroy();
    const mockStoreData = Object.assign(storeData);
    storeData.modules.goldplus.actions.fetchWidgetData = jest.fn().mockRejectedValue('error');

    wrapper = mount({ store: new Vuex.Store(mockStoreData) });
    await wrapper.vm.handleFetchWidgetData();

    expect(wrapper.vm.$bugsnag.notify).toBeCalled();
    expect(wrapper.vm.$alert).toBeCalled();
  });

  it('should return list feature ', () => {
    const mockStatisticSummary = {
      button_visible: true,
      key: 2,
      label: 'Sedang di-review',
      value: 1,
    };

    wrapper.vm.handleWidgetLabelClicked(mockStatisticSummary);
    expect(wrapper.vm.$router.push).toBeCalled();
  });
});
