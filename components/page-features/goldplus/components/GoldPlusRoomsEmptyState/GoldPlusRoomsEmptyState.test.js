import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusRoomsEmptyState from '.';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: true,
  isDesktop: false,
};

localVueWithBuefy.use(Vuex);

const mockEmptyTitle = 'mock empty state title';
const mockEmptyMessage = 'mock empty state message';

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-statistic',
    },
    $device,
  },
  propsData: {
    emptyTitle: mockEmptyTitle,
    emptyMessage: mockEmptyMessage,
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusRoomsEmptyState, finalData);
};

describe('EmptyState.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  jest.mock(`~/assets/images/goldplus/empty_state_v1.png`, () => {
    return 'empty_state_v1';
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-rooms-empty-state').exists()).toBeTruthy();
  });

  it('should return empty title properly', () => {
    expect(wrapper.vm.emptyTitle).toEqual(mockEmptyTitle);
  });

  it('should return empty message properly', () => {
    expect(wrapper.vm.emptyMessage).toEqual(mockEmptyMessage);
  });
});
