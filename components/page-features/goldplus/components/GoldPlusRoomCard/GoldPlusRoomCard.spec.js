import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusRoomCard from '.';
import mockLazy from '~/utils/mocks/lazy';
import MkCard from '~/components/global/molecules/MkCard';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';
import MkCardHeader from '~/components/global/molecules/MkCard/components/MkCardHeader';
import localVueWithBuefy from '~/utils/addBuefy';

global.window = Object.create(window);

const $device = {
  isMobile: true,
  isDesktop: false,
};

const $tracker = { send: jest.fn() };

const stubs = {
  MkCard,
  MkCardBody,
  MkCardHeader,
};

mockLazy(localVueWithBuefy);
localVueWithBuefy.use(Vuex);

global.open = jest.fn(() => true);

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      getters: {
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mockRoomData = {
  address: 'jl. mauk raya',
  area_formatted: 'Mauk, Tangerang',
  gender: 0,
  gp_status: { key: 9, value: 'Goldplus 1' },
  key: 9,
  value: 'Goldplus 1',
  id: 1000008687,
  photo: {
    large: 'https://static8.kerupux.com/uploads/cache/data/style/2020-09-03/euQvo7Cj-540x720.jpg',
    medium: 'https://static8.kerupux.com/uploads/cache/data/style/2020-09-03/euQvo7Cj-360x480.jpg',
    real: 'https://static8.kerupux.com/uploads/data/style/2020-09-03/euQvo7Cj.jpeg',
    small: 'https://static8.kerupux.com/uploads/cache/data/style/2020-09-03/euQvo7Cj-240x320.jpg',
  },
  room_title: 'Kost syariah berdikari GP1',
};

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-statistic',
    },
    $device,
    $tracker,
  },
  propsData: {
    hiddenMainAction: false,
  },
  stubs,
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusRoomCard, finalData);
};

describe('GoldPlusRoomCard.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.goldplus-room-card').exists()).toBeTruthy();
  });

  it('Should return proper gender text based on the key', () => {
    const genders = [
      {
        key: 0,
        value: 'Campur',
      },
      {
        key: 1,
        value: 'Putra',
      },
      {
        key: 2,
        value: 'Putri',
      },
      {
        key: '',
        value: '',
      },
    ];

    genders.forEach((gender) => {
      mockRoomData.gender = gender.key;

      wrapper.setProps({
        roomData: { ...mockRoomData },
      });

      expect(wrapper.vm.genderType).toEqual(gender.value);
    });
  });

  it("Should emitted 'main-action-clicked' when footer link is clicked", async () => {
    expect(wrapper.vm.hiddenMainAction).toBe(false);

    const button = wrapper.find('.goldplus-room-card__footer-link');

    button.trigger('click');

    await wrapper.vm.$nextTick();

    expect(wrapper.emitted('main-action-clicked')).toBeTruthy();
  });
});
