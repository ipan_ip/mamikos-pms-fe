import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import GoldPlusFeatures from '../GoldPlusFeatures';
import localVueWithBuefy from '~/utils/addBuefy';

global.window = Object.create(window);

const $device = {
  isMobile: true,
  isDesktop: false,
};

const $tracker = { send: jest.fn() };

const mockComponent = {
  template: `<div />`,
};

const stubs = {
  GoldPlusFeatureCard: mockComponent,
};

const $router = {
  push: jest.fn(),
};

localVueWithBuefy.use(Vuex);

global.open = jest.fn(() => true);

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      getters: {
        user: () => {
          return {
            user_id: 1,
          };
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $route: {
      name: 'goldplus-statistic',
    },
    $device,
    $router,
    $tracker,
  },
  stubs,
  store,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(GoldPlusFeatures, finalData);
};

describe('GoldPlusFeatures.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.gold-plus-features').exists()).toBeTruthy();
  });

  it('should route to statistic page', () => {
    wrapper.vm.handleLinkClicked('statistic');
    expect($router.push).toBeCalled();
  });

  it('should go to tnc page', () => {
    wrapper.vm.handleLinkClicked('tnc');

    global.window = Object.create(window);
    const url =
      'https://help.mamikos.com/syarat-dan-ketentuan/?category=snk-pemilik&subCategory=mamikos-goldplus&slug=syarat-dan-ketentuan-mamikos-goldplus';
    Object.defineProperty(window, 'location', {
      value: {
        href: url,
      },
    });
    expect(window.location.href).toEqual(url);
  });

  it('should go to page payment list - unpaid invoice when cta lihat selengkapnya clicked', () => {
    wrapper.vm.handleLinkClicked('payment');

    expect(wrapper.vm.$router.push).toBeCalledWith({
      path: '/goldplus/payment',
    });
  });

  it('should route to gp-list page', () => {
    wrapper.vm.handleLinkClicked('list');
    expect($router.push).toBeCalled();
  });

  it('should call $tracker.send on Mobile', () => {
    wrapper.vm.trackStatisticPageVisited();
    wrapper.vm.trackTnCClicked();
    expect(wrapper.vm.$tracker.send).toBeCalled();
  });

  it('should call $tracker.send on Desktop', () => {
    wrapper.vm.$device.isMobile = false;
    wrapper.vm.$device.isDesktop = true;

    wrapper.vm.trackStatisticPageVisited();
    wrapper.vm.trackTnCClicked();

    expect(wrapper.vm.$tracker.send).toBeCalled();
  });
});
