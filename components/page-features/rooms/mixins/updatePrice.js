import { validationMixin } from 'vuelidate';
import RoomAdditionalPrice from '../../rooms-price/components/RoomAdditionalPrice';
import RoomPriceCollapseWrapper from '../../rooms-price/components/RoomPriceCollapseWrapper';
import roomPrices from '../components/PropertyRoomUpdateCard/data/roomPrices';
import MkButton from '~/components/global/atoms/MkButton';
import MkCurrencyInput from '~/components/global/molecules/MkCurrencyInput';

export default {
  components: {
    MkButton,
    MkCurrencyInput,
    RoomAdditionalPrice,
    RoomPriceCollapseWrapper,
  },
  mixins: [validationMixin],
  validations() {
    const roomPrices = {};
    Object.keys(this.roomPrices).forEach((key) => {
      if (this.roomPrices[key].selected) {
        roomPrices[key] = this.roomPrices[key].rules;
      } else {
        roomPrices[key] = { value: {} };
      }
    });
    return {
      roomPrices,
    };
  },
  data() {
    return {
      roomPrices,
      isUpdating: false,
      selectedKos: null,
      isOpen: false,
      isLoadingRoom: true,
    };
  },
  computed: {
    firstPriceKey() {
      return Object.keys(this.roomPrices)[0];
    },
    isFlashSale() {
      return (
        this.selectedKos.is_price_flash_sale &&
        Object.keys(this.selectedKos.is_price_flash_sale).some(
          (key) => this.selectedKos.is_price_flash_sale[key],
        )
      );
    },
  },
  watch: {
    // when selectedKos is updated using new data from api response, update roomPrices data
    selectedKos: {
      deep: true,
      handler(selectedKos) {
        if (selectedKos)
          Object.keys(roomPrices).forEach((key) => {
            roomPrices[key].value = selectedKos[key];
            if (!roomPrices[key].locked) roomPrices[key].selected = selectedKos[key] > 0;
          });
        return roomPrices;
      },
    },
  },
  created() {
    this.getPropertyPrice();
    this.$changes.init();
  },
  methods: {
    handlePriceInput() {
      this.$changes.update(true);
    },
    async handleUpdatePriceClicked() {
      this.isUpdating = true;
      const {
        price_monthly,
        price_yearly,
        price_weekly,
        price_daily,
        price_3_month,
        price_6_month,
      } = this.roomPrices;

      const res = await this.$api.updatePropertyRoomPrice({
        room_id: this.selectedKos._id,
        // we don't update room available here
        room_available: this.selectedKos.room_available,
        price_monthly: price_monthly.value,
        price_yearly: price_yearly.selected ? price_yearly.value : 0,
        price_weekly: price_weekly.selected ? price_weekly.value : 0,
        price_daily: price_daily.selected ? price_daily.value : 0,
        '3_month': price_3_month.selected ? price_3_month.value : 0,
        '6_month': price_6_month.selected ? price_6_month.value : 0,
      });

      if (res.data.status) {
        await this.$store.dispatch('profile/getLatestProfile');
        await this.getPropertyPrice();
        this.$changes.update(false);

        this.showToast('Harga berhasil diupdate');
      } else {
        Array.isArray(res.data.messages) && this.showToast(res.data.messages.join(', '));
      }
      this.isUpdating = false;
    },
    getPropertyPrice() {
      this.isLoadingRoom = true;
      // get two kos because of buggy API (legacy API)
      const params = [{ room_id: this.$route.params.kos_id, limit: 2, offset: 0 }];
      if (this.$device.isDesktop) {
        const CancelToken = this.$axios.CancelToken;
        this.sourceHttp = CancelToken.source();
        params.push(this.sourceHttp.token);
      }
      return this.$api.getActiveProperty(...params).then((res) => {
        this.selectedKos = res.data.rooms[0];
        this.isLoadingRoom = false; // for pages update kamar
      });
    },
    showToast(message) {
      const toastOptions = {
        message,
        duration: 2000,
        position: 'is-bottom',
      };
      this.$device.isMobile && (toastOptions.container = '#propertyPrice');
      this.$buefy.toast.open(toastOptions);
    },
    handleRoomPriceCheckboxClicked(flashSaleKey) {
      if (this.selectedKos.is_price_flash_sale[flashSaleKey]) {
        this.showToast('Promo ngebut aktif, tidak bisa update');
        return;
      }

      if (flashSaleKey === 'monthly') {
        this.showToast('Harga Per Bulan wajib diisi');
      }
    },
  },
  beforeRouteLeave(to, from, next) {
    // for removing toast if change route
    const toastBottom = document.querySelector('.notices.is-bottom');
    if (toastBottom && toastBottom.hasChildNodes()) {
      toastBottom.remove();
    }
    next();
  },
};
