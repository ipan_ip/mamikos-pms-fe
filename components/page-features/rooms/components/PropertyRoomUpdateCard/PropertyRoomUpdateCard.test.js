import { mount } from '@vue/test-utils';
import VueRouter from 'vue-router';
import PropertyRoomUpdateCard from './PropertyRoomUpdateCard';
import localVueWithBuefy from '~/utils/addBuefy';
import mockLazy from '~/utils/mocks/lazy';
import MkButton from '~/components/global/atoms/MkButton';
import MkCurrencyInput from '~/components/global/molecules/MkCurrencyInput';

mockLazy(localVueWithBuefy);

localVueWithBuefy.filter('currencyIDR', jest.fn());

const $device = { isDesktop: true };

const $api = {
  getActiveProperty: jest.fn().mockResolvedValue({
    data: {
      rooms: [
        {
          _id: 1,
          room_title: 'room',
          photo_url: {
            small: 'small_url',
          },
          price_3_month: 0,
          price_6_month: 0,
          price_daily: 100000,
          price_monthly: 1000000,
          price_weekly: 12312323,
          price_yearly: 0,
          is_price_flash_sale: {
            daily: false,
            weekly: false,
            monthly: false,
            annually: true,
            quarterly: false,
            semiannually: false,
          },
        },
      ],
    },
  }),
  updatePropertyRoomPrice: jest.fn().mockResolvedValue({ data: { status: true } }),
};
const router = new VueRouter();
localVueWithBuefy.use(VueRouter);

describe('PropertyRoomUpdateCard.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(PropertyRoomUpdateCard, {
      localVue: localVueWithBuefy,
      router,
      mocks: {
        $store: {
          dispatch: jest.fn(),
        },
        $axios: {
          CancelToken: { source: jest.fn(() => ({ token: 'token', cancel: jest.fn() })) },
        },
        $buefy: { toast: { open: jest.fn() } },
        $changes: { update: jest.fn(), init: jest.fn() },
        $api,
        $device,
      },
      stubs: {
        MkButton,
        MkCurrencyInput,
        'nuxt-link': { template: '<div></div>' },
      },
    });
  });

  it('should mount properly', () => {
    expect(wrapper.find('.property-room-update-card').exists()).toBeTruthy();
    expect(wrapper.vm.$changes.init).toBeCalled();
  });

  it('should change input value', () => {
    wrapper.find('input.input').trigger('input');
    expect(wrapper.vm.$changes.update).toBeCalled();
    expect(wrapper.vm.$changes.update).toBeCalledWith(true);
  });

  it('should update price when button is clicked', async () => {
    const wait = async () => {
      await $api.updatePropertyRoomPrice;
      await Promise.resolve;
      await wrapper.vm.$store.dispatch;
      await wrapper.vm.getPropertyPrice;
      await $api.getPropertyPrice;
      await Promise.resolve;
      await Promise.prototype.then;
    };
    wrapper.vm.handleUpdatePriceClicked();
    expect(wrapper.vm.isUpdating).toBe(true);
    await wait();
    expect(wrapper.vm.$changes.update).toBeCalled();
    expect(wrapper.vm.$buefy.toast.open).toBeCalled();

    // failed response
    $api.updatePropertyRoomPrice = jest.fn().mockResolvedValue({ data: { status: false } });
    wrapper.vm.handleUpdatePriceClicked();
    expect(wrapper.vm.isUpdating).toBe(true);
    await wait();
    expect(wrapper.vm.$changes.update).toBeCalled();
    expect(wrapper.vm.$buefy.toast.open).toBeCalled();
  });

  it('should get property when route changes', async () => {
    wrapper.vm.$router.push('path');
    expect(wrapper.vm.$axios.CancelToken.source).toBeCalled();
    await wrapper.vm.sourceHttp.cancel;
    expect(wrapper.vm.sourceHttp.cancel).toBeCalled();
  });

  it('should show toast when disabled checkbox is clicked', async () => {
    wrapper.vm.showToast = jest.fn();

    const annuallyCheckbox = wrapper.findAll('.b-checkbox').at(5);
    annuallyCheckbox.trigger('click');
    await wrapper.vm.$nextTick;
    expect(wrapper.vm.roomPrices.price_yearly.selected).toBeFalsy();
    expect(annuallyCheckbox.find('input').element.disabled).toBeTruthy();
    expect(wrapper.vm.showToast).toBeCalledWith('Promo ngebut aktif, tidak bisa update');

    const monthlyCheckbox = wrapper.findAll('.b-checkbox').at(0);
    monthlyCheckbox.trigger('click');
    await wrapper.vm.$nextTick;
    expect(wrapper.vm.roomPrices.price_monthly.selected).toBeTruthy();
    expect(monthlyCheckbox.find('input').element.disabled).toBeTruthy();
    expect(wrapper.vm.showToast).toBeCalledWith('Harga Per Bulan wajib diisi');
  });
});
