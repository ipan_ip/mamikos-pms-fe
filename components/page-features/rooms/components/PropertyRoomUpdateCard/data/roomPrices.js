import { between } from 'vuelidate/lib/validators';
export default {
  price_monthly: {
    label: 'Harga Per Bulan',
    selected: true,
    value: 0,
    locked: true,
    errorMessage: 'Harga per bulan min. Rp50.000 dan maks. Rp100.000.000.',
    rules: {
      value: { between: between(50000, 100000000) },
    },
    flashSaleKey: 'monthly',
  },
  price_daily: {
    label: 'Harga Per Hari',
    selected: false,
    value: 0,
    errorMessage: 'Harga per hari min. Rp10.000 dan maks. Rp10.000.000.',
    rules: {
      value: { between: between(10000, 10000000) },
    },
    flashSaleKey: 'daily',
  },
  price_weekly: {
    label: 'Harga Per Minggu',
    selected: false,
    value: 0,
    errorMessage: 'Harga per minggu min. Rp50.000 dan maks. Rp50.000.000.',
    rules: {
      value: { between: between(50000, 50000000) },
    },
    flashSaleKey: 'weekly',
  },
  price_3_month: {
    label: 'Harga Per 3 Bulan',
    selected: false,
    value: 0,
    description: 'Sebagai harga hitungan sewa 3 bulanan dan dibayarkan per-3 bulan sekali',
    errorMessage: 'Harga per 3 bulan min. Rp100.000 dan maks. Rp100.000.000.',
    rules: {
      value: { between: between(100000, 100000000) },
    },
    flashSaleKey: 'quarterly',
  },
  price_6_month: {
    label: 'Harga Per 6 Bulan',
    selected: false,
    value: 0,
    description: 'Sebagai harga hitungan sewa 6 bulanan dan dibayarkan per-6 bulan sekali',
    errorMessage: 'Harga per 6 bulan min. Rp100.000 dan maks. Rp100.000.000.',
    rules: {
      value: { between: between(100000, 100000000) },
    },
    flashSaleKey: 'semiannually',
  },
  price_yearly: {
    label: 'Harga Per Tahun',
    selected: false,
    value: 0,
    description: 'Sebagai harga hitungan sewa tahunan dan dibayarkan setahun sekali',
    errorMessage: 'Harga per tahun min. Rp100.000 dan maks. Rp100.000.000.',
    rules: {
      value: { between: between(100000, 100000000) },
    },
    flashSaleKey: 'annually',
  },
};
