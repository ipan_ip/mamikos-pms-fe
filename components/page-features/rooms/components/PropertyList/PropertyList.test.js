import { shallowMount } from '@vue/test-utils';
import PropertyList from '../PropertyList';
import localVueWithBuefy from '~/utils/addBuefy';

describe('PropertyList.vue', () => {
  const wrapper = shallowMount(PropertyList, {
    localVue: localVueWithBuefy,
    propsData: {
      properties: [
        {
          _id: 1,
          room_title: 'room',
          photo_url: {
            small: 'small_url',
          },
        },
      ],
      loading: true,
    },
    mocks: {
      $route: {
        params: {
          kos_id: '12',
        },
        query: {},
      },
    },
    stubs: {
      'nuxt-link': { template: '<div></div>' },
    },
  });

  it('should mount properly', () => {
    expect(wrapper.find('.property-list').exists()).toBeTruthy();
  });

  it('should call scroll event listener', () => {
    wrapper.find({ ref: 'propertyList' }).trigger('scroll');
    expect(wrapper.emitted().updateData).toBeTruthy();
  });

  it('should remove scroll event listener', () => {
    const propertyList = wrapper.find('.property-list');
    propertyList.element.removeEventListener = jest.fn();
    wrapper.destroy();
    expect(propertyList.element.removeEventListener).toBeCalled();
  });
});
