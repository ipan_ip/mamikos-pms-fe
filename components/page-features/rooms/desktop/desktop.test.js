import { shallowMount } from '@vue/test-utils';
import desktop from '../desktop';
import localVueWithBuefy from '~/utils/addBuefy';
const $api = {
  getActiveProperty: jest.fn().mockResolvedValue({
    data: {
      rooms: [
        {
          _id: 1,
          room_title: 'room',
          photo_url: {
            small: 'small_url',
          },
        },
      ],
    },
  }),
};

const $route = {
  query: {},
  name: 'kos-rooms',
};

const PropertyList = { template: '<div></div>' };

describe('desktop.vue', () => {
  let wrapper;
  const mount = () => {
    wrapper = shallowMount(desktop, {
      localVue: localVueWithBuefy,
      mocks: {
        $route,
        $api,
        $router: {
          replace: jest.fn(),
          push: jest.fn(),
        },
      },
      stubs: {
        'nuxt-child': { template: '<div></div>' },
        PropertyList,
      },
    });
  };

  it('should mount properly', async () => {
    mount();
    await wrapper.vm.getData;
    await $api.getActiveProperty;
    await Promise.resolve;
    await Promise.prototype.then;

    expect(wrapper.find('.room-page').exists()).toBeTruthy();
    expect(wrapper.vm.$router.replace).toBeCalled();
  });

  it('should back to dashboard when back button is clicked', () => {
    mount();
    wrapper.find('.room-page__back-icon').trigger('click');
  });

  it('should catch the error response', async () => {
    $api.getActiveProperty = jest.fn().mockRejectedValue({});
    mount();

    await wrapper.vm.getData;
    await $api.getActiveProperty;
    await Promise.resolve;
    await Promise.prototype.then;

    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should handle query params single', () => {
    $route.query.single = true;
    mount();
    expect(wrapper.find('.room-page__back-icon').element).toBeTruthy();
    expect(wrapper.find(PropertyList).element).toBeFalsy();
  });
});
