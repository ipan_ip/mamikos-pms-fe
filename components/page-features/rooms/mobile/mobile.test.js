import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mobile from '../mobile';
import mockLazy from '~/utils/mocks/lazy';
import localVueWithBuefy from '~/utils/addBuefy';

mockLazy(localVueWithBuefy);
localVueWithBuefy.use(Vuex);

const $api = {
  getActiveProperty: jest.fn().mockResolvedValue({
    data: {
      rooms: [],
    },
  }),
};
const $store = { commit: jest.fn() };
const $route = { name: 'kos-rooms' };
const PropertyList = { template: '<div></div>' };

const mockStore = {
  modules: {
    mobile: {
      namespaced: true,
      state: {
        hideTopNavbar: false,
        topNavbarTitle: 'Update Kamar',
        topNavbarAttributes: { titleAlign: 'center' },
      },
      mutations: {
        setHideTopNavbar(state, isHide) {
          state.hideTopNavbar = isHide;
        },
        setTopNavbarTitle(state, title) {
          state.topNavbarTitle = title;
        },
        setTopNavbarAttributes(state, attributes) {
          state.topNavbarAttributes = attributes;
        },
      },
    },
  },
};

describe('mobile.vue', () => {
  let wrapper;
  const mount = () => {
    wrapper = shallowMount(mobile, {
      localVue: localVueWithBuefy,
      mocks: {
        $api,
        $store,
        $route,
      },
      stubs: {
        'nuxt-link': { template: '<div></div>' },
        PropertyList,
      },
      store: new Vuex.Store(mockStore),
    });
  };

  it('should mount properly', async () => {
    mount();
    await wrapper.vm.getData;
    await $api.getActiveProperty;
    await Promise.resolve;
    await Promise.prototype.then;

    expect(wrapper.find('.property-list').exists()).toBeTruthy();
  });

  it('should handle proper api response', async () => {
    $api.getActiveProperty = jest.fn().mockResolvedValue({
      data: {
        rooms: [
          {
            _id: 1,
            room_title: 'room',
            photo_url: {
              small: 'small_url',
            },
          },
        ],
      },
    });
    mount();
    await wrapper.vm.getData;
    await $api.getActiveProperty;
    await Promise.resolve;
    await Promise.prototype.then;

    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should catch the error response', async () => {
    $api.getActiveProperty = jest.fn().mockRejectedValue({});
    mount();

    await wrapper.vm.getData;
    await $api.getActiveProperty;
    await Promise.resolve;
    await Promise.prototype.then;

    expect(wrapper.vm.isLoading).toBeFalsy();
  });

  it('should listen window scroll to get data', () => {
    wrapper.vm.getData = jest.fn();
    window.mobileKosRoomsScrolled();
    expect(wrapper.vm.getData).toBeCalled();
  });

  it('should remove event listener', () => {
    window.removeEventListener = jest.fn();
    wrapper.destroy();
    expect(window.removeEventListener).toBeCalled();
    expect(window.removeEventListener).toBeCalledWith('scroll', window.mobileKosRoomsScrolled);
  });
});
