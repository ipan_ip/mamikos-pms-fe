import Vuex from 'vuex';
import { mount } from '@vue/test-utils';
import BookingSuggestionModal from './BookingSuggestionModal';
import localVueWithBuefy from '~/utils/addBuefy';

localVueWithBuefy.use(Vuex);
const store = new Vuex.Store({
  modules: {
    profile: {
      namespaced: true,
      state: { user: { user_id: 10 } },
      getters: {
        user(state) {
          return state.user;
        },
      },
    },
  },
});

const $api = {
  getOXAbTest: jest.fn().mockResolvedValue({ data: { status: true } }),
};

jest.mock('~/utils/oxAbTest', () => ({
  forceBbk: jest.fn(({ onDefaultReceived, onVariantAReceived }) => {
    // don't need to test abtest here
    onVariantAReceived();
    onDefaultReceived();
  }),
}));

describe('BookingSuggestionModal.vue', () => {
  const wrapper = mount(BookingSuggestionModal, {
    localVue: localVueWithBuefy,
    propsData: {
      active: true,
    },
    store,
    mocks: {
      $api,
      $buefy: { toast: { open: jest.fn() } },
    },
    stubs: {
      RouterLink: { template: '<a></a>' },
    },
  });

  it('should mount properly', async () => {
    await wrapper.vm.$api.getOXAbTest;
    expect(wrapper.find('.suggestion-modal__image').exists()).toBeTruthy();
    expect(wrapper.vm.notActiveKos).toBe(0);
  });

  it('should open alert when clicking backdrop', () => {
    // manually change closable
    wrapper.vm.closable = false;
    wrapper.find('.c-mk-modal__background').trigger('click');
    expect(wrapper.vm.$buefy.toast.open).toBeCalled();
  });
});
