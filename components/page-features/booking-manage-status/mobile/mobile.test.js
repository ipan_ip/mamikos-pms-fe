import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import localVueWithBuefy from '~/utils/addBuefy';
import mockLazy from '~/utils/mocks/lazy';

const mobile = require('./mobile').default;
mockLazy(localVueWithBuefy);

const responseGetMamipayRooms = {
  data: {
    status: true,
    data: [
      {
        last_update: 'yesterday',
        photo: {
          medium: 'photo-medium',
          gender: 1,
          room_title: 'title bos',
          total_waiting: 19,
          room_available: 20,
        },
      },
    ],
    has_more: true,
  },
};

const $api = {
  getBbkStatus: jest.fn().mockResolvedValue({
    data: { status: true, bbk_status: { approve: 1, waiting: 2, reject: 1, other: 1 } },
  }),
  getMamipayRooms: jest.fn().mockResolvedValue(responseGetMamipayRooms),
};

localVueWithBuefy.use(Vuex);
const store = new Vuex.Store({
  modules: {
    profile: {
      namespaced: true,
      state: { membership: { is_mamipay_user: true } },
      getters: {
        membership(state) {
          return state.membership;
        },
      },
    },
  },
});
let wrapper;
const mount = () => {
  wrapper = shallowMount(mobile, {
    localVue: localVueWithBuefy,
    mocks: {
      $api,
      $mamikosUrl: 'https://mamikos.com',
    },
    store,
    stubs: {
      BButton: { template: '<button @click="$emit(`click`)"></button>' },
    },
  });
};

describe('mobile.vue', () => {
  it('should mount properly', async () => {
    mount();
    expect(wrapper.vm.isLoading).toBe(true);

    await wrapper.vm.$api.getBbkStatus;
    await Promise.prototype.then;

    expect(sessionStorage.getItem('isSuggestionModalShown')).toBe('true');
    expect(wrapper.vm.isLoading).toBe(false);
  });

  it('should handle choices event properly', async () => {
    const choicesButtons = wrapper.findAll('.manage-booking-status__choice');

    choicesButtons.at(0).trigger('click');
    expect(wrapper.vm.isAllSelected).toBe(true);
    expect(wrapper.vm.selectedChoice).toBe('not_active');

    await wrapper.vm.$api.getMamipayRooms;
    await Promise.prototype.then;

    choicesButtons.at(1).trigger('click');
    expect(wrapper.vm.isAllSelected).toBe(false);
    expect(wrapper.vm.selectedChoice).toBe('approve');
  });

  it('should handle scroll event to get another data', async () => {
    const choicesButtons = wrapper.findAll('.manage-booking-status__choice');

    wrapper.vm.$api.getMamipayRooms = jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: [],
        has_more: true,
      },
    });

    choicesButtons.at(0).trigger('click');
    expect(wrapper.vm.isAllSelected).toBe(true);
    expect(wrapper.vm.selectedChoice).toBe('not_active');

    await wrapper.vm.$api.getMamipayRooms;
    await Promise.prototype.then;

    choicesButtons.at(1).trigger('click');
    expect(wrapper.vm.isAllSelected).toBe(false);
    expect(wrapper.vm.selectedChoice).toBe('approve');

    await wrapper.vm.$api.getMamipayRooms;
    await Promise.prototype.then;
    choicesButtons.at(2).trigger('click');
    expect(wrapper.vm.isAllSelected).toBe(false);
    expect(wrapper.vm.selectedChoice).toBe('waiting');

    await wrapper.vm.$api.getMamipayRooms;
    await Promise.prototype.then;
    choicesButtons.at(3).trigger('click');
    expect(wrapper.vm.isAllSelected).toBe(false);
    expect(wrapper.vm.selectedChoice).toBe('not_active');

    wrapper.vm.$api.getMamipayRooms = jest.fn().mockResolvedValue(responseGetMamipayRooms);
  });

  it('should go to create kos page', async () => {
    const choicesButtons = wrapper.findAll('.manage-booking-status__choice');

    wrapper.vm.$api.getMamipayRooms = jest.fn().mockResolvedValue({
      data: {
        status: true,
        data: [],
        has_more: true,
      },
    });

    choicesButtons.at(0).trigger('click');
    await wrapper.vm.$api.getMamipayRooms;
    await Promise.prototype.then;

    const buttonGoToCreateKosPage = wrapper.find('.empty-state-booking-status__empty-button');

    expect(buttonGoToCreateKosPage.exists()).toBeTruthy();
    wrapper.vm.$api.getMamipayRooms = jest.fn().mockResolvedValue(responseGetMamipayRooms);
  });

  it('should handle scroll event to get another data', async () => {
    mount();
    wrapper.vm.$api.getMamipayRooms = jest
      .fn()
      .mockResolvedValue({ data: { ...responseGetMamipayRooms.data, has_more: false } });
    // dispatch scroll manually
    window.dispatchEvent(new CustomEvent('scroll'));
    expect(wrapper.vm.isLoading).toBe(true);

    await wrapper.vm.$api.getMamipayRooms;
    await Promise.prototype.then;

    expect(wrapper.vm.selectedChoice).toBe('waiting');
  });

  it('should remove event listener before destroy', () => {
    document.removeEventListener = jest.fn();
    mount();
    wrapper.destroy();
    expect(document.removeEventListener).toBeCalledTimes(1);
    document.removeEventListener.mockRestore();
  });
});
