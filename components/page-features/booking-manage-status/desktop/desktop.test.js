import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import localVueWithBuefy from '~/utils/addBuefy';
import mockLazy from '~/utils/mocks/lazy';

document.querySelector = jest.fn().mockReturnValue({ scrollTop: 0 });
const desktop = require('./desktop').default;
mockLazy(localVueWithBuefy);

const responseGetMamipayRooms = {
  data: {
    status: true,
    data: [
      {
        last_update: 'yesterday',
        photo: {
          medium: 'photo-medium',
          gender: 1,
          room_title: 'title bos',
          total_waiting: 19,
          room_available: 20,
        },
      },
    ],
    has_more: true,
  },
};

const $api = {
  getBbkStatus: jest.fn().mockResolvedValue({
    data: { status: true, bbk_status: { approve: 1, waiting: 2, reject: 1, other: 1 } },
  }),
  getMamipayRooms: jest.fn().mockResolvedValue(responseGetMamipayRooms),
};

// mock query selector
document.querySelector = jest.fn().mockReturnValue(document.body);

localVueWithBuefy.use(Vuex);
const store = new Vuex.Store({
  modules: {
    profile: {
      namespaced: true,
      state: { membership: { is_mamipay_user: true } },
      getters: {
        membership(state) {
          return state.membership;
        },
      },
    },
  },
});
let wrapper;
const mount = () => {
  wrapper = shallowMount(desktop, {
    localVue: localVueWithBuefy,
    mocks: {
      $api,
      $mamikosUrl: 'https://mamikos.com',
    },
    store,
  });
};

describe('desktop.vue', () => {
  it('should mount properly', async () => {
    mount();
    expect(wrapper.vm.isLoading).toBe(true);

    await wrapper.vm.$api.getBbkStatus;
    await Promise.prototype.then;

    expect(sessionStorage.getItem('isSuggestionModalShown')).toBe('true');
    expect(wrapper.vm.isLoading).toBe(false);
  });

  it('should handle choices event properly', async () => {
    const choicesButtons = wrapper.findAll('.manage-booking-status__choice');

    choicesButtons.at(0).trigger('click');
    expect(wrapper.vm.isAllSelected).toBe(true);
    expect(wrapper.vm.selectedChoice).toBe('not_active');

    await wrapper.vm.$api.getMamipayRooms;
    await Promise.prototype.then;

    choicesButtons.at(1).trigger('click');
    expect(wrapper.vm.isAllSelected).toBe(false);
    expect(wrapper.vm.selectedChoice).toBe('approve');
  });

  it('should handle scroll event to get another data', async () => {
    mount();
    wrapper.vm.$api.getMamipayRooms = jest
      .fn()
      .mockResolvedValue({ data: { ...responseGetMamipayRooms.data, has_more: false } });
    // dispatch scroll manually
    document.body.dispatchEvent(new CustomEvent('scroll'));
    expect(wrapper.vm.isLoading).toBe(true);

    await wrapper.vm.$api.getMamipayRooms;
    await Promise.prototype.then;

    expect(wrapper.vm.selectedChoice).toBe('waiting');
  });

  it('should remove event listener before destroy', () => {
    document.querySelector.mockClear();
    mount();
    wrapper.destroy();
    expect(document.querySelector).toBeCalledTimes(2);
  });
});
