import { mapGetters } from 'vuex';

export default {
  data() {
    return {
      isNoPropertyModalActive: false,
      isConfirmModalActive: false,
    };
  },
  computed: {
    ...mapGetters('profile', [
      'isPremium',
      'activeKos',
      'activeApartment',
      'membership',
      'user',
      'isBbk',
    ]),
    hasActiveListing() {
      return this.activeKos > 0 || this.activeApartment > 0;
    },
    hangingTrx() {
      return this.$store.state.profile.ownerInvoice.is_hanging_purchase;
    },
  },
  methods: {
    handlePremiumEntryAction(entryPoint) {
      const MAMIKOS_URL = process.env.MAMIKOS_URL;

      if (entryPoint === 'sidebar' || entryPoint === 'marketingCard') {
        this.trackPremiumEntryPointClick();
      }

      switch (this.membership.status) {
        case 'Coba Trial':
          if (this.hasActiveListing) {
            window.location.href = `${MAMIKOS_URL}/promosi-kost/premium-package`;
          } else {
            this.toggleAddPropertyModal();
          }
          break;
        case 'Upgrade ke Premium':
          if (this.hasActiveListing) {
            window.location.href = `${MAMIKOS_URL}/promosi-kost/premium-package`;
          } else {
            this.toggleAddPropertyModal();
          }
          break;
        case 'Premium':
          if (this.hasActiveListing) {
            this.$router.push('/premium-dashboard');
          } else {
            this.toggleAddPropertyModal();
          }
          break;
        case 'Konfirmasi Pembayaran':
          if (this.hangingTrx) {
            this.togglePaymentConfirmationModal();
          } else {
            this.$router.push('/premium-dashboard');
          }
      }
    },
    toggleAddPropertyModal() {
      this.isNoPropertyModalActive = !this.isNoPropertyModalActive;
    },
    togglePaymentConfirmationModal() {
      this.isConfirmModalActive = !this.isConfirmModalActive;
    },
    trackPremiumEntryPointClick() {
      const redirectionSource = this.$device.isDesktop ? 'sidebar' : 'kelola navbar';

      this.$tracker.send('moe', [
        '[Owner] OD Pemasaran Kos - Premium Clicked',
        {
          owner_id: this.user.user_id,
          owner_name: this.user.name,
          owner_phone_number: this.user.phone_number,
          owner_email: this.user.email || '',
          is_premium: this.isPremium,
          saldo_amount: this.membership.views,
          is_promoted: null,
          is_mamirooms: !!this.user.is_mamirooms || false,
          redirection_source: redirectionSource,
          is_booking: this.isBbk,
          interface: this.$device.isMobile ? 'mobile' : 'desktop',
        },
      ]);
    },
  },
};
