import { createLocalVue, shallowMount } from '@vue/test-utils';
import AllocationCard from './AllocationCard';
import MkCard from '~/components/global/molecules/MkCard';

const localVue = createLocalVue();
const stubs = {
  MkCard,
};

Object.defineProperty(window, 'location', {
  value: { href: 'mamikos.com' },
});

describe('AllocationCard.vue', () => {
  const wrapper = shallowMount(AllocationCard, { localVue, stubs });

  it('should mount properly', () => {
    expect(wrapper.find('.allocation-card').exists()).toBeTruthy();
  });

  it('should redirect to iklan saya page', () => {
    process.env.MAMIKOS_URL = 'mamikos.com';
    wrapper.vm.goToOwnerKosList();
    expect(window.location.href).toBe('mamikos.com/ownerpage/kos');
  });
});
