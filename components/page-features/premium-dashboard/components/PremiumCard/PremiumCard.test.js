import Vuex from 'vuex';
import dayjs from 'dayjs';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import PremiumCard from './PremiumCard';
import MkCard from '~/components/global/molecules/MkCard';
import MkCardFooter from '~/components/global/molecules/MkCard/components/MkCardFooter';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';
import MkCardTitle from '~/components/global/molecules/MkCard/components/MkCardTitle';
import MkCardHeader from '~/components/global/molecules/MkCard/components/MkCardHeader';
import MkIcon from '~/components/global/atoms/MkIcon';
import DailyAllocationPopper from '~/components/page-features/dashboard/components/DailyAllocationPopper';
import ModalDailyBalanceAllocation from '~/components/page-features/dashboard/components/ModalDailyBalanceAllocation/ModalDailyBalanceAllocation';
import { toIDR } from '~/utils/currencyString';

process.env.MAMIKOS_URL = 'mamikos.com';
const localVue = createLocalVue();
localVue.use(Vuex);
localVue.filter('currencyIDR', toIDR);

Object.defineProperty(window, 'location', {
  value: { href: 'mamikos.com' },
});

const stubs = {
  MkCard,
  MkCardFooter,
  MkCardBody,
  MkCardTitle,
  MkCardHeader,
  MkIcon,
  DailyAllocationPopper,
  ModalDailyBalanceAllocation,
};

const invoice = {
  invoice: {
    status: 'Konfirmasi Pembayaran',
    invoice_url: 'mamikos.com/invoice',
    detail_package: {
      name: 'Paket 1 Hari',
      type: 'package',
    },
  },
};

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        ownerInvoice: invoice,
      },
      getters: {
        user: () => {
          return { user_id: 10 };
        },
        membership: () => {
          return {
            active_premium_package_name: '11 Maret 2021',
            active: '10 Maret 2021',
            views: 10000,
          };
        },
        isEligibleToDailyAllocation: () => {
          return false;
        },
        isOnDailyAllocation: () => {
          return false;
        },
      },
      actions: {
        updateDailyBalanceAllocation: jest.fn(),
      },
    },
  },
};
const store = new Vuex.Store(storeData);
const $bugsnag = {
  notify: jest.fn(),
};
const $tracker = { send: jest.fn() };
const $device = { isMobile: false };
const $buefy = { toast: { open: jest.fn() } };
const $dayjs = dayjs;
const mocks = { $bugsnag, $tracker, $device, $buefy, $dayjs };

const mountData = {
  localVue,
  stubs,
  store,
  mocks,
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };
  return shallowMount(PremiumCard, finalData);
};

const wrapperNotOnConfirmation = shallowMount(PremiumCard, {
  ...mountData,
  computed: {
    ownerOnConfirmation() {
      return false;
    },
  },
});

describe('PremiumCard.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.premium-card').exists()).toBeTruthy();
  });

  it('should return correct invoice url and owner status', () => {
    expect(wrapper.vm.invoiceUrl).toBe('mamikos.com/invoice');
    expect(wrapper.vm.ownerOnConfirmation).toBeTruthy();
    expect(wrapper.vm.ownerPremium).toBeFalsy();
  });

  it('should return correct purchase type & purchase name ', () => {
    expect(wrapper.vm.purchaseName).toBe('Paket 1 Hari');
    expect(wrapper.vm.purchaseType).toBe('Paket');

    wrapper.vm.$store.state.profile.ownerInvoice.invoice.detail_package.type = 'balance';
    expect(wrapper.vm.purchaseType).toBe('Saldo');
  });

  it('should redirect or show modal when user wants to extend premium', () => {
    wrapperNotOnConfirmation.vm.extendPremium();
    expect(window.location.href).toBe('mamikos.com/promosi-kost/premium-package');
  });

  it('should handle buy balance correctly', () => {
    wrapperNotOnConfirmation.vm.handleBuyBalanceClicked();
    expect(window.location.href).toBe('mamikos.com/ownerpage/premium/balance');
  });

  it('shoudl handle daily allocation switch correctly', async () => {
    wrapper.vm.handleDailyAllocationSwitch();
    await wrapper.vm.$nextTick();
    try {
      expect(mocks.$buefy.toast.open).toBeCalledWith({
        duration: 2000,
        message: 'Alokasi Harian telah diaktifkan',
        position: 'is-bottom',
      });
    } catch {
      expect(wrapper.vm.isLoading).toBeTruthy();
    }
  });
});
