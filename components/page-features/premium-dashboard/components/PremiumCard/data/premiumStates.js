let actions = {
  text: null,
  type: null,
  data: null,
};
const MAMIKOS_URL = process.env.MAMIKOS_URL;
export default {
  Premium() {
    if (this.isOnBScenario) {
      actions = {
        text: 'Beli Saldo',
        type: 'alert',
        data: 'Pastikan Anda memiliki kost atau apartemen aktif terlebih dahulu',
      };
    } else {
      actions = {
        text: 'Atur Premium',
        type: 'alert',
        data: 'Pastikan Anda memiliki kost atau apartemen aktif terlebih dahulu',
      };
    }
    if ((this.balanceStatus === 'verif' || this.balanceStatus === '') && !this.isOnBScenario) {
      if (this.activeKos > 0 || this.activeApartment > 0) {
        if (this.isUserVerified) {
          actions.type = 'link';
          actions.data = `${MAMIKOS_URL}/promosi-kost/premium-package`;
        } else {
          actions.text = 'Verifikasi Nomor';
          actions.type = 'link';
          actions.data = `${MAMIKOS_URL}/promosi-kost/premium-package`;
        }
      }
    } else if (
      (this.balanceStatus === 'verif' || this.balanceStatus === '') &&
      this.isOnBScenario
    ) {
      if (this.activeKos > 0 || this.activeApartment > 0) {
        if (this.isUserVerified) {
          actions.type = 'link';
          actions.data = `${MAMIKOS_URL}/ownerpage/premium/balance`;
        } else {
          actions.text = 'Verifikasi Nomor';
          actions.type = 'link';
          actions.data = `${MAMIKOS_URL}/ownerpage/premium/balance`;
        }
      }
    } else {
      actions.type = 'alert';
      actions.data = 'Mohon selesaikan proses pembelian saldo Anda terlebih dahulu.';
    }
    return actions;
  },
  'Coba Trial'() {
    if (this.isOnBScenario) {
      actions = {
        text: 'Gunakan Premium',
        type: 'link',
        data: `${MAMIKOS_URL}/ownerpage/premium/balance`,
      };
    } else {
      actions = {
        text: 'Lihat Paket',
        type: 'link',
        data: `${MAMIKOS_URL}/promosi-kost/premium-package`,
      };
    }
    if (this.isUserVerified) {
      if ((this.activeKos > 0 || this.activeApartment > 0) && !this.isOnBScenario) {
        actions.type = 'link';
        actions.data = `${MAMIKOS_URL}/promosi-kost/premium-package`;
      } else if ((this.activeKos > 0 || this.activeApartment > 0) && this.isOnBScenario) {
        actions.type = 'link';
        actions.data = `${MAMIKOS_URL}/ownerpage/premium/balance`;
      } else {
        actions.text = 'Verifikasi Nomor';
        actions.type = 'alert';
        actions.data = 'Pastikan Anda memiliki kost atau apartemen aktif terlebih dahulu';
      }
    }
    return actions;
  },
  'Konfirmasi Pembayaran'() {
    if (this.isOnBScenario) {
      actions.text = 'Konfirmasi Pembayaran';
      actions.type = 'link';
      actions.data = `${MAMIKOS_URL}/ownerpage/premium/balance/confirmation`;
    } else {
      actions.text = 'Konfirmasi Pembayaran';
      actions.type = 'link';
      actions.data = `${MAMIKOS_URL}/ownerpage/premium/purchase/confirmation`;
    }
    return actions;
  },
  'Proses Verifikasi'() {
    if (this.isOnBScenario) {
      actions.text = 'Proses Verifikasi';
      actions.type = 'link';
      actions.data = `${MAMIKOS_URL}/ownerpage/premium/balance/confirmation`;
    } else {
      actions.text = 'Rincian Pembayaran';
      actions.type = 'link';
      actions.data = `${MAMIKOS_URL}/ownerpage/premium/purchase/confirmation`;
    }
    return actions;
  },
  'Upgrade ke Premium'() {
    if (this.isOnBScenario) {
      actions = {
        text: 'Gunakan Premium',
        type: 'alert',
        data: 'Pastikan Anda memiliki kost atau apartemen aktif terlebih dahulu',
      };
    } else {
      actions = {
        text: 'Lihat Paket',
        type: 'alert',
        data: 'Pastikan Anda memiliki kost atau apartemen aktif terlebih dahulu',
      };
    }
    if (this.activeKos > 0 || this.activeApartment > 0) {
      if (this.isUserVerified && !this.isOnBScenario) {
        actions.type = 'link';
        actions.data = `${MAMIKOS_URL}/promosi-kost/premium-package`;
      } else if (this.isUserVerified && this.isOnBScenario) {
        actions.type = 'link';
        actions.data = `${MAMIKOS_URL}/ownerpage/premium/balance`;
      } else {
        actions.text = 'Verifikasi Nomor';
        actions.type = 'link';
        actions.data = `${MAMIKOS_URL}/ownerpage`;
      }
    }
    return actions;
  },
};
