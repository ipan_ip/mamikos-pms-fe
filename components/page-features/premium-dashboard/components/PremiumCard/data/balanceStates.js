const action = {
  text: 'Beli Saldo',
  type: 'link',
  data: null,
};
const MAMIKOS_URL = process.env.MAMIKOS_URL;
export default {
  // owner already chose balance option, then owner will choose payment method, then pay it
  add() {
    action.text = 'Konfirmasi';
    action.type = 'link';
    action.data = `${MAMIKOS_URL}/ownerpage/premium/balance/confirmation`;
    return action;
  },
  // after owner pay and confirm it, owner waits approval from admin
  waiting() {
    action.text = 'Proses Verifikasi';
    action.type = 'link';
    action.data = `${MAMIKOS_URL}/ownerpage/premium/balance/confirmation`;
    return action;
  },
  // admin has been approved payment
  verif() {
    if (this.isUserVerified && !this.isOnTrial) {
      if (this.activeKos > 0) {
        action.type = 'link';
        action.data = `${MAMIKOS_URL}/ownerpage/premium/balance`;
      } else if (this.activeApartment > 0) {
        action.type = 'link';
        action.data = `${MAMIKOS_URL}/ownerpage/premium/balance`;
      } else {
        action.type = 'alert';
        action.data = 'Pastikan Anda memiliki kost atau apartemen aktif terlebih dahulu';
      }
    } else {
      action.type = 'alert';
      action.data = 'Silahkan update ke premium.';
    }
    return action;
  },
};
