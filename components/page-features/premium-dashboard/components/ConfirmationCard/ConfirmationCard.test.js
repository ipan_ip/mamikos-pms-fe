import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import ConfirmationCard from './ConfirmationCard';
import MkCard from '~/components/global/molecules/MkCard';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';
import MkCardFooter from '~/components/global/molecules/MkCard/components/MkCardFooter';

process.env.MAMIKOS_URL = 'mamikos.com';
const localVue = createLocalVue();
localVue.use(Vuex);

Object.defineProperty(window, 'location', {
  value: { href: 'mamikos.com' },
});

const storeData = {
  modules: {
    profile: {
      state: {
        ownerInvoice: {
          invoice: {
            invoice_url: 'mamikos.com/invoice',
            detail_package: {
              name: 'Paket 1 Hari',
              type: 'package',
            },
          },
        },
      },
    },
  },
};

const store = new Vuex.Store(storeData);

const stubs = {
  MkCard,
  MkCardBody,
  MkCardFooter,
};

describe('ConfirmationCard.vue', () => {
  const wrapper = shallowMount(ConfirmationCard, { localVue, store, stubs });

  it('should mount properly', () => {
    expect(wrapper.find('.confirmation-card').exists()).toBeTruthy();
  });

  it('should return correct invoice url', () => {
    expect(wrapper.vm.invoiceUrl).toBe('mamikos.com/invoice');
  });

  it('should return correct purchase type & purchase name ', () => {
    expect(wrapper.vm.purchaseName).toBe('Paket 1 Hari');
    expect(wrapper.vm.purchaseType).toBe('Paket');
  });

  it('should return correct changePurchaseUrl', () => {
    expect(wrapper.vm.changePurchaseUrl).toBe('mamikos.com/promosi-kost/premium-package');

    const wrapperBalance = shallowMount(ConfirmationCard, {
      localVue,
      store,
      stubs,
      computed: {
        purchaseType() {
          return 'Saldo';
        },
      },
    });
    expect(wrapperBalance.vm.changePurchaseUrl).toBe(
      'mamikos.com/ownerpage/premium/balance?change-package=true',
    );
  });

  it('should return correct redirection confirmation link and change purchase link', () => {
    wrapper.vm.handleConfirmationBtn();
    expect(window.location.href).toBe('mamikos.com/invoice');
  });

  it('should return correct redirection change purchase link', () => {
    wrapper.vm.handleChangePurchase();
    expect(window.location.href).toBe('mamikos.com/promosi-kost/premium-package');
  });
});
