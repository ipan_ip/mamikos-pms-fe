import { shallowMount } from '@vue/test-utils';
import SurveySkeleton from '../SurveySkeleton';
import localVueWithBuefy from '~/utils/addBuefy';

const mountComponent = () => {
  return shallowMount(SurveySkeleton, {
    localVue: localVueWithBuefy,
  });
};

describe('render component surveySkeleton', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mountComponent();
  });
  it('should render skeleton properly ', () => {
    expect(wrapper.find('.stories-skeleton')).toBeTruthy();
  });
});
