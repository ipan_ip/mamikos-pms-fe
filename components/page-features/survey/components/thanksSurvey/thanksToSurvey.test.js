import Vuex from 'vuex';
import { mount } from '@vue/test-utils';
import ThanksToSurvey from '../thanksSurvey';
import localVueWithBuefy from '~/utils/addBuefy';

const mocks = {
  $router: { push: jest.fn() },
  $device: { isDesktop: true },
};
const storeData = {
  modules: {
    survey: {
      namespaced: true,
      state: { surveyPreviousPath: { prevPath: { name: '/test' } } },
    },
  },
};
localVueWithBuefy.use(Vuex);
const mountComponent = () => {
  return mount(ThanksToSurvey, {
    localVue: localVueWithBuefy,
    mocks,
    store: new Vuex.Store(storeData),
  });
};

describe('thanksToSurvey.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mountComponent();
  });

  describe('render component thanksToSurvey', () => {
    it('should render  back button with content "Kembali"', () => {
      const button = wrapper.find('.c-navigation');
      expect(button.text()).toBe('Kembali');
    });

    it('should render  title with content "Terima Kasih"', () => {
      const button = wrapper.find('.c-title__content');
      expect(button.text()).toBe('Terima Kasih');
    });
  });

  describe('computed value in thanksToSurvey Component', () => {
    it('should return isDesktop "true" when width more then 1207px', () => {
      window.innerWidth = 1366;
      expect(wrapper.vm.isDesktop).toBeTruthy();
    });
  });

  describe('execute method in thanksToSurvey Component', () => {
    it('should go to Dashboard page when method "gotoDashboard" to be called', () => {
      const button = wrapper.findAll('button');
      button.trigger('click');
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/' });
    });

    it('should go previous page that url comes from store ', () => {
      const link = wrapper.findAll('a');
      link.trigger('click');
      expect(wrapper.vm.$router.push).toBeCalledWith({ name: '/test' });
    });

    it('should go feedback page that url from store are not be set or null ', () => {
      wrapper.vm.$store.state.survey.surveyPreviousPath = false;
      wrapper.vm.prevPage();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-survey/feedback' });
    });
  });
});
