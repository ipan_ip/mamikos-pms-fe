import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import SurveyQuestions from '../SurveyQuestions';
import localVueWithBuefy from '~/utils/addBuefy';

const mocks = {
  $router: { push: jest.fn() },
  $device: { isDesktop: true },
  $tracker: { send: jest.fn() },

  $store: {
    commit: jest.fn(),
  },

  $bugsnag: {
    notify: jest.fn(),
  },
  $alert: jest.fn(),

  $api: {
    getPollingQuestions: jest.fn().mockResolvedValue({
      data: {
        data: {
          id: 1,
          key: 'exit_goldplus',
          description: 'User polling',
          questions: [
            {
              id: 11,
              question: 'test questions',
              type: 'checklist',
              options: [
                {
                  id: 1,
                  option: 'test options',
                  is_other: false,
                },
                {
                  id: 2,
                  option: 'Lainnya',
                  is_other: true,
                },
              ],
            },
          ],
          has_submitted: false,
        },
      },
    }),
    postFeedbackSurvey: jest
      .fn()
      .mockResolvedValue({ data: { status: true, action_code: 'exit' } }),
  },
};

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        user: {
          name: 'test Name',
          email: 'test@mail.com',
          phone_number: '083176408437',
        },

        isPremium: false,
      },
      getters: {
        user(state) {
          return state.user;
        },

        isPremium(state) {
          return state.isPremium;
        },
      },
    },
    survey: {
      namespaced: true,
      mutations: {
        setSurveyPreviousPath: jest.fn(),
      },
    },
  },
};

localVueWithBuefy.use(Vuex);
const mountComponent = () => {
  return shallowMount(SurveyQuestions, {
    localVue: localVueWithBuefy,
    mocks,
    store: new Vuex.Store(storeData),
  });
};

describe('SurveyQuestions.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mountComponent();
  });

  describe('render component SurveyQuestions', () => {
    it('should render back button with content "Kembali"', () => {
      const backButton = wrapper.find('.c-navigation');
      expect(backButton.text()).toBe('Kembali');
    });

    it('should render title with content "Mohon beritahu yang perlu kami perbaiki"', () => {
      const backButton = wrapper.find('.c-title__content');
      expect(backButton.text()).toBe('Mohon beritahu yang perlu kami perbaiki');
    });
  });

  describe('computed value in SurveyQuestions component', () => {
    it('should isDesktop are returned "true" value when width of screen more than 1208', () => {
      window.innerWidth = 1366;
      expect(wrapper.vm.isDesktop).toBeTruthy();
    });
  });
  describe('execute method in SurveyQuestions component', () => {
    it('should set "otherId" when opetions value is "Lainya"', () => {
      wrapper.vm.checkOptions(1, 'Lainnya');
      expect(wrapper.vm.othersId).toBe(1);
    });

    it('should no set "otherId"  value when opetions value is not "Lainya"', () => {
      wrapper.vm.checkOptions(1, 'test');
      expect(wrapper.vm.othersId).toBe('');
    });

    it('should generete object based on response model and covert to "Y" or "N"', () => {
      wrapper.vm.optionIds = { 1: true, 2: false };
      wrapper.vm.uploadFeedback();
      expect(wrapper.vm.allResponseData).toEqual([
        { id: '1', value: 'Y' },
        { id: '2', value: 'N' },
      ]);
    });

    it('should generete object based on response model in textarea Input', () => {
      wrapper.vm.othersId = 1;
      wrapper.vm.ownerFeedback = 'test feedback';
      wrapper.vm.optionIds = { 0: true, 1: true, 2: false };
      wrapper.vm.uploadFeedback();
      expect(wrapper.vm.allResponseData).toEqual([
        { id: '0', value: 'Y' },
        { id: '1', value: 'test feedback' },
        { id: '2', value: 'N' },
      ]);
    });

    it('should checking other box that is selected or not and checling input length from  textarea cant to be empty', () => {
      wrapper.vm.othersId = 1;
      wrapper.vm.ownerFeedback = 'test feedback';
      wrapper.vm.optionIds = { 0: true, 1: false, 2: false };
      wrapper.vm.uploadFeedback();
      expect(wrapper.vm.allResponseData).toEqual([
        { id: '0', value: 'Y' },
        { id: '1', value: 'N' },
        { id: '2', value: 'N' },
      ]);
    });

    it('should no check that other are selected and textarea cant to be empty, if empty warning value is "true" ', () => {
      wrapper.vm.optionIds = { 0: true, 1: false, 2: true };
      wrapper.vm.othersId = 2;
      wrapper.vm.ownerFeedback = '';
      wrapper.vm.sendFeedback();
      expect(wrapper.vm.warning).toBeTruthy();
    });

    it('should no check that other are selected and textarea cant to be empty, if not empty warning value is "false" ', () => {
      wrapper.vm.optionIds = { 0: true, 1: false, 2: true };
      wrapper.vm.othersId = 2;
      wrapper.vm.ownerFeedback = 'test feedback';
      wrapper.vm.sendFeedback();
      expect(wrapper.vm.warning).toBeFalsy();
    });

    it('should redirect to dashoard when submit status is "true"', () => {
      wrapper.vm.checkSubmitStatus(true);
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/' });
    });

    it('should redirect to close survey page when response from server is "exit"', () => {
      wrapper.vm.sendResponseToApi();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-survey/close' });
    });

    it('should redirect to offers GP package page when response from server is not "exit"', async () => {
      wrapper.vm.$api.postFeedbackSurvey = jest
        .fn()
        .mockResolvedValue({ data: { status: true, action_code: 'test' } });
      await wrapper.vm.sendResponseToApi();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-survey/offers' });
    });

    it('should show alert when erron in connection from client to server', async () => {
      wrapper.vm.$api.postFeedbackSurvey = jest
        .fn()
        .mockResolvedValue({ data: { status: false, action_code: 'test' } });
      await wrapper.vm.sendResponseToApi();
      expect(wrapper.vm.$alert).toBeCalledWith(
        'is-danger',
        'Gagal',
        'Terjadi galat, silakan coba lagi.',
      );
    });

    it('should show alert when erron in connection from client to server', async () => {
      wrapper.vm.$api.postFeedbackSurvey = jest.fn().mockRejectedValue();
      await wrapper.vm.sendResponseToApi();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-survey/offers' });
    });

    it('should redirect to landing exit GP survey', () => {
      wrapper.vm.prevPage();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-survey' });
    });

    it('should not assign value in variable "questionList" when question from server is "false"', async () => {
      wrapper.vm.$api.getPollingQuestions = jest
        .fn()
        .mockResolvedValue({ data: { data: { questions: false } } });

      await wrapper.vm.getQuestionList();
      expect(wrapper.vm.questionList).toBeFalsy();
    });

    it('should send tracker "Gp riview visited" with attribute already set in tracking spec', async () => {
      wrapper.vm.$device.isDesktop = false;
      wrapper.vm.$store.state.profile.user.name = null;
      wrapper.vm.sendTrackerEvent();
      await wrapper.vm.$nextTick(() => {
        expect(wrapper.vm.$device.isDesktop).toBeFalsy();
        expect(wrapper.vm.$tracker.send).toBeTruthy();
        expect(wrapper.vm.$store.state.profile.user.name).toBe(null);
      });
    });

    it('should send tracker "Gp riview submitted" with attribute already set in tracking spec', async () => {
      wrapper.vm.$device.isDesktop = false;
      wrapper.vm.$store.state.profile.user.name = null;
      wrapper.vm.sendSubmitedTracker();
      await wrapper.vm.$nextTick(() => {
        expect(wrapper.vm.$device.isDesktop).toBeFalsy();
        expect(wrapper.vm.$tracker.send).toBeTruthy();
        expect(wrapper.vm.$store.state.profile.user.name).toBe(null);
      });
    });
  });
});
