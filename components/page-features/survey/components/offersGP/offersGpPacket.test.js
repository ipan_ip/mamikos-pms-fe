import { shallowMount } from '@vue/test-utils';
import offersGP from '../offersGP';
import localVueWithBuefy from '~/utils/addBuefy';

const mocks = {
  $router: {
    push: jest.fn(),
  },

  $device: {
    isDesktop: true,
  },

  $store: {
    commit: jest.fn(),
  },
};

const mountComponents = () => {
  return shallowMount(offersGP, {
    localVue: localVueWithBuefy,
    mocks,
  });
};

describe('offersGpPacket.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mountComponents();
  });

  describe('render components offersGpPacket', () => {
    it('should render back link with content "Kembali"', () => {
      expect(wrapper.find('.c-navigation').text()).toBe('Kembali');
    });

    it('should render title with content "Ada paket-paket lain yang gak kalah oke nih. Tertarik?"', () => {
      expect(wrapper.find('.c-title__content').text()).toBe(
        'Ada paket-paket lain yang gak kalah oke nih. Tertarik?',
      );
    });
  });

  describe('execute method in offersGpPacket', () => {
    it('should goto feedback page when function "prevPage" are called', () => {
      wrapper.vm.prevPage();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-survey/feedback' });
    });

    it('should redirect to close page when function "closeSurvey" are called', () => {
      wrapper.vm.closeSurvey();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-survey/close' });
    });

    it('should redirect to GP package list page when function "gotoGpPackages" are called', () => {
      wrapper.vm.gotoGpPackages();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/goldplus/submission/packages' });
    });
  });
});
