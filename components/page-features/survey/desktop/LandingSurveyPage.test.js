import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import LandingPageSurvey from '../desktop';
import localVueWithBuefy from '~/utils/addBuefy';

const mocks = {
  $api: {
    getPollingQuestions: jest.fn().mockResolvedValue({ data: { data: { has_submitted: false } } }),
  },
  $router: { push: jest.fn() },
  $tracker: { send: jest.fn() },
  $device: { isDesktop: true },
  $route: {
    query: {
      started_at: '2020-12-08',
      ended_at: '2021-01-08',
      gp_package: 'gp1',
      invoice_id: '2',
      expiry_time: '2021-01-18',
      has_statistic: '1',
    },
  },
};

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        user: {
          name: 'test Name',
          email: 'test@mail.com',
          phone_number: '083176408437',
        },

        isPremium: false,
      },
      getters: {
        user(state) {
          return state.user;
        },

        isPremium(state) {
          return state.isPremium;
        },
      },
    },

    statistic: {
      namespaced: true,
      state: {
        statisticData: {},
        personalStatisticData: false,
        trackerRedirection: 'ownerDashboard',
      },

      getters: {
        redirectionHistory(state) {
          return state.trackerRedirection;
        },
      },

      mutations: {
        setPersonalStatisticData: jest.fn(),
        setStatisticData: jest.fn(),
        setPackageStatus: jest.fn(),
        setInvoiceId: jest.fn(),
        redirectionHistory: jest.fn(),
        setTrackerRedirection: jest.fn(),
      },
    },
  },
};

localVueWithBuefy.use(Vuex);
const mountComponent = () => {
  return shallowMount(LandingPageSurvey, {
    localVue: localVueWithBuefy,
    mocks,
    store: new Vuex.Store(storeData),
  });
};

describe('LandingPageSurvey.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mountComponent();
    wrapper.vm.isLoadSurveyData = false;
  });

  describe('render component LandingPageSurvey correctly', () => {
    it('should render title with content "Halo, test Name!"', () => {
      const title = wrapper.find('.c-title__content');
      expect(title.text()).toBe('Halo, test Name!');
    });

    it('should render illustration correctly', () => {
      expect(wrapper.find('.c-illustration')).toBeTruthy();
    });
  });

  describe('should computed value in LandingSurveyPage component', () => {
    it('should return "GoldPlus 1" when param from scheme is gp1', () => {
      expect(wrapper.vm.goldplusPackageStatus).toBe('GoldPlus 1');
    });

    it('should return "GoldPlus 2" when param from scheme is gp2', () => {
      wrapper.vm.$route.query.gp_package = 'gp2';
      expect(wrapper.vm.goldplusPackageStatus).toBe('GoldPlus 2');
    });

    it('should return "notification" when ownerRedirection in store is no equal with "ownerDashboard"', () => {
      wrapper.vm.$store.state.statistic.trackerRedirection = 'test notif';
      expect(wrapper.vm.ownerRedirection).toBe('notification');
    });
  });

  describe('should execute method in LandingPageSurvey', () => {
    it('should change route page on Dasboard Owner', () => {
      wrapper.vm.prevPage();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/' });
    });

    it('should change route page on feedback page', () => {
      wrapper.vm.nextPage();
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-survey/feedback' });
    });

    it('should redirect to close page when "has_submitted" are true', () => {
      wrapper.vm.isHasSubmited(true);
      expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/exit-gp-survey/close' });
    });

    it('should set value isLoadSurveyData to be "true" when connection are resolved', () => {
      wrapper.vm.getPollingStatus();
      expect(wrapper.vm.isLoadSurveyDatah).toBeFalsy();
    });

    it('should send tracker data that already set before in tracking spec ', async () => {
      wrapper.vm.$store.state.profile.user.name = null;
      wrapper.vm.sendTrackerEvent();
      await wrapper.vm.$nextTick(() => {
        wrapper.vm.sendTrackerEvent();
        expect(wrapper.vm.user.name).toBe(null);
        expect(wrapper.vm.$tracker.send).toBeTruthy();
      });
    });
  });
});
