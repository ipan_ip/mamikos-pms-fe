import cloneDeep from 'lodash/cloneDeep';
import sortBy from 'lodash/sortBy';

export default {
  methods: {
    sort(dt) {
      const data = cloneDeep(dt);
      let dataNumber = data.filter((dt) => /^\d*$/.test(dt.name));
      let dataString = data.filter((dt) => !/^\d*$/.test(dt.name));

      dataNumber = sortBy(dataNumber, (o) => parseInt(o.name));
      dataString = sortBy(dataString, (o) => o.name);
      return dataNumber.concat(dataString);
    },
  },
};
