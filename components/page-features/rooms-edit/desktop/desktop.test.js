import { shallowMount } from '@vue/test-utils';
import desktop from './desktop';
import localVueWithBuefy from '~/utils/addBuefy';

const $api = {
  getPropertyRooms: jest.fn().mockResolvedValue({
    data: {
      data: [{ id: 1, name: 'a', floor: '2', occupied: false }],
      'room-data': { is_bbk: true, is_gp_kos: true },
    },
  }),
  getActiveProperty: jest.fn().mockResolvedValue({
    data: {
      rooms: [
        {
          _id: 1,
          room_title: 'room',
          photo_url: {
            small: 'small_url',
          },
          price_3_month: 0,
          price_6_month: 0,
          price_daily: 100000,
          price_monthly: 1000000,
          price_weekly: 12312323,
          price_yearly: 0,
          is_price_flash_sale: {
            daily: false,
            weekly: false,
            monthly: false,
            annually: true,
            quarterly: false,
            semiannually: false,
          },
        },
      ],
    },
  }),
};

const $device = { isDesktop: true };

describe('desktop.vue', () => {
  // mock directive
  localVueWithBuefy.directive('popup-order', {
    bind(el, { value: { onVisible } }) {
      onVisible();
    },
  });

  const mount = () =>
    shallowMount(desktop, {
      localVue: localVueWithBuefy,
      mocks: {
        $api,
        $route: {
          params: {
            kos_id: 11,
          },
        },
        $changes: { init: jest.fn(), update: jest.fn() },
        $popupOrder: { next: jest.fn(), destroy: jest.fn() },
        $buefy: { toast: { open: jest.fn() } },
        $axios: {
          CancelToken: { source: jest.fn(() => ({ token: 'token', cancel: jest.fn() })) },
        },
        $device,
      },
      stubs: {
        ModalAdd: {
          template: `<div>
          <div class="modal-add-done" @click="$emit('done', type, room)"></div>
          <div class="modal-add-intercepted" @click="$emit('intercepted', type, room.id)"></div>
        </div>`,
          data() {
            return { type: 'add', room: {} };
          },
          methods: {
            toggleModal(active, type, room, roomNames) {
              this.type = type;
              this.room = room;
            },
          },
        },
        BInput: {
          template: '<input class="b-input" @input="$emit(`input`, $event.target.value)"/>',
        },
      },
    });

  const wrapper = mount();

  it('should mount properly', async () => {
    await wrapper.vm.$api.getPropertyRooms;
    expect(wrapper.vm.isLoading).toBe(false);
    expect(wrapper.vm.totalPage).toBe(1);
    expect(wrapper.vm.roomNames).toEqual(['a']);
    expect(wrapper.find('.room-edit-page').exists()).toBeTruthy();
  });

  it('should add new room when ModalAdd is emitting done event', () => {
    wrapper.vm.$refs.addModal.toggleModal(true, 'edit', {
      id: 1,
      name: `bos`,
      floor: `2`,
      occupied: false,
    });
    wrapper.find('.modal-add-done').trigger('click');
    expect(wrapper.vm.data[0].name).toBe('bos');

    wrapper.vm.$refs.addTenantInterceptModal.toggleModal = jest.fn();
    wrapper.find('.modal-add-intercepted').trigger('click');
    expect(wrapper.vm.$refs.addTenantInterceptModal.toggleModal).toBeCalled();
    wrapper.vm.$refs.addTenantInterceptModal.toggleModal.mockRestore();

    wrapper.vm.$refs.addModal.toggleModal(true, 'add', {
      id: 2,
      name: `mantap`,
      floor: `2`,
      occupied: false,
    });
    wrapper.find('.modal-add-done').trigger('click');
    expect(wrapper.vm.data.length).toBe(2);

    wrapper.find('.modal-add-intercepted').trigger('click');
    expect(wrapper.vm.$buefy.toast.open).toBeCalled();
    wrapper.vm.$buefy.toast.open.mockClear();
  });

  it('should back to first page when owner is changing occupied dropdown', () => {
    wrapper.vm.occupied = 'available';
    expect(wrapper.vm.currentPage).toBe(1);
  });

  it('should reset search keyword when input is filled by empty', () => {
    wrapper.find('.b-input').trigger('input');
    expect(wrapper.vm.searchKeyword).toBe('');

    wrapper.find('.b-input').setValue('bos');
    wrapper.find('.b-input').trigger('keypress.enter');
    expect(wrapper.vm.searchKeyword).toBe('bos');
    expect(wrapper.vm.filteredData.length).toBe(1);

    wrapper.find('.b-input').setValue('bro');
    wrapper.find('.b-input').trigger('keypress.enter');
    expect(wrapper.vm.searchKeyword).toBe('bro');
    expect(wrapper.vm.filteredData.length).toBe(0);
  });

  it('should destroy popup order when component is destroyed', () => {
    wrapper.destroy();
    expect(wrapper.vm.$popupOrder.destroy).toBeCalled();
  });
});
