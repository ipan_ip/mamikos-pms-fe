import { shallowMount } from '@vue/test-utils';
import ModalAdd from './ModalAdd';
import localVueWithBuefy from '~/utils/addBuefy';

const room = { id: 1, name: 'name', occupied: true, disable: false, floor: '1' };

const propertyRoomResponse = {
  data: {
    meta: { message: 'message' },
    status: true,
    unit: { data: 'data' },
  },
};

describe('ModalAdd.vue', () => {
  const wrapper = shallowMount(ModalAdd, {
    localVue: localVueWithBuefy,
    propsData: {
      roomsCount: 1,
    },
    mocks: {
      $changes: { update: jest.fn(), init: jest.fn() },
      $route: { params: { kos_id: 1 } },
      $store: { dispatch: jest.fn().mockResolvedValue({}) },
      $buefy: { toast: { open: jest.fn() } },
      $api: {
        postPropertyRoom: jest
          .fn()
          .mockResolvedValueOnce(propertyRoomResponse)
          .mockResolvedValueOnce({ data: { ...propertyRoomResponse.data, status: false } })
          .mockResolvedValueOnce(propertyRoomResponse),
      },
    },
    stubs: {
      'b-input': {
        template: `<input :value="value" @change="$emit('input', $event.target.value)"/>`,
        model: { event: 'input', prop: 'value' },
        props: ['value'],
      },
      'b-checkbox': {
        template: `<label><input type="checkbox" @change="$emit('input', $event.target.value)"/></label>`,
        model: { event: 'input', prop: 'value' },
        props: ['value'],
      },
      'mk-button': {
        template: `<div @click="$emit('clicked')">bos</div>`,
      },
    },
  });
  it('should mount properly', () => {
    expect(wrapper.find('.add-modal').exists()).toBe(true);
    expect(wrapper.vm.roomsCount).toBe(1);
  });

  it('should toggle modal from parent', () => {
    wrapper.vm.toggleModal(true, 'add', room, [room.name]);
    expect(wrapper.vm.$changes.update).toBeCalled();
    expect(wrapper.vm.roomNames).toEqual([room.name]);
    expect(wrapper.vm.name.value).toEqual(room.name);
  });

  it('should show error when form is not completed', async () => {
    const inputs = wrapper.findAll('input');
    const inputData = [
      {
        name: 'name',
        input: inputs.at(0),
        cases: [
          { value: '', expect: 'emptyName' },
          {
            value: Array.from(Array(51).keys()).reduce((s, val) => s + 'a', ''),
            expect: 'maxLength',
          },
          { value: 'name', expect: 'duplicateName' },
        ],
      },
      {
        name: 'floor',
        input: inputs.at(1),
        cases: [
          {
            value: Array.from(Array(51).keys()).reduce((s, val) => s + 'a', ''),
            expect: 'maxLength',
          },
        ],
      },
    ];
    for (let j = 0; j < inputData.length; j++) {
      for (let i = 0; i < inputData[j].cases.length; i++) {
        inputData[j].input.element.value = inputData[j].cases[i].value;
        inputData[j].input.trigger('change');
        await wrapper.vm.$nextTick();
        expect(wrapper.vm[inputData[j].name].error).toBe(inputData[j].cases[i].expect);
        expect(wrapper.find('.add-modal__error-message').exists()).toBe(true);
      }
    }
  });

  it('should call update changes plugin', () => {
    const checkbox = wrapper.find('input[type="checkbox"]');

    checkbox.trigger('click');
    expect(wrapper.vm.$changes.update).toBeCalled();
  });

  it('should submit room properly', async () => {
    const button = wrapper.find('.add-modal__action');
    button.trigger('click');

    // success response
    expect(wrapper.vm.isSendingData).toBe(true);

    await Promise.resolve;
    await wrapper.vm.$api.postPropertyRoom;
    await Promise.resolve;
    await Promise.resolve;
    await wrapper.vm.$store.dispatch;
    await Promise.resolve;

    expect(wrapper.vm.$buefy.toast.open).toBeCalled();
    expect(wrapper.vm.$store.dispatch).toBeCalledWith('profile/getLatestProfile');
    expect(wrapper.vm.isSendingData).toBe(false);

    // error response
    button.trigger('click');

    expect(wrapper.vm.isSendingData).toBe(true);

    await Promise.resolve;
    await wrapper.vm.$api.postPropertyRoom;
    await Promise.resolve;

    expect(wrapper.vm.isSendingData).toBe(false);
    expect(wrapper.vm.errResponseMessage).toBe(propertyRoomResponse.data.meta.message);

    // intercepted
    wrapper.setProps({ isBbk: true, isGoldPlusKost: true });
    wrapper.setData({ initOccupied: false });

    jest.clearAllMocks();
    const postPropertyRoom = jest.spyOn(wrapper.vm.$api, 'postPropertyRoom');
    wrapper.setData({ occupied: true });
    button.trigger('click');
    expect(wrapper.vm.isSendingData).toBe(false);
    expect(postPropertyRoom).not.toBeCalled();
    expect(wrapper.vm.occupied).toBe(false);
    postPropertyRoom.mockRestore();
  });

  it('should close modal properly', () => {
    wrapper.find('.add-modal__close').trigger('click');
    expect(wrapper.vm.active).toBe(false);
    expect(wrapper.vm.$changes.update).toBeCalled();
  });

  it('should unmount properly', () => {
    wrapper.destroy();
    expect(wrapper.vm.$changes.update).toBeCalled();
  });
});
