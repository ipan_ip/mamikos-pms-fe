import { shallowMount } from '@vue/test-utils';
import ModalDelete from './ModalDelete';
import localVueWithBuefy from '~/utils/addBuefy';

describe('ModalDelete.vue', () => {
  const wrapper = shallowMount(ModalDelete, {
    localVue: localVueWithBuefy,
    mocks: {
      $store: { dispatch: jest.fn().mockResolvedValue({}) },
      $buefy: { toast: { open: jest.fn() } },
      $api: { deletePropertyRoom: jest.fn().mockResolvedValueOnce({ data: { status: true } }) },
    },
    stubs: {
      'mk-button': {
        template: `<div @click="$emit('clicked')">bos</div>`,
      },
    },
  });

  it('should mount properly', () => {
    expect(wrapper.find('.delete-modal').exists()).toBe(true);
  });

  it('should toggle modal properly through parent component', async () => {
    const messageEl = wrapper.find('.delete-modal__message');

    // open modal
    wrapper.vm.toggleModal(true, { id: 1, occupied: true });
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.active).toBe(true);
    expect(wrapper.vm.roomId).toBe(1);
    expect(wrapper.vm.occupied).toBe(true);
    expect(messageEl.text()).toBe(
      'Kamar yang akan dihapus adalah kamar terisi, yakin ingin menghapus?',
    );

    // close modal
    wrapper.vm.toggleModal(false, { id: 1, occupied: false });
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.active).toBe(false);
    expect(wrapper.vm.roomId).toBe(1);
    expect(wrapper.vm.occupied).toBe(false);
    expect(messageEl.text()).toBe('Yakin ingin menghapus kamar?');
  });

  it('should delete properly', async () => {
    const buttons = wrapper.findAll('.delete-modal__action');

    buttons.at(1).trigger('click');

    await Promise.resolve;
    await wrapper.vm.$api.deletePropertyRoom;
    await Promise.resolve;
    await Promise.resolve;
    await wrapper.vm.$store.dispatch;
    await Promise.resolve;

    expect(wrapper.vm.$buefy.toast.open).toBeCalled();
    expect(wrapper.vm.$store.dispatch).toBeCalledWith('profile/getLatestProfile');
    expect(wrapper.vm.isSendingData).toBe(false);

    // click cancel button
    wrapper.setMethods({ toggleModal: jest.fn() });
    buttons.at(0).trigger('click');
    expect(wrapper.vm.toggleModal).toBeCalledWith(false);
  });
});
