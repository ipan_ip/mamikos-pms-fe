import { mount } from '@vue/test-utils';
import ListRoom from './ListRoom';
import localVueWithBuefy from '~/utils/addBuefy';
import mockWindowProperty from '~/utils/mockWindowProperty';

const propsData = {
  data: [
    {
      id: 57299,
      name: '123',
      floor: '123',
      occupied: true,
      disable: false,
      show: false,
      gp_badge: { show: true },
      number: 1,
      floorError: null,
      nameError: 'duplicateName',
      type: 'add',
    },
  ],
  availabilityOption: 'all',
  searchKeyword: '',
};

mockWindowProperty('scroll', jest.fn());

describe('ListRoom.vue', () => {
  const wrapper = mount(ListRoom, {
    localVue: localVueWithBuefy,
    propsData,
    mocks: {
      $changes: { update: jest.fn(), init: jest.fn() },
      $store: { state: { addTenant: { webviewInterface: false } } },
      $buefy: { toast: { open: jest.fn() } },
    },
  });

  beforeEach(() => {
    window.scroll.mockClear();
    wrapper.vm.$changes.update.mockClear();
    wrapper.vm.$changes.init.mockClear();
    wrapper.vm.$buefy.toast.open.mockClear();
  });

  // mock id body
  document.getElementsByTagName('body')[0].id = 'dataBody';

  it('should mount properly', () => {
    expect(wrapper.vm.rooms[0].id).toBe(propsData.data[0].id);
    expect(wrapper.vm.rooms[0].show).toBeTruthy();
  });

  it('should show error message when data has error value', () => {
    expect(wrapper.find('.detail-room__error-message').text()).toBe(
      'Nomor/ nama sudah dipakai kamar lain.',
    );
  });

  it('should handle click event of more icon', async () => {
    wrapper.find('.detail-room__more').trigger('click');

    expect(wrapper.vm.showMore).toBeTruthy();
    expect(wrapper.vm.selectedRoom.id).toBe(propsData.data[0].id);

    await wrapper.vm.$nextTick;
    expect(wrapper.find('.snackbar-more__body').exists()).toBeTruthy();

    wrapper.find('.snackbar-more__add').trigger('click');

    await wrapper.vm.$nextTick;
    await wrapper.vm.$nextTick;
    await wrapper.vm.$nextTick;

    expect(wrapper.vm.showMore).toBeFalsy();
    expect(window.scroll).toBeCalled();
  });

  it('should handle data when availability option is changed', async () => {
    wrapper.setProps({ availabilityOption: 'available' });
    await wrapper.vm.$nextTick;
    expect(wrapper.vm.rooms[0].show).toBeFalsy();
  });

  it('should handle data when searchKeyword prop is changed', async () => {
    wrapper.setProps({ availabilityOption: 'all', searchKeyword: '12' });
    await wrapper.vm.$nextTick;
    expect(wrapper.vm.rooms[0].show).toBeTruthy();
  });

  it('should validate data when input is changed', async () => {
    wrapper.vm.rooms[0].name = '';
    wrapper.find('.input').trigger('input');
    await wrapper.vm.$nextTick;
    expect(wrapper.vm.$changes.update).toBeCalled();
    expect(wrapper.vm.rooms[0].nameError).toBe('emptyName');

    await wrapper.vm.$nextTick;
    expect(wrapper.find('.detail-room__error-message').text()).toBe('Nomor/ nama masih kosong.');
  });
});
