import { shallowMount } from '@vue/test-utils';
import StateNoRooms from '../StateNoRooms';
import localVueWithBuefy from '~/utils/addBuefy';
import MkButton from '~/components/global/atoms/MkButton';

const stubs = { MkButton };

const mount = (adtMountData = {}) => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      stubs,
    },
    ...adtMountData,
  };

  return shallowMount(StateNoRooms, mountData);
};

describe('StateNoRooms.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.no-property').exists()).toBeTruthy();
  });

  it('should handle add kos function', () => {
    wrapper.vm.$emit = jest.fn();
    const button = wrapper.find('button');
    button.trigger('click');
    expect(wrapper.vm.$emit).toBeCalled();
    expect(wrapper.vm.$emit.mock.calls[0][0]).toBe('addRooms');
  });
});
