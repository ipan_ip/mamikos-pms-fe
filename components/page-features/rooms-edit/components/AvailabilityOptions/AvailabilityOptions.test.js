import { shallowMount } from '@vue/test-utils';
import AvailabilityOptions from './AvailabilityOptions';
import localVueWithBuefy from '~/utils/addBuefy';

describe('AvailabilityOptions.vue', () => {
  const wrapper = shallowMount(AvailabilityOptions, {
    localVue: localVueWithBuefy,
    propsData: {
      availableRoomCount: 0,
      totalRoomCount: 0,
      value: '0',
    },
  });
  const options = wrapper.findAll('.availability-option__item');
  it('should mount properly', () => {
    expect(options.length).toBe(3);
  });

  it('should emit correct option when option is clicked', () => {
    options.at(0).trigger('click');
    expect(wrapper.emitted()['update:value'][0]).toEqual(['all']);
  });
});
