import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import ModalAddTenantIntercept from '../ModalAddTenantIntercept';
import localVueWithBuefy from '~/utils/addBuefy';
import mockWindowProperty from '~/utils/mockWindowProperty';

const localVue = localVueWithBuefy;
localVue.use(Vuex);

describe('ModalAddTenantIntercept.vue', () => {
  const wrapper = shallowMount(ModalAddTenantIntercept, {
    localVue: localVueWithBuefy,
    store: new Vuex.Store({ modules: { addTenant: { state: { webviewInterface: 'ios' } } } }),
    stubs: {
      MkButton: {
        template: `<button class="mk-button" @click="$emit('clicked')">mk button</button>`,
      },
    },
  });

  it('should render modal add tenant intercept if isActive', () => {
    wrapper.setData({ isActive: true });
    expect(wrapper.find('.modal-add-tenant-intercept').exists()).toBe(true);
  });

  it('should set query string properly', () => {
    const mamikosUrl = 'https://mamikos.com';
    const addTenantUrl = `${mamikosUrl}/ownerpage/manage/add-tenant/select-kost`;

    wrapper.setData({ kosId: 123, roomId: 999, mamikosUrl });
    expect(wrapper.vm.addTenantUrl).toBe(
      `${addTenantUrl}?interface=ios&k=123&r=999&from=update-kamar`,
    );

    wrapper.setData({ kosId: '', roomId: '', mamikosUrl });
    expect(wrapper.vm.addTenantUrl).toBe(`${addTenantUrl}?interface=ios&from=update-kamar`);

    wrapper.vm.$store.state.addTenant.webviewInterface = '';
    expect(wrapper.vm.addTenantUrl).toBe(`${addTenantUrl}?from=update-kamar`);
  });

  it('should set isActive to false if close button clicked', () => {
    const closeButton = wrapper.find('.modal-add-tenant-intercept__icon-close');
    closeButton.trigger('click.native');
    expect(wrapper.vm.isActive).toBe(false);
  });

  it('should go to add tenant page when button clicked', () => {
    mockWindowProperty('window.location', { replace: jest.fn() });

    const spyReplace = jest.spyOn(window.location, 'replace');
    const mamikosUrl = 'https://mamikos.com';
    const addTenantUrl = `${mamikosUrl}/ownerpage/manage/add-tenant/select-kost`;
    wrapper.vm.$store.state.addTenant.webviewInterface = 'android';
    wrapper.setData({ kosId: 123, roomId: 999, mamikosUrl });

    wrapper.find('.mk-button').trigger('click');
    expect(spyReplace).toBeCalledWith(
      `${addTenantUrl}?interface=android&k=123&r=999&from=update-kamar`,
    );
    spyReplace.mockRestore();
  });
});
