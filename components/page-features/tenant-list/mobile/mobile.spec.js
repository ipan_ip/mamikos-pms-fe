import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mobile from '../mobile';
import mockWindowProperty from '~/utils/mockWindowProperty';

jest.mock('js-cookie', () => {
  const Cookies = {
    get: jest.fn().mockImplementation(() => 'undefined'),
  };

  global.Cookies = Cookies;
  return global.Cookies;
});

const flushPromises = () => {
  return new Promise((resolve) => setImmediate(resolve));
};

const localVue = createLocalVue();
localVue.use(Vuex);

const tenantList = [
  {
    tenant_id: 2307,
    tenant_name: 'Chris evans',
    tenant_phone_number: '084574746254',
    tenant_photo: null,
    tenant_photo_id: null,
    tenant_photo_identifier: null,
    tenant_photo_identifier_id: null,
    kost_name: 'Kost Zahira Papua Yogyakarta',
    room_number: '8',
    room_id: 15576845,
    contract_id: 13441,
    transfer_status_raw: 'unpaid',
    transfer_status: 'Belum Dibayar',
  },
  {
    tenant_id: 2280,
    tenant_name: 'chris evans',
    tenant_phone_number: '087556565651',
    tenant_photo: null,
    tenant_photo_id: null,
    tenant_photo_identifier: null,
    tenant_photo_identifier_id: null,
    kost_name: 'Kost Gani Putri',
    room_number: '4',
    room_id: 61301818,
    contract_id: 12981,
    transfer_status_raw: 'unpaid',
    transfer_status: 'Belum Dibayar',
  },
];

const mocks = {
  $mamikosUrl: 'mamikos.com',
  $bugsnag: { notify: jest.fn() },
  $tracker: { send: jest.fn() },
  $device: { isMobile: false },
  $api: {
    getTenants: jest.fn().mockResolvedValue({
      data: {
        status: true,
        tenant_list: tenantList,
        has_more: true,
      },
    }),
  },
};

const store = {
  modules: {
    profile: {
      state: {
        data: {
          user: {
            user_id: 123,
            name: 'Mamikos',
            phone_number: '088888222888',
          },
        },
      },
    },
  },
};

describe('mobile.vue', () => {
  const createWrapper = () =>
    shallowMount(mobile, {
      localVue,
      mocks,
      store: new Vuex.Store(store),
    });

  const wrapper = createWrapper();

  it('should render tenant list properly', async () => {
    await flushPromises();

    expect(wrapper.find('.tenant-list-mobile').exists()).toBe(true);
    expect(wrapper.vm.tenants).toEqual(tenantList);
    expect(wrapper.vm.hasMore).toBe(true);
  });

  it('should send tracker properly', () => {
    expect(mocks.$tracker.send).toBeCalledWith('moe', [
      '[Owner] List Penyewa Visited',
      {
        owner_id: 123,
        owner_name: 'Mamikos',
        owner_phone_number: '088888222888',
        owner_email: '',
        interface: 'desktop',
        redirection_source: '',
      },
    ]);

    mocks.$device.isMobile = true;
    wrapper.vm.trackTenantListPageVisited();
    expect(mocks.$tracker.send).toBeCalledWith('moe', [
      '[Owner] List Penyewa Visited',
      {
        owner_id: 123,
        owner_name: 'Mamikos',
        owner_phone_number: '088888222888',
        owner_email: '',
        interface: 'mobile',
        redirection_source: '',
      },
    ]);
  });

  it('should handle scroll properly', async () => {
    jest.clearAllMocks();

    const newLoadedTenants = [
      {
        tenant_id: 2289,
        tenant_name: 'new',
        tenant_phone_number: '087556565651',
        tenant_photo: null,
        tenant_photo_id: null,
        tenant_photo_identifier: null,
        tenant_photo_identifier_id: null,
        kost_name: 'Kost Gani Putri',
        room_number: '4',
        room_id: 61301818,
        contract_id: 12981,
        transfer_status_raw: 'unpaid',
        transfer_status: 'Belum Dibayar',
      },
    ];

    mocks.$api.getTenants = jest.fn().mockResolvedValue({
      data: {
        status: true,
        has_more: false,
        tenant_list: newLoadedTenants,
      },
    });

    // not scrolled to bottom
    mockWindowProperty('window.innerHeight', 100);
    mockWindowProperty('window.pageYOffset', 50);
    mockWindowProperty('document.body.offsetHeight', 180);
    wrapper.vm.handleWindowScrolled();
    await flushPromises();

    expect(mocks.$api.getTenants).not.toBeCalled();

    // scrolled to bottom but isLoading: true
    await wrapper.setData({ isLoading: true });
    mockWindowProperty('window.innerHeight', 100);
    mockWindowProperty('window.pageYOffset', 50);
    mockWindowProperty('document.body.offsetHeight', 150);
    wrapper.vm.handleWindowScrolled();
    await flushPromises();

    expect(mocks.$api.getTenants).not.toBeCalled();

    // scrolled to bottom
    await wrapper.setData({ isLoading: false });
    mockWindowProperty('window.innerHeight', 100);
    mockWindowProperty('window.pageYOffset', 50);
    mockWindowProperty('document.body.offsetHeight', 150);
    wrapper.vm.handleWindowScrolled();
    await flushPromises();

    expect(wrapper.vm.tenants).toEqual([...tenantList, ...newLoadedTenants]);
    expect(wrapper.vm.hasMore).toBe(false);
    jest.clearAllMocks();

    // scrolled to bottom but has_more: false
    mockWindowProperty('window.innerHeight', 100);
    mockWindowProperty('window.pageYOffset', 50);
    mockWindowProperty('document.body.offsetHeight', 150);
    wrapper.vm.handleWindowScrolled();
    await flushPromises();

    expect(mocks.$api.getTenants).not.toBeCalled();

    jest.clearAllMocks();
  });

  it('should handle error on get tenant list properly', async () => {
    mocks.$api.getTenants = jest.fn().mockResolvedValue({
      data: {
        status: false,
        has_more: false,
        tenant_list: [],
        meta: {
          message: 'Hahaha Error',
        },
      },
    });

    await wrapper.vm.getTenantList({ limit: 0, offset: 2 });
    await flushPromises();
    await wrapper.vm.$nextTick();

    expect(wrapper.find('.tenant-list-mobile__toast').text()).toBe('Hahaha Error');
    expect(wrapper.find('.tenant-list-mobile__toast').attributes('value')).toBe('true');

    await wrapper.setData({ isShowToast: false });
    expect(wrapper.find('.tenant-list-mobile__toast').text()).toBe('');

    mocks.$api.getTenants = jest.fn().mockRejectedValue('Catch Error');
    await wrapper.vm.getTenantList({ limit: 0, offset: 2 });
    await flushPromises();
    await wrapper.vm.$nextTick();

    expect(mocks.$bugsnag.notify).toBeCalledWith('Catch Error');
    expect(wrapper.find('.tenant-list-mobile__toast').attributes('value')).toBe('true');
  });

  it('should remove window scroll event listener before component destroyed', () => {
    const removeEventListener = jest.fn();
    mockWindowProperty('window.removeEventListener', removeEventListener);
    wrapper.vm.$destroy();

    expect(removeEventListener).toBeCalled();
  });

  it('should has correct add tenant url', () => {
    let wrapper = createWrapper();
    wrapper = expect(wrapper.find('.tenant-list-mobile__add-tenant-link').attributes('to')).toBe(
      '/add-tenant/on-boarding',
    );

    global.Cookies.get = jest.fn().mockImplementation(() => 'true');
    wrapper = createWrapper();

    expect(wrapper.find('.tenant-list-mobile__add-tenant-link').attributes('to')).toBe(
      '/add-tenant/select-method',
    );
  });
});
