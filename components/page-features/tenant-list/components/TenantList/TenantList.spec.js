import { createLocalVue, shallowMount } from '@vue/test-utils';
import TenantList from '../TenantList';

const localVue = createLocalVue();

const tenants = [
  {
    tenant_id: 2307,
    tenant_name: 'Chris evans',
    tenant_phone_number: '084574746254',
    tenant_photo: null,
    tenant_photo_id: null,
    tenant_photo_identifier: null,
    tenant_photo_identifier_id: null,
    kost_name: 'Kost Zahira Papua Yogyakarta',
    room_number: '8',
    room_id: 15576845,
    contract_id: 13441,
    transfer_status_raw: 'unpaid',
    transfer_status: 'Belum Dibayar',
  },
  {
    tenant_id: 2280,
    tenant_name: 'chris evans',
    tenant_phone_number: '087556565651',
    tenant_photo: null,
    tenant_photo_id: null,
    tenant_photo_identifier: null,
    tenant_photo_identifier_id: null,
    kost_name: 'Kost Gani Putri',
    room_number: '4',
    room_id: 61301818,
    contract_id: 12981,
    transfer_status_raw: 'unpaid',
    transfer_status: 'Belum Dibayar',
  },
  {
    tenant_id: 2311,
    tenant_name: 'Test Bakwan Bakmi satu',
    tenant_phone_number: '0871263712739',
    tenant_photo: null,
    tenant_photo_id: null,
    tenant_photo_identifier: null,
    tenant_photo_identifier_id: null,
    kost_name: 'Kost Garden Abepura ',
    room_number: 'x3',
    room_id: 66591690,
    contract_id: 13472,
    transfer_status_raw: 'unpaid',
    transfer_status: 'Belum Dibayar',
  },
  {
    tenant_id: 2325,
    tenant_name: 'zayn malik',
    tenant_phone_number: '087556565123',
    tenant_photo: null,
    tenant_photo_id: null,
    tenant_photo_identifier: null,
    tenant_photo_identifier_id: null,
    kost_name: 'Kost Zahira Papua Yogyakarta',
    room_number: '30',
    room_id: 15576845,
    contract_id: 13733,
    transfer_status_raw: 'unpaid',
    transfer_status: 'Belum Dibayar',
  },
];

describe('TenantList.vue', () => {
  const wrapper = shallowMount(TenantList, {
    localVue,
    propsData: {
      isEmpty: true,
    },
  });

  it('should render tenant list', () => {
    expect(wrapper.find('.tenant-list').exists()).toBe(true);
  });

  it('should render empty state when isEmpty is true', () => {
    // expect(wrapper.find('.tenant-list--empty').exists()).toBe(true);
    expect(wrapper.find('.tenant-list__empty-state').exists()).toBe(true);
    expect(wrapper.find('.tenant-list__list').exists()).toBe(false);
  });

  it('should render all tenant item if not empty', async () => {
    await wrapper.setProps({ tenants, isEmpty: false });

    expect(wrapper.find('.tenant-list__list-item').exists()).toBe(true);
    expect(wrapper.findAll('.tenant-list__list-item').length).toBe(tenants.length);
  });

  it('should render loading skeleton when isLoading is true', async () => {
    await wrapper.setProps({ isLoadingMore: true });

    expect(wrapper.find('.tenant-list__loading').exists()).toBe(true);

    await wrapper.setProps({ isLoadingMore: false });

    expect(wrapper.find('.tenant-list__loading').exists()).toBe(false);
  });
});
