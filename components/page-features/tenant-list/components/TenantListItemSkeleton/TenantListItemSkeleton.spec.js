import { createLocalVue, shallowMount } from '@vue/test-utils';
import TenantListItemSkeleton from '../TenantListItemSkeleton';

const localVue = createLocalVue();

describe('TenantListItemSkeleton.vue', () => {
  const wrapper = shallowMount(TenantListItemSkeleton, {
    localVue,
  });

  it('should render tenant list item skeleton properly', () => {
    expect(wrapper.find('.tenant-list-item-skeleton').exists()).toBe(true);

    expect(wrapper.find('.tenant-list-item-skeleton__avatar').attributes()).toEqual(
      expect.objectContaining({ ellipsed: 'true', width: '48px', height: '48px' }),
    );

    expect(wrapper.findAll('.tenant-list-item-skeleton__content > *').length).toBe(2);
  });
});
