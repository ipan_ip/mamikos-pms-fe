import { createLocalVue, shallowMount } from '@vue/test-utils';
import TenantListItem from '../TenantListItem';

const localVue = createLocalVue();

const tenant = {
  tenant_id: 2307,
  tenant_name: 'Chris evans',
  tenant_phone_number: '084574746254',
  tenant_photo: null,
  tenant_photo_id: null,
  tenant_photo_identifier: null,
  tenant_photo_identifier_id: null,
  kost_name: 'Kost Zahira Papua Yogyakarta',
  room_number: '8',
  room_id: 15576845,
  contract_id: 13441,
  transfer_status_raw: 'unpaid',
  transfer_status: 'Belum Dibayar',
};

describe('TenantListItem.vue', () => {
  const wrapper = shallowMount(TenantListItem, {
    localVue,
    propsData: { tenant },
    mocks: {
      $mamikosUrl: 'mamikos.com',
    },
  });

  it('should render tenant list item', () => {
    expect(wrapper.find('.tenant-list-item').exists()).toBe(true);
  });

  it('should set photo src properly', async () => {
    expect(wrapper.find('.tenant-list-item__avatar').attributes('src')).toBe('');

    await wrapper.setProps({
      tenant: { ...tenant, tenant_photo: { small: 'tenant-photo-small.jpg' } },
    });

    expect(wrapper.find('.tenant-list-item__avatar').attributes('src')).toBe(
      'tenant-photo-small.jpg',
    );
  });

  it('should set status label color properly', async () => {
    expect(wrapper.find('.tenant-list-item__details-status').attributes('color')).toBe('red');

    await wrapper.setProps({
      tenant: { ...tenant, transfer_status_raw: 'paid' },
    });

    expect(wrapper.find('.tenant-list-item__details-status').attributes('color')).toBe('green');
  });

  it('should set profile tenant link properly', () => {
    const profileTenantUrl = 'mamikos.com/ownerpage/manage/profile-tenant/13441/biodata-tenant';

    expect(wrapper.find('.tenant-details-name').attributes('href')).toBe(profileTenantUrl);
    expect(wrapper.find('.tenant-list-item__avatar-link').attributes('href')).toBe(
      profileTenantUrl,
    );
  });
});
