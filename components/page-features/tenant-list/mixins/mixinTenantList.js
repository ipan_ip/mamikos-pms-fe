import _get from 'lodash/get';
import Cookies from 'js-cookie';

export default {
  data() {
    return {
      isLoading: false,
      isLoaded: false,
      tenants: [],
      toastMessage: '',
      isShowToast: false,
      offset: 0,
      limit: 20,
      hasMore: true,
    };
  },
  computed: {
    isEmpty() {
      return this.isLoaded && this.tenants.length === 0;
    },
    addTenantRoutePath() {
      return Cookies.get('mami-dbet-onboarding-shown') === 'true'
        ? '/add-tenant/select-method'
        : '/add-tenant/on-boarding';
    },
  },
  watch: {
    isShowToast(isShow) {
      !isShow && (this.toastMessage = '');
    },
  },
  methods: {
    async getTenantList(params) {
      this.isLoading = true;
      try {
        const response = await this.$api.getTenants(params);
        const responseStatus = _get(response, 'data.status', false);
        if (responseStatus) {
          const tenantList = _get(response, 'data.tenant_list') || [];

          this.tenants = [...this.tenants, ...tenantList];
          this.hasMore = !!_get(response, 'data.has_more', false);
          this.offset = params.offset + this.limit;
          this.isLoaded = true;
          this.isLoading = false;
        } else {
          const errorMessage =
            _get(response, 'data.meta.message') || 'Terjadi galat, silakan coba lagi.';
          this.showToast(true, errorMessage);
          this.isLoaded = true;
          this.isLoading = false;
        }
      } catch (error) {
        this.showToast(true, 'Terjadi galat, silakan coba lagi.');
        this.isLoaded = true;
        this.$bugsnag.notify(error);
        this.isLoading = false;
      }
    },
    loadMoreTenants(isTriggered) {
      if (!this.isLoading && this.hasMore && !!isTriggered) {
        this.getTenantList({
          limit: this.limit,
          offset: this.offset,
        });
      }
    },
    showToast(isShow, message) {
      this.toastMessage = message;
      this.isShowToast = isShow;
    },
    getOwnerProfile(key, defaultVal = '') {
      const fullKey = `profile.data.user.${key}`;
      return _get(this.$store.state, fullKey) || defaultVal;
    },
    trackTenantListPageVisited() {
      this.$tracker.send('moe', [
        '[Owner] List Penyewa Visited',
        {
          owner_id: this.getOwnerProfile('user_id', null),
          owner_name: this.getOwnerProfile('name'),
          owner_phone_number: this.getOwnerProfile('phone_number'),
          owner_email: this.getOwnerProfile('email'),
          interface: this.$device.isMobile ? 'mobile' : 'desktop',
          redirection_source: '',
        },
      ]);
    },
  },
};
