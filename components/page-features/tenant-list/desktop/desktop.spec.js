import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import desktop from '../desktop';

jest.mock('js-cookie', () => {
  const Cookies = {
    get: jest.fn().mockImplementation(() => 'undefined'),
  };

  global.Cookies = Cookies;
  return global.Cookies;
});

const flushPromises = () => {
  return new Promise((resolve) => setImmediate(resolve));
};

const localVue = createLocalVue();
localVue.use(Vuex);

const tenantList = [
  {
    tenant_id: 2307,
    tenant_name: 'Chris evans',
    tenant_phone_number: '084574746254',
    tenant_photo: null,
    tenant_photo_id: null,
    tenant_photo_identifier: null,
    tenant_photo_identifier_id: null,
    kost_name: 'Kost Zahira Papua Yogyakarta',
    room_number: '8',
    room_id: 15576845,
    contract_id: 13441,
    transfer_status_raw: 'unpaid',
    transfer_status: 'Belum Dibayar',
  },
  {
    tenant_id: 2280,
    tenant_name: 'chris evans',
    tenant_phone_number: '087556565651',
    tenant_photo: null,
    tenant_photo_id: null,
    tenant_photo_identifier: null,
    tenant_photo_identifier_id: null,
    kost_name: 'Kost Gani Putri',
    room_number: '4',
    room_id: 61301818,
    contract_id: 12981,
    transfer_status_raw: 'unpaid',
    transfer_status: 'Belum Dibayar',
  },
];

const stubs = {
  TenantList: {
    template: `<div />`,
    methods: {
      scroll(params) {
        this.$emit('scrolled', { target: { ...params } });
      },
    },
  },
};

const mocks = {
  $mamikosUrl: 'mamikos.com',
  $bugsnag: { notify: jest.fn() },
  $tracker: { send: jest.fn() },
  $device: { isMobile: false },
  $api: {
    getTenants: jest.fn().mockResolvedValue({
      data: {
        status: true,
        tenant_list: tenantList,
        has_more: true,
      },
    }),
  },
};

const store = {
  modules: {
    profile: {
      state: {
        data: {
          user: {
            user_id: 123,
            name: 'Mamikos',
            phone_number: '088888222888',
            email: 'mamikos@mamikos.com',
          },
        },
      },
    },
  },
};

describe('desktop.vue', () => {
  const createWrapper = () =>
    shallowMount(desktop, {
      localVue,
      mocks,
      stubs,
      store: new Vuex.Store(store),
    });

  const wrapper = createWrapper();

  it('should render tenant list properly', async () => {
    await flushPromises();

    expect(wrapper.find('.tenant-list').exists()).toBe(true);
    expect(wrapper.vm.tenants).toEqual(tenantList);
    expect(wrapper.vm.hasMore).toBe(true);
  });

  it('should send tracker properly', () => {
    expect(mocks.$tracker.send).toBeCalledWith('moe', [
      '[Owner] List Penyewa Visited',
      {
        owner_id: 123,
        owner_name: 'Mamikos',
        owner_phone_number: '088888222888',
        owner_email: 'mamikos@mamikos.com',
        interface: 'desktop',
        redirection_source: '',
      },
    ]);

    mocks.$device.isMobile = true;
    wrapper.vm.trackTenantListPageVisited();
    expect(mocks.$tracker.send).toBeCalledWith('moe', [
      '[Owner] List Penyewa Visited',
      {
        owner_id: 123,
        owner_name: 'Mamikos',
        owner_phone_number: '088888222888',
        owner_email: 'mamikos@mamikos.com',
        interface: 'mobile',
        redirection_source: '',
      },
    ]);
  });

  it('should handle scroll properly', async () => {
    jest.clearAllMocks();
    const newLoadedTenants = [
      {
        tenant_id: 2289,
        tenant_name: 'new',
        tenant_phone_number: '087556565651',
        tenant_photo: null,
        tenant_photo_id: null,
        tenant_photo_identifier: null,
        tenant_photo_identifier_id: null,
        kost_name: 'Kost Gani Putri',
        room_number: '4',
        room_id: 61301818,
        contract_id: 12981,
        transfer_status_raw: 'unpaid',
        transfer_status: 'Belum Dibayar',
      },
    ];

    mocks.$api.getTenants = jest.fn().mockResolvedValue({
      data: {
        status: true,
        has_more: false,
        tenant_list: newLoadedTenants,
      },
    });

    // not scrolled to bottom
    await wrapper
      .find(stubs.TenantList)
      .vm.scroll({ scrollTop: 100, scrollHeight: 300, clientHeight: 100 });
    await flushPromises();

    expect(mocks.$api.getTenants).not.toBeCalled();

    // scrolled to bottom but isLoading: true
    await wrapper.setData({ isLoading: true });
    await wrapper
      .find(stubs.TenantList)
      .vm.scroll({ scrollTop: 200, scrollHeight: 300, clientHeight: 100 });
    await flushPromises();

    expect(mocks.$api.getTenants).not.toBeCalled();
    jest.clearAllMocks();

    // scrolled to bottom
    await wrapper.setData({ isLoading: false });
    await wrapper
      .find(stubs.TenantList)
      .vm.scroll({ scrollTop: 200, scrollHeight: 300, clientHeight: 100 });
    await flushPromises();

    expect(wrapper.vm.tenants).toEqual([...tenantList, ...newLoadedTenants]);
    expect(wrapper.vm.hasMore).toBe(false);
    jest.clearAllMocks();

    // scrolled to bottom but has_more: false
    await wrapper
      .find(stubs.TenantList)
      .vm.scroll({ scrollTop: 200, scrollHeight: 300, clientHeight: 100 });

    expect(mocks.$api.getTenants).not.toBeCalled();
  });

  it('should handle get tenant list data properly', async () => {
    wrapper.setData({ tenants: [{ ...tenantList[0] }] });
    mocks.$api.getTenants = jest.fn().mockResolvedValue({
      data: {
        status: true,
        has_more: false,
      },
    });

    await wrapper.vm.getTenantList({ limit: 1, offset: 1 });
    await flushPromises();
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.tenants.length).toBe(1);
  });

  it('should handle error on get tenant list properly', async () => {
    mocks.$api.getTenants = jest.fn().mockResolvedValue({
      data: {
        status: false,
        has_more: false,
        tenant_list: [],
        meta: {
          message: 'Hahaha Error',
        },
      },
    });

    await wrapper.vm.getTenantList({ limit: 0, offset: 2 });
    await flushPromises();
    await wrapper.vm.$nextTick();

    expect(wrapper.find('.tenant-list__toast').text()).toBe('Hahaha Error');
    expect(wrapper.find('.tenant-list__toast').attributes('value')).toBe('true');

    await wrapper.setData({ isShowToast: false });
    mocks.$api.getTenants = jest.fn().mockResolvedValue({
      data: {
        status: false,
        has_more: false,
        tenant_list: [],
      },
    });

    await wrapper.vm.getTenantList({ limit: 0, offset: 2 });
    await flushPromises();
    await wrapper.vm.$nextTick();

    expect(wrapper.find('.tenant-list__toast').text()).toBe('Terjadi galat, silakan coba lagi.');
    expect(wrapper.find('.tenant-list__toast').attributes('value')).toBe('true');

    await wrapper.setData({ isShowToast: false });
    expect(wrapper.find('.tenant-list__toast').text()).toBe('');

    mocks.$api.getTenants = jest.fn().mockRejectedValue('Catch Error');
    await wrapper.vm.getTenantList({ limit: 0, offset: 2 });
    await flushPromises();
    await wrapper.vm.$nextTick();

    expect(mocks.$bugsnag.notify).toBeCalledWith('Catch Error');
    expect(wrapper.find('.tenant-list__toast').attributes('value')).toBe('true');
  });

  it('should has correct add tenant url', () => {
    let wrapper = createWrapper();
    wrapper = expect(wrapper.find('.tenant-list__add-tenant-link').attributes('to')).toBe(
      '/add-tenant/on-boarding',
    );

    global.Cookies.get = jest.fn().mockImplementation(() => 'true');
    wrapper = createWrapper();

    expect(wrapper.find('.tenant-list__add-tenant-link').attributes('to')).toBe(
      '/add-tenant/select-method',
    );
  });
});
