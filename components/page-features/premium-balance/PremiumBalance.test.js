import { shallowMount } from '@vue/test-utils';
import PremiumBalance from './PremiumBalance';
import localVueWithBuefy from '~/utils/addBuefy';
import MkButton from '~/components/global/atoms/MkButton';

const $device = {
  isMobile: false,
  isDesktop: true,
};

const params = {
  dailyBudget: null,
  balance: null,
};

const $api = {
  allocateDailyBudget: jest
    .fn()
    .mockResolvedValueOnce({ data: { status: true, message: 'message' } })
    .mockResolvedValueOnce({ data: { status: false, message: 'message' } })
    .mockRejectedValueOnce(),
};

const mount = () =>
  shallowMount(PremiumBalance, {
    localVue: localVueWithBuefy,
    mocks: {
      $route: {
        params,
        meta: {
          type: 'kos',
        },
      },
      $router: {
        push: jest.fn(),
      },
      $buefy: { toast: { open: jest.fn() } },
      $api,
      $device,
    },
    stubs: {
      MkButton,
    },
  });
describe('PremiumBalance', () => {
  let wrapper;
  it('mount properly', () => {
    wrapper = mount();
    expect(wrapper.vm.$router.push).toBeCalled();

    // set params in order to mount properly
    params.balance = 1000;
    params.dailyBudget = 1000;

    // Desktop view
    wrapper = mount();
    expect(wrapper.find('.premium-balance__header').exists()).toBeTruthy();

    // Mobile View
    $device.isDesktop = false;
    $device.isMobile = true;
    wrapper = mount();
    expect(wrapper.find('.premium-balance__header').exists()).toBeFalsy();
  });

  it('should disabled submit button when dailyBudget is less than 5000', async () => {
    wrapper.vm.dailyBudget = 3000;
    await wrapper.vm.$nextTick();
    expect(wrapper.find('.premium-balance__submit').attributes('disabled')).toBeTruthy();
  });

  it('should submit allocation', async () => {
    wrapper.vm.isDirty = true;
    wrapper.vm.dailyBudget = 5000;
    await wrapper.vm.$nextTick();
    wrapper.find('.premium-balance__submit').trigger('click');
    expect(wrapper.vm.isLoading).toBeTruthy();
    await Promise.resolve;
    expect(wrapper.vm.isLoading).toBeFalsy();
    expect(wrapper.vm.$router.push).toBeCalled();

    // wait until button re-appears
    await wrapper.vm.$nextTick();

    // return status false
    wrapper.find('.premium-balance__submit').trigger('click');
    await Promise.resolve;
    expect(wrapper.vm.$buefy.toast.open).toBeCalled();

    // wait until button re-appears
    await wrapper.vm.$nextTick();

    // return error response
    wrapper.find('.premium-balance__submit').trigger('click');
    await Promise.reject;
    expect(wrapper.vm.$buefy.toast.open).toBeCalled();
  });
});
