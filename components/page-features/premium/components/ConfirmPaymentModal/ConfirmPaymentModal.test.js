import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import ConfirmPaymentModal from './ConfirmPaymentModal';
import localVueWithBuefy from '~/utils/addBuefy';

const $device = {
  isMobile: false,
  isDesktop: true,
};

localVueWithBuefy.use(Vuex);

const mount = () => {
  const mountData = {
    ...{
      localVue: localVueWithBuefy,
      mocks: {
        $device,
      },
    },
  };

  return shallowMount(ConfirmPaymentModal, mountData);
};

describe('ConfirmPaymentModal.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should show the modal', () => {
    wrapper.vm.$device.isMobile = true;
    expect(wrapper.find('.confirm-payment-modal').exists()).toBe(true);
  });

  it('should close the modal by emitting toggle with false value', () => {
    wrapper.vm.closeModal();
    expect(wrapper.emitted('input')[0][0]).toBe(false);
  });
});
