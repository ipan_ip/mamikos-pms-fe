import { shallowMount, createLocalVue } from '@vue/test-utils';
import { BgModal, BgButton } from 'bangul-vue';
import HasNoPropertyModal from './HasNoPropertyModal';

const localVue = createLocalVue();

describe('HasNoPropertyModal.vue', () => {
  const propsData = {
    value: true,
  };

  const wrapper = shallowMount(HasNoPropertyModal, {
    localVue,
    propsData,
    stubs: { BgModal, BgButton },
  });

  it('should show the modal', () => {
    expect(wrapper.find('.has-no-property-modal').exists()).toBe(true);
  });

  it('should close the modal by emitting toggle with false value', () => {
    wrapper.vm.closeModal();
    expect(wrapper.emitted('input')[0][0]).toBe(false);
  });
});
