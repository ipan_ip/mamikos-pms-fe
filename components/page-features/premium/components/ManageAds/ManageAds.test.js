import { shallowMount } from '@vue/test-utils';
import ManageAds from './ManageAds';
import localVueWithBuefy from '~/utils/addBuefy';

import ModalTopAds from '~/components/page-features/premium/components/ModalTopAds/ModalTopAds';

const $api = {
  getRoomData: jest
    .fn()
    .mockResolvedValue({
      data: {
        status: true,
        room: {
          _id: 12345,
          premium_daily_budget: 10000,
          status_promote: 1,
          gender: 0,
          promo_status: 'expired',
          photo_url: { small: 'static.mamikos.com' },
        },
        membership: {
          views: 5000,
        },
      },
    })
    .mockResolvedValueOnce({
      data: {
        status: false,
      },
    })
    .mockRejectedValueOnce(),
  getRoomStatisticsReport: jest
    .fn()
    .mockResolvedValue({
      data: {
        view_ads_count: 20,
        love_count: 3,
        used_balance: 2000,
      },
    })
    .mockRejectedValueOnce(),
  deactivatePremiumAllocation: jest
    .fn()
    .mockResolvedValueOnce({ data: { status: true, message: 'success' } })
    .mockResolvedValueOnce({ data: { status: false, message: 'failed' } })
    .mockRejectedValueOnce(),
  activatePremiumAllocation: jest
    .fn()
    .mockResolvedValueOnce({ data: { status: true, message: 'success' } })
    .mockResolvedValueOnce({ data: { status: false, message: 'failed' } })
    .mockRejectedValueOnce(),
};
const topAdsModal = jest.fn().mockResolvedValue(null);
const $alert = jest.fn((messages) => messages);
const $route = { meta: { type: 'kos' }, params: { id: 333 } };
const $router = { push: jest.fn() };
const $buefy = { toast: { open: jest.fn() } };
const $bugsnag = { notify: jest.fn() };

const mountData = {
  localVue: localVueWithBuefy,
  mocks: {
    $api,
    $alert,
    $route,
    $router,
    $buefy,
    $bugsnag,
    topAdsModal,
  },
  stubs: {
    ModalTopAds,
  },
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };
  return shallowMount(ManageAds, finalData);
};

delete window.location;
window.location = { assign: jest.fn() };

describe('ManageAds', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', async () => {
    wrapper.setData({ property: { gender: 0 } });
    wrapper.setData({ property: { promo_status: 'expired' } });
    wrapper.setData({ property: { photo_url: { small: 'static.mamikos.com' } } });
    wrapper.vm.isLoading = false;

    await wrapper.vm.$nextTick();
    expect(wrapper.find('.manage-ads__ads-report').exists()).toBeTruthy();
    expect(wrapper.vm.roomImageUrl).toEqual('static.mamikos.com');
    expect(wrapper.vm.promoStatus).toEqual({
      status: 'expired',
      text: 'Kedaluwarsa',
      style: 'expired',
    });
    expect(wrapper.vm.promoStatus).toEqual({
      status: 'expired',
      text: 'Kedaluwarsa',
      style: 'expired',
    });
  });

  it('should show alert then redirect user to kos page when room not found', () => {
    expect($alert).toHaveBeenCalledWith('is-warning', 'Gagal', 'Data tidak ditemukan.');
    expect($bugsnag.notify).toHaveBeenCalled();
    expect(window.location.assign).toHaveBeenCalledWith(
      `${process.env.MAMIKOS_URL}/ownerpage/apartment`,
    );
  });

  it('should get SwitchState properly', () => {
    wrapper.vm.getSwitchState(1);
    expect(wrapper.vm.topAdsSwitchState).toBeTruthy();
    wrapper.vm.getSwitchState(0);
    expect(wrapper.vm.topAdsSwitchState).toBeFalsy();
  });

  it('should set switchState properly', () => {
    wrapper.vm.setSwitchState(false);
    expect(wrapper.vm.topAdsSwitchState).toBeFalsy();
  });

  it('should get room statistics properly', () => {
    wrapper.vm.getRoomStatisticsReport();
    expect(wrapper.vm.roomStatistic.view).toBe(20);
    expect(wrapper.vm.roomStatistic.love).toBe(3);
    expect(wrapper.vm.roomStatistic.balanceUsed).toBe(2000);
  });

  it('should handle top ads switch deactivation properly', async () => {
    wrapper.find('.switch').trigger('click');
    // handle deactivation
    wrapper.vm.handleTopAdsSwitch(false);
    expect(wrapper.vm.showTopAdsModal).toBeTruthy();

    const openToastSpy = jest.spyOn(wrapper.vm, 'openToast');
    wrapper.setMethods({
      getTopAdsModalDisplay: jest.fn(() => {
        return false;
      }),
    });

    wrapper.vm.handleTopAdsSwitch(false);
    await Promise.resolve;
    await wrapper.vm.$nextTick();
    expect(openToastSpy).toBeCalledWith('success');

    wrapper.vm.handleTopAdsSwitch(false);
    await Promise.resolve;
    await wrapper.vm.$nextTick();
    expect(openToastSpy).toBeCalledWith('failed');
    openToastSpy.mockRestore();

    wrapper.vm.handleTopAdsSwitch(false);
    await Promise.reject;
    expect($bugsnag.notify).toBeCalled();
  });

  it('should handle top ads switch activation properly', async () => {
    wrapper.vm.handleTopAdsSwitch(true);

    await Promise.resolve;
    expect(wrapper.vm.adsBalance).toBe(5000);

    // if premium_daily_budget less than 5000, redirect to premiumBalance
    wrapper.setData({ property: { premium_daily_budget: 1000 } });
    const goToPremiumBalanceSpy = jest.spyOn(wrapper.vm, 'goToPremiumBalance');
    wrapper.vm.handleTopAdsSwitch(true);
    await wrapper.vm.$nextTick();
    expect(goToPremiumBalanceSpy).toBeCalled();
  });

  it('should call getRoomStatisticsReport if timeSelected value changes ', async () => {
    const getRoomStatisticsReportSpy = jest.spyOn(wrapper.vm, 'getRoomStatisticsReport');
    wrapper.vm.timeSelected = 2;
    await wrapper.vm.$nextTick();
    expect(getRoomStatisticsReportSpy).toBeCalled();
  });

  it('should handle getTopAdsModalDisplay properly', () => {
    wrapper.vm.getTopAdsModalDisplay();
    expect(wrapper.vm.getTopAdsModalDisplay()).toBe(true);
  });

  it('should open buefy toast properly', async () => {
    await wrapper.vm.openToast();
    expect($buefy.toast.open).toBeCalled();
  });

  it('should redirect into premium balance properly', async () => {
    await wrapper.vm.goToPremiumBalance();
    expect($router.push).toBeCalled();
  });
});
