import { shallowMount } from '@vue/test-utils';
import ModalPriceUpdate from './ModalPriceUpdate';
import localVueWithBuefy from '~/utils/addBuefy';

describe('ModalPriceUpdate.vue', () => {
  const propsData = {
    active: true,
  };

  const wrapper = shallowMount(ModalPriceUpdate, {
    localVue: localVueWithBuefy,
    propsData,
    mocks: { $router: { push: jest.fn() } },
  });

  it('should close the modal', () => {
    wrapper.vm.closeModal();
    expect(wrapper.emitted('close')).toBeTruthy();
  });

  it('should go to the update price route', () => {
    const closeModalSpy = jest.spyOn(wrapper.vm, 'closeModal');

    wrapper.vm.updateRoomsPrice();
    expect(closeModalSpy).toBeCalled();
    expect(wrapper.vm.$router.push).toBeCalledWith('/kos/rooms-price');
  });
});
