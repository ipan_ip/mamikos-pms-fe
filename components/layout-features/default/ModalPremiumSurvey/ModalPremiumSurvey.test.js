import Vuex from 'vuex';
import { shallowMount } from '@vue/test-utils';
import ModalPremiumSurvey from './ModalPremiumSurvey';
import localVueWithBuefy from '~/utils/addBuefy';

import MkModal, { mixin as modalMixin } from '~/components/global/molecules/MkModal/MkModal';
import MkCard from '~/components/global/molecules/MkCard';
import MkCardBody from '~/components/global/molecules/MkCard/components/MkCardBody';
import MkButton from '~/components/global/atoms/MkButton';

const propsData = {
  active: true,
};

const stubs = {
  MkModal,
  MkCard,
  MkCardBody,
  MkButton,
  BCheckbox: { template: '<input type="checkbox"/>' },
};

const $api = {
  submitPremiumSurvey: jest
    .fn()
    .mockResolvedValueOnce({
      data: { status: true },
    })
    .mockResolvedValueOnce({ data: { status: false, meta: { message: 'failed' } } })
    .mockRejectedValueOnce(),
};

const $alert = jest.fn((messages) => messages);

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        data: {
          survey: ['Kamar masih terisi penuh', 'Harga dan keuntungan tidak sesuai'],
        },
      },
      getters: {
        membership: () => {
          return {
            active_package: 43061,
            package_name: 'Paket 4 Bulan Premium',
          };
        },
      },
    },
  },
};

localVueWithBuefy.use(Vuex);
const store = new Vuex.Store(storeData);

const mount = () => {
  return shallowMount(ModalPremiumSurvey, {
    localVue: localVueWithBuefy,
    mixin: [modalMixin],
    propsData,
    mocks: {
      $api,
      $alert,
    },
    stubs,
    store,
  });
};

describe('ModalPremiumSurvey', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.premium-survey-modal').exists()).toBeTruthy();
    expect(wrapper.vm.packageId).toBe(43061);
    expect(wrapper.vm.packageName).toBe('Paket 4 Bulan Premium');
  });

  it('should submit survey properly', async () => {
    wrapper.vm.reasons = ['Kamar masih terisi penuh', 'Harga dan keuntungan tidak sesuai'];
    wrapper.vm.isSendingData = false;
    await wrapper.vm.$nextTick();
    wrapper.find('.premium-survey-modal__submit').trigger('click');
    expect(wrapper.vm.isSendingData).toBeTruthy();
    await wrapper.vm.$api.submitPremiumSurvey;
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isSendingData).toBeFalsy();
    expect(wrapper.vm.isShowCard).toBeFalsy();
  });

  it('should show alert if submitted survey failed', async () => {
    wrapper.vm.reasons = ['Kamar masih terisi penuh', 'Harga dan keuntungan tidak sesuai'];
    wrapper.vm.isSendingData = false;
    await wrapper.vm.$nextTick();
    wrapper.find('.premium-survey-modal__submit').trigger('click');
    expect(wrapper.vm.$alert).toBeCalled();
  });
});
