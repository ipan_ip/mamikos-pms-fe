import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import MkSidebar from './MkSidebar';
import mockWindowProperty from '~/utils/mockWindowProperty';

const localVue = createLocalVue();
localVue.use(Vuex);

const sidebarItems = [
  {
    name: 'Home',
    type: 'internal',
    data: '/',
    urlNames: ['index'],
    icon: 'home',
  },
  {
    name: 'Chat',
    type: 'external',
    data: '/chat',
    urlNames: [],
    icon: 'chat',
    counterFn() {
      return 5;
    },
  },
  {
    name: 'Manajemen Kos',
    id: 'kosManagement',
    data: '/',
    urlNames: [],
    icon: 'kos-management',
    children: [
      {
        name: 'Pengajuan Booking',
        type: 'action',
        urlNames: [],
        data(context) {
          context.$test();
        },
      },
      {
        name: 'Laporan Keuangan',
        data: '/',
        type: 'internal',
        urlNames: [],
      },
      {
        name: 'Kelola Tagihan',
        type: 'internal',
        data: `/billing-management`,
        urlNames: ['billing-management'],
      },
      {
        name: 'Penyewa',
        data: '/',
        type: 'internal',
        urlNames: [],
      },
    ],
    isHiddenFn(context) {
      return !!context.$store.state.hideKosManagement;
    },
  },
];

mockWindowProperty('window.location', { href: '' });

const mocks = {
  $router: {
    push: jest.fn(),
  },
  $route: { name: 'index' },
  $test: jest.fn(),
};

const stubs = {
  BgSidebar: { template: '<div><slot /></div>' },
  BgSidebarItem: { template: `<div @click="$emit('click')"><slot /></div>` },
  BgSidebarItemMulti: { template: '<div><slot /></div>' },
};

describe('MkSidebar.vue', () => {
  const wrapper = shallowMount(MkSidebar, {
    localVue,
    mocks,
    stubs,
    propsData: { items: [] },
    store: new Vuex.Store({ state: { hideKosManagement: false } }),
  });

  const getSidebarItems = () => wrapper.findAll('.c-mk-sidebar__item');
  const getSidebarItemMulti = () => wrapper.findAll('.c-mk-sidebar__item-multiple');
  const getSidebarItemMultiChild = () => wrapper.findAll('.c-mk-sidebar__item-multiple-child');

  it('should render MkSidebar', () => {
    expect(wrapper.find('.c-mk-sidebar').exists()).toBe(true);
  });

  it('should render sidebar content properly', async () => {
    await wrapper.setProps({ items: [...sidebarItems] });

    expect(getSidebarItems().length).toBe(2);
    expect(getSidebarItemMulti().length).toBe(1);
    expect(getSidebarItemMultiChild().length).toBe(4);
  });

  it('should handle multi items opened state properly', async () => {
    wrapper.vm.$route.name = 'billing-management';
    await wrapper.vm.$nextTick();

    expect(
      getSidebarItemMulti()
        .at(0)
        .attributes('opened'),
    ).toBe('true');
  });

  it('should handle hide item properly', async () => {
    await wrapper.setProps({ items: [...sidebarItems] });

    wrapper.vm.$store.state.hideKosManagement = true;
    await wrapper.vm.$nextTick();

    expect(getSidebarItems().length).toBe(2);
    expect(getSidebarItemMulti().length).toBe(0);
    expect(wrapper.find('#sidebarItemkosManagement').exists()).toBe(false);
  });

  it('should handle counter properly', async () => {
    const chatCounterSidebarItems = [...sidebarItems];
    delete chatCounterSidebarItems[1].counterFn;
    chatCounterSidebarItems[1].counter = 2;

    await wrapper.setProps({ items: [...chatCounterSidebarItems] });
    expect(
      getSidebarItems()
        .at(1)
        .attributes('counter'),
    ).toBe('2');
  });

  it('should handle sidebar item click properly', async () => {
    wrapper.vm.$store.state.hideKosManagement = false;
    await wrapper.vm.$nextTick();

    getSidebarItems()
      .at(0)
      .trigger('click');
    await wrapper.vm.$nextTick();

    expect(mocks.$router.push).toBeCalledWith('/');

    getSidebarItems()
      .at(1)
      .trigger('click');
    await wrapper.vm.$nextTick();

    expect(window.location.href).toBe('/chat');

    getSidebarItemMultiChild()
      .at(0)
      .trigger('click');

    await wrapper.vm.$nextTick();
    expect(mocks.$test).toBeCalled();
  });
});
