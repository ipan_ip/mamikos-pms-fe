import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import MkBottomNavbar from './MkBottomNavbar';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('MkBottomNavbar.vue', () => {
  const $router = { push: jest.fn() };
  const $route = { path: '/' };

  const mockStore = {
    modules: {
      profile: {
        namespaced: true,
        state: {
          unreadMessages: 50,
          data: { user_id: 12345, cs_id: 123 },
        },
        getters: {
          user(state) {
            const { user = {} } = state.data;
            return user;
          },
          membership(state) {
            const { membership = {} } = state.data;
            return membership;
          },
        },
      },
    },
  };

  const wrapper = shallowMount(MkBottomNavbar, {
    localVue,
    store: new Vuex.Store(mockStore),
    mocks: { $router, $route },
  });

  it('should load the component', () => {
    expect(wrapper.find('.c-mk-bottom-navbar').exists()).toBe(true);
  });

  it('should open the chat room', () => {
    const menuSelected = {
      label: 'Chat',
      icon: 'chat',
      showBadge: false,
      badgeNumber: 0,
      route: null,
    };

    const sendbirdChatRoom = {
      handleSbWidgetButtonClicked: jest.fn(),
      sbWidgetButton: { click: jest.fn() },
    };
    wrapper.vm.$refs = { sendbirdChatRoom };

    wrapper.vm.changeMenu(menuSelected);
    expect(wrapper.vm.$refs.sendbirdChatRoom.handleSbWidgetButtonClicked).toBeCalled();
    expect(wrapper.vm.$refs.sendbirdChatRoom.sbWidgetButton.click).toBeCalled();
  });

  it('should show the home page', () => {
    const menuSelected = {
      label: 'Home',
      icon: 'home',
      showBadge: false,
      badgeNumber: 0,
      route: '/',
    };

    const sendbirdChatRoom = {
      handleCloseClicked: jest.fn(),
    };
    wrapper.vm.$refs = { sendbirdChatRoom };

    wrapper.vm.changeMenu(menuSelected);
    expect($router.push).toBeCalled();
  });
});
