import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import MkHeader from './MkHeader';
import localVueWithBuefy from '~/utils/addBuefy';

localVueWithBuefy.use(Vuex);

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        data: {
          gp_status: '',
          is_mamirooms: '',
        },
      },
      getters: {
        user: () => {
          return {
            name: '',
            phone_number: '',
            email: '',
          };
        },
        isBbk: () => jest.fn(),
        isPremium: () => jest.fn(),
      },
      actions: {
        getNotifications: () => jest.fn(),
      },
    },
  },
};

describe('MkHeader.vue', () => {
  const store = new Vuex.Store(storeData);
  const propsData = { notification: { count: 1 } };
  const $event = { stopPropagation: jest.fn() };
  const $tracker = { send: jest.fn() };
  const $device = { isMobile: false };

  const wrapper = shallowMount(MkHeader, {
    localVue: localVueWithBuefy,
    propsData,
    store,
    mocks: { $tracker, $device },
  });

  it('should mount the component', () => {
    expect(wrapper.find('.c-mk-header').exists()).toBe(true);
  });

  it('should close the user dropdown and show the notification dropdown', () => {
    wrapper.vm.handleMenuClicked($event, 'isNotificationDropdownOpened', 'notificationMenu');
    expect(wrapper.vm.isUserDropdownOpened).toBe(false);
    expect(wrapper.vm.isNotificationDropdownOpened).toBe(true);
  });

  it('should close the notification dropdown and show the user dropdown', () => {
    wrapper.vm.handleMenuClicked($event, 'isUserDropdownOpened', 'userMenu');
    expect(wrapper.vm.isNotificationDropdownOpened).toBe(false);
    expect(wrapper.vm.isUserDropdownOpened).toBe(true);
  });
});
