import MkSendbird from './MkSendbird';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
jest.mock('~/utils/addTag', () => ({
  prependScript: jest.fn((url, callback) => callback()),
}));

const localVue = createLocalVue();
localVue.use(Vuex);

const handleCloseClickedSpy = jest.spyOn(MkSendbird.methods, 'handleCloseClicked');

const mutations = {
  updateUnreadMessages: jest.fn(),
};

const storeData = {
  modules: {
    profile: {
      namespaced: true,
      state: {
        data: {
          gp_status: '',
          is_mamirooms: '',
        },
      },
      getters: {
        isBbk: () => jest.fn(),
        isPremium: () => jest.fn(),
      },
      mutations,
    },
  },
};

const sendBirdConnectSpy = jest.fn();
const removeEventSpy = jest.fn();
const toggleBoardSpy = jest.fn();
const channelList = [];
const userSendbird = {};

global.prependScript = jest.fn();
global.SendBird = jest.fn(() => ({
  connect: sendBirdConnectSpy,
  isSessionOpened: true,
  GroupChannel: {
    createMyGroupChannelListQuery: jest.fn(() => ({
      includeEmpty: true,
      limit: 0,
      hasNext: true,
      next: jest.fn((callback) => callback(channelList, null)),
    })),
    getTotalUnreadChannelCount: jest.fn((fn) => fn(3, null)),
  },
}));
global.window = {
  addEventListener: jest.fn(),
  removeEventListener: removeEventSpy,
};
global.location = {
  origin: 'https://owner-jambu.kerupux.com',
};

describe('MkSendbird.vue', () => {
  const store = new Vuex.Store(storeData);
  const propsData = { userId: 123 };
  const $tracker = { send: jest.fn() };
  let $device = { isMobile: false, isDesktop: true };
  const $bugsnag = {
    notify: jest.fn(),
  };
  let wrapper = null;

  const mountWrapper = (options) => {
    wrapper = shallowMount(MkSendbird, {
      localVue,
      propsData,
      store,
      mocks: { $tracker, $device, $bugsnag },
      ...options,
    });
  };

  mountWrapper();

  it('Mutations updateUnreadMessages should be called correctly when the total message data is changed', async () => {
    await wrapper.setData({ totalMessage: 1 });

    expect(mutations.updateUnreadMessages).toBeCalled();
  });

  it('Method connectSbWidget should handled correctly', () => {
    const testCase = [
      {
        sbWidget: null,
      },
      {
        sbWidget: {
          startWithConnect: jest.fn((appId, userId, userName, callback) => callback()),
          registerLocalHandlerCb: jest.fn((event, callback) => callback()),
          toggleBoard: toggleBoardSpy,
        },
      },
    ];

    for (const test of testCase) {
      global.sbWidget = test.sbWidget;

      mountWrapper();

      wrapper.vm.connectSbWidget();

      if (test.sbWidget) expect(sendBirdConnectSpy).toBeCalled();
    }
  });

  it('Method connectSbWidget should handled error correctly', () => {
    global.document = {
      querySelector: jest.fn().mockReturnValue(null),
    };
    global.SendBird = jest.fn(() => ({
      connect: jest.fn((id, callback) => callback(userSendbird, 'error')),
      isSessionOpened: true,
      GroupChannel: {
        createMyGroupChannelListQuery: jest.fn(() => ({
          includeEmpty: true,
          limit: 0,
          hasNext: true,
          next: jest.fn((callback) => callback(channelList, 'error')),
        })),
        getTotalUnreadChannelCount: jest.fn((fn) => fn(3, null)),
      },
    }));

    mountWrapper();

    wrapper.vm.connectSbWidget();
  });

  it('Method handleCloseClicked should be called correctly when user click the close button', () => {
    const testCase = [
      {
        isMobile: true,
        isDesktop: false,
      },
      {
        isMobile: false,
        isDesktop: true,
      },
    ];

    for (const test of testCase) {
      $device = { isMobile: test.isMobile, isDesktop: test.isDesktop };

      mountWrapper({
        data() {
          return {
            isShow: true,
          };
        },
      });

      const closeElement = wrapper.find('span.mk-sb-widget__close');

      expect(closeElement.exists()).toBe(true);

      closeElement.trigger('click');

      expect(handleCloseClickedSpy).toBeCalled();
      if (test.isMobile) {
        expect(toggleBoardSpy).toBeCalled();
      } else {
        expect(wrapper.emitted('handle-toggle-chat')).toBeTruthy();
      }
    }
  });

  it('Method handleSbWidgetButtonClicked should be handled correctly and open the chat when user click it', () => {
    /* I can't find any related element that call this method but still I leave it as it is since I don't know if it's has another purpose */

    wrapper.vm.handleSbWidgetButtonClicked();

    expect(wrapper.vm.isShow).toBe(true);
  });

  it('Should add and remove listener correctly', () => {
    $device = { isMobile: true, isDesktop: false };

    mountWrapper();

    wrapper.setData({
      sbWidgetButton: {
        removeEventListener: removeEventSpy,
      },
    });
    wrapper.vm.connectSbWidget();
    wrapper.destroy();

    expect(removeEventSpy).toBeCalled();
  });
});
