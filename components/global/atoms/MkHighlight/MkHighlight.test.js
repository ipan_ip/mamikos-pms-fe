import { createLocalVue, shallowMount } from '@vue/test-utils';
import MkHighlight from '../MkHighlight';

const localVue = createLocalVue();

describe('MkHighlight', () => {
  const wrapper = shallowMount(MkHighlight, {
    localVue,
    propsData: {
      width: 200,
      height: 200,
      placement: 'top',
    },
  });

  const highlight = () => wrapper.find('.highlight-info');

  it('should render highlight', () => {
    expect(highlight().exists()).toBe(true);
  });

  it('should set style properly', async () => {
    expect(highlight().element.style.height).toBe('200px');
    expect(highlight().element.style.width).toBe('200px');

    await wrapper.setProps({ height: 0, width: 0 });
    expect(highlight().element.style.height).toBe('');
    expect(highlight().element.style.width).toBe('');
  });

  it('should set placement properly', async () => {
    await wrapper.setProps({
      width: 100,
      height: 100,
      placement: 'top',
    });

    wrapper.vm.setPlacement();

    expect(highlight().element.style.top).toBe('-25px');

    await wrapper.setProps({
      width: 100,
      height: 100,
      placement: 'left',
    });

    wrapper.vm.setPlacement();

    expect(highlight().element.style.left).toBe('-25px');
  });
});
