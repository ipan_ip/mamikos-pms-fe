import { shallowMount } from '@vue/test-utils';
import MkBadge from './MkBadge';

describe('MkBadge.vue', () => {
  const wrapper = shallowMount(MkBadge);

  const validatorType = MkBadge.props.type.validator;

  it('should mount properly', () => {
    const findBadge = () => wrapper.find('.c-mk-badge');
    expect(findBadge().exists()).toBe(true);
  });

  describe('validator props type', () => {
    it('should failed with incorrect type', () => {
      expect(validatorType('failed')).toBe(false);
    });

    it('should passes with correct type', () => {
      expect(validatorType('success')).toBe(true);
      expect(validatorType('error')).toBe(true);
    });
  });
});
