import { createLocalVue, shallowMount } from '@vue/test-utils';
import MkNumberCounter from '../MkNumberCounter';

const localVue = createLocalVue();

describe('MkNumberCounter', () => {
  const wrapper = shallowMount(MkNumberCounter, {
    localVue,
    propsData: {
      number: 10,
      limit: 9,
      isNumberFormatted: false,
    },
  });

  const numberCounter = () => wrapper.find('.c-mk-number-counter');

  it('should render number counter', () => {
    expect(numberCounter().exists()).toBe(true);
  });

  it('should render number as is if not formatted', () => {
    expect(numberCounter().text()).toBe('10');
  });

  it('should render max limit number if is formatted and number length is more than limit', async () => {
    await wrapper.setProps({ isNumberFormatted: true, limit: 1 });

    expect(numberCounter().text()).toBe('9+');
  });

  it('should render number as is if is formatted and number length is not than limit', async () => {
    await wrapper.setProps({ isNumberFormatted: true, limit: 2 });

    expect(numberCounter().text()).toBe('10');
  });
});
