import { shallowMount } from '@vue/test-utils';
import MkFieldSelect from './MkFieldSelect';

describe('MkFieldSelect.vue', () => {
  const propsData = {
    options: [
      { value: 'daily', label: 'Hari' },
      { value: 'weekly', label: 'Minggu' },
      { value: 'monthly', label: 'Bulan' },
      { value: 'yearly', label: 'Tahun' },
    ],
  };

  const wrapper = shallowMount(MkFieldSelect, { propsData });

  it('should emit the changed value', async () => {
    wrapper.vm.selectedValue = 'weekly';
    await wrapper.vm.$nextTick();
    expect(wrapper.emitted('change')).toBeTruthy();
  });

  it('should handle on focus properly', () => {
    const selectElement = wrapper.find('.c-field-select__select');
    selectElement.trigger('focus');

    expect(wrapper.emitted('focus')[0]).toBeTruthy();
  });

  it('should handle on blur properly', () => {
    const selectElement = wrapper.find('.c-field-select__select');
    selectElement.trigger('blur');

    expect(wrapper.emitted('blur')[0]).toBeTruthy();
  });
});
