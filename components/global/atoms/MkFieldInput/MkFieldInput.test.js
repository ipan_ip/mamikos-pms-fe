import { shallowMount } from '@vue/test-utils';
import MkFieldInput from './MkFieldInput';

describe('MkFieldInput.vue', () => {
  let wrapper;

  const createComponent = (options) => {
    wrapper = shallowMount(MkFieldInput, {
      ...options,
    });
  };

  const validatorType = MkFieldInput.props.type.validator;

  it('should mount properly', () => {
    createComponent();

    const findFieldInput = () => wrapper.find('.c-field-input');
    expect(findFieldInput().exists()).toBe(true);
  });

  describe('validator props type', () => {
    it('should failed with incorrect type', () => {
      expect(validatorType('radio')).toBe(false);
    });

    it('should passes with correct type', () => {
      expect(validatorType('password')).toBe(true);
      expect(validatorType('number')).toBe(true);
    });
  });

  describe('input events', () => {
    it('should emit input event when value updated', async () => {
      createComponent();

      const findInput = () => wrapper.find('input[type="text"]');

      await findInput().setValue('some value');
      expect(wrapper.emitted().update).toBeTruthy();
      expect(wrapper.emitted().input).toBeTruthy();
    });

    it('should emit update event when on change', () => {
      createComponent();

      const findInput = () => wrapper.find('input[type="text"]');

      findInput().trigger('change');
      expect(wrapper.emitted().update).toBeTruthy();
    });

    it('should change type if click show password', async () => {
      createComponent({ propsData: { type: 'password' } });

      const findShowPassword = () => wrapper.find('i');
      findShowPassword().trigger('click');

      await wrapper.vm.$nextTick();
      expect(wrapper.vm.inputType).toBe('text');
    });
  });
});
