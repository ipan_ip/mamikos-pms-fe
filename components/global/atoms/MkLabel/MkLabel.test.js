import { shallowMount } from '@vue/test-utils';

import MkLabel from './MkLabel';

describe('MkLabel.vue', () => {
  it('can be rendered properly', () => {
    const wrapper = shallowMount(MkLabel);

    expect(wrapper.find('.c-mk-label').exists()).toBe(true);
  });

  it('should show the slot', () => {
    const wrapper = shallowMount(MkLabel, {
      slots: {
        default: 'test slot',
      },
    });

    expect(wrapper.find('.c-mk-label').text()).toBe('test slot');
  });
});
