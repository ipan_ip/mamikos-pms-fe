import { shallowMount, createLocalVue } from '@vue/test-utils';
import MkBackNavDesktop from '../MkBackNavDesktop';

const localVue = createLocalVue();

describe('MkBackNavDesktop.vue', () => {
  const wrapper = shallowMount(MkBackNavDesktop, {
    localVue,
  });

  it('should render top navbar properly', () => {
    expect(wrapper.find('.c-mk-back-nav-desktop').exists()).toBe(true);
  });

  it('should render title properly', async () => {
    await wrapper.setProps({ title: 'Title ' });

    expect(wrapper.find('.c-mk-back-nav-desktop__title').text()).toBe('Title');
  });

  it('should emit back when button back clicked', () => {
    wrapper.find('.c-mk-back-nav-desktop__back').vm.$emit('click');

    expect(wrapper.emitted('back')[0]).toBeTruthy();
  });
});
