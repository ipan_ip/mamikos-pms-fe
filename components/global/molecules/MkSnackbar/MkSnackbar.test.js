import { shallowMount } from '@vue/test-utils';

import MkSnackbar from './MkSnackbar';

const mountComponent = (propsData = {}) => {
  return shallowMount(MkSnackbar, {
    propsData: {
      active: true,
      ...propsData,
    },
  });
};

describe('MkSnackbar.vue', () => {
  it('should render the component properly', () => {
    const wrapper = mountComponent();

    expect(wrapper.exists()).toBe(true);
  });

  it("shouldn't show the snackbar when 'active' prop is false", () => {
    const wrapper = mountComponent({ active: false });

    expect(wrapper.find('.c-mk-snackbar__background').exists()).toBe(false);
    expect(wrapper.find('c-mk-snackbar__frame').exists()).toBe(false);
  });

  it('should emit "update:active" event when backdrop exist & clicked', () => {
    const wrapper = mountComponent({
      backdrop: true,
    });

    const backdrop = wrapper.find('.c-mk-snackbar__background');

    backdrop.trigger('click');

    expect(wrapper.emitted()['update:active']).toBeTruthy();
  });
});
