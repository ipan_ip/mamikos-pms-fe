import { createLocalVue, shallowMount } from '@vue/test-utils';
import MkActionCard from './MkActionCard';

const localVue = createLocalVue();
const stubs = { BgIcon: { template: "<div class='bg-icon'></div>" } };

describe('MkActionCard.vue', () => {
  const wrapper = shallowMount(MkActionCard, { localVue, stubs });

  it('should render MkActionCard', () => {
    expect(wrapper.find('.mk-action-card').exists()).toBe(true);
  });

  it('should render left content properly', async () => {
    const mainLeft = () => wrapper.find('.mk-action-card__main-left');
    const mainIcon = () => wrapper.find('.mk-action-card__main-icon-icon');
    const mainIconText = () => wrapper.find('.mk-action-card__main-icon-text');

    expect(mainLeft().exists()).toBe(false);
    expect(wrapper.vm.hasIcon).toBe(false);
    expect(wrapper.vm.hasMainContent).toBe(false);

    await wrapper.setProps({ iconName: 'icon-name' });
    expect(mainLeft().exists()).toBe(true);
    expect(mainIcon().exists()).toBe(true);
    expect(mainIconText().exists()).toBe(false);

    await wrapper.setProps({ iconName: '', iconText: '20' });
    expect(mainLeft().exists()).toBe(true);
    expect(mainIcon().exists()).toBe(false);
    expect(mainIconText().exists()).toBe(true);
  });

  it('should render main content properly', async () => {
    const content = () => wrapper.find('.mk-action-card__main-content');
    const title = () => wrapper.find('.mk-action-card__main-content-title');
    const subtitle = () => wrapper.find('.mk-action-card__main-content-subtitle');

    expect(content().exists()).toBe(false);
    expect(title().exists()).toBe(false);
    expect(subtitle().exists()).toBe(false);

    await wrapper.setProps({ title: 'title', subtitle: 'subtitle' });
    expect(content().exists()).toBe(true);
    expect(title().exists()).toBe(true);
    expect(subtitle().exists()).toBe(true);
    expect(title().text()).toBe('title');
    expect(subtitle().text()).toBe('subtitle');
  });

  it('should render description properly', async () => {
    const description = () => wrapper.find('.mk-action-card__description');

    expect(description().exists()).toBe(false);

    await wrapper.setProps({ description: 'description ' });
    expect(description().exists()).toBe(true);
    expect(description().text()).toBe('description');
  });
});
