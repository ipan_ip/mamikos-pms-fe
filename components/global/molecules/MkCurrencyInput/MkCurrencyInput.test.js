import { createLocalVue, shallowMount } from '@vue/test-utils';
import MkCurrencyInput from '../MkCurrencyInput';
import { toIDR } from '~/utils/currencyString';

const localVue = createLocalVue();
localVue.filter('currencyIDR', toIDR);

describe('MkCurrencyInput', () => {
  const wrapper = shallowMount(MkCurrencyInput, { localVue });

  it('should render input', () => {
    expect(wrapper.find('input').exists()).toBe(true);
  });

  it('should handle input properly', () => {
    const input = wrapper.find('input');
    input.element.value = '';
    input.trigger('input');

    expect(wrapper.emitted('input')[0][0]).toBe(0);

    input.element.value = '10000';
    input.trigger('input');

    expect(wrapper.emitted('input')[1][0]).toBe(10000);

    input.element.value = '10000000000000000';
    input.trigger('input');

    expect(wrapper.emitted('input')[2][0]).toBe(10000000000000);
  });

  it('should handle focus properly', async () => {
    const input = wrapper.find('input');
    input.element.value = '10000';
    input.trigger('input');
    input.trigger('focus');
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.isFocus).toBe(true);
    expect(wrapper.find('input').attributes('type')).toBe('number');
  });

  it('should handle value changes', async () => {
    wrapper.setProps({ value: 999 });
    const input = wrapper.find('input');
    input.trigger('focus');
    await wrapper.vm.$nextTick();

    expect(wrapper.find('input').element.value).toBe('999');
  });
});
