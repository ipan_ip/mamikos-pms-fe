import { shallowMount, createLocalVue } from '@vue/test-utils';
import MkTopNavbarMweb from '../MkTopNavbarMweb';

const localVue = createLocalVue();

describe('MkTopNavbarMweb.vue', () => {
  const wrapper = shallowMount(MkTopNavbarMweb, {
    localVue,
  });

  it('should render top navbar properly', () => {
    expect(wrapper.find('.c-mk-top-navbar-mweb').exists()).toBe(true);
  });

  it('should render title properly', async () => {
    await wrapper.setProps({ title: 'Title ' });

    expect(wrapper.find('.c-mk-top-navbar-mweb__title').text()).toBe('Title');
  });

  it('should render back button properly', async () => {
    await wrapper.setProps({ isShowBackButton: true });

    expect(wrapper.find('.c-mk-top-navbar-mweb__back').exists()).toBe(true);

    wrapper.find('.c-mk-top-navbar-mweb__back').vm.$emit('click');

    expect(wrapper.emitted('back')[0]).toBeTruthy();
  });
});
