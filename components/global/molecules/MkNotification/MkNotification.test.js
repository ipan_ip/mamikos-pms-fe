import { shallowMount } from '@vue/test-utils';

import MkNotification from './MkNotification';

describe('MkNotification.vue', () => {
  it('should render the component properly', () => {
    const wrapper = shallowMount(MkNotification);

    expect(wrapper.exists()).toBe(true);
  });

  it('should render all the notifications', () => {
    const wrapper = shallowMount(MkNotification, {
      propsData: {
        notifications: [
          { url: 'http://abc.test', photo: '', review: 'test', title: 'test' },
          { url: 'http://abc.test', photo: '', review: 'test', title: 'test' },
          { url: 'http://abc.test', photo: '', review: 'test', title: 'test' },
        ],
      },
    });

    expect(wrapper.findAll('.c-notification__item').length).toBe(3);
  });

  it('should redirect the page to notification page when "Lihat Semua" clicked', () => {
    const wrapper = shallowMount(MkNotification);
    const { location } = window;

    delete window.location;
    window.location = { assign: jest.fn() };

    const seeMoreBtn = wrapper.find('.c-notification__see-more');

    seeMoreBtn.trigger('click');

    expect(window.location.assign).toHaveBeenCalledWith(
      `${process.env.MAMIKOS_URL}/ownerpage/notification`,
    );

    // restore mock
    window.location = location;
  });

  it('should show loading indicator when "loading" prop set to true', () => {
    const wrapper = shallowMount(MkNotification, {
      propsData: { loading: true },
    });

    expect(wrapper.find('.c-loader').exists()).toBe(true);
  });

  it('should show trigger handleImageError method when image failed to fetch', () => {
    const wrapper = shallowMount(MkNotification, {
      propsData: {
        notifications: [{ url: 'http://abc.test', photo: '', review: 'test', title: 'test' }],
      },
    });

    const imgElem = wrapper.find('.c-notification__item-image');

    imgElem.trigger('error');

    expect(imgElem.element.src).toMatch(/placeholder_loading/);
  });
});
