import { shallowMount } from '@vue/test-utils';
import MkPagination from '../MkPagination';
import localVueWithBuefy from '~/utils/addBuefy';

const localVue = localVueWithBuefy;

const attrs = {
  total: 10,
  'per-page': 2,
};

describe('MkPagination', () => {
  const wrapper = shallowMount(MkPagination, { localVue, attrs, propsData: { current: 1 } });

  it('should render pagination component', () => {
    expect(wrapper.find('.c-mk-pagination').exists()).toBe(true);
  });

  it('should return totalPage properly', () => {
    expect(wrapper.vm.totalPage).toBe(5);
    expect(wrapper.find('.c-mk-pagination__page-info').text()).toBe('Halaman 1 dari 5');
  });

  it('should emit update:current when currentPage changed', () => {
    const bPagination = wrapper.find('b-pagination-stub');
    bPagination.vm.$emit('update:current', 2);

    expect(wrapper.emitted('update:current')[0][0]).toBe(2);
  });
});
