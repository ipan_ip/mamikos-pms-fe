import { shallowMount } from '@vue/test-utils';

import MkAlert from './MkAlert';
import localVueWithBuefy from '~/utils/addBuefy';

const defaultProps = {
  isShown: true,
  message: 'test message',
};

const mountComponent = (propsData = {}) => {
  return shallowMount(MkAlert, {
    localVue: localVueWithBuefy,
    propsData: {
      ...defaultProps,
      ...propsData,
    },
  });
};

describe('MkAlert.vue', () => {
  it('can be rendered properly', () => {
    const wrapper = mountComponent();

    expect(wrapper.find('.c-mk-alert__content').exists()).toBe(true);
  });

  it('should emit "update:isShown" and "closed" event when close button clicked', () => {
    const wrapper = mountComponent();

    const closeButton = wrapper.find('.c-mk-alert__close-icon');

    closeButton.trigger('click');

    expect(wrapper.emitted().closed).toBeTruthy();
    expect(wrapper.emitted()['update:isShown']).toBeTruthy();
  });

  it("should show the title when 'title' prop is provided", () => {
    const wrapper = mountComponent({
      title: 'test title',
    });

    expect(wrapper.find('.c-mk-alert__title').exists()).toBe(true);
  });

  it("shouldn't show the title when 'title' prop isn't provided", () => {
    const wrapper = mountComponent();

    expect(wrapper.find('.c-mk-alert__title').exists()).toBe(false);
  });

  it("shouldn't show the close button when 'closable' prop is set to false", () => {
    const wrapper = mountComponent({
      closable: false,
    });

    expect(wrapper.find('.c-mk-alert__title').exists()).toBe(false);
  });
});
