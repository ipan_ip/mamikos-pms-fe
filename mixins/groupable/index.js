export default (parent) => ({
  beforeCreate() {
    if (this.$parent.$options.name !== parent) {
      throw new Error(`Only use ${this.$parent.$options.name} for ${parent} parent.`);
    }
  },
});
