import { mapGetters } from 'vuex';

export default {
  computed: {
    ...mapGetters('profile', ['getBlacklistRouterNames']),
  },
  created() {
    if (this.getBlacklistRouterNames.includes(this.$route.name)) {
      this.$router.replace('/');
    }
  },
};
