export default {
  data() {
    return {
      view: '',
    };
  },
  created() {
    this.createView();
  },
  updated() {
    this.createView();
  },
  methods: {
    createView() {
      this.component.then(({ default: PageView }) => {
        this.$options.components.PageView = PageView;
        this.view = 'PageView';
      });
    },
  },
};
