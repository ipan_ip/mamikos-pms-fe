export default function({ from, store }) {
  store.commit('route/setPreviousRoute', {
    name: from ? from.name : '',
    path: from ? from.path : '/',
  });
}
