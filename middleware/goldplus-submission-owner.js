function checkGPSubmissionStatus(store, redirect) {
  const isGPActive = store.state.goldplus.isGoldPlusActive;

  if (!isGPActive) {
    return;
  }
  return redirect('/goldplus');
}

export default function({ store, redirect }) {
  const user = store.getters['profile/user'];

  if (!Object.keys(user).length) {
    store.dispatch('goldplus/getActiveStatus').then(() => {
      checkGPSubmissionStatus(store, redirect);
    });
  } else checkGPSubmissionStatus(store, redirect);
}
