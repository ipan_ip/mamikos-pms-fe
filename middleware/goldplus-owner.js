function checkGPStatus(store, redirect) {
  const isGPActive = store.state.goldplus.isGoldPlusActive;
  const isGPOwner = store.state.profile.data.gp_owner;

  if (!isGPActive && !isGPOwner) return redirect('/');
}

export default function({ store, redirect }) {
  const user = store.getters['profile/user'];

  if (!Object.keys(user).length) {
    store.dispatch('profile/getProfile').then(() => {
      checkGPStatus(store, redirect);
    });
  } else checkGPStatus(store, redirect);
}
