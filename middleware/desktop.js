export default function({ store, route, app }) {
  if (app.$device.isMobile) return;

  store.commit('desktop/setHideSidebar', false);
  store.commit('desktop/setHideHeader', false);
  store.commit('desktop/setBodyBackground', '');

  route.meta.forEach((meta) => {
    if (meta.hideSidebar) {
      store.commit('desktop/setHideSidebar', true);
    }

    if (meta.hideHeader) {
      store.commit('desktop/setHideHeader', true);
    }

    if (meta.desktopBodyBackground) {
      store.commit('desktop/setBodyBackground', meta.desktopBodyBackground);
    }
  });
}
