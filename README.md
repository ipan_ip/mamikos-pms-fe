# pms-fe

> Owner Dashboard front-end repository

## Build Setup

```bash
# install dependencies
$ yarn install

# copy enviroment variables
$ cp .env.sample .env

# as webpack-copy-plugin can't be used for this repo, you need to build sendbird manually.
# call this command every upgrade sendbird-web-widget or first time installation
$ yarn sendbird-build

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

---

## Development

As pms-fe used mami53 for authentication purpose. So it needs mami53 running on your device or you can use staging. Just update `MAMIKOS_URL` and `API_URL` in `.env`. If you want notification appear on your browser, ensure you update those envs with `https`. And the last, you need add another localhost alias in hosts file(`etc/hosts`) with format `xxx.kerupux.com`. For example:
- local.kerupux.com refers to your mamikos project (mami53)
- owner-local.kerupux.com refers to pms fe

As this repo used oauth system for calling API to mami53, make sure you have run this command in mami53 `php artisan passport:keys`. And ensure `OWNER_DASHBOARD_URL` in mami53 env is pointing to your pms-fe local. `OWNER_DASHBOARD_URL` (mami53 env) must **have same domain** as `MAMIKOS_URL` (pms env). And then you can open pms-fe on owner-local.kerupux.com:3002. If you don't like using port number on your address bar, you can use nginx to proxy it. 



### Merge Request

Please **always** tick squash and remove branch in your MR in order to easily maintain historical commit in `master` or `develop` branch.

---

## File Structure

| Directory   | Description                                                                                       |
| ----------- | ------------------------------------------------------------------------------------------------- |
| .deployment | contain deployment config for kubernetes.                                                         |
| api         | contain all API endpoints.                                                                        |
| assets      | webpack will build (minimize, etc) all files in this directory according to .                     |
| components  | has 3 types components. They are `global`, `layout-features`, and `page-features`                 |
| layouts     | has 2 types layout for each layout. They are mobile and desktop view.                             |
| libs        | for external libs. Ex: sendbird submodule.                                                        |
| middleware  | all scripts which are called before going to page or layout.                                      |
| mixins      | global mixin which can be reused.                                                                 |
| pages (DEPRECATED) | page container which determine to use mobile or desktop layout.                                   |
| plugins     | to add vue's plugin. Please add `$` for prefix name. Ex: `$alert`.                                |
| routes      | all vue routes. It has `pages` directory that stores your page. Now we decided to use vue router manually |
| static      | all assets in this directory won't be built by webpack. So be careful if you put your asset here. |
| store       | add your vuex module here.                                                                        |
| utils       | if you find your code can be reused again, please refactor it to be utility code here.            |

---

## Deployment

Our deployment is managed by jenkins either production or staging. Go to jusmelon.kerupux.com to deploy.

### Staging

Choose `mamikos-pms-fe-staging-1` or `mamikos-pms-fe-staging-2` in jusmelon.kerupux.com. If you don't have access, please contact devops. Deployment steps:

- Head to https://jusmelon.kerupux.com/
- Choose job
- Click **Build Now** on top left side. New pipeline will appear on **Build History** section at the top.
- Click arrow button which is next to the number **#N**. Choose **Console Output**.
- Click **Input requested**. Then choose your branch. Wait until building is complete.

### Production

Will be handled by devops.

---

## Connect to API

Create your entity endpoints under `/api/endpoints` folder. Create your endpoint as function in object. Then, export it by default.

```javascript
export default {
  getUser() {
    return this.post('/oauth/user');
  },
};
```

`this` refers to `axios`. So you can use axios as usual. And then, register your endpoint's file to `api/index.js`.

```javascript
// api/index.js
import tenant from './endpoints/tenant';

const apiInterfaces = {
  ...tenant,
};
```

Please **don't** chain it using `then` or `catch` in endpoint's file. You can chain them on your `page` container by using `this.$api`.

```javascript
// user.vue
...
mounted(){
  this.$api.getUser()
    .then(res => {
      console.log(res);
    })
    .catch(err=>{
      console.log(err);
    })
  ;
}
```

`/api/service` is service which handle including `access_token` and requesting new token by passing `refresh_token` when `access_token` is expired.

---

## Components

There are 3 types of components. They are:

### global

There are 3 types of global components. These components can be reused on your other component. It follows atomic design rule. You can reach [this](https://bradfrost.com/blog/post/atomic-web-design/) if you don't familiar with it. The rules:

- no vuex
- no api call
- provide data via props

As this repo use Buefy, please check availability component first before create new one. If you want to update buefy component, try to override it in your component. You can customize buefy style on [this](https://buefy.org/documentation/customization).

### Layout's Component

If you want to create component that will exist only on certain layout, you should create within this directory. You can use vuex and call api. But if you want to make it globally, you have to avoid vuex and api call on your component.

### Page's Component

This directory has same explanation as layout's component, but it is used for components which are used on specific page.

> If you find component which will be used for page and layout. You can put in either layout or page component directory.

> Please always use `scoped` to prevent unintentionally override parent/global style.

---

## Unit Test

Create `[filename].test.js` on `components/atoms/[filename]/`.

---

## Styles

When working on style, you find some styles that can be reused, please add to ~/assets/styles/desktop/\*. It follows [ITCSS architecture](https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/)

---

## Layouts

You can put your layout under `/layouts` directory. If you need to separate mobile and desktop layout, please create `mobile` and `desktop` directory in your layout directory. Ex: `layouts/default/mobile`

---

## Pages (DEPRECATED) 

This feature is deprecated as nuxt doesn't support combination of nested route and subpath. Please use manual vue router in `routes/index.js`.

---

## Routes

All routes that you store. Please put your page under `routes/pages` directory. Please group your page into one directory if you have more than one page. If your page supports mobile and desktop view, please refer your code to `routes/pages/index.vue`.

---

## Error Reporting

This repo uses bugsnag like mami53. Just chain like this 
```
  this.$bugsnag.notify();
```

---

## Popup Order

There is an order for any popup in default layout. The order as follow:
- `default` layout's modal
- component modal
- onboarding view
