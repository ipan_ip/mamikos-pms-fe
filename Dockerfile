FROM node:11.13.0-alpine

RUN apk update && apk upgrade && \
  apk add --no-cache git

RUN mkdir -p /app

WORKDIR /app

COPY . ./

RUN yarn install

RUN yarn run sendbird-build

RUN yarn run build

# NUXT SERVER HOSTNAME
ENV SERVER_HOSTNAME=0.0.0.0
# NUXT PORT
ENV SERVER_PORT=3000

EXPOSE $SERVER_PORT

CMD ["yarn", "start"]
