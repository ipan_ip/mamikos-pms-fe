const MAMIKOS_URL = process.env.MAMIKOS_URL;

/**
 * type:
 * - internal: link which is inside this site
 * - external: link which is outside this site
 * - action: call javascript function
 */
export default [
  {
    name: 'Home',
    type: 'internal',
    data: '/',
    urlNames: ['index', 'booking-manage-status'],
    icon: 'home',
  },
  {
    name: 'Properti Saya',
    type: 'internal',
    data: '/',
    urlNames: ['/'],
    icon: 'kos-marketing',
    children: [
      {
        name: 'Kos',
        data: `${MAMIKOS_URL}/ownerpage/kos`,
        type: 'external',
        urlNames: [
          'kos-rooms',
          'kos-rooms-index',
          'kos-rooms-kos_id',
          'kos-rooms-kos_id-edit',
          'kos-rooms-kos_id-price',
          'kos-rooms-price-index',
          'kos-rooms-price-kos_id',
          'kos-reviews',
        ],
        action(ctx) {
          ctx.sendDashboardTracker('[Owner] OD Properti Saya - Kos Clicked');
        },
      },
      {
        name: 'Apartemen',
        data: `${MAMIKOS_URL}/ownerpage/apartment`,
        type: 'external',
        urlNames: [
          'apartment-premium-id-premium',
          'apartment-premium-id-promo',
          'apartment-premium-id-balance',
        ],
        action(ctx) {
          ctx.sendDashboardTracker('[Owner] OD Properti Saya - Apartemen Clicked');
        },
      },
      {
        name: 'Pekerjaan',
        data: `${MAMIKOS_URL}/ownerpage/vacancy`,
        type: 'external',
        urlNames: [],
        action(ctx) {
          ctx.sendDashboardTracker('[Owner] OD Properti Saya - Pekerjaan Clicked');
        },
      },
    ],
  },
  {
    name: 'Pemasaran Kos',
    id: 'kosMarketing',
    data: '/',
    urlNames: [],
    icon: 'promote',
    children: [
      {
        name: 'Premium',
        data: '/premium-dashboard',
        type: 'internal',
        urlNames: [
          'premium-dashboard',
          'premium-dashboard-kelola-iklan',
          'premium-dashboard-kelola-promo',
          'premium-dashboard-kelola-saldo',
        ],
      },
    ],
  },
  {
    name: 'Manajemen Kos',
    id: 'kosManagement',
    data: '/',
    urlNames: [],
    icon: 'kos-management',
    children: [
      {
        name: 'Pengajuan Booking',
        type: 'external',
        data: `${MAMIKOS_URL}/ownerpage/manage/all/booking`,
        urlNames: ['owner-manage-booking'],
      },
      {
        name: 'Laporan Keuangan',
        data: '/financial-report',
        type: 'internal',
        urlNames: ['finance-report'],
        isBbkRequired: true,
        action(ctx) {
          ctx.sendDashboardTracker('[Owner] OD - Laporan Keuangan Clicked', {
            redirection_source: 'tab_manajemen_kos',
          });
        },
      },
      {
        name: 'Kelola Tagihan',
        type: 'internal',
        data: `/billing-management`,
        urlNames: ['billing-management'],
        action(ctx) {
          ctx.sendDashboardTracker('[Owner] OD - Kelola Tagihan Clicked', {
            redirection_source: 'tab_manajemen_kos',
          });
        },
      },
      {
        name: 'Penyewa',
        data: '/tenant-list',
        type: 'internal',
        urlNames: ['tenant-list'],
        isBbkRequired: true,
        action(ctx) {
          ctx.sendDashboardTracker('[Owner] OD - Penyewa Clicked', {
            redirection_source: 'tab_manajemen_kos',
          });
        },
      },
    ],
    isHiddenFn(context) {
      return !!context.$store.getters['profile/isNewOwner'];
    },
  },
  {
    name: 'Statistik',
    type: 'internal',
    data: '/statistic',
    urlNames: ['statistic-menu'],
    icon: 'chart-kpi',
  },
  {
    name: 'Akun',
    type: 'external',
    data: `${MAMIKOS_URL}/ownerpage/settings`,
    urlNames: [],
    icon: 'account',
  },
];
