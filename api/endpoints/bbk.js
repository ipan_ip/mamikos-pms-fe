export default {
  getBbkStatus() {
    return this.get('/oauth/owner/bbk/status');
  },
  registerBbk() {
    return this.post('/oauth/owner/bbk/register');
  },
};
