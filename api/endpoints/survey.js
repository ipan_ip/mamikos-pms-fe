export default {
  getPollingQuestions() {
    return this.get('/oauth/user/polling/detail/exit_goldplus');
  },

  postFeedbackSurvey(key, data) {
    return this.post(`/oauth/user/polling/submit/${key}`, data);
  },
};
