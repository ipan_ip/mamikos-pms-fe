export default {
  updateAllProperties() {
    return this.post('/oauth/owner/data/update-room');
  },
  postSurveyRoom(data, params = {}) {
    return this.post('/oauth/owner/data/survey/updateroom', data, { params });
  },
  getActiveProperty(params, cancelToken = null) {
    return this.get('/oauth/owner/data/list/update', { params, cancelToken });
  },
  updatePropertyRoomPrice(data) {
    return this.post('/oauth/owner/update_room_price', data);
  },
  fetchAllPropertyRooms(params) {
    return this.get('/oauth/owner/data/kos', { params });
  },
  getPropertyRooms(params) {
    return this.get('/oauth/owner/data/kos/room-unit', { params });
  },
  deletePropertyRoom(param) {
    return this.delete(`/oauth/owner/data/kos/room-unit/single/${param}`);
  },
  postPropertyRoom(data) {
    return this.post('/oauth/owner/data/kos/room-unit/single', data);
  },
  updatePropertyRoom(data, roomId) {
    return this.put('/oauth/owner/data/kos/room-unit/single/' + roomId, data);
  },
  updateBulkPropertyRoom(data) {
    return this.post('/oauth/owner/data/kos/room-unit/bulk', data);
  },
  getPriceComponents(roomId) {
    return this.get(`/oauth/owner/room/price-component/${roomId}`);
  },
  postPriceComponentActivation(roomId, type, data) {
    return this.post(`/oauth/owner/room/price-component/toggle/${roomId}/${type}`, data);
  },
  postPriceComponent(roomId, data) {
    return this.post(`/oauth/owner/room/price-component/${roomId}`, data);
  },
  updatePriceComponent(priceId, data) {
    return this.put(`/oauth/owner/room/price-component/${priceId}`, data);
  },
  deletePriceComponent(priceId) {
    return this.delete(`/oauth/owner/room/price-component/${priceId}`);
  },
  getPropertyReviews(params) {
    return this.get('/oauth/owner/room/review', { params });
  },
};
