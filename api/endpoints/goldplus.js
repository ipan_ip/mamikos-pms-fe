export default {
  // GENERAL
  getOwnerGoldPlusStatus() {
    return this.get('/oauth/me/goldplus-submission');
  },
  getGoldPlusWidgetData() {
    return this.get('/oauth/room/goldplus/widget-room');
  },
  getGoldPlusRoomDetail(id) {
    return this.get(`/oauth/room/goldplus/${id}`);
  },

  // STATISTICS
  fetchGoldPlusStatisticRoomList(params) {
    return this.get(`/oauth/room/goldplus`, { params });
  },
  getGoldPlusStatisticRoomListFilters() {
    return this.get('/oauth/room/goldplus/filters');
  },
  getGoldPlusStatistics(id, type, params) {
    return this.get(`/oauth/room/goldplus/${id}/statistics/${type}`, { params });
  },
  getGoldPlusStatisticFilters(type, params) {
    return this.get(`/oauth/room/goldplus/statistics/filters/${type}`, { params });
  },

  // SUBMISSION/ACQUISITION
  fetchGoldPlusSubmissionOnboardingContent() {
    return this.get(`/oauth/goldplus/submissions/onboarding`);
  },
  fetchGoldPlusSubmissionRoomList(params) {
    return this.get('/oauth/room/goldplus/list-kost', { params });
  },
  getGoldPlusActiveContract() {
    return this.get('/oauth/me/goldplus/active-contract');
  },
  getGoldPlusSubmissionRoomListFilters() {
    return this.get('/oauth/room/goldplus/list-kost/filters');
  },
  sendGoldPlusSubmissionRequest(payload) {
    return this.post(`/oauth/goldplus/submissions`, payload);
  },
  getGoldPlusPackages() {
    return this.get(`/oauth/goldplus/packages/`);
  },
  getGoldPlusSubmissionTnCContent() {
    return this.get(`/oauth/goldplus/submissions/tnc/`);
  },
  getGoldPlusSubmissionStatus() {
    return this.get('/oauth/me/goldplus-submission/status');
  },

  // PAYMENT
  getGoldPlusBillingSummary() {
    return this.get(`oauth/mamipay/request/owner/goldplus/billing/summary`);
  },
  getGoldPlusBillingList(type, limit, offset) {
    return this.get(
      `oauth/mamipay/request/owner/goldplus/billing/${type}?limit=${limit}&offset=${offset}`,
    );
  },
  getBillingContractDetail(id) {
    return this.get(`oauth/mamipay/request/property/contract/order/${id}`);
  },
  createBillingContractOrder(property_contract_id) {
    return this.post('oauth/mamipay/request/property/contract/order', { property_contract_id });
  },
  getBillingInvoice(invoice_id) {
    return this.get(`oauth/mamipay/request/invoice/show/${invoice_id}`);
  },
  getBillingAlertPending() {
    return this.get(`oauth/mamipay/request/owner/goldplus/billing/show/pending`);
  },

  // WIDGET UNSUBSCRIBE
  getWidgetUnsubscribeStatus() {
    return this.get(`/oauth/mamipay/request/owner/goldplus/widget/unsubscribe`);
  },
};
