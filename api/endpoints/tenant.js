export default {
  /**
   * Get all tenants in paginations
   * @param { limit: Number, offset: Number, access: String } params
   */
  getTenants(params) {
    return this.get('/oauth/mamipay/request/dashboard/tenant', { params });
  },
};
