export default {
  getKosList(params) {
    return this.get('/oauth/mamipay/request/room/list', { params });
  },
  getKosListWithContractRequest(params) {
    return this.get('/oauth/owner/booking/room/requests', { params });
  },
  getContractAbTest() {
    return this.get('/oauth/owner/tenant/dashboard');
  },
  sendFormSetup(setting) {
    return this.post('/oauth/owner/tenant/link', setting);
  },
  getContractRequestList(params) {
    return this.get('/oauth/owner/contract-submission', { params });
  },
  getContractRequestDetail(id) {
    return this.get(`/oauth/owner/contract-submission/${id}`);
  },
  getRoomAllotment(kosId, params = {}) {
    return this.get(`/oauth/mamipay/request/owner/booking/room-allotment/${kosId}`, { params });
  },
  acceptContractRequest(contractId, params = {}) {
    return this.post(`/oauth/owner/contract-submission/confirm/${contractId}`, params);
  },
  rejectContractRequest(contractId) {
    return this.post(`/oauth/owner/contract-submission/reject/${contractId}`);
  },
};
