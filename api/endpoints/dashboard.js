export default {
  getBookingDashboardData() {
    return this.get('/oauth/owner/booking/dashboard');
  },
};
