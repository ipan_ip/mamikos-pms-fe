export default {
  getOXAbTest() {
    return this.get('/oauth/config/ab-test/owner-experience');
  },
};
