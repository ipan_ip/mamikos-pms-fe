export default {
  /**
   * Get finance report every month or total
   * @param { month: Number, year: Number } params
   */
  getFinanceReport(params) {
    return this.get('/oauth/owner/data/income', { params });
  },
};
