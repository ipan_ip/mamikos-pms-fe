export default {
  postPremiumTrial() {
    return this.post('/oauth/owner/premium/trial');
  },
  submitPremiumSurvey(data) {
    return this.post('/oauth/owner/data/survey/premium', data);
  },
  getDailyActivationAllocation() {
    return this.get('/oauth/owner/premium/allocation/daily/setting');
  },
  getPromotion(propertyId) {
    return this.get('/oauth/owner/promotion/' + propertyId);
  },
  updatePromotion(data) {
    return this.post('/oauth/owner/promotion/', data);
  },
  getRoomData(room) {
    return this.get(`/oauth/owner/data/list/${room.type}/${room.id}`);
  },
  getRoomStatisticsReport(data) {
    return this.get(`/oauth/stories/${data.id}/report?type=${data.time}`);
  },
  activatePremiumAllocation(data) {
    return this.post(`/oauth/owner/premium/allocation/${data.id}`, data.payload);
  },
  deactivatePremiumAllocation(propertyId) {
    return this.get(`/oauth/owner/premium/allocation/${propertyId}/deactivate`);
  },
  allocateDailyBudget(propertyId, data) {
    return this.post('/oauth/owner/premium/allocation/' + propertyId, data);
  },
  checkAbTestStatus() {
    return this.get('/oauth/owner/premium/ab-test/check');
  },
  getOwnerInvoice() {
    return this.get('/oauth/owner/premium/invoice');
  },
};
