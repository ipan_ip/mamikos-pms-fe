import { getAccessToken, getRefreshToken, setToken } from '../cookiesToken';
import redirectLogin from '~/utils/redirectLogin';
import { clearAuthTracker } from '~/utils/authTracker';

/**
 * Axios interceptor for response
 * When getting 200, just pass it
 */
export function callback(response) {
  return response;
}

/**
 * When getting 401, try to refresh token then call api again
 * Except it, toast it to screen
 * this = vue instance
 */
export function error(error) {
  const originalRequest = error.config;
  if (error.response) {
    // 401, try to refresh token then call api again
    if (error.response.status === 401) {
      // when failed on refreshing token, redirect to login page
      if (originalRequest.url === '/oauth/token') {
        /*
         * When response code is 401, try to refresh the token.
         * Eject the interceptor so it doesn't loop in case
         * token refresh causes the 401 response
         */
        clearAuthTracker();
        this.$axios.interceptors.response.eject(window.responseInterceptor);
        redirectLogin();
        this.$bugsnag.notify(new Error('refreshing token failed'));
        return Promise.reject(error);
      }

      // when access_token can't be used anymore(expired), refresh that token
      const refreshToken = getRefreshToken();
      return this.$api
        .refreshToken({
          refresh_token: refreshToken,
        })
        .then((res) => {
          if (res.status === 200) {
            setToken(res.data);
            originalRequest.headers.Authorization = `Bearer ${getAccessToken()}`;
            return this.$axios(originalRequest);
          }
        });
    }
    // all error except 401, toast it to screen
    else {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      this.$bugsnag.notify(error);
    }
  }

  return Promise.reject(error);
}
