import Vue from 'vue';
import bugsnag from '@bugsnag/js';
import bugsnagVue from '@bugsnag/plugin-vue';

const { BUGSNAG_API_KEY } = process.env;
const bugsnagClient = bugsnag(BUGSNAG_API_KEY);

window.bugsnagClient = bugsnagClient;

bugsnagClient.use(bugsnagVue, Vue);

export default (ctx, inject) => {
  inject('bugsnag', bugsnagClient);
};
