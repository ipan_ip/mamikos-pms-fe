/**
 * for prevent leaving url,
 */

export default ({ app, store }, inject) => {
  app.router.beforeEach((to, from, next) => {
    if (store.state.changes && store.state.changes.showWarning && store.state.changes.hasChanges) {
      const answer = window.confirm(
        'Do you really want to leave? Changes you made may not be saved!',
      );
      if (answer) {
        // restore to false when user confirm yes
        store.commit('changes/updateShowWarning', false);
        store.commit('changes/updateHasChanges', false);
        // remove event listener also
        window.removeEventListener('beforeunload', window.beforeunloadhandler);
        next();
      } else {
        next(false);
      }
    } else {
      next();
    }
  });

  const init = () => {
    if (!store.state.changes) {
      store.registerModule('changes', {
        namespaced: true,
        state: {
          hasChanges: false,
          showWarning: false,
        },
        mutations: {
          updateShowWarning(state, isShow) {
            state.showWarning = isShow;
          },
          updateHasChanges(state, isChange) {
            state.hasChanges = isChange;
          },
        },
      });
    }

    store.commit('changes/updateShowWarning', true);
  };
  const update = (val) => {
    store.commit('changes/updateHasChanges', val);

    window.removeEventListener('beforeunload', window.beforeunloadhandler);
    if (val) {
      window.beforeunloadhandler = (event) => {
        event.returnValue = `Are you sure you want to leave?`;
      };
      window.addEventListener('beforeunload', window.beforeunloadhandler);
    }
  };
  inject('changes', { init, update });
};
