import Vue from 'vue';
import { toIDR } from '~/utils/currencyString';
import { toThousands } from '~/utils/number';

Vue.filter('currencyIDR', toIDR);
Vue.filter('toThousands', toThousands);
