import { BugsnagSourceMapUploaderPlugin } from 'webpack-bugsnag-plugins';
import CompressionPlugin from 'compression-webpack-plugin';
import manifest from './manifest.json';
import routesjs from './routes';

// import S3Plugin from 'webpack-s3-plugin'; comment this line for waiting decision of S3

require('dotenv').config();

const APP_VERSION = require('./package').version;

const {
  BUGSNAG_API_KEY,
  SERVER_PORT,
  SERVER_HOSTNAME,
  PROJECT_MODE,

  // comment these lines for waiting decision of S3
  // AWS_S3_KEY_ID,
  // AWS_S3_SECRET_KEY,
  ASSET_URL,
  API_URL,
  APP_NAME,
  MOENGAGE_APP_ID,
  OAUTH_CLIENT_ID,
  OAUTH_CLIENT_SECRET,
  API_LOGGER_URL,
} = process.env;

// comment these lines for waiting decision of S3
/*
const s3Plugin = new S3Plugin({
  s3Options: {
    accessKeyId: AWS_S3_KEY_ID, // Your AWS access key
    secretAccessKey: AWS_S3_SECRET_KEY, // Your AWS secret key
    region: 'ap-southeast-1', // The region of your S3 bucket
  },
  s3UploadOptions: {
    Bucket: 'pms-service-bucket', // Your bucket name
    ACL: 'private',
    // Here we set the Content-Encoding header for all the gzipped files to 'gzip'
    ContentEncoding(fileName) {
      if (/\.gz/.test(fileName)) {
        return 'gzip';
      }
    },
    // Here we set the Content-Type header for the gzipped files to their appropriate values, so the browser can interpret them properly
    ContentType(fileName) {
      if (/\.css/.test(fileName)) {
        return 'text/css';
      }
      if (/\.js/.test(fileName)) {
        return 'text/javascript';
      }
    },
  },
  basePath: APP_NAME, // This is the name the uploaded directory will be given
  directory: 'dist', // This is the directory you want to upload
});
 */
const polyfillFeatures = ['scroll', 'scrollIntoView'].join('%2C');

export default {
  server: {
    port: SERVER_PORT,
    host: SERVER_HOSTNAME,
  },
  env: {
    MOENGAGE_APP_ID,
    API_URL,
    OAUTH_CLIENT_ID,
    OAUTH_CLIENT_SECRET,
    APP_VERSION,
    API_LOGGER_URL,
  },
  mode: PROJECT_MODE,
  /*
   ** Headers of the page
   */
  head: {
    title: 'Mamikos - Solusi Kos Tanpa Beban',
    meta: [
      { charset: 'utf-8' },
      { hid: 'viewport', name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'Iklankan dan kelola usaha kos Anda secara online dengan mudah. Anda juga dapat iklankan apartemen dan lowongan kerja.' ||
          process.env.npm_package_description,
      },
      { hid: 'og:tittle', name: 'og:title', content: 'Mamikos - Solusi Kos Tanpa Beban' },
      {
        hid: 'og:description',
        name: 'og:description',
        content:
          'Iklankan dan kelola usaha kos Anda secara online dengan mudah. Anda juga dapat iklankan apartemen dan lowongan kerja.',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      { src: `https://polyfill.io/v3/polyfill.min.js?features=${polyfillFeatures}`, body: true },
    ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#1baa56' },
  /*
   ** Global CSS
   */
  css: ['~/assets/styles/desktop/index.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '@/plugins/bugsnag.js',
    '@/plugins/api',
    '@/plugins/dayjs',
    '@/plugins/alert',
    '@/plugins/filters',
    '@/plugins/ga',
    '@/plugins/tracker',
    '@/plugins/lazyload',
    '@/plugins/changes',
    '@/plugins/tour',
    '@/plugins/popupOrder',
    '@/plugins/vueNumeric',
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    ['nuxt-buefy', { css: false }],
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    '@nuxtjs/device',
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /**
   * vue-router configuration
   */
  router: {
    prefetchLinks: false,
    middleware: ['auth', 'route', 'mobile', 'desktop'],
    extendRoutes(routes, resolve) {
      routes.push(...routesjs);
    },
  },
  pwa: {
    workbox: {
      swDest: `${__dirname}/static/serviceworker.js`,
      swURL: 'serviceworker.js',
      importScripts: ['https://cdn.moengage.com/webpush/releases/serviceworker_cdn.min.latest.js'],
    },
    manifest,
  },
  /*
   ** Build configuration
   */
  build: {
    splitChunks: {
      layouts: true,
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, { app, store, route, params, query, env, isDev, isHMR, redirect, error }) {
      // add hash on chunk file
      config.output.chunkFilename = '[name][chunkhash].js';
      // send source map only on production
      if (['staging', 'production'].includes(APP_NAME)) {
        config.devtool = '#source-map';
        config.plugins.push(
          new BugsnagSourceMapUploaderPlugin({
            apiKey: BUGSNAG_API_KEY,
            appVersion: APP_VERSION,
            overwrite: true,
          }),
          new CompressionPlugin({
            test: /\.(js|css)$/,
            filename: '[path].gz[query]',
            algorithm: 'gzip',
          }),
          // s3Plugin, comment this line for waiting decision of S3
        );
        if (ASSET_URL) {
          // set asset path based on ASSET_URL value in env
          config.output.publicPath = ASSET_URL;
        }
      }
    },
  },
};
