export function toIDR(val, prefix = '.', hideSymbol = false) {
  const currencyString = hideSymbol ? '' : 'Rp';
  const formatted = (val / 1).toFixed(0).replace('.', '');
  return `${currencyString}${formatted.toString().replace(/\B(?=(\d{3})+(?!\d))/g, prefix)}`;
}
