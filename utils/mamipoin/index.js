export const formatPoint = function(point) {
  return parseInt(point) === 0 ? '-' : point;
};
