import Chart from 'chart.js';

export function initAlwaysShowLastTooltipPlugin() {
  Chart.pluginService.register({
    beforeRender(chart) {
      if (chart.config.options.tooltips.showAllTooltips) {
        // create an array of tooltips
        // we can't use the chart tooltip because there is only one tooltip per chart
        chart.pluginTooltips = [];

        chart.config.data.datasets.forEach(function(dataset, i) {
          chart.getDatasetMeta(i).data.forEach(function(sector, j) {
            chart.pluginTooltips.push(
              new Chart.Tooltip(
                {
                  _chart: chart.chart,
                  _chartInstance: chart,
                  _data: chart.data,
                  _options: chart.options.tooltips,
                  _active: [sector],
                },
                chart,
              ),
            );
          });
        });

        // turn off normal tooltips
        chart.options.tooltips.enabled = false;
      }
    },
    afterDraw(chart, easing) {
      if (chart.config.options.tooltips.showAllTooltips) {
        // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
        if (!chart.allTooltipsOnce) {
          if (easing !== 1) return;
          chart.allTooltipsOnce = true;
        }

        // turn on tooltips
        chart.options.tooltips.enabled = true;

        Chart.helpers.each(chart.pluginTooltips, function(tooltip, index) {
          // show only last tooltip
          if (chart.pluginTooltips.length === index + 1) {
            tooltip.initialize();
            tooltip.update();
            tooltip.pivot();
            tooltip.transition(easing).draw();
          }
        });

        chart.options.tooltips.enabled = false;
      }
    },
  });
}
