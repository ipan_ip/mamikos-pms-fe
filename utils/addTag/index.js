function loadError(oError) {
  throw new URIError('The script ' + oError.target.src + " didn't load correctly.");
}

export function appendScriptToHead(url, onloadFunction) {
  const newScript = document.createElement('script');
  newScript.onerror = loadError;
  if (onloadFunction) {
    newScript.onload = onloadFunction;
  }
  document.head.appendChild(newScript);
  newScript.src = url;
}

export function prependScript(url, onloadFunction) {
  const newScript = document.createElement('script');
  newScript.onerror = loadError;
  if (onloadFunction) {
    newScript.onload = onloadFunction;
  }
  document.head.insertBefore(newScript, document.head.firstElementChild);
  newScript.src = url;
}
