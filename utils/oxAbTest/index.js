/**
 *
 * onDefaultReceived = default = control = closable
 * onVariantAReceived = can't be closed
 */
export function forceBbk({ userId, endpoint, onDefaultReceived, onVariantAReceived }, axios) {
  const SESSION_NAME = 'AbTestForceBbkVarianHandler';

  // call handler from session storage instead of calling api
  if (sessionStorage && sessionStorage.getItem(SESSION_NAME)) {
    const handlerName = sessionStorage.getItem(SESSION_NAME);
    typeof arguments[0][handlerName] === 'function' && arguments[0][handlerName]();
    return;
  }

  const storeToSession = (val) => {
    sessionStorage && sessionStorage.setItem(SESSION_NAME, val);
  };
  const wrappedDefaultCb = () => {
    storeToSession('onDefaultReceived');
    onDefaultReceived();
  };
  const wrappedVariantACb = () => {
    storeToSession('onVariantAReceived');
    onVariantAReceived();
  };
  /**
   * {
   *   "status": true,
   *   "meta": {
   *     "response_code": 200,
   *     "code": 200,
   *     "severity": "OK",
   *     "message": "The request has succeeded. An entity corresponding to the requested resource is sent in the response."
   *   },
   *   "data": {
   *     "force_bbk": {
   *       "experiment_id": "173",
   *       "use_varian": true,
   *       "varian": "varian_a"
   *     }
   *   },
   * }
   */
  endpoint()
    .then((res) => {
      if (!res.data.status) {
        wrappedDefaultCb();
        return;
      }
      const testResponse = res.data.data.force_bbk;
      if (testResponse.use_varian) {
        switch (testResponse.varian) {
          case 'control':
            wrappedDefaultCb();
            break;
          case 'varian_a':
            wrappedVariantACb();
            break;
          default:
            wrappedDefaultCb();
            break;
        }
      } else if (testResponse.experiment_id) {
        axios
          .$post(
            process.env.SPLITTER_URL,
            {
              user_id: userId,
              experiment_id: testResponse.experiment_id,
              // get from cookies first, if not exist, find on localStorage
              device_id:
                Cookies && Cookies.get('USER_DATA')
                  ? JSON.parse(Cookies.get('USER_DATA')).deviceUuid
                  : localStorage.getItem('MOE_DATA')
                  ? JSON.parse(localStorage.getItem('MOE_DATA')).USER_DATA.deviceUuid
                  : null,
              session_id:
                localStorage &&
                localStorage.getItem('MOE_DATA') &&
                JSON.parse(localStorage.getItem('MOE_DATA')).SESSION
                  ? JSON.parse(localStorage.getItem('MOE_DATA')).SESSION.sessionKey
                  : null,
            },
            {
              timeout: 5000,
              transformRequest: [
                function(data, headers) {
                  headers.Authorization = 'MmUyODYzMTI3YTNiZDYwNDA2NDRjNzkxZTMyMjZmMWY=';
                  headers['x-api-key'] = 'Kfs0pOEQns3Ir25JQkFm45Q8rNNyOOXd9cxobGv2';
                  headers['Content-Type'] = 'application/json';
                  return JSON.stringify(data);
                },
              ],
            },
          )
          /**
           * Response example
           * "status": "success",
           * "data": {
           *   "user_id": 3473012,
           *   "device_id": "839c77c0-8c4e-492f-bf65-e5db9ef9b412",
           *   "session_id": "681fcea9-9d84-45df-a27c-157d56f0ca4e",
           *   "experiment_id": 0,
           *   "experiment_value": "",
           *   "is_active": false
           * },
           * message": "experiment id is not active or does not exist or user id not eligible to get the experiment"
           */
          .then((res) => {
            const { is_active, experiment_id, experiment_value } = res.data;
            if (
              is_active &&
              experiment_value !== 'control' &&
              experiment_id === testResponse.experiment_id
            ) {
              wrappedVariantACb();
            } else {
              wrappedDefaultCb();
            }
          })
          .catch(() => {
            wrappedDefaultCb();
          });
      } else {
        wrappedDefaultCb();
      }
    })
    .catch(() => {
      wrappedDefaultCb();
    });
}
