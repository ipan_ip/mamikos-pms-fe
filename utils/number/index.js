export function toThousands(poin = 0) {
  return parseInt(+poin || 0)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}
