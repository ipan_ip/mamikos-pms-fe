export const toPascalCase = (text) => {
  return text
    .match(/[a-z]+/gi)
    .map(function(word) {
      return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
    })
    .join('');
};

export const toDromedaryCase = (text) => {
  return text
    .match(/[a-z]+/gi)
    .map(function(word) {
      return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
    })
    .join('')
    .replace(/^(.)/, function(word) {
      return word.toLowerCase();
    });
};

export const toSnakeCase = (text) => {
  return text
    .match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
    .map((word) => word.toLowerCase())
    .join('_');
};

export function shortenText(text) {
  return text && text.length > 60 ? text.substring(0, 60) + '...' : text;
}

export const truncate = (text, maxLength = 52) => {
  return text && text.length > maxLength ? text.substring(0, maxLength) + '...' : text;
};

export const firstWord = (text = '') => {
  if (typeof text !== 'string') return '';

  return text.replace(/ .*/, '');
};
