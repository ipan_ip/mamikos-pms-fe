import Cookies from 'js-cookie';
import { getCurrentUTCTime } from '~/utils/datetime';

const authFlag = Cookies.get('auth_flag');
const lastLogin = localStorage.getItem('user_last_login');

export const initAuthTracker = (instance, userData) => {
  if (lastLogin) {
    if (!authFlag) {
      Cookies.set('auth_flag', 1, { expires: 1 });

      // Track auth user every time session expired.
      trackAuthenticatedUser(userData, lastLogin, instance);
    }
  } else {
    if (!lastLogin) {
      localStorage.setItem('user_last_login', getCurrentUTCTime());

      // Track user's first time login at a time.
      trackAuthenticatedUser(userData, getCurrentUTCTime(), instance);
    }

    if (!authFlag) {
      Cookies.set('auth_flag', 1, { expires: 1 });
    }
  }
};

export const clearAuthTracker = () => {
  // Remove tracking items when user isn't in logged in states.
  localStorage.removeItem('user_last_login');
  Cookies.remove('auth_flag');
};

const trackAuthenticatedUser = (userData, lastLogin, instance) => {
  instance.$tracker.send('logger', [
    '[Web] Open App when Login',
    {
      timestamp: getCurrentUTCTime(),
      name: userData.name,
      phone_number: userData.phone_number,
      email: userData.email,
      last_login: lastLogin,
      is_owner: true,
      device: window.innerWidth < 768 ? 'mobile' : 'desktop',
    },
  ]);
};
