/**
 * @param {string} property 	window object property
 * @param {any} value					mock value of window object property
 *
 * examples:
 * import mockWindowProperty from '~/utils/mockWindowProperty'
 * mockWindowProperty('innerHeight', 200)
 * mockWindowProperty('document.querySelector', jest.fn())
 */

export default (property, value) => {
  let objectTarget = global.window;
  let definedProperty = '';
  const deepProperty = property.split('.');
  if (deepProperty.length > 1) {
    deepProperty.forEach((prop, index) => {
      index !== deepProperty.length - 1
        ? (objectTarget = objectTarget[prop])
        : (definedProperty = prop);
    });
  } else definedProperty = property;
  return Object.defineProperty(objectTarget, definedProperty, {
    value,
    writable: true,
  });
};
