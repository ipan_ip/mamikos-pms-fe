import { createLocalVue } from '@vue/test-utils';
import buefy from 'buefy';

const localVue = createLocalVue();

localVue.use(buefy);

export default localVue;
